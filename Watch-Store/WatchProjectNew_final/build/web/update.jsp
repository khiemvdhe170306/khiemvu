<%-- 
    Document   : update
    Created on : Jun 27, 2023, 11:58:28 PM
    Author     : khiem
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html lang="vi">

    <head>
        <!-- Required meta tags-->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Colorlib Templates">
        <meta name="author" content="Colorlib">
        <meta name="keywords" content="Colorlib Templates">

        <!-- Title Page-->
        <title>Watch</title>

        <!-- Icons font CSS-->
        <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
        <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
        <!-- Font special for pages-->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">

        <!-- Vendor CSS-->
        <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
        <link href="vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">

        <!-- Main CSS-->
        <link href="css/main.css" rel="stylesheet" media="all">
        <style>
            .BackButton{
                width: 80px;
                height: 30px;
                border-radius:10px;
                background-color: red;
                margin-left: 100px;
                text-align: center;

            }
            .BackButton a{
                color: white;
            }
        </style>
    </head>

    <body>
        <jsp:include page="header-footer/header.jsp" />  
        <div class="page-wrapper bg-gra-03 p-t-45 p-b-50">

            <div class="BackButton"><a href="crud">Back</a></div>
            <div class="wrapper wrapper--w790">
                <div class="card card-5">
                    <div class="card-heading">
                        <h2 class="title">${param.pid eq null?'ADD':'UPDATE'} Product Information</h2>
                    </div>

                    <div class="card-body">
                        <!--                        FORM  
                        -->
                        <h4 style="color:red">${requestScope.ms}</h4>
                        <form method="POST" action="management">
                            <input type="hidden" name="manage" value="${param.pid eq null?'add':'update'}">
                            <input type="hidden" name="pid" value="${param.pid}">
                            <div class="form-row">
                                <div class="name">Name</div>
                                <div class="value">
                                    <div class="input-group">
                                        <input class="input--style-5" type="text" name="name" value="${param.name}"   required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="name">Price</div>
                                <div class="value">
                                    <div class="input-group">
                                        <input class="input--style-5" type="number" min="10" name="price" value="${param.price}" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="name">Describe</div>
                                <div class="value">
                                    <div class="input-group">
                                        <input class="input--style-5" type="text" name="describe"value="${param.describe}" required>
<!--                                        <textarea class="input--style-5" name="describe"value="${param.describe}" rows="4" cols="38"></textarea>-->
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="name">Image-URL-1</div>
                                <div class="value">
                                    <div class="input-group">
                                        <input class="input--style-5" type="text" name="image1" required value="${param.image1}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="name">Image-URL-2</div>
                                <div class="value">
                                    <div class="input-group">
                                        <input class="input--style-5" type="text" name="image2" required value="${param.image2}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="name">Image-URL-3</div>
                                <div class="value">
                                    <div class="input-group">
                                        <input class="input--style-5" type="text" name="image3" required value="${param.image3}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="name">Image-URL-4</div>
                                <div class="value">
                                    <div class="input-group">
                                        <input class="input--style-5" type="text" name="image4" required value="${param.image4}">
                                    </div>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="name">Brand</div>
                                <div class="value">
                                    <div class="input-group">
                                        <div class="rs-select2 js-select-simple select--no-search">
                                            <select name="brand">
                                                <option disabled="disabled"  required>Choose option</option>

                                                <c:forEach items="${requestScope.category}" var="c">
                                                    <option value="${c.id}" ${param.brand eq c.id?'selected="selected"':''}>${c.name}</option>
                                                </c:forEach>



                                            </select>
                                            <div class="select-dropdown"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                    <c:set var="msg" value="${param.detail}" />                                                
                                    <c:set var="arrayofmsg" value="${fn:split(msg,',')}" /> 
                                    
                            <div class="form-row">
                                <div class="name">Size</div>
                                <div class="value">
                                    <div class="input-group">
                                        <div class="rs-select2 js-select-simple select--no-search">
                                            <select name="size">
                                                <option disabled="disabled"  required>Choose size</option>
                                                <option value="small" required ${arrayofmsg[0] eq 'small' ?'selected':''} >small</option>
                                                <option value="medium" required  ${arrayofmsg[0] eq 'medium' ?'selected':''} >medium</option>
                                                <option value="big" required  ${arrayofmsg[0] eq 'big' ?'selected':''} >big</option>
                                            </select>
                                            <div class="select-dropdown"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="name">Material</div>
                                <div class="value">
                                    <div class="input-group">
                                        <div class="rs-select2 js-select-simple select--no-search">
                                            <select name="material">
                                                <option disabled="disabled"  required>Choose material</option>
                                                <option value="leather" required ${arrayofmsg[1] eq 'leather' ?'selected':''} >leather</option>                                               
                                                <option value="gold" required ${arrayofmsg[1] eq 'gold' ?'selected':''}>gold</option>
                                                <option value="silicone" required ${arrayofmsg[1] eq 'silicone' ?'selected':''}>silicone</option>
                                                <option value="platinum" required ${arrayofmsg[1] eq 'platinum' ?'selected':''}>platinum</option>
                                            </select>
                                            <div class="select-dropdown"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="name">Type</div>
                                <div class="value">
                                    <div class="input-group">
                                        <div class="rs-select2 js-select-simple select--no-search">
                                            <select name="type">
                                                <option disabled="disabled"  required }>Choose Type</option>
                                                <option value="classic" required ${arrayofmsg[2] eq 'classic' ?'selected':''}>classic</option>
                                                <option value="electronics" required  ${arrayofmsg[2] eq 'electronics' ?'selected':''} >electronics</option>
                                                

                                            </select>
                                            <div class="select-dropdown"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="name">Color</div>
                                <div class="value">
                                    <div class="input-group">
                                        <div class="rs-select2 js-select-simple select--no-search">
                                            <select name="color">
                                                <option disabled="disabled"  required>Choose color</option>
                                                <option value="red" required ${arrayofmsg[3] eq 'red' ?'selected':''}>red</option>
                                                <option value="blue" required ${arrayofmsg[3] eq 'blue' ?'selected':''}>blue</option>
                                                <option value="black" required ${arrayofmsg[3] eq 'black' ?'selected':''}>black</option>
                                                <option value="yellow" required ${arrayofmsg[3] eq 'yellow' ?'selected':''}>yellow</option>
                                                <option value="green" required ${arrayofmsg[3] eq 'green' ?'selected':''}>green</option>
                                                <option value="white" required ${arrayofmsg[3] eq 'white' ?'selected':''}>white</option>
                                                <option value="orange" required ${arrayofmsg[3] eq 'orange' ?'selected':''}>orange</option>
                                                <option value="purple" required ${arrayofmsg[3] eq 'purple' ?'selected':''}>purple</option>
                                            </select>
                                            <div class="select-dropdown"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <button class="btn btn--radius-2 btn--red" type="submit">${param.pid eq null?'ADD':'UPDATE'} product</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <jsp:include page="header-footer/footer.jsp" />  
        <!-- Jquery JS-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <!-- Vendor JS-->
        <script src="vendor/select2/select2.min.js"></script>
        <script src="vendor/datepicker/moment.min.js"></script>
        <script src="vendor/datepicker/daterangepicker.js"></script>

        <!-- Main JS-->
        <script src="js/global.js"></script>

    </body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
<!-- end document-->
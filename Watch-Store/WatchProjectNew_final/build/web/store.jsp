<%-- 
    Document   : store
    Created on : Jun 3, 2023, 12:45:34 PM
    Author     : khiem
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

        <title>Electro - HTML Ecommerce Template</title>

        <!-- Google font -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">

        <!-- Bootstrap -->
        <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css"/>

        <!-- Slick -->
        <link type="text/css" rel="stylesheet" href="css/slick.css"/>
        <link type="text/css" rel="stylesheet" href="css/slick-theme.css"/>

        <!-- nouislider -->
        <link type="text/css" rel="stylesheet" href="css/nouislider.min.css"/>

        <!-- Font Awesome Icon -->
        <link rel="stylesheet" href="css/font-awesome.min.css">

        <!-- Custom stlylesheet -->
        <link type="text/css" rel="stylesheet" href="css/home.css"/>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
            input[type="checkbox"],input[type='radio'] {
                accent-color:red;
            }

        </style>
    </head>
    <body>
        <jsp:include page="header-footer/homeheader.jsp" />  

        <!-- NAVIGATION -->
        <nav id="navigation">
            <!-- container -->
            <div class="container">
                <!-- responsive-nav -->
                <div id="responsive-nav">
                    <!-- NAV -->
                    <ul class="main-nav nav navbar-nav">
                        <li ${param.cid eq null ?'class="active"':''}><a href="home">Home</a></li>
                        <li ${param.cid eq '0'?'class="active"':''} ><a href="products?cid=${0}">Watch</a></li>
                            <c:forEach items="${requestScope.homecat}" var="cat">
                            <li ${param.cid eq cat.id ?'class="active"':''}><a href="products?cid=${cat.id}">${cat.name}</a></li>
                            </c:forEach>
                    </ul>
                    <!-- /NAV -->
                </div>
                <!-- /responsive-nav -->
            </div>
            <!-- /container -->
        </nav>
        <!-- /NAVIGATION -->
        <!-- SECTION -->
        <div class="section">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- ASIDE -->
                    <div id="aside" class="col-md-3">
                        <!-- aside Widget -->
                        <div class="aside">

                            <form action="products" id="forms">

                                <h4 class="aside-title">Brand</h4><br>
                                <input type="radio" name="cid" id="AllBrand" value="0" ${param.cid eq 0?'checked':''}>
                                <label for="AllBrand">All </label><br><br>
                                <c:forEach items="${requestScope.homecat}" var="cat">
                                    <input type="radio" name="cid" id="${cat.name}" value="${cat.id}"  ${param.cid eq cat.id?'checked':''}>
                                    <label for="${cat.name}">${cat.name} </label><br><br>
                                </c:forEach>
                                <br><br>
                                <h4 class="aside-title">Material</h4><br>
                                <input type='hidden' name='cid'value='${param.cid}' />
                                <input type="checkbox" name="AllMaterial" class="materialchecks" id="AllMaterial"  onclick="checkOnlyOne(this.value)" value="on" ${(param.gold eq null && param.platinum eq null && param.leather eq null && param.silicone eq null)?'checked':''}>
                                <label for="AllMaterial">All </label><br><br>
                                <input type="checkbox" name="gold" class="materialchecks" onclick="checkOne()" value="gold" ${param.gold eq 'gold'?'checked':''}/>
                                <label for="gold">gold </label><br><br>
                                <input type="checkbox" name="platinum" class="materialchecks" onclick="checkOne()" value="platinum" ${param.platinum eq 'platinum'?'checked':''} />
                                <label for="platinum"> platinum </label><br><br>
                                <input type="checkbox" name="leather" class="materialchecks" onclick="checkOne()" value="leather" ${param.leather eq 'leather'?'checked':''}/>
                                <label for="leather">leather </label><br><br>
                                <input type="checkbox" name="silicone" class="materialchecks" onclick="checkOne()" value="silicone" ${param.silicone eq 'silicone'?'checked':''}/>
                                <label for="silicone">silicone </label><br><br>

                                <h4 class="aside-title">Size</h4><br>
                                <input type="checkbox" name="AllSize" class="sizechecks" id="AllSize" onclick="checkOnlyTwo(this.value)" value="on" ${(param.small eq null && param.medium eq null && param.big eq null)?'checked':''}>
                                <label for="AllSize">All </label><br><br>
                                <input type="checkbox" name="small" class="sizechecks" onclick="checkTwo()" value="small"${param.small eq 'small'?'checked':''}/>
                                <label for="small">small </label><br><br>
                                <input type="checkbox" name="medium" class="sizechecks" onclick="checkTwo()" value="medium"${param.medium eq 'medium'?'checked':''}/>
                                <label for="medium">medium </label><br><br>
                                <input type="checkbox" name="big" class="sizechecks" onclick="checkTwo()" value="big" ${param.big eq 'big'?'checked':''}/>
                                <label for="big">big </label><br><br>

                                <h4 class="aside-title">Style</h4><br>
                                <input type="checkbox" name="AllStyle" class="stylechecks" id="AllStyle" onclick="checkOnlyThree(this.value)" value="on" ${(param.electronics eq null && param.classic eq null)?'checked':''}>
                                <label for="AllStyle">All </label><br><br>
                                <input type="checkbox" name="classic" class="stylechecks" onclick="checkThree()" value="classic" ${param.classic eq 'classic'?'checked':''}/>
                                <label for="classic">classic </label><br><br>
                                <input type="checkbox" name="electronics" class="stylechecks" onclick="checkThree()" value="electronics" ${param.electronics eq 'electronics'?'checked':''}/>
                                <label for="electronics">electronics </label><br><br>

                                <h4 class="aside-title">Color</h4><br>
                                <select name="color">
                                    <option value="Allcolor"  required>allColor</option>
                                    <option value="red" required ${param.color eq 'red' ?'selected':''}>red</option>
                                    <option value="blue" required ${param.color eq 'blue' ?'selected':''}>blue</option>
                                    <option value="black" required ${param.color eq 'black' ?'selected':''}>black</option>
                                    <option value="yellow" required ${param.color eq 'yellow' ?'selected':''}>yellow</option>
                                    <option value="green" required ${param.color eq 'green' ?'selected':''}>green</option>
                                    <option value="white" required ${param.color eq 'white' ?'selected':''}>white</option>
                                    <option value="orange" required ${param.color eq 'orange' ?'selected':''}>orange</option>
                                    <option value="purple" required ${param.color eq 'purple' ?'selected':''}>purple</option>

                                </select>
                                <!-- aside Widget -->
                                <div class="aside">
                                    <h3 class="aside-title">Price-$</h3>
                                    <div class="price-filter">
                                        <div id="price-slider"></div>
                                        <div class="input-number price-min">
                                            <input id="price-min" name="price-min" type="number" value="${requestScope.min==null?'1':requestScope.min}">
                                            <span class="qty-up">+</span>
                                            <span class="qty-down">-</span>
                                        </div>
                                        <span>-</span>
                                        <div class="input-number price-max">
                                            <input id="price-max" name="price-max" type="number" value="${requestScope.max==null?'99999':requestScope.max}">
                                            <span class="qty-up">+</span>
                                            <span class="qty-down">-</span>
                                        </div>
                                    </div>
                                </div><br>
                                <!-- /aside Widget -->
                                <input type="submit" name="submit" value="FILTER">
                                <button type="button" onclick="All()">ALL</button>

                            </form>

                        </div>
                        <!-- /aside Widget -->



                        <!-- /aside Widget -->
                    </div>
                    <!-- /ASIDE -->
                    <!-- STORE -->
                    <div id="store" class="col-md-9">
                        <!-- store top filter -->
                        <div class="store-filter clearfix">
                            <div class="store-sort">
                                <label>

                                    <c:set var="url" value="${requestScope.URLpath}"/>
                                    <!--                                    <select   name="s1" id="city_id">
                                                                            <option value="<c:choose><c:when test="${fn:contains(requestScope.URLpath, '&sort=')}">${fn:substring(requestScope.URLpath, 0,fn:indexOf(requestScope.URLpath, "&sort"))}${fn:substring(requestScope.URLpath,fn:indexOf(requestScope.URLpath, "&sort")+7,fn:length(requestScope.URLpath))}&sort=0&page=1</c:when><c:otherwise>${requestScope.URLpath}&sort=0&page=1</c:otherwise></c:choose>" ${param.sort eq '0'?'selected':''}>Default</option>
                                                                            <option value="<c:choose><c:when test="${fn:contains(requestScope.URLpath, '&sort=')}">${fn:substring(requestScope.URLpath, 0,fn:indexOf(requestScope.URLpath, "&sort"))}${fn:substring(requestScope.URLpath,fn:indexOf(requestScope.URLpath, "&sort")+7,fn:length(requestScope.URLpath))}&sort=1&page=1</c:when><c:otherwise>${requestScope.URLpath}&sort=1&page=1</c:otherwise></c:choose>" ${param.sort eq '0'?'selected':''}>Increase</option>
                                                                            <option value="<c:choose><c:when test="${fn:contains(requestScope.URLpath, '&sort=')}">${fn:substring(requestScope.URLpath, 0,fn:indexOf(requestScope.URLpath, "&sort"))}${fn:substring(requestScope.URLpath,fn:indexOf(requestScope.URLpath, "&sort")+7,fn:length(requestScope.URLpath))}&sort=2&page=1</c:when><c:otherwise>${requestScope.URLpath}&sort=2&page=1</c:otherwise></c:choose>" ${param.sort eq '0'?'selected':''}>Decrease</option>
                                                                                </select>-->
                                            <ul class="price-sort" >
                                                <span style="color:chocolate;padding-top: 20px">Sort by:</span> 
                                                <li>
                                                        <a style="text-align:center" class="<c:if test="${fn:contains(requestScope.URLpath, '&sort=1')}">red</c:if>"
                                               href="<c:choose><c:when test="${fn:contains(requestScope.URLpath, '&sort=')}">${fn:substring(requestScope.URLpath, 0,fn:indexOf(requestScope.URLpath, "&sort"))}${fn:substring(requestScope.URLpath,fn:indexOf(requestScope.URLpath, "&sort")+7,fn:length(requestScope.URLpath))}&sort=1&page=1</c:when><c:otherwise>${requestScope.URLpath}&sort=1&page=1</c:otherwise></c:choose>">
                                                       <i class="fa fa-sort-amount-desc">Price Low</i></a></li>
                                               <li>
                                                       <a class="<c:if test="${fn:contains(requestScope.URLpath, '&sort=2')}">red</c:if>"
                                               href="<c:choose><c:when test="${fn:contains(requestScope.URLpath, '&sort=')}">${fn:substring(requestScope.URLpath, 0,fn:indexOf(requestScope.URLpath, "&sort"))}${fn:substring(requestScope.URLpath,fn:indexOf(requestScope.URLpath, "&sort")+7,fn:length(requestScope.URLpath))}&sort=2&page=1</c:when><c:otherwise>${requestScope.URLpath}&sort=2&page=1</c:otherwise></c:choose>">
                                                       <i class="fa fa-sort-amount-desc">Price High</i></a></li>
                                               <li>
                                                       <a class="<c:if test="${fn:contains(requestScope.URLpath, '&sort=3')}">red</c:if>"
                                               href="<c:choose><c:when test="${fn:contains(requestScope.URLpath, '&sort=')}">${fn:substring(requestScope.URLpath, 0,fn:indexOf(requestScope.URLpath, "&sort"))}${fn:substring(requestScope.URLpath,fn:indexOf(requestScope.URLpath, "&sort")+7,fn:length(requestScope.URLpath))}&sort=3&page=1</c:when><c:otherwise>${requestScope.URLpath}&sort=3&page=1</c:otherwise></c:choose>">
                                                       <i class="fa fa-sort-amount-desc">New Product</i></a></li>
                                               <li>
                                                       <a class="<c:if test="${fn:contains(requestScope.URLpath, '&sort=4')}">red</c:if>"
                                               href="<c:choose><c:when test="${fn:contains(requestScope.URLpath, '&sort=')}">${fn:substring(requestScope.URLpath, 0,fn:indexOf(requestScope.URLpath, "&sort"))}${fn:substring(requestScope.URLpath,fn:indexOf(requestScope.URLpath, "&sort")+7,fn:length(requestScope.URLpath))}&sort=4&page=1</c:when><c:otherwise>${requestScope.URLpath}&sort=4&page=1</c:otherwise></c:choose>">
                                                       <i class="fa fa-sort-amount-desc">Top Selling</i></a></li>
                                               <li>
                                                       <a class="<c:if test="${fn:contains(requestScope.URLpath, '&sort=5')}">red</c:if>"
                                               href="<c:choose><c:when test="${fn:contains(requestScope.URLpath, '&sort=')}">${fn:substring(requestScope.URLpath, 0,fn:indexOf(requestScope.URLpath, "&sort"))}${fn:substring(requestScope.URLpath,fn:indexOf(requestScope.URLpath, "&sort")+7,fn:length(requestScope.URLpath))}&sort=5&page=1</c:when><c:otherwise>${requestScope.URLpath}&sort=5&page=1</c:otherwise></c:choose>">
                                                       <i class="fa fa-sort-amount-desc">Top Rating</i></a></li>
                                               <li>
                                                       <a class="<c:if test="${fn:contains(requestScope.URLpath, '&sort=6')}">red</c:if>"
                                               href="<c:choose><c:when test="${fn:contains(requestScope.URLpath, '&sort=')}">${fn:substring(requestScope.URLpath, 0,fn:indexOf(requestScope.URLpath, "&sort"))}${fn:substring(requestScope.URLpath,fn:indexOf(requestScope.URLpath, "&sort")+7,fn:length(requestScope.URLpath))}&sort=6&page=1</c:when><c:otherwise>${requestScope.URLpath}&sort=6&page=1</c:otherwise></c:choose>">
                                                       <i class="fa fa-sort-amount-desc">Low Sale</i></a></li>
                                               <li>
                                                       <a class="<c:if test="${fn:contains(requestScope.URLpath, '&sort=7')}">red</c:if>"
                                               href="<c:choose><c:when test="${fn:contains(requestScope.URLpath, '&sort=')}">${fn:substring(requestScope.URLpath, 0,fn:indexOf(requestScope.URLpath, "&sort"))}${fn:substring(requestScope.URLpath,fn:indexOf(requestScope.URLpath, "&sort")+7,fn:length(requestScope.URLpath))}&sort=7&page=1</c:when><c:otherwise>${requestScope.URLpath}&sort=7&page=1</c:otherwise></c:choose>">
                                                       <i class="fa fa-sort-amount-desc">Low Rating</i></a></li>

                                            </ul>

                                        </label>
                                    </div>

                                </div>
                                <!-- /store top filter -->
                                    <div style="color: #000">Find:${fn:length(requestScope.products)} Products</div>

                        <!-- store products -->
                        <div class="row">
                            <!-- product -->
                            <c:set var="list" value="${requestScope.products}"/>
                            <c:if test="${((list==null))}">
                                <c:redirect url="products?cid=${0}" ></c:redirect>
                            </c:if>
                            <c:if test="${list.size()==0}">
                                <h2>No Product</h2>
                            </c:if>
                            <c:if test="${((list!=null)&&(list.size()>0))}">

                                <c:forEach items="${requestScope.products}" var="o" begin="${requestScope.page*9-9}" end="${requestScope.page*9-1}">
                                    <div class="col-md-4 col-xs-6">
                                        <div class="product">
                                            <div class="product-img">
                                                <c:set var="msg" value="${o.image}" />                                                
                                                <c:set var="arrayofmsg" value="${fn:split(msg,',')}" />                                             
                                                <c:forEach var="i" begin="0" end="0">
                                                    <img src="${arrayofmsg[i]}" alt="alt"/>
                                                </c:forEach>
                                                <div class="product-label">
                                                    <span class="sale">Sold:${o.sale}</span>
                                                    <span class="new">
                                                        <c:forEach varStatus="loop" begin="1" end="${o.rating}" step="1">
                                                            <i class="fa fa-star"></i>
                                                        </c:forEach> 
                                                        <c:forEach varStatus="loop" begin="1" end="${5-o.rating}" step="1">
                                                            <i class="fa fa-star-o empty"></i>
                                                        </c:forEach></span>

                                                </div>
                                            </div>
                                            <div class="product-body">
                                                <c:set var="cat" value="${requestScope.cat}" />
                                                <p class="product-category">${cat.name}</p>
                                                <h3 class="product-name"><a href="#">${o.name}</a></h3>
                                                <h4 class="product-price">${o.price}0$ <del class="product-old-price">${o.price*2}0$</del></h4>

                                                <div class="product-btns">
                                                    <button class="quick-view"><a target="_blank" href="products?id=${o.id}&page=1"><i class="fa fa-eye"></i></a><span class="tooltipp">quick view</span></button>
                                                </div>
                                            </div>
                                            <%
                                            String uri=request.getRequestURI();
                                            String query=request.getQueryString();
                                            String URL="";
                                            if(uri==null&&query==null){
                                                URL+="products?cid=0";
                                            }else if(uri!=null&&query==null){

                                            URL+=uri.substring(uri.lastIndexOf("/")+1, uri.length());
                                            }else{
                                                URL+=uri.substring(uri.lastIndexOf("/")+1, uri.length())+"?"+query;}
                                
                                            %>

                                            <div class="add-to-cart">
                                                <c:if test="${sessionScope.account eq null}">
                                                    <button class="add-to-cart-btn"><a href="login?url=<%=URL.replaceAll("&", ";")%>"><i class="fa fa-shopping-cart"></i>add to cart</a> </button>
                                                </c:if>
                                                <c:if test="${sessionScope.account ne null}">
                                                    <button class="add-to-cart-btn"><a href="addCart?id=${o.id}&number=1&url=products?cid=${param.cid}&aid=${sessionScope.account.aid}"><i class="fa fa-shopping-cart"></i>add to cart</a> </button>
                                                </c:if>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /product -->

                                </c:forEach>
                            </c:if>


                            <!-- /product -->
                        </div>
                        <!-- /store products -->

                        <!-- store bottom filter -->
                        <div class="store-filter clearfix">

                            <ul class="store-pagination">
                                <c:if test="${requestScope.page>1}">
                                    <li><a href="products?cid=${requestScope.cid}&page=${requestScope.page-1}
                                           "><i class="fa fa-angle-left"></i></a></li> 
                                        </c:if>
                                        <% int i=1; %>

                                <c:forEach items="${requestScope.products}" var="o" step="9" >
                                    <c:set var="i" value="<%=i%>"/>
                                    <li
                                        <c:if test="${requestScope.page eq i}">
                                            class="active"
                                        </c:if>
                                        ><a href="${requestScope.URLpath}&page=<%=i%>"><%=i%></a></li> 
                                        <% i++;%>
                                    </c:forEach>

                                <c:if test="${list.size()>=(requestScope.page*9+1)}">
                                    <li><a href="products?cid=${requestScope.cid}&page=${requestScope.page+1}"><i class="fa fa-angle-right"></i></a></li>
                                        </c:if>

                            </ul>
                        </div>
                        <!-- /store bottom filter -->
                    </div>
                    <!-- /STORE -->
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- /SECTION -->

        <jsp:include page="header-footer/footer.jsp" />  

        <!-- jQuery Plugins -->
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/slick.min.js"></script>
        <script src="js/nouislider.min.js"></script>
        <script src="js/jquery.zoom.min.js"></script>
        <script src="js/main.js"></script>
        <script type="text/javascript">
                                    function All() {
                                        checkOnlyOne(document.getElementById('AllMaterial'));
                                        document.getElementById("AllMaterial").checked = true;
                                        checkOnlyTwo(document.getElementById('AllSize'));
                                        document.getElementById("AllSize").checked = true;
                                        checkOnlyThree(document.getElementById('AllStyle'));
                                        document.getElementById("AllStyle").checked = true;
                                        document.getElementById("AllBrand").checked = true;

                                    }
                                    function checkOnlyOne(b) {

                                        var x = document.getElementsByClassName('materialchecks');
                                        var i;

                                        for (i = 0; i < x.length; i++) {
                                            if (x[i].value !== b)
                                                x[i].checked = false;
                                        }
                                    }
                                    function checkOne() {
                                        document.getElementById("AllMaterial").checked = false;
                                    }
                                    function checkOnlyTwo(b) {

                                        var x = document.getElementsByClassName('sizechecks');
                                        var i;

                                        for (i = 0; i < x.length; i++) {
                                            if (x[i].value !== b)
                                                x[i].checked = false;
                                        }
                                    }
                                    function checkTwo() {
                                        document.getElementById("AllSize").checked = false;
                                    }
                                    function checkOnlyThree(b) {

                                        var x = document.getElementsByClassName('stylechecks');
                                        var i;

                                        for (i = 0; i < x.length; i++) {
                                            if (x[i].value !== b)
                                                x[i].checked = false;
                                        }
                                    }
                                    function checkThree() {
                                        document.getElementById("AllStyle").checked = false;
                                    }


        </script>

    </body>
</html>


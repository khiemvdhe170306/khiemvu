<%-- 
    Document   : profile
    Created on : Jun 25, 2023, 4:38:21 PM
    Author     : khiem
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">


        <title>profile with data and skills - Bootdey.com</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" rel="stylesheet">
        <style type="text/css">
            body{
                margin-top:20px;
                color: #1a202c;
                text-align: left;
                background-color: #e2e8f0;
            }
            .main-body {
                padding: 15px;
            }
            .card {
                box-shadow: 0 1px 3px 0 rgba(0,0,0,.1), 0 1px 2px 0 rgba(0,0,0,.06);
            }

            .card {
                position: relative;
                display: flex;
                flex-direction: column;
                min-width: 0;
                word-wrap: break-word;
                background-color: #fff;
                background-clip: border-box;
                border: 0 solid rgba(0,0,0,.125);
                border-radius: .25rem;
            }

            .card-body {
                flex: 1 1 auto;
                min-height: 1px;
                padding: 1rem;
            }

            .gutters-sm {
                margin-right: -8px;
                margin-left: -8px;
            }

            .gutters-sm>.col, .gutters-sm>[class*=col-] {
                padding-right: 8px;
                padding-left: 8px;
            }
            .mb-3, .my-3 {
                margin-bottom: 1rem!important;
            }

            .bg-gray-300 {
                background-color: #e2e8f0;
            }
            .h-100 {
                height: 100%!important;
            }
            .shadow-none {
                box-shadow: none!important;
            }
            .update{
                width: 100px;
                height:35px;
                background: #EE4646;
                border-radius: 10px;
                font-size: smaller;
                font-family: sans-serif;
            }
            .oke{
                display: flex;
                justify-content: space-around
            }
            input[type="file"] {
                display: none;
            }
            .custom-file-upload {
                border: 1px solid #ccc;
                display: inline-block;
                padding: 6px 12px;
                cursor: pointer;

            }

        </style>
    </head>
    <body>
        <jsp:include page="header-footer/header.jsp" />  

        <div class="container">
            <div class="main-body">

                <nav aria-label="breadcrumb" class="main-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="accountmanagement">Manager</a></li>
                        <li class="breadcrumb-item active" aria-current="page">User Profile</li>
                    </ol>
                </nav>
                <c:if test="${requestScope.cid ne null}">
                    <h1>Update success</h1>
                </c:if>

                <div class="row gutters-sm">
                    <div class="col-md-4 mb-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex flex-column align-items-center text-center">

                                    <c:if test="${requestScope.accountImage eq null}">
                                        <img src="img/face.png" width="200" class="rounded-circle">
                                        <form action="imguser" method="post" enctype="multipart/form-data">
                                            <input type="file" class="hidden" name="imageProfile" id="files"  onchange="this.form.submit()" />
                                            <label class="custom-file-upload" for="files">Create your Avatar</label>
                                            <input type="hidden" name="add" value="${requestScope.account.aid}">
                                        </form> 
                                    </c:if>
                                    <c:if test="${requestScope.accountImage ne null}">
                                        <img src="data:image/jpg;base64,${requestScope.accountImage}" data-index="0" width="200" height="200"class="rounded-circle">
                                        <form action="imguser" method="post" enctype="multipart/form-data">

                                            <input type="file" class="hidden" name="imageProfile" id="files"  onchange="this.form.submit()" />
                                            <label class="custom-file-upload" for="files">Update your Avatar</label>
                                            <input type="hidden" name="update" value="${requestScope.account.aid}">
                                        </form> 
                                    </c:if>


                                    <div class="mt-3">
                                        <h4>${requestScope.account.userName}</h4>

                                        <c:if test="${requestScope.account.role eq 1}">
                                            <p class="text-secondary mb-1">USER</p>


                                        </c:if>
                                        <c:if test="${requestScope.account.role eq 2}">
                                            <p class="text-secondary mb-1">ADMIN</p>

                                        </c:if>

                                        <!--                                        <input type="file" data-index="0" onChange="swapImage(this)">-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="card mb-3">
                            <div class="card-body">
                                <form action="uploaduser" method="post">

                                    <div class="row">
                                        <div class="col-sm-3">
                                            <h6 class="mb-0">Full Name</h6>
                                        </div>
                                        <div class="col-sm-9 text-secondary">

                                            ${requestScope.accountInfo.fullname==null?'no data':requestScope.accountInfo.fullname}
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <h6 class="mb-0">Email</h6>
                                        </div>
                                        <div class="col-sm-9 text-secondary">
                                            ${requestScope.accountInfo.fullname==null?'no data':requestScope.accountInfo.email}
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <h6 class="mb-0">Phone</h6>
                                        </div>
                                        <div class="col-sm-9 text-secondary">
                                            ${requestScope.accountInfo.fullname==null?'no data':requestScope.accountInfo.phone}
                                        </div>
                                    </div>
                                    <hr>

                                    <div class="row">
                                        <div class="col-sm-3">
                                            <h6 class="mb-0">Address</h6>
                                        </div>
                                        <div class="col-sm-9 text-secondary">
                                            ${requestScope.accountInfo.fullname==null?'no data':requestScope.accountInfo.address}
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <h6 class="mb-0">Age</h6>
                                        </div>
                                        <div class="col-sm-9 text-secondary">
                                            ${requestScope.accountInfo.age eq null?'no data':requestScope.accountInfo.age}
                                        </div>
                                    </div>
                                    <hr>
                                    <div class="row">
                                        <div class="col-sm-3">
                                            <h6 class="mb-0">Gender</h6>
                                        </div>
                                         <div class="col-sm-9 text-secondary"><c:if test="${sessionScope.accountinfo eq null}">no data</c:if><c:if test="${sessionScope.accountinfo ne null}">${sessionScope.accountinfo.gender?'male':'female'}</c:if></div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-12 oke">
                                                <button class="update" type="submit" value="UpdateAccount" name="update"/> Update Account
                                                <button class="update" type="submit" value="UpdateProfile" name="update" />Update Profile
                                            </div>
                                        </div>
                                                <input type="hidden" name="aid" value="${requestScope.account.aid}">
                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <jsp:include page="header-footer/footer.jsp" />  

        <script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.bundle.min.js"></script>
        <script type="text/javascript">
                                                var URL = window.URL || window.webkitURL;

                                                window.swapImage = function (elm) {
                                                    var index = elm.dataset.index;
                                                    // URL.createObjectURL is faster then using the filereader with base64
                                                    var url = URL.createObjectURL(elm.files[0]);
                                                    document.querySelector('img[data-index="' + index + '"]').src = url;
                                                    window.location.href = "upload?&img=" + url;
                                                };
        </script>
    </body>
</html>
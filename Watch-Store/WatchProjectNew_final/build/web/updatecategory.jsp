<%-- 
    Document   : update
    Created on : Jun 27, 2023, 11:58:28 PM
    Author     : khiem
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="vi">

    <head>
        <!-- Required meta tags-->
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Colorlib Templates">
        <meta name="author" content="Colorlib">
        <meta name="keywords" content="Colorlib Templates">

        <!-- Title Page-->
        <title>Watch</title>

        <!-- Icons font CSS-->
        <link href="vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
        <link href="vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
        <!-- Font special for pages-->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">

        <!-- Vendor CSS-->
        <link href="vendor/select2/select2.min.css" rel="stylesheet" media="all">
        <link href="vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">

        <!-- Main CSS-->
        <link href="css/main.css" rel="stylesheet" media="all">
        <style>
            .BackButton{
                width: 80px;
                height: 30px;
                border-radius:10px;
                background-color: red;
                margin-left: 100px;
                text-align: center;

            }
            .BackButton a{
                color: white;
            }
        </style>
    </head>

    <body>
        <jsp:include page="header-footer/header.jsp" />  
        <div class="page-wrapper bg-gra-03 p-t-45 p-b-50">

            <div class="BackButton"><a href="crudcategory">Back</a></div>
            <div class="wrapper wrapper--w790">
                <div class="card card-5">
                    <div class="card-heading">
                        <h2 class="title">${requestScope.category eq null?'ADD':'UPDATE'} Product Information</h2>
                    </div>

                    <div class="card-body">
                        <!--                        FORM  
                        -->
                        <h4 style="color:red">${requestScope.ms}</h4>
                        <form method="POST" action="categorymanagement">
                            <input type="hidden" name="manage" value="${requestScope.category eq null?'add':'update'}">
                            <input type="hidden" name="cid" value="${requestScope.category.id}">
                            <div class="form-row">
                                <div class="name">Name</div>
                                <div class="value">
                                    <div class="input-group">
                                        <input class="input--style-5" type="text" name="name" value="${requestScope.category.name}"   required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="name">Describe</div>
                                <div class="value">
                                    <div class="input-group">
                                        <input class="input--style-5" type="text" name="describe"value="${requestScope.category.describe}" required>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <button class="btn btn--radius-2 btn--red" type="submit">${requestScope.category eq null?'ADD':'UPDATE'} Category</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <jsp:include page="header-footer/footer.jsp" />  
        <!-- Jquery JS-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <!-- Vendor JS-->
        <script src="vendor/select2/select2.min.js"></script>
        <script src="vendor/datepicker/moment.min.js"></script>
        <script src="vendor/datepicker/daterangepicker.js"></script>

        <!-- Main JS-->
        <script src="js/global.js"></script>

    </body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
<!-- end document-->

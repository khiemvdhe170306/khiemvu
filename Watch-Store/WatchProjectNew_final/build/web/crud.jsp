<%-- 
    Document   : crud
    Created on : Jun 26, 2023, 10:16:06 PM
    Author     : khiem
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib  prefix="fn"  uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ page import="dal.DAOCart" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Bootstrap Simple Data Table</title>
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <style>
            body {
                color: #566787;
                font-family: 'Roboto', sans-serif;

            }
            .table-responsive {
                margin: 30px 0;
            }
            .table-wrapper {
                min-width: 1000px;
                background: #fff;
                padding: 20px;
                box-shadow: 0 1px 1px rgba(0,0,0,.05);
            }
            .table-title {
                padding-bottom: 10px;
                margin: 0 0 10px;
                min-width: 100%;
            }
            .table-title h2 {
                margin: 8px 0 0;
                font-size: 22px;
            }
            .search-box {
                position: relative;
                float: right;
                display: flex;
                flex-direction: column;
                align-content: flex-end;
            }
            .search-box input {
                height: 34px;
                border-radius: 20px;
                padding-left: 35px;
                border-color: #ddd;
                box-shadow: none;
            }
            .search-box input:focus {
                border-color: #3FBAE4;
            }
            .search-box i {
                color: #a0a5b1;
                position: absolute;
                font-size: 19px;
                top: 8px;
                left: 10px;
            }
            table.table tr th, table.table tr td {
                border-color: #e9e9e9;
            }
            table.table-striped tbody tr:nth-of-type(odd) {
                background-color: #fcfcfc;
            }
            table.table-striped.table-hover tbody tr:hover {
                background: #f5f5f5;
            }
            table.table th i {
                font-size: 13px;
                margin: 0 5px;
                cursor: pointer;
            }
            table.table td:last-child {
                width: 130px;
            }
            table.table td a {
                color: #a0a5b1;
                display: inline-block;
                margin: 0 5px;
            }
            table.table td a.view {
                color: #03A9F4;
            }
            table.table td a.edit {
                color: #FFC107;
            }
            table.table td a.delete {
                color: #E34724;
            }
            table.table td i {
                font-size: 19px;
            }
            .pagination {
                float: right;
                margin: 0 0 5px;
            }
            .pagination li a {
                border: none;
                font-size: 95%;
                width: 30px;
                height: 30px;
                color: #999;
                margin: 0 2px;
                line-height: 30px;
                border-radius: 30px !important;
                text-align: center;
                padding: 0;
            }
            .pagination li a:hover {
                color: #666;
            }
            .pagination li.active a {
                background: #03A9F4;
            }
            .pagination li.active a:hover {
                background: #0397d6;
            }
            .pagination li.disabled i {
                color: #ccc;
            }
            .pagination li i {
                font-size: 16px;

            }
            .hint-text {
                float: left;
                margin-top: 6px;
                font-size: 95%;
            }
            .flex{
                display: flex;
                justify-content: flex-end;
            }
            .no{
                width: 100%;
               background-image: url(../img/login2.png)
            }
        </style>
        <script>
            $(document).ready(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>
    </head>
    <body>
        <jsp:include page="header-footer/header.jsp" />  
        <jsp:include page="header-footer/manager.jsp"/>


        <div class="container-fluid">
            <div class="no">


                <div class="table-responsive">
                    <div class="table-wrapper">
                        <div class="table-title">
                            <div class="row">
                                <div class="col-sm-7"><h2 class="flex">Product <b>Details</b></h2><div><a href="crud">Show all</a></div></div>

                                <div class="col-sm-5">

                                    <div class="search-box">

                                        <form action="crud" id="myForm">


                                            <input type="text" class="form-control" placeholder="Search&hellip;" name="search" id="pw">
                                        </form>
                                        <div><a target="_blank" href="management?manage=add">ADD NEW</a></div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <table class="table table-striped table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>ID<a href="crud?sort=3"><i class="fa fa-sort" aria-hidden="true"></i></a></th>
                                    <th>Name </th>
                                    <th>Price<c:if test="${param.sort eq 1}"><a href="crud?sort=2"><i class="fa fa-sort" aria-hidden="true"></i></a></c:if><c:if test="${param.sort eq 2 || param.sort eq null || param.sort>2}"><a href="crud?sort=1"><i class="fa fa-sort" aria-hidden="true"></i></a></c:if></th>
                                    <th>Category</th>
                                    <th>Category Describe</th>
                                    <th>Detail</th>
                                    <th>Sale<a href="crud?sort=4"><i class="fa fa-sort" aria-hidden="true"></i></a></th>
                                    <th>Rating<a href="crud?sort=5"><i class="fa fa-sort" aria-hidden="true"></i></a></th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${requestScope.product}" var="c" begin="${param.page*10-10}" end="${param.page*10-1}">
                                    <tr>
                                        <td>${c.id}</td>
                                        <td>${c.name}</td>
                                        <td>${c.price}$</td>
                                        <td>${c.category.name}</td>
                                        <td>${c.category.describe} </td>
                                        <td>${c.details}</td>
                                         <td>${c.sale}</td>
                                         <td>${c.rating} <i class="fa fa-star"></i></td>
                                        <td>
                                            <a target="_blank" href="products?id=${c.id}&page=1" class="view" title="View" data-toggle="tooltip"><i class="material-icons">&#xE417;</i></a>
                                            <a target="_blank" href="management?pid=${c.id}&manage=update" class="edit" title="Edit" data-toggle="tooltip"><i class="material-icons">&#xE254;</i></a>
                                            <a href="#" onclick="doDelete(${c.id})" class="delete" title="Delete" data-toggle="tooltip"><i class="material-icons">&#xE872;</i></a>
                                        </td>
                                    </tr>
                                </c:forEach>


                            </tbody>
                        </table>
                        <div class="clearfix">
                            <div class="hint-text">Showing <b>${fn:length(requestScope.product)} products</div>
                            <ul class="pagination">
                                <c:if test="${param.page>1}">
                                    <li class="page-item"><a <c:if test="${param.search eq null}">href="crud?page=${param.page-1}&sort=${param.sort}"</c:if><c:if test="${param.search ne null}">href="crud?page=${param.page-1}&search=${param.search}&sort=${param.sort}"</c:if> 
                                                                                                  class="page-link"><i class="fa fa-angle-double-left"></i></a></li>
                                        </c:if>

                                <% int i=1; %>

                                <c:forEach items="${requestScope.product}" var="o" step="10" >
                                    <c:set var="i" value="<%=i%>"/>
                                    <li
                                        <c:if test="${param.page eq i}">
                                            class="page-item active"
                                        </c:if>
                                        ><a <c:if test="${param.search eq null}">href="crud?page=<%=i%>&sort=${param.sort}"</c:if><c:if test="${param.search ne null}">href="crud?page=<%=i%>&search=${param.search}&sort=${param.sort}"</c:if> class="page-link"><%=i%></a></li> 
                                        <% i++;%>
                                    </c:forEach>

                                <c:if test="${fn:length(requestScope.product)>(requestScope.page*10+1)}">
                                    <li class="page-item"><a <c:if test="${param.search eq null}">href="crud?page=${param.page+1}&sort=${param.sort}"</c:if><c:if test="${param.search ne null}">href="crud?page=${param.page+1}&search=${param.search}&sort=${param.sort}"</c:if>class="page-link"><i class="fa fa-angle-double-right"></i></a></li>
                                </c:if>

                            </ul>
                        </div>
                    </div>
                </div> 
            </div>
        </div>   
        <jsp:include page="header-footer/footer.jsp" />  
        <script type="text/javascript">
            document.getElementById("pw").addEventListener("keyup", function (event) {
                event.preventDefault();
                if (event.keyCode === 13) {
                    document.getElementById("myForm").submit();
                }
            }
            );
            function doDelete(id){
                if(confirm("are U sure to delete with id="+id)){
                    window.location="management?manage=delete&id="+id;
                }
            }
        </script>
    </body>
</html>
<%-- 
Document   : SaleChart
Created on : Jul 5, 2023, 10:18:32 PM
Author     : khiem
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>

        </style>
    </head>
    <body>
        <jsp:include page="header-footer/header.jsp" /> 
        <jsp:include page="header-footer/manager.jsp"/>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js"></script>
        <div style="display: flex;flex-direction: column;justify-content: center;align-items:center;margin: 50px 0px;text-decoration: #000;">
            <div>


                <form action="chart">
                    Year <select name="year" onchange="this.form.submit()">
                        <option value="0" ${param.year eq 0?'selected':''}>All</option>
                        <option value="2023" ${param.year eq 2023?'selected':''}>2023</option>
                        <option value="2022" ${param.year eq 2022?'selected':''}>2022</option>  
                    </select>
                    Month <select name="month" onchange="this.form.submit()">
                        <option value="0" ${param.month eq 0?'selected':''}>All</option>
                        <option value="1" ${param.month eq 1?'selected':''}>January</option>
                        <option value="2" ${param.month eq 2?'selected':''}>February</option>
                        <option value="3" ${param.month eq 3?'selected':''}>March</option>
                        <option value="4" ${param.month eq 4?'selected':''}>April</option>
                        <option value="5" ${param.month eq 5?'selected':''}>May</option>
                        <option value="6" ${param.month eq 6?'selected':''}>June</option>
                        <option value="7" ${param.month eq 7?'selected':''}>July</option>
                        <option value="8" ${param.month eq 8?'selected':''}>August</option>
                        <option value="9" ${param.month eq 9?'selected':''}>September</option>
                        <option value="10" ${param.month eq 10?'selected':''}>October</option>
                        <option value="11" ${param.month eq 11?'selected':''}>November</option>
                        <option value="12" ${param.month eq 12?'selected':''}>December</option>           
                    </select>
                    <a href="chart"><button type="button">ALL</button></a>
                </form></div><br><br><br><br>
            <h3 style="color:greenyellow">Best Selling Product</h3>
            <div style="display: flex;border: #007bff solid 2px;padding: 5px;border-radius: 10px;">
                <c:if test="${fn:length(requestScope.products) eq 0}"><h3>No product Sold</h3></c:if>
                <c:forEach items="${requestScope.products}" var="p" begin="0" end="4">
                    <div style="margin: 0px 10px;">
                        <c:set var="msg" value="${p.p.image}" />                                                
                        <c:set var="arrayofmsg" value="${fn:split(msg,',')}" />                                             
                        <a target="_blank" href="products?id=${p.p.id}&page=1"> <img src="${arrayofmsg[0]}" alt="alt" width="100" height="100"/></a>  
                        <div>${p.p.name}</div>
                        <div>Sale: ${p.y}</div>

                    </div>
                </c:forEach>
            </div>
                 <br><br>
                <div style="display: flex;flex-direction: column;border: #007bff solid 2px;padding: 5px;border-radius: 10px;">
                    <div> <h4 style="color:greenyellow">Total Products sold:</h4> <h4>${requestScope.totalSale}</h4></div>
                <div> <h4 style="color:greenyellow">Total Money earn:</h4><h4>${requestScope.totalMoney}$</h4></div>
                </div>
            <br><br><br><br>
            <div>
                <c:forEach items="${requestScope.chart}" var="c" begin="0" end="10">
                    <input type="hidden" name="x" value="${c.month}">
                    <input type="hidden" name="y" value="${c.y}">
                </c:forEach>
            </div>
            <br><br><br><br>
            <div>
                <c:forEach items="${requestScope.chartbrand}" var="cb" begin="0" end="10">
                    <input type="hidden" name="xbrand" value="${cb.brand.name}">
                    <input type="hidden" name="ybrand" value="${cb.y}">
                </c:forEach>
            </div>
            <div style="color: greenyellow;width: 100%;display: flex;justify-content: center;align-items:center;"><canvas id="myChart" style="width:100%;max-width:800px"></canvas></div>
            
            <br><br><br><br><br>
            <div style="color: greenyellow;width: 100%;display: flex;justify-content: center;align-items:center;"><canvas id="myChart1" style="width:100%;max-width:800px"></canvas></div>
            <br><br><br><br><br><br><br><br>
        </div>
        <jsp:include page="header-footer/footer.jsp" /> 
        <script>


            //chart 1
            let xValues = [];
            let yValues = [];
            var x = document.getElementsByName("x");
            var y = document.getElementsByName("y");
            //document.getElementById("demo").innerHTML = x[0].value;
            for (i = 0; i < x.length; i++) {
                xValues.push(x[i].value);
                yValues.push(y[i].value);
            }




            new Chart("myChart", {
                type: "line",
                data: {
                    labels: xValues,
                    datasets: [{
                            fill: false,
                            lineTension: 0,
                            backgroundColor: "rgba(0,0,255,1.0)",
                            borderColor: "rgba(0,0,255,0.1)",
                            data: yValues
                        }]
                },
                options: {
                    legend: {display: false},
                    title: {
                        display: true,
                        text: "DATA ANALASIS FOR TOTAL SALE GROWTH"
                    },
                    scales: {
                        yAxes: [{ticks: {min: 0}}]
                    }
                }
            });


            //chart 2
            var xValues1 = [];
            var yValues1 = [];
            var barColors = ["red", "green", "blue", "brown"];
            var xbrand = document.getElementsByName("xbrand");
            var ybrand = document.getElementsByName("ybrand");
            for (i = 0; i < xbrand.length; i++) {
                xValues1.push(xbrand[i].value);
                yValues1.push(ybrand[i].value);
            }


            new Chart("myChart1", {
                type: "bar",
                data: {
                    labels: xValues1,
                    datasets: [{
                            backgroundColor: barColors,
                            data: yValues1
                        }]
                },
                options: {
                    legend: {display: false},
                    title: {
                        display: true,
                        text: "SALE DISTRIBUTION OF EACH BRAND"
                    },
                    scales: {
                        yAxes: [{ticks: {min: 0}}]
                    }
                }
            });
        </script>

    </body>
</html>

<%-- 
    Document   : login
    Created on : Jun 6, 2023, 2:56:36 PM
    Author     : khiem
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="css/login.css"/>
    </head>
    <body>
        
        <div class="wrapper">
            <div class="title-text">
                <div class="title login">Login Form</div>
                <div class="title signup">Signup Form</div>
            </div>
            <div class="form-container">
                <div class="slide-controls">
                    <input type="radio" name="slide" id="login" checked>
                    <input type="radio" name="slide" id="signup">
                    <label for="login" class="slide login">Login</label>
                    <label for="signup" class="slide signup">Signup</label>
                    <div class="slider-tab"></div>
                </div>
                <div class="form-inner">
                    
                    <form action="login" class="login" method="post">
                       
                        <input type='hidden' name='URL'value='${requestScope.urlRedirect}' />
                        <div class="field">
                            <input type="text" placeholder="Email Address" name="email" required>
                        </div>
                        <div class="field">
                            <input type="password" placeholder="Password" name="password" required>
                        </div>
                        
                        <h4 style="color:chocolate">${requestScope.ms}</h4>
                        <div class="field btn">
                            <div class="btn-layer"></div>
                            <input type="submit" value="Login">
                        </div>
                        
                        <div class="signup-link">Not a member? <a href="">Signup now</a></div>
                    </form>
                    <form  class="signup">
                        <input type="hidden"  value="${requestScope.urlRedirect}" id="URL">
                        <input type="hidden" value="${requestScope.urlRedirect}">
                        <% int x=0;%>
                        <c:forEach items="${requestScope.list}" var="c">
                            <input type="hidden" value="${c.userName}" id="${c.aid}">
                              
                              <%x++;%>
                             
                        </c:forEach>
                            
                              <input type="hidden"  value="<%=x%>" id="num">
                              
                        <div class="field">
                            <input type="text" placeholder="Email Address" name="email"  id="email"required>
                        </div>
                        <div class="field">
                            <input type="password" placeholder="Password" name="password" id="password"required>
                        </div>
                        <div class="field">
                            <input type="password"  placeholder="Confirm password" name="confirm_password" id="confirm_password" required>
                        </div>
                       
                        <h4 id="demo" style="color:chocolate"></h4>
                        <h4 id="demo2" style="color:chocolate"></h4>

                        <div class="field btn">
                            <div class="btn-layer"></div>
                            <input type="button" onclick="matchPassword()" value="Sign" name="Sign">
                            
                        </div>
                    </form>
                </div>
            </div>
        </div>
                        
        <script src="js/login.js"></script>
        
    </body>
</html>

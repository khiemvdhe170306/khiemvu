<%-- 
    Document   : home
    Created on : May 31, 2023, 6:25:38 PM
    Author     : khiem
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

        <title>Electro - HTML Ecommerce Template</title>

        <!-- Google font -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">

        <!-- Bootstrap -->
        <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css"/>

        <!-- Slick -->
        <link type="text/css" rel="stylesheet" href="css/slick.css"/>
        <link type="text/css" rel="stylesheet" href="css/slick-theme.css"/>

        <!-- nouislider -->
        <link type="text/css" rel="stylesheet" href="css/nouislider.min.css"/>

        <!-- Font Awesome Icon -->
        <link rel="stylesheet" href="css/font-awesome.min.css">

        <!-- Custom stlylesheet -->
        <link type="text/css" rel="stylesheet" href="css/home.css"/>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>
    <body>
        <jsp:include page="header-footer/homeheader.jsp" />  


        <!-- NAVIGATION -->
        <nav id="navigation">
            <!-- container -->
            <div class="container">
                <!-- responsive-nav -->
                <div id="responsive-nav">
                    <!-- NAV -->
                    <ul class="main-nav nav navbar-nav">
                        <li class="active"><a href="home">Home</a></li>
                        <li><a href="products?cid=${0}">Watch</a></li>
                        <c:forEach items="${requestScope.homecat}" var="cat">
                        <li><a href="products?cid=${cat.id}">${cat.name}</a></li>
                        </c:forEach>
                        
                    </ul>
                    <!-- /NAV -->
                </div>
                <!-- /responsive-nav -->
            </div>
            <!-- /container -->
        </nav>
        <!-- /NAVIGATION -->

        <!-- SECTION -->
        <div class="section">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <!-- shop -->
                    <div class="col-md-3 col-xs-6">
                        <div class="shop">
                            <div class="shop-img">
                                <img src="./img/fossil/Heritage HP.png" alt="">
                            </div>
                            <div class="shop-body">
                                <h3>Fossil<br>Brand</h3>
                                <a href="#" class="cta-btn">Shop now <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- /shop -->

                    <!-- shop -->
                    <div class="col-md-3 col-xs-6">
                        <div class="shop">
                            <div class="shop-img">
                                <img src="./img/Rolex/SUBMARINE.png" alt="">
                            </div>
                            <div class="shop-body">
                                <h3>Rolex<br>Brand</h3>
                                <a href="#" class="cta-btn">Shop now <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- /shop -->

                    <!-- shop -->
                    <div class="col-md-3 col-xs-6">
                        <div class="shop">
                            <div class="shop-img">
                                <img src="./img/Casio/g-shock black.png" alt="">
                            </div>
                            <div class="shop-body">
                                <h3>Casio<br>Brand</h3>
                                <a href="#" class="cta-btn">Shop now <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- /shop -->
                    <!-- shop -->
                    <div class="col-md-3 col-xs-6">
                        <div class="shop">
                            <div class="shop-img">
                                <img src="./img/Tissot/tissot excellent.png" alt="">
                            </div>
                            <div class="shop-body">
                                <h3>Tissot<br>Brand</h3>
                                <a href="#" class="cta-btn">Shop now <i class="fa fa-arrow-circle-right"></i></a>
                            </div>
                        </div>
                    </div>
                    <!-- /shop -->
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- /SECTION -->

        <!-- SECTION -->
        <div class="section">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <h3 class="title">New Products</h3>
                    <!-- /section title -->

                    <!-- Products tab & slick -->
                    <div class="col-md-12">
                        <div class="row">
                            <div class="products-tabs">
                                <!-- tab -->
                                <div id="tab1" class="tab-pane active">
                                    <div class="products-slick" data-nav="#slick-nav-1">
                                        <!-- product -->
                                        <c:forEach items="${requestScope.homepro}" var="o" begin="0" end="10">
                                            <div class="product">
                                                <div class="product-img">
                                                    <c:set var="msg" value="${o.image}" />                                                
                                                    <c:set var="arrayofmsg" value="${fn:split(msg,',')}" />                                             
                                                    <c:forEach var="i" begin="0" end="0">
                                                        <img src="${arrayofmsg[i]}" alt="alt"/>
                                                    </c:forEach>
                                                    <div class="product-label">
                                                        <span class="sale">Sold:${o.sale}</span>
                                                        <span class="new">
                                                            <c:forEach varStatus="loop" begin="1" end="${o.rating}" step="1">
                                                                <i class="fa fa-star"></i>
                                                            </c:forEach> 
                                                            <c:forEach varStatus="loop" begin="1" end="${5-o.rating}" step="1">
                                                                <i class="fa fa-star-o empty"></i>
                                                            </c:forEach></span>

                                                    </div>
                                                </div>
                                                <div class="product-body">
                                                    <p class="product-category">${o.category.name}</p>
                                                    <h3 class="product-name"><a href="#">${o.name}</a></h3>
                                                    <h4 class="product-price">${o.price}0$ <del class="product-old-price">${o.price*2}0$</del></h4>

                                                    <div class="product-btns">
                                                        
                                                        <button class="quick-view"><a target="_blank" href="products?id=${o.id}&page=1"><i class="fa fa-eye"></i></a><span class="tooltipp">quick view</span></button>

                                                    </div>
                                                </div>
                                                <div class="add-to-cart">
                                                    <c:if test="${sessionScope.account eq null}">
                                                    <button class="add-to-cart-btn"><a href="login?url=home"><i class="fa fa-shopping-cart"></i>add to cart</a> </button>
                                                </c:if>
                                                <c:if test="${sessionScope.account ne null}">
                                                    <button class="add-to-cart-btn"><a href="addCart?id=${o.id}&number=1&url=home&aid=${sessionScope.account.aid}"><i class="fa fa-shopping-cart"></i>add to cart</a> </button>
                                                </c:if>
                                                </div>
                                            </div>
                                            <!-- /product -->
                                        </c:forEach>

                                    </div>
                                    <div id="slick-nav-1" class="products-slick-nav"></div>
                                </div>
                                <!-- /tab -->
                            </div>
                        </div>
                    </div>
                    <!-- Products tab & slick -->
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- /SECTION -->

        <!-- HOT DEAL SECTION -->
        <div id="hot-deal" class="section">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <div class="col-md-12">
                        <div class="hot-deal" style="color:white;">

                            <ul class="hot-deal-countdown">
                                <c:forEach items="${requestScope.homecat}" var="homecat">
                                    <li>
                                        <div>
                                            <h3></h3>
                                            <span>${homecat.name}</span>
                                        </div>
                                    </li>
                                </c:forEach>


                            </ul>
                            <h2 class="text-uppercase">hot deal this week</h2>
                            <p>New Brand Up to 50% OFF</p>
                            <a class="primary-btn cta-btn" href="products?cid=0">Shop now</a>
                        </div>
                    </div>
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- /HOT DEAL SECTION -->
        <!-- SECTION -->
        <div class="section">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <h3 class="title">Best selling Products</h3>
                    <!-- /section title -->

                    <!-- Products tab & slick -->
                    <div class="col-md-12">
                        <div class="row">
                            <div class="products-tabs">
                                <!-- tab -->
                                <div id="tab1" class="tab-pane active">
                                    <div class="products-slick" data-nav="#slick-nav-2">
                                        <!-- product -->
                                        <c:forEach items="${requestScope.Salepro}" var="o" begin="0" end="10">
                                            <div class="product">
                                                <div class="product-img">
                                                    <c:set var="msg" value="${o.image}" />                                                
                                                    <c:set var="arrayofmsg" value="${fn:split(msg,',')}" />                                             
                                                    <c:forEach var="i" begin="0" end="0">
                                                        <img src="${arrayofmsg[i]}" alt="alt"/>
                                                    </c:forEach>
                                                    <div class="product-label">
                                                        <span class="sale">Sold:${o.sale}</span>
                                                        <span class="new">
                                                            <c:forEach varStatus="loop" begin="1" end="${o.rating}" step="1">
                                                                <i class="fa fa-star"></i>
                                                            </c:forEach> 
                                                            <c:forEach varStatus="loop" begin="1" end="${5-o.rating}" step="1">
                                                                <i class="fa fa-star-o empty"></i>
                                                            </c:forEach></span>

                                                    </div>
                                                </div>
                                                <div class="product-body">
                                                    <p class="product-category">${o.category.name}</p>
                                                    <h3 class="product-name"><a href="#">${o.name}</a></h3>
                                                    <h4 class="product-price">${o.price}0$ <del class="product-old-price">${o.price*2}0$</del></h4>

                                                    <div class="product-btns">
                                                        
                                                        <button class="quick-view"><a href="products?id=${o.id}&page=1"><i class="fa fa-eye"></i></a><span class="tooltipp">quick view</span></button>

                                                    </div>
                                                </div>
                                                <div class="add-to-cart">
                                                    <c:if test="${sessionScope.account eq null}">
                                                    <button class="add-to-cart-btn"><a href="login?url=home"><i class="fa fa-shopping-cart"></i>add to cart</a> </button>
                                                </c:if>
                                                <c:if test="${sessionScope.account ne null}">
                                                    <button class="add-to-cart-btn"><a href="addCart?id=${o.id}&number=1&url=home&aid=${sessionScope.account.aid}"><i class="fa fa-shopping-cart"></i>add to cart</a> </button>
                                                </c:if>
                                                </div>
                                            </div>
                                            <!-- /product -->
                                        </c:forEach>

                                    </div>
                                    <div id="slick-nav-2" class="products-slick-nav"></div>
                                </div>
                                <!-- /tab -->
                            </div>
                        </div>
                    </div>
                    <!-- Products tab & slick -->
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- /SECTION -->
        <!-- SECTION -->
        <div class="section">
            <!-- container -->
            <div class="container">
                <!-- row -->
                <div class="row">
                    <h3 class="title">Best rated Products</h3>
                    <!-- /section title -->

                    <!-- Products tab & slick -->
                    <div class="col-md-12">
                        <div class="row">
                            <div class="products-tabs">
                                <!-- tab -->
                                <div id="tab1" class="tab-pane active">
                                    <div class="products-slick" data-nav="#slick-nav-3">
                                        <!-- product -->
                                        <c:forEach items="${requestScope.Ratingpro}" var="o" begin="0" end="10">
                                            <div class="product">
                                                <div class="product-img">
                                                    <c:set var="msg" value="${o.image}" />                                                
                                                    <c:set var="arrayofmsg" value="${fn:split(msg,',')}" />                                             
                                                    <c:forEach var="i" begin="0" end="0">
                                                        <img src="${arrayofmsg[i]}" alt="alt"/>
                                                    </c:forEach>
                                                    <div class="product-label">
                                                        <span class="sale">Sold:${o.sale}</span>
                                                        <span class="new">
                                                            <c:forEach varStatus="loop" begin="1" end="${o.rating}" step="1">
                                                                <i class="fa fa-star"></i>
                                                            </c:forEach> 
                                                            <c:forEach varStatus="loop" begin="1" end="${5-o.rating}" step="1">
                                                                <i class="fa fa-star-o empty"></i>
                                                            </c:forEach></span>

                                                    </div>
                                                </div>
                                                <div class="product-body">
                                                    <p class="product-category">${o.category.name}</p>
                                                    <h3 class="product-name"><a href="#">${o.name}</a></h3>
                                                    <h4 class="product-price">${o.price}0$ <del class="product-old-price">${o.price*2}0$</del></h4>

                                                    <div class="product-btns">
                                                        
                                                        <button class="quick-view"><a href="products?id=${o.id}&page=1"><i class="fa fa-eye"></i></a><span class="tooltipp">quick view</span></button>

                                                    </div>
                                                </div>
                                                <div class="add-to-cart">
                                                    <c:if test="${sessionScope.account eq null}">
                                                    <button class="add-to-cart-btn"><a href="login?url=home"><i class="fa fa-shopping-cart"></i>add to cart</a> </button>
                                                </c:if>
                                                <c:if test="${sessionScope.account ne null}">
                                                    <button class="add-to-cart-btn"><a href="addCart?id=${o.id}&number=1&url=home&aid=${sessionScope.account.aid}"><i class="fa fa-shopping-cart"></i>add to cart</a> </button>
                                                </c:if>
                                                </div>
                                            </div>
                                            <!-- /product -->
                                        </c:forEach>

                                    </div>
                                    <div id="slick-nav-3" class="products-slick-nav"></div>
                                </div>
                                <!-- /tab -->
                            </div>
                        </div>
                    </div>
                    <!-- Products tab & slick -->
                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
        </div>
        <!-- /SECTION -->

        <!-- FOOTER -->
        <jsp:include page="header-footer/footer.jsp" />  


        <!-- jQuery Plugins -->
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/slick.min.js"></script>
        <script src="js/nouislider.min.js"></script>
        <script src="js/jquery.zoom.min.js"></script>
        <script src="js/main.js"></script>

    </body>
</html>

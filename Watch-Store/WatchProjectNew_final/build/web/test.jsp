<%-- 
    Document   : test
    Created on : Jul 8, 2023, 5:14:10 PM
    Author     : khiem
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <table class="table table-striped table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Name </th>                      
                                    <th>Description</th>                        
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${requestScope.homecat}" var="c" begin="${param.page*10-10}" end="${param.page*10-1}">
                                    <tr>
                                        <td>${c.id}</td>
                                        <td>${c.name}</td>     
                                        <td>${c.describe}</td>                                        
                                        <td>                                           
                                            
                                        </td>
                                    </tr>
                                </c:forEach>


                            </tbody>
                        </table>
    </body>
</html>

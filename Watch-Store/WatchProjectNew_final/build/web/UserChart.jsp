<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <style>

        </style>
    </head>
    <body>
        <jsp:include page="header-footer/header.jsp" /> 
        <jsp:include page="header-footer/manager.jsp"/>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js"></script>
        <div style="display: flex;flex-direction: column;justify-content: center;align-items:center;margin: 50px 0px;text-decoration: #000;">
            <div>


                <form action="userchart">
                    Year <select name="year" onchange="this.form.submit()">
                        <option value="0" ${param.year eq 0?'selected':''}>All</option>
                        <option value="2023" ${param.year eq 2023?'selected':''}>2023</option>
                        <option value="2022" ${param.year eq 2022?'selected':''}>2022</option>  
                    </select>
                    Month <select name="month" onchange="this.form.submit()">
                        <option value="0" ${param.month eq 0?'selected':''}>All</option>
                        <option value="1" ${param.month eq 1?'selected':''}>January</option>
                        <option value="2" ${param.month eq 2?'selected':''}>February</option>
                        <option value="3" ${param.month eq 3?'selected':''}>March</option>
                        <option value="4" ${param.month eq 4?'selected':''}>April</option>
                        <option value="5" ${param.month eq 5?'selected':''}>May</option>
                        <option value="6" ${param.month eq 6?'selected':''}>June</option>
                        <option value="7" ${param.month eq 7?'selected':''}>July</option>
                        <option value="8" ${param.month eq 8?'selected':''}>August</option>
                        <option value="9" ${param.month eq 9?'selected':''}>September</option>
                        <option value="10" ${param.month eq 10?'selected':''}>October</option>
                        <option value="11" ${param.month eq 11?'selected':''}>November</option>
                        <option value="12" ${param.month eq 12?'selected':''}>December</option>           
                    </select>
                    <a href="userchart"><button type="button">ALL</button></a>
                </form></div><br><br><br><br>
            <h3 style="color:greenyellow">Top User </h3>
            <div style="display: flex;border: #007bff solid 2px;padding: 5px;border-radius: 10px;">

                <c:forEach items="${requestScope.account}" var="a" begin="0" end="4">
                    <div style="margin: 0px 10px;">
                        <c:if test="${a.image eq null}">
                            <a target="_blank" href="accountinfo?aid=${a.aid}" ><img src="img/face.png" width="100" height="100" style="border-radius:50px;margin: 5px "></a>    
                            </c:if>  
                            <c:if test="${a.image ne null}">
                            <a target="_blank" href="accountinfo?aid=${a.aid}" ><img src="data:image/jpg;base64,${a.image}" data-index="0" width="100" height="100" style="border-radius:50px;margin: 5px "></a>     
                            </c:if>

                        <div style="color: #03A9F4">${a.userName}</div>
                        <div><a target="_blank" href="userspending?aid=${a.aid}&money=${a.money}">Purchase: ${a.money}$</a></div>
                    </div>
                </c:forEach>
            </div>
            <br><br><br><br>
            <div>
                <c:forEach items="${requestScope.rating}" var="c" begin="0" end="10">
                    <input type="hidden" name="xbrand" value="${c.x} Star">
                    <input type="hidden" name="ybrand" value="${c.y}">
                </c:forEach>
            </div>
            <div>
                <c:forEach items="${requestScope.gender}" var="c" begin="0" end="10">
                    <input type="hidden" name="x" value="${c.month}">
                    <input type="hidden" name="y" value="${c.y}">
                </c:forEach>
            </div>
            <div>
                <c:forEach items="${requestScope.age}" var="c" begin="0" end="10">
                    <input type="hidden" name="xage" value="${c.month} years old">
                    <input type="hidden" name="yage" value="${c.y}">
                </c:forEach>
            </div>
            <div style="color: greenyellow;width: 100%;display: flex;justify-content: center;align-items:center;"><canvas id="myChart1" style="width:100%;max-width:800px"></canvas></div>
            <br><br><br><br><br>
            <div style="color: greenyellow;width: 100%;display: flex;justify-content: center;align-items:center;"><canvas id="myChart" style="width:100%;max-width:800px"></canvas></div>
            <br><br><br><br><br>
            <div style="color: greenyellow;width: 100%;display: flex;justify-content: center;align-items:center;"><canvas id="myChart2" style="width:100%;max-width:800px"></canvas></div>
            <br><br><br><br><br><br><br><br>
        </div>
        <script>
            //chart 2
            var xValues1 = [];
            var yValues1 = [];
            var barColors = ["red", "brown", "yellow", "blue", "green"];
            var xbrand = document.getElementsByName("xbrand");
            var ybrand = document.getElementsByName("ybrand");
            for (i = 0; i < xbrand.length; i++) {
                xValues1.push(xbrand[i].value);
                yValues1.push(ybrand[i].value);
            }


            new Chart("myChart1", {
                type: "bar",
                data: {
                    labels: xValues1,
                    datasets: [{
                            backgroundColor: barColors,
                            data: yValues1
                        }]
                },
                options: {
                    legend: {display: false},
                    title: {
                        display: true,
                        text: "CUSTOMER SATISFACTION BY RATING"
                    },
                    scales: {
                        yAxes: [{ticks: {min: 0}}]
                    }
                }
            });
            //pie chart
            var xValues = [];
            var yValues = [];
            var x = document.getElementsByName("x");
            var y = document.getElementsByName("y");
            //document.getElementById("demo").innerHTML = x[0].value;
            for (i = 0; i < x.length; i++) {
                xValues.push(x[i].value);
                yValues.push(y[i].value);
            }
            var barColors1 = [ 
                "green",
                "pink",
                "#2b5797",
                "#e8c3b9",
                "#1e7145"
            ];

            new Chart("myChart", {
                type: "pie",
                data: {
                    labels: xValues,
                    datasets: [{
                            backgroundColor: barColors1,
                            data: yValues
                        }]
                },
                options: {
                    title: {
                        display: true,
                        text: "Gender of Customer "
                    }
                }
            });
            //pie chart 2
            var xValues2 = [];
            var yValues2 = [];
            var xage = document.getElementsByName("xage");
            var yage = document.getElementsByName("yage");
            //document.getElementById("demo").innerHTML = x[0].value;
            for (i = 0; i < xage.length; i++) {
                xValues2.push(xage[i].value);
                yValues2.push(yage[i].value);
            }
            var barColors2 = [ 
                "yellow",
                "blue",
                "green",
                "red",
                "#1e7145"
            ];

            new Chart("myChart2", {
                type: "pie",
                data: {
                    labels: xValues2,
                    datasets: [{
                            backgroundColor: barColors2,
                            data: yValues2
                        }]
                },
                options: {
                    title: {
                        display: true,
                        text: "Age of Customer "
                    }
                }
            });
        </script>
        <jsp:include page="header-footer/footer.jsp" /> 
    </body>
</html>

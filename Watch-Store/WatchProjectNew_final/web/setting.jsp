<%-- 
    Document   : profile
    Created on : Jun 25, 2023, 4:38:21 PM
    Author     : khiem
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib  prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">


        <title>profile with data and skills - Bootdey.com</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" rel="stylesheet">
        <style type="text/css">
            body{
                margin-top:20px;
                color: #1a202c;
                text-align: left;
                background-color: #e2e8f0;
            }
            .main-body {
                padding: 15px;
            }
            .card {
                box-shadow: 0 1px 3px 0 rgba(0,0,0,.1), 0 1px 2px 0 rgba(0,0,0,.06);
            }

            .card {
                position: relative;
                display: flex;
                flex-direction: column;
                min-width: 0;
                word-wrap: break-word;
                background-color: #fff;
                background-clip: border-box;
                border: 0 solid rgba(0,0,0,.125);
                border-radius: .25rem;
            }

            .card-body {
                flex: 1 1 auto;
                min-height: 1px;
                padding: 1rem;
            }

            .gutters-sm {
                margin-right: -8px;
                margin-left: -8px;
            }

            .gutters-sm>.col, .gutters-sm>[class*=col-] {
                padding-right: 8px;
                padding-left: 8px;
            }
            .mb-3, .my-3 {
                margin-bottom: 1rem!important;
            }

            .bg-gray-300 {
                background-color: #e2e8f0;
            }
            .h-100 {
                height: 100%!important;
            }
            .shadow-none {
                box-shadow: none!important;
            }

        </style>
    </head>
    <body>
     <jsp:include page="header-footer/header.jsp" />  

        <div class="container">
            <div class="main-body">

                <nav aria-label="breadcrumb" class="main-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="home">Home</a></li>
                        <li class="breadcrumb-item"><a href="profile">User Profile</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Account Info</li>
                    </ol>
                </nav>

                <div class="row gutters-sm">
                    <div class="col-md-4 mb-3">
                        <div class="card">
                            <div class="card-body">
                                <div class="d-flex flex-column align-items-center text-center">

                                    <c:if test="${sessionScope.accountimage eq null}">
                                        <img src="img/face.png" width="200" class="rounded-circle">
                                        <form action="img" method="post" enctype="multipart/form-data">
                                            <input type="file" class="hidden" name="imageProfile" id="files"  onchange="this.form.submit()" />
                                                <label class="custom-file-upload" for="files">Create your Avatar</label>
                                            <input type="hidden" name="add" value="${sessionScope.account.aid}">
                                        </form> 
                                    </c:if>
                                    <c:if test="${sessionScope.accountimage ne null}">
                                        <img src="data:image/jpg;base64,${sessionScope.accountimage}" data-index="0" width="200" height="200"class="rounded-circle">
                                        <form action="img" method="post" enctype="multipart/form-data">
                                            
                                            <input type="file" class="hidden" name="imageProfile" id="files"  onchange="this.form.submit()" />
                                                <label class="custom-file-upload" for="files">Update your Avatar</label>
                                            <input type="hidden" name="update" value="${sessionScope.account.aid}">
                                        </form> 
                                    </c:if>


                                    <div class="mt-3">
                                        <h4>${sessionScope.account.userName}</h4>
                                        <c:if test="${sessionScope.account.role eq 1}">
                                            <p class="text-secondary mb-1">USER</p>
                               
                                        </c:if>
                                            <c:if test="${sessionScope.account.role eq 2}">
                                            <p class="text-secondary mb-1">ADMIN</p>
                               
                                        </c:if>
                                        
<!--                                        <input type="file" data-index="0" onChange="swapImage(this)">-->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-8">
                        <div class="card mb-3">
                            <div class="card-body">
                                <form action="upload" >
                                    <c:if test="${param.update eq 1}">
                                        <input type="hidden" name="update" value="UpdateProfile">
                                        <c:if test="${sessionScope.accountinfo eq null}">
                                            <input type="hidden" name="create" value="CreateProfile">
                                        </c:if>
                                        <div class="row">

                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Full Name</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <input type="text" name="fullname" value="${sessionScope.accountinfo.fullname==null?'':sessionScope.accountinfo.fullname}" required>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Email</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <input type="email"  name="email" value="${sessionScope.accountinfo.fullname==null?'':sessionScope.accountinfo.email}" required>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Phone</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <input type="tel" name="phone" title="10 phone number" pattern="[0-9]{3}[0-9]{3}[0-9]{4}" value="${sessionScope.accountinfo.fullname==null?'':sessionScope.accountinfo.phone}" required>
                                            </div>
                                        </div>
                                        <hr>

                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Address</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <input type="text" name="address" value="${sessionScope.accountinfo.fullname==null?'':sessionScope.accountinfo.address}">
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Age</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <input type="number" name="age" min="10" value="${sessionScope.accountinfo.age==null?'':sessionScope.accountinfo.age}" required>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Gender</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <select name="gender">
                                                    <option value="1" ${sessionScope.accountinfo.gender?'selected':''}>Male</option>
                                                    <option value="0" ${!sessionScope.accountinfo.gender?'selected':''}>Female</option>
                                                </select>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <input type="submit" value="Save" />
                                            </div>
                                        </div>
                                    </c:if>
                                    <c:if test="${param.update eq 2}">
                                        <input type="hidden" name="update" value="UpdateAccount">
                                        <input type="hidden" id="account2" value="${sessionScope.account.userName}">
                                        <input type="hidden" id="password2" value="${sessionScope.account.passWord}">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Your Account</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <input type="text" id="account1" placeholder="AccountName" value="">
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Your Password</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <input type="password" placeholder="AccountPassword" id="password1" value="" required>
                                            </div>
                                        </div>
                                        <hr>
                                        
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">New Password</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <input type="password" placeholder="Password" name="password" id="password"required>
                                            </div>
                                        </div>
                                        <hr>
                                        <h4 id="demo" style="color:chocolate"></h4>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <input type="button" onclick="matchPassword()" value="Submit" name="Sign">
                                            </div>
                                        </div>
                                        
                                    </c:if>


                                </form>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
         <jsp:include page="header-footer/footer.jsp" />  

        <script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.bundle.min.js"></script>
        <script type="text/javascript">
                                                    function matchPassword() {
                                                        var em1 = document.getElementById("account1").value;
                                                        var em2 = document.getElementById("account2").value;
                                                        var pw1 = document.getElementById("password1").value;
                                                        var pw2 = document.getElementById("password2").value;
                                                        var pw = document.getElementById("password").value;
                                                        

                                                       
                                                        if(!pw){
                                                             document.getElementById("demo").innerHTML = "Please fill out the form";
                                                        }
                                                        else if (pw1 !== pw2 && em1 !== em2)
                                                        {

                                                            document.getElementById("demo").innerHTML = "Not match password and email!!! Enter again"+pw;
                                                        } else if (em1 !== em2) {
                                                            document.getElementById("demo").innerHTML = "Not match email!!! Enter again";
                                                        } else if (pw1 !== pw2) {
                                                            document.getElementById("demo").innerHTML = "Not match password!!! Enter again";

                                                        } else {
                                                            window.location.href = "upload?"+"&pass="+pw+"&olduser="+em1+"&oldpass="+pw1+"&update=UpdateAccount";
                                                        }
                                                    }
        </script>
    </body>
</html>
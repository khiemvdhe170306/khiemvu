<%-- 
    Document   : cart
    Created on : Jun 23, 2023, 10:41:08 PM
    Author     : khiem
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!doctype html>
<html>
    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title>Snippet - BBBootstrap</title>
        <link href='https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css' rel='stylesheet'>
        <link href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css' rel='stylesheet'>
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
        <link rel="stylesheet" href="css/cart.css"/>
        <style>
            .img-back{
                background-image: url(../img/login2.png);
                width: 100%;
            }

        </style>
    </head>
    <body className='snippet-body'>
        <jsp:include page="header-footer/header.jsp" />  


        <div class="img-back">
            <c:if test="${param.ms ne null}">
                <h3 style="padding-left: 100px;color: green;padding-top:100px; ">Thanks you alot for buying stuff in our website and hope you enjoy your precious time here!!!</h3>
            </c:if>
            <div class="container mt-5 p-3 rounded cart">
                <div class="row no-gutters">
                    <div class="col-md-8">
                        <div class="product-details mr-2">
                            <div class="d-flex flex-row align-items-center"><i class="fa fa-long-arrow-left"></i><span class="ml-2">Continue Shopping</span></div>
                            <hr>
                            <h6 class="mb-0">Shopping cart</h6>
                            <div class="d-flex justify-content-between"><span>You have ${fn:length(requestScope.list)} items in your cart</span></div>
                            <c:set var="total" value="${0}"/>
                            <c:forEach items="${requestScope.list}" var="o">

                                <div class="d-flex justify-content-between align-items-center mt-3 p-2 items rounded">
                                    <c:set var="msg" value="${o.image}" />                                                
                                    <c:set var="arrayofmsg" value="${fn:split(msg,',')}" />   
                                    <c:set var="total" value="${total + o.price*o.number}" />
                                    <c:forEach var="i" begin="0" end="0">
                                        <div class="d-flex flex-row"><img class="rounded" src="${arrayofmsg[i]}" width="120">
                                        </c:forEach>                         
                                        <div class="ml-2"><span class="font-weight-bold d-block">${o.name}</span><span class="spec">Product ID:${o.pid}</span>      
                                            <span class="spec"><a target="_blank" href="products?id=${o.pid}&page=1"><i class="material-icons">&#xE417;</i></a></span></div>
                                    </div>
                                    <div class="d-flex flex-row align-items-center"><span class="d-block">Number:${o.number}</span><span class="d-block ml-5 font-weight-bold">price:${o.price}$</span><a href="deleteCart?pid=${o.pid}&aid=${sessionScope.account.aid}"><i class="fa fa-trash-o ml-3 text-black-50"></i></a></div>
                                </div>
                            </c:forEach>
                            <div class="d-flex justify-content-between align-items-center mt-3 p-2 items rounded">
                                <div class="d-flex justify-content-between information"><span class="d-block ml-5 font-weight-bold">Total</span></div>
                                <div class="d-flex flex-row align-items-center"><span class="d-block ml-5 font-weight-bold">${total}$</span></div>
                            </div>
                        </div>

                    </div>
                    <div class="col-md-4">
                        <div class="payment-info">
                            <div class="d-flex justify-content-between align-items-center"><span>Customer details</span></div>
                            <div style="margin-bottom: 10px"><label class="credit-card-label">FullName</label><input style="width: 100%" type="text" class=" credit-inputs" value="${sessionScope.accountinfo.fullname}"></div>
                            <div style="margin-bottom: 10px"><label class="credit-card-label">Email</label><input style="width: 100%" type="text" class=" credit-inputs" value="${sessionScope.accountinfo.email}"></div>
                            <div style="margin-bottom: 10px"><label class="credit-card-label">Address</label><input style="width: 100%" type="text" class=" credit-inputs" value="${sessionScope.accountinfo.address}"></div>
                            <div style="margin-bottom: 10px"><label class="credit-card-label">Phone</label><input style="width: 100%" type="text" class=" credit-inputs" value="${sessionScope.accountinfo.phone}"></div>
                            <hr class="line">
                             <div class="d-flex justify-content-between information"><span>Total</span><span>${total}$</span></div>
                              <div class="d-flex justify-content-between information"><span>Tax fee(5%)</span><span>${total/20}$</span></div>
                            <div class="d-flex justify-content-between information"><span>Total(Incl. taxes)</span><span>${total+total/20}$</span></div>
                            <form action="checkout">
                                <input type="hidden" name="totalMoney" value="${total+total/20}">
                                <button class="btn btn-primary btn-block d-flex justify-content-between mt-3" type="submit"><span>Checkout<i class="fa fa-long-arrow-right ml-1"></i></span></button>

                            </form>
                        </div>
                    </div>

                </div>
            </div>

        </div>
        <jsp:include page="header-footer/footer.jsp" />  
        <script type='text/javascript' src='https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js'></script>
        <script type='text/javascript' src='#'></script>
        <script type='text/javascript' src='#'></script>
        <script type='text/javascript' src='#'></script>
        <script type='text/javascript'>#</script>
        <script type='text/javascript'>var myLink = document.querySelector('a[href="#"]');
            myLink.addEventListener('click', function (e) {
                e.preventDefault();
            });</script>

    </body>
</html>
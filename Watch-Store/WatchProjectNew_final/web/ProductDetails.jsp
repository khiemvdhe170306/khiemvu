<%-- 
    Document   : ProductDetails
    Created on : Jun 3, 2023, 1:23:03 PM
    Author     : khiem
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

        <title>Electro - HTML Ecommerce Template</title>

        <!-- Google font -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">

        <!-- Bootstrap -->
        <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css"/>

        <!-- Slick -->
        <link type="text/css" rel="stylesheet" href="css/slick.css"/>
        <link type="text/css" rel="stylesheet" href="css/slick-theme.css"/>

        <!-- nouislider -->
        <link type="text/css" rel="stylesheet" href="css/nouislider.min.css"/>

        <!-- Font Awesome Icon -->
        <link rel="stylesheet" href="css/font-awesome.min.css">

        <!-- Custom stlylesheet -->
        <link type="text/css" rel="stylesheet" href="css/home.css"/>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
            .Dell-info{
                border: 3.5px solid gray;
                box-shadow: 2px 2px 5px gray;
                border-radius: 5px;

            }
            .underline p{
                border-bottom: 3px solid gray;
                width: 98%;
                display: block;
                margin: 2%;
            }
            .line p{
                width: 98%;
                display: block;
                margin: 2%;
            }
            footer{
                margin-top: 500px;
            }
            .title{
                margin-top:50px;
            }

        </style>


    </head>
    <body>
        <script src="ckeditor/ckeditor.js"></script> 
        <jsp:include page="header-footer/homeheader.jsp" />  

        <!-- NAVIGATION -->
        <nav id="navigation">
            <!-- container -->
            <div class="container">
                <!-- responsive-nav -->
                <div id="responsive-nav">
                    <!-- NAV -->
                    <ul class="main-nav nav navbar-nav">
                        <li class="active"><a href="home">Home</a></li>
                        <li><a href="products?cid=${0}">Watch</a></li>
                        <li><a href="products?cid=${1}">Casio</a></li>
                        <li><a href="products?cid=${2}">Fossil</a></li>
                        <li><a href="products?cid=${3}">Tissot</a></li>
                        <li><a href="products?cid=${4}">Rolex</a></li>


                    </ul>
                    <!-- /NAV -->
                </div>
                <!-- /responsive-nav -->
            </div>
            <!-- /container -->
        </nav>
        <!-- /NAVIGATION -->
        <!-- SECTION -->
        <div class="section">
            <!-- container -->
            <div class="container-fluid">
                <!-- row -->
                <div class="row">

                    <c:set var="c" value="${requestScope.product}"/>
                    <!-- Product main img -->
                    <div class="col-md-5 col-md-push-2">
                        <div id="product-main-img">
                            <c:set var="msg" value="${c.image}" />
                            <c:set var="arrayofmsg" value="${fn:split(msg,',')}" />
                            <c:forEach var="i" begin="0" end="3">                  
                                <div class="product-preview">
                                    <img src="${arrayofmsg[i]}" alt="${arrayofmsg[i]}"/>
                                </div>
                            </c:forEach>
                        </div>
                    </div>
                    <!-- /Product main img -->

                    <!-- Product thumb imgs -->
                    <div class="col-md-2  col-md-pull-5"> 
                        <div id="product-imgs">
                            <c:set var="msg" value="${c.image}" />
                            <c:set var="arrayofmsg" value="${fn:split(msg,',')}" />
                            <c:forEach var="i" begin="0" end="3">                  
                                <div class="product-preview">
                                    <img src="${arrayofmsg[i]}" alt="null">
                                </div>
                            </c:forEach>



                        </div>
                    </div>
                    <!-- /Product thumb imgs -->

                    <!-- Product details -->
                    <div class="col-md-5">
                        <div class="product-details">
                            <c:set var="c" value="${requestScope.product}"/>
                            <h2 class="product-name">${c.name}</h2>
                            <div>
                                <div class="rating-stars">
                                    <c:forEach varStatus="loop" begin="1" end="${requestScope.totalrate}" step="1">
                                        <i class="fa fa-star"></i>
                                    </c:forEach> 
                                    <c:forEach varStatus="loop" begin="1" end="${5-requestScope.totalrate}" step="1">
                                        <i class="fa fa-star-o"></i>
                                    </c:forEach> 
                                        <p>${requestScope.ratedetail[5]+requestScope.ratedetail[4]+requestScope.ratedetail[3]+requestScope.ratedetail[2]} Reviews</p>
                                </div>
                               

                            </div>

                            <div>
                                <h3 class="product-price">${c.price}$ <del class="product-old-price">${c.price * 2}$ </del></h3>
                                <span class="product-available"></span>
                                <span></span>

                            </div>
                                <c:set var="arrayofdetail" value="${fn:split(c.details,',')}" /> 
                            <div class="product-options">
                                <label>
                                    Size: ${arrayofdetail[0]}

                                </label>
                                    <label>
                                    Material: ${arrayofdetail[1]}

                                    </label>
                                    
                            </div>
                            <div class="product-options">
                                
                                    <label>
                                    Style: ${arrayofdetail[2]}

                                </label>
                                <label>
                                    Color: ${arrayofdetail[3]}

                                </label>
                            </div>
                            <form action="${sessionScope.account eq null?'login':'addCart'}">
                                <div class="add-to-cart">
                                    <input type="hidden" value="${sessionScope.account.aid}" name="aid" >
                                    <input type="hidden" value="${param.id}" name="id">
                                    <input type="hidden" value="products?id=${c.id}" name="url">

                                    <div class="qty-label">
                                        Qty
                                        <div class="input-number">
                                            <input type="number" name="number" value="1" min="1">
                                            <span class="qty-up">+</span>
                                            <span class="qty-down">-</span>
                                        </div>
                                    </div>

                                    <button class="add-to-cart-btn"><i class="fa fa-shopping-cart"></i> add to cart</button>
                                </div>
                            </form>


                            

                            <ul class="product-links">
                                <li>Category:</li>
                                <li><p>${c.category.name}</p></li>
                            </ul>

                            <ul class="product-links">
                                <li>Share:</li>
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                <li><a href="#"><i class="fa fa-envelope"></i></a></li>
                            </ul>

                        </div>
                    </div>
                    <!-- /Product details -->

                    <!-- Product tab -->
                    <div class="col-md-12" style="margin-bottom: 200px;">
                        <div id="product-tab">
                            <!-- product tab nav -->
                            <ul class="tab-nav">
                                <li class="active"><a data-toggle="tab" href="#tab1">Description</a></li>
                                <li><a data-toggle="tab" href="#tab2">Details</a></li>

                            </ul>
                            <!-- /product tab nav -->

                            <!-- product tab content -->
                            <div class="tab-content">
                                <!-- tab1  -->
                                <div id="tab1" class="tab-pane fade in active">
                                    <div class="row">
                                        <div class="col-md-12"> 
                                            <div class="Dell-info line">
                                                <p>${c.describe}</p>
                                                <!-- test filter -->
                                                
                                                <c:forEach var="i" begin="0" end="2">                  
                                                </c:forEach>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!-- /tab1  -->

                                <!-- tab2  -->
                                <div id="tab2" class="tab-pane fade in">
                                    <div class="row">
                                        <div class="col-md-12"> 
                                            <div class="Dell-info underline">  
                                                <c:set var="details" value="${c.details}" />
                                                <c:set var="arrayofdetails" value="${fn:split(details,',')}" />
                                                <p>Belt:${arrayofdetails[1]}</p>
                                                <p>Caliber:11 1/2''',25 mm </p>
                                                <p> MovementSwiss ${arrayofdetails[2]}</p>
                                                <p>Glass:Sapphire</p>
                                                <p>Size: ${arrayofdetails[0]}</p>
                                                <p>Color:${arrayofdetails[3]}</p>
                                                
                                            </div>
                                        </div>

                                    </div>
                                </div>
                                <!-- /tab2  -->

                                <!-- tab3  -->

                            </div>
                            <!-- /product tab content  -->
                        </div>
                    </div>
                    <!-- /product tab -->


                    <div class="comment" style="margin:50px auto;width: 80%;margin-bottom: 0px">

                        <div id="tab3" class="tab-pane fade in" >
                            <div class="row">
                                <c:if test="${requestScope.rating ne null}">
                                    <!-- Rating -->
                                    <div class="col-md-3">
                                        <div style="position: relative;bottom: 50px;"> <h2>Comment</h2></div>
                                        <div id="rating">
                                            <div class="rating-avg">
                                                <span>${requestScope.totalrate}.0</span>
                                                <div class="rating-stars">
                                                    <c:forEach varStatus="loop" begin="1" end="${requestScope.totalrate}" step="1">
                                                        <i class="fa fa-star"></i>
                                                    </c:forEach> 
                                                    <c:forEach varStatus="loop" begin="1" end="${5-requestScope.totalrate}" step="1">
                                                        <i class="fa fa-star-o"></i>
                                                    </c:forEach> 
                                                </div>
                                            </div>
                                            <ul class="rating">
                                                <li>
                                                    <div class="rating-stars">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                    <div class="rating-progress">
                                                        <div style="width: ${requestScope.ratedetail[5]/fn:length(requestScope.rating)*100}%;"></div>
                                                    </div>
                                                    <span class="sum">${requestScope.ratedetail[5]}</span>
                                                </li>
                                                <li>
                                                    <div class="rating-stars">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star-o"></i>
                                                    </div>
                                                    <div class="rating-progress">
                                                        <div style="width: ${requestScope.ratedetail[4]/fn:length(requestScope.rating)*100}%;"></div>
                                                    </div>
                                                    <span class="sum">${requestScope.ratedetail[4]}</span>
                                                </li>
                                                <li>
                                                    <div class="rating-stars">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star-o"></i>
                                                        <i class="fa fa-star-o"></i>
                                                    </div>
                                                    <div class="rating-progress">
                                                        <div style="width: ${requestScope.ratedetail[3]/fn:length(requestScope.rating)*100}%;"></div>
                                                    </div>
                                                    <span class="sum">${requestScope.ratedetail[3]}</span>
                                                </li>
                                                <li>
                                                    <div class="rating-stars">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star-o"></i>
                                                        <i class="fa fa-star-o"></i>
                                                        <i class="fa fa-star-o"></i>
                                                    </div>
                                                    <div class="rating-progress">
                                                        <div style="width: ${requestScope.ratedetail[2]/fn:length(requestScope.rating)*100}%;"></div>
                                                    </div>
                                                    <span class="sum">${requestScope.ratedetail[2]}</span>
                                                </li>
                                                <li>
                                                    <div class="rating-stars">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star-o"></i>
                                                        <i class="fa fa-star-o"></i>
                                                        <i class="fa fa-star-o"></i>
                                                        <i class="fa fa-star-o"></i>
                                                    </div>
                                                    <div class="rating-progress">
                                                        <div style="width: ${requestScope.ratedetail[1]/fn:length(requestScope.rating)*100}%;"></div>
                                                    </div>
                                                    <span class="sum">${requestScope.ratedetail[1]}</span>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- /Rating -->
                                    
                                    <!-- Reviews -->
                                    <div class="col-md-9">
                                        <div id="reviews">
                                            <ul class="reviews" style="border:1px solid #888; padding: 5px;border-radius: 10px;margin-bottom: 10px;">
                                                <h3 style="color:red">${requestScope.error}</h3>
                                                <c:forEach items="${requestScope.rating}" var="r" begin="${param.page*4-4}" end="${requestScope.page*4-1}">
                                                    <li >
                                                        <div class="review-heading">
                                                            <div style="display: flex;"> 
                                                                <c:if test="${r.image eq null}">
                                                                    <img src="img/face.png" alt="none" data-index="0" width="20" height="20" class="rounded-circle">
                                                                </c:if><c:if test="${r.image ne null}">
                                                                    <img src="data:image/jpg;base64,${r.image}" alt="none" data-index="0" width="20" height="20" class="rounded-circle"></c:if>
                                                                <h5 class="name" style="color:${r.a.userName eq sessionScope.account.userName?'red':''}">${r.a.userName eq sessionScope.account.userName?'Me':r.a.userName}</h5></div>   

                                                            <p class="date">${r.date}</p>
                                                            <div class="review-rating">
                                                                <c:set var="a" value="${r.rating}"></c:set>
                                                                <c:set var="b" value="${5 - r.rating}"></c:set>
                                                                <c:forEach varStatus="loop" begin="1" end="${a}" step="1">
                                                                    <i class="fa fa-star"></i>
                                                                </c:forEach> 
                                                                <c:forEach varStatus="loop" begin="1" end="${b}" step="1">
                                                                    <i class="fa fa-star-o empty"></i>
                                                                </c:forEach> 

                                                            </div>
                                                        </div>
                                                        <div class="review-body">
                                                            <p>${r.comment}</p>
                                                        </div>
                                                    </li>
                                                </c:forEach>
                                            </ul>
                                            <% int i=1; %>
                                            <ul class="reviews-pagination">
                                                <c:if test="${param.page>1}"><li class="page-item "><a href="products?id=${requestScope.product.id}&page=${param.page-1}">Pre</a></li></c:if>
                                                    <c:forEach items="${requestScope.rating}" var="o" step="4" >
                                                        <c:set var="i" value="<%=i%>"/>
                                                    <li<c:if test="${param.page eq i}"> class="page-item active"</c:if>>
                                                        <a href="products?id=${requestScope.product.id}&page=<%=i%>" class="page-link"><%=i%></a></li> 
                                                        <% i++;%>
                                                    </c:forEach>
                                                    <c:if test="${fn:length(requestScope.rating)>=(param.page*4+1)}">
                                                    <li class="page-item"><a href="products?id=${requestScope.product.id}&page=${param.page+1}" class="page-link">Next</a></li></c:if>
                                                </ul>
                                            </div>
                                        </div>
                                </c:if>
                                <c:if test="${requestScope.rating eq null}">
                                    <!-- Rating -->
                                    <div class="col-md-3">

                                        <div id="rating">
                                            <div class="rating-avg">
                                                <span>0.0</span>
                                                <div class="rating-stars">
                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                    <i class="fa fa-star-o"></i>
                                                </div>
                                            </div>
                                            <ul class="rating">
                                                <c:forEach var="i" begin="0" end="4">
                                                    <li>
                                                    <div class="rating-stars">
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                        <i class="fa fa-star"></i>
                                                    </div>
                                                    <div class="rating-progress">
                                                        <div ></div>
                                                    </div>
                                                    <span class="sum">0</span>
                                                </li>
                                                </c:forEach>
                                                
                                                
                                                
                                            </ul>
                                        </div>
                                    </div>
                                    <!-- /Rating -->

                                    <!-- Reviews -->
                                    <div class="col-md-6">
                                        <div id="reviews">
                                            <ul class="reviews">
                                                <li><div class="review-heading">
                                                        <h5 class="name"></h5>
                                                        <p class="date"></p>
                                                        <div class="review-rating">

                                                        </div>
                                                    </div>
                                                    <div class="review-body">
                                                        <h3>Be the first person to comment</h3>
                                                    </div>
                                                </li>

                                            </ul>
                                        </div>
                                    </div>
                                </c:if>





                                <!-- /Reviews -->
                                <!-- Review Form -->
                                <div class="col-md-10" style="margin: 50px 0px;">
                                    <div id="review-form">
                                        <form class="review-form" action="rating">
                                            <input type="hidden" name="pid" value="${param.id}">
                                            <input type="hidden" name="aid" value="${sessionScope.account.aid}">
                                            <textarea class="input" name="comment" id="editor1" placeholder="Your Review"></textarea>
                                            <div class="input-rating">
                                                <span>Your Rating: </span>
                                                <div class="stars">
                                                    <input id="star5" name="rating" value="5" type="radio" required><label for="star5"></label>
                                                    <input id="star4" name="rating" value="4" type="radio" required><label for="star4"></label>
                                                    <input id="star3" name="rating" value="3" type="radio" required><label for="star3"></label>
                                                    <input id="star2" name="rating" value="2" type="radio" required><label for="star2"></label>
                                                    <input id="star1" name="rating" value="1" type="radio" required><label for="star1"></label>
                                                </div>
                                            </div>
                                            <c:if test="${sessionScope.account eq null}">
                                                <a href="login?url=products?id=${param.id}">Login to comment</a>    
                                            </c:if>
                                            <c:if test="${sessionScope.account ne null}">
                                                <button type="submit" class="primary-btn">Submit</button>    
                                            </c:if>

                                        </form>
                                    </div>
                                </div>
                                <!-- /Review Form -->

                            </div>
                        </div>
                    </div>





                </div>
                <!-- /row -->
            </div>
            <!-- /container -->
            <!-- slick-tab -->

            <div class="container">
                <div class="col-md-12">
                    <div class="row">
                        <h3 class="title">Products you may also like</h3>

                        <div class="products-tabs">
                            <!-- tab -->
                            <div id="tab1" class="tab-pane active">
                                <div class="products-slick" data-nav="#slick-nav-1">
                                    <!-- product -->
                                    <c:forEach items="${requestScope.products}" var="o">
                                        <div class="product">
                                            <div class="product-img">
                                                <c:set var="msg" value="${o.image}" />                                                
                                                <c:set var="arrayofmsg" value="${fn:split(msg,',')}" />                                             
                                                <c:forEach var="i" begin="0" end="0">
                                                    <img src="${arrayofmsg[i]}" alt="alt"/>
                                                </c:forEach>
                                                <div class="product-label">
                                                    <span class="sale">Sold:${o.sale}</span>
                                                    <span class="new">
                                                        <c:forEach varStatus="loop" begin="1" end="${o.rating}" step="1">
                                                            <i class="fa fa-star"></i>
                                                        </c:forEach> 
                                                        <c:forEach varStatus="loop" begin="1" end="${5-o.rating}" step="1">
                                                            <i class="fa fa-star-o empty"></i>
                                                        </c:forEach></span>

                                                </div>
                                            </div>
                                            <div class="product-body">
                                                <p class="product-category">${o.category.name}</p>
                                                <h3 class="product-name"><a href="#">${o.name}</a></h3>
                                                <h4 class="product-price">${o.price}0$ <del class="product-old-price">${o.price*2}0$</del></h4>

                                                <div class="product-btns">
                                                    <button class="quick-view"><a target="_blank" href="products?id=${o.id}&page=1"><i class="fa fa-eye"></i></a><span class="tooltipp">quick view</span></button>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- /product -->
                                    </c:forEach>

                                </div>
                                <div id="slick-nav-1" class="products-slick-nav"></div>
                            </div>
                            <!-- /tab -->
                        </div>
                    </div>
                </div>
            </div>

            <!-- /slick -->
        </div>
        <!-- /SECTION -->

        <!-- Section -->


        <jsp:include page="header-footer/footer.jsp" />  

        <!-- jQuery Plugins -->
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/slick.min.js"></script>
        <script src="js/nouislider.min.js"></script>
        <script src="js/jquery.zoom.min.js"></script>
        <script src="js/main.js"></script>
        <script >
            CKEDITOR.replace( 'editor1');
        </script>
    </body>
</html>

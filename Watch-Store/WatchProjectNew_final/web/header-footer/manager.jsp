<%-- 
    Document   : header
    Created on : Jun 19, 2023, 3:02:58 PM
    Author     : khiem
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

        <title>Electro - HTML Ecommerce Template</title>

        <!-- Google font -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">

        <!-- Bootstrap -->
        <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css"/>

        <!-- Slick -->
        <link type="text/css" rel="stylesheet" href="css/slick.css"/>
        <link type="text/css" rel="stylesheet" href="css/slick-theme.css"/>

        <!-- nouislider -->
        <link type="text/css" rel="stylesheet" href="css/nouislider.min.css"/>

        <!-- Font Awesome Icon -->
        <link rel="stylesheet" href="css/font-awesome.min.css">

        <!-- Custom stlylesheet -->
        <link type="text/css" rel="stylesheet" href="css/home.css"/>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
            .navbar-nav{
                flex-direction: row;
            }
        </style>
    </head>
    <body>
        <!-- NAVIGATION -->
        <nav id="navigation">
            <!-- container -->
            <div class="container">
                <!-- responsive-nav -->
                <div id="responsive-nav">
                    <!-- NAV -->
                    <ul class="main-nav nav navbar-nav">
                        <c:if test="${param.aid eq null}">
                            <li><a href="home">Home</a></li>
                            <li <%if(request.getRequestURI().contains("crud.jsp")){%> class="active" <%}%> ><a href="crud">Manage Product</a></li>
                            <li <%if(request.getRequestURI().contains("crudcategory.jsp")){%> class="active" <%}%> ><a href="crudcategory">Manage Brand</a></li>
                            <li <%if(request.getRequestURI().contains("accountView.jsp")){%> class="active" <%}%> ><a href="accountmanagement">Manage Account</a></li>
                            <li <%if(request.getRequestURI().contains("TransactionAll.jsp")){%> class="active" <%}%> ><a href="transaction">Manage Transaction</a></li>
                            <li <%if(request.getRequestURI().contains("SaleChart.jsp")){%> class="active" <%}%> ><a href="chart">Statistics Sale Growth</a></li>
                            <li <%if(request.getRequestURI().contains("UserChart.jsp")){%> class="active" <%}%> ><a href="userchart">Statistics Customer</a></li>
                            </c:if>
                        
                            <c:if test="${param.aid ne null}">
                            <li ${param.cid ne null ?'class="active"':''}><a href="accountmanagement">Manage Account</a></li>
                            <li ${param.cid ne null ?'class="active"':''}><a href="crud">Account profile</a></li>
                            </c:if>
                           

                    </ul>
                    <!-- /NAV -->
                </div>
                <!-- /responsive-nav -->
            </div>
            <!-- /container -->
        </nav>
        <!-- /NAVIGATION -->
        <!-- jQuery Plugins -->
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/slick.min.js"></script>
        <script src="js/nouislider.min.js"></script>
        <script src="js/jquery.zoom.min.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>

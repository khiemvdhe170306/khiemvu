<%-- 
    Document   : header
    Created on : Jun 19, 2023, 3:02:58 PM
    Author     : khiem
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

        <title>Electro - HTML Ecommerce Template</title>

        <!-- Google font -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">

        <!-- Bootstrap -->
        <link type="text/css" rel="stylesheet" href="css/bootstrap.min.css"/>

        <!-- Slick -->
        <link type="text/css" rel="stylesheet" href="css/slick.css"/>
        <link type="text/css" rel="stylesheet" href="css/slick-theme.css"/>

        <!-- nouislider -->
        <link type="text/css" rel="stylesheet" href="css/nouislider.min.css"/>

        <!-- Font Awesome Icon -->
        <link rel="stylesheet" href="css/font-awesome.min.css">

        <!-- Custom stlylesheet -->
        <link type="text/css" rel="stylesheet" href="css/home.css"/>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
            .manage{
                color:white;
            }
            .product-img img{
                width: 20px;
                height: 20px;
            }
        </style>
    </head>
    <body>
        <!-- HEADER -->
        <header>
            <!-- TOP HEADER -->
            <div id="top-header">
                <div class="container">
                    <ul class="header-links pull-left">
                        <li><a href="#"><i class="fa fa-phone"></i> +021-95-51-84</a></li>
                        <li><a href="#"><i class="fa fa-envelope-o"></i> email@email.com</a></li>
                        <li><a href="#"><i class="fa fa-map-marker"></i> 1734 Stonecoal Road</a></li>
                    </ul>
                    <ul class="header-links pull-right">
                        <c:if test="${(sessionScope.account==null)}">
                            <%
                                String uri=request.getRequestURI();
                                String query=request.getQueryString();
                                String URL="";
                                if((uri.contains("home.jsp"))||uri==null){
                                    URL+="home";
                                }else if(uri.contains("ProductDetails.jsp")){
                                       
                                        URL+=uri.substring(uri.lastIndexOf("/")+1, uri.length());
                                         URL.replace("ProductDetails.jsp", "products");
                                    }
                                else if(!uri.contains("home.jsp") &&query==null){

                                URL+=uri.substring(uri.lastIndexOf("/")+1, uri.length());
                                }else{
                                    URL+=uri.substring(uri.lastIndexOf("/")+1, uri.length())+"?"+query;}
                                
                            %>
                             
                             <li><a href="login?url=<%=URL.replaceAll("&", ";")%>${requestScope.product.id eq null?'':'&pid='}${param.id}">Login</a></li>
                            <li><a href="signup">Register</a></li>
                            </c:if>
                            <c:if test="${(sessionScope.account!=null)}">
                                <%
                                    String uri=request.getRequestURI();
                                String query=request.getQueryString();
                                String URL="";
                                if((uri.contains("home.jsp"))||uri==null){
                                    URL+="home";
                                }else if(uri.contains("ProductDetails.jsp")){
                                       
                                        URL+=uri.substring(uri.lastIndexOf("/")+1, uri.length());
                                         URL.replace("ProductDetails.jsp", "products");
                                    }
                                else if(!uri.contains("home.jsp") &&query==null){

                                URL+=uri.substring(uri.lastIndexOf("/")+1, uri.length());
                                }else{
                                    URL+=uri.substring(uri.lastIndexOf("/")+1, uri.length())+"?"+query;}
                                
                                %>
                            
                            <li><a href="logout?url=<%=URL.replaceAll("&", ";")%>${requestScope.product.id eq null?'':'&pid='}${param.id}">Logout</a></li>
                            <li><a target="_blank" href="profile"><c:if test="${sessionScope.accountimage eq null}"><img src="img/face.png" width="20" height="20" style="border-radius: 100px;"></c:if><c:if test="${sessionScope.accountimage ne null}"><img src="data:image/jpg;base64,${sessionScope.accountimage}" data-index="0" width="20" height="20" style="border-radius: 100px;"> </c:if>My Account</a></li>
                            </c:if>

                    </ul>
                </div>
            </div>
            <!-- /TOP HEADER -->

            <!-- MAIN HEADER -->
            <div id="header">
                <!-- container -->
                <div class="container">
                    <!-- row -->
                    <div class="row">
                        <!-- LOGO -->
                        <div class="col-md-3">
                            <div class="header-logo">
                                <a href="home" class="logo">
                                    <img src="./img/logo.png" alt="">
                                </a>
                            </div>
                        </div>
                        <!-- /LOGO -->

                        <!-- SEARCH BAR -->
                        <div class="col-md-6">
                            <div class="header-search">
                                <form action="search">
                                    <input type="hidden" name="cid" value="0">
                                    <input class="input" placeholder="Search here" name="search">
                                    <input type="hidden" name="page" value="1">
                                    <button class="search-btn" >Search</button>
                                </form>
                            </div>
                        </div>
                        <!-- /SEARCH BAR -->

                        <!-- ACCOUNT -->
                        <div class="col-md-3 clearfix">
                            <div class="header-ctn">
                                <c:if test="${sessionScope.account.role eq 2}">
                                    <a href="crud" class="manage">
                                        <i class="fa fa-shopping-bag"></i>
                                        <span>Mangager</span>
                                    </a>
                                </c:if>
                                <!-- Wishlist -->
                                

                                    <!-- Cart -->
                                    <div class="dropdown">

                                        <a class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                                            <i class="fa fa-shopping-cart"></i>

                                            <span>Your Cart</span>
                                            <div class="qty">${fn:length(sessionScope.cartList)}</div>
                                        </a>
                                        <div class="cart-dropdown">
                                            <div class="cart-list">
                                                 <c:set var="total" value="${0}"/>
                                                <c:forEach items="${sessionScope.cartList}" var="o">
                                                    <div class="product-widget">
                                                        <c:set var="msg" value="${o.image}" />                                                
                                                        <c:set var="arrayofmsg" value="${fn:split(msg,',')}" />   
                                                       
                                                        <c:set var="i" value="${0}"/>
                                                         <c:set var="total" value="${total + o.price*o.number}" />
                                                        <img src="${arrayofmsg[i]}" alt="" width="100" height="100">
                                                        <div class="product-body">
                                                            <h3 class="product-name"><a target="_blank" href="products?id=${o.pid}&page=1">${o.name}</a></h3>
                                                            <h4 class="product-price"><span class="qty">${o.number}x</span>$${o.price}</h4>
                                                        </div>
                                                    </div>
                                                </c:forEach>
                                            </div>
                                            <div class="cart-summary">
                                                <small>${fn:length(sessionScope.cartList)} Item(s) selected</small>
                                                <h5>SUBTOTAL: $${total}</h5>
                                            </div>
                                            <div class="cart-btns">
                                                <a target="_blank" href="cartUpload">View Cart</a>
                                                <a target="_blank" href="cartUpload">Checkout  <i class="fa fa-arrow-circle-right"></i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /Cart -->
                              
                                <!-- Menu Toogle -->
                                <div class="menu-toggle">
                                    <a href="#">
                                        <i class="fa fa-bars"></i>
                                        <span>Menu</span>
                                    </a>
                                </div>
                                <!-- /Menu Toogle -->
                            </div>
                        </div>
                        <!-- /ACCOUNT -->
                    </div>
                    <!-- row -->
                </div>
                <!-- container -->
            </div>
            <!-- /MAIN HEADER -->
        </header>
        <!-- /HEADER -->
        <!-- jQuery Plugins -->
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/slick.min.js"></script>
        <script src="js/nouislider.min.js"></script>
        <script src="js/jquery.zoom.min.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>

<%-- 
    Document   : Slider
    Created on : 21-09-2023, 08:36:40
    Author     : admin
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<!DOCTYPE html>
<!-- Coding By CodingNepal - www.codingnepalweb.com -->
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Slider</title>
        <!-- Google Fonts Link For Icons -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Material+Symbols+Rounded:opsz,wght,FILL,GRAD@48,400,0,0" />
        <style>
            * {
                margin: 0;
                padding: 0;
                box-sizing: border-box;
            }

            body {
                display: flex;
                align-items: center;
                justify-content: center;
                min-height: 100vh;
                background: #f1f4fd;
            }

            .container {
                max-width: 1200px;
                width: 95%;
            }

            .slider-wrapper {
                position: relative;
            }

            .slider-wrapper .slide-button {
                position: absolute;
                top: 50%;
                outline: none;
                border: none;
                height: 50px;
                width: 50px;
                z-index: 5;
                color: #fff;
                display: flex;
                cursor: pointer;
                font-size: 2.2rem;
                background: #000;
                align-items: center;
                justify-content: center;
                border-radius: 50%;
                transform: translateY(-50%);
            }

            .slider-wrapper .slide-button:hover {
                background: #404040;
            }

            .slider-wrapper .slide-button#prev-slide {
                left: -25px;
                display: none;
            }

            .slider-wrapper .slide-button#next-slide {
                right: -25px;
            }

            .slider-wrapper .image-list {
                display: grid;
                grid-template-columns: repeat(10, 1fr);
                gap: 18px;
                font-size: 0;
                list-style: none;
                margin-bottom: 30px;
                overflow-x: auto;
                scrollbar-width: none;
            }

            .slider-wrapper .image-list::-webkit-scrollbar {
                display: none;
            }

            .slider-wrapper .image-list .image-item {
                width: 325px;
                height: 400px;
                object-fit: cover;
            }

            .container .slider-scrollbar {
                height: 24px;
                width: 100%;
                display: flex;
                align-items: center;
            }

            .slider-scrollbar .scrollbar-track {
                background: #ccc;
                width: 100%;
                height: 2px;
                display: flex;
                align-items: center;
                border-radius: 4px;
                position: relative;
            }

            .slider-scrollbar:hover .scrollbar-track {
                height: 4px;
            }

            .slider-scrollbar .scrollbar-thumb {
                position: absolute;
                background: #000;
                top: 0;
                bottom: 0;
                width: 50%;
                height: 100%;
                cursor: grab;
                border-radius: inherit;
            }

            .slider-scrollbar .scrollbar-thumb:active {
                cursor: grabbing;
                height: 8px;
                top: -2px;
            }

            .slider-scrollbar .scrollbar-thumb::after {
                content: "";
                position: absolute;
                left: 0;
                right: 0;
                top: -10px;
                bottom: -10px;
            }

            /* Styles for mobile and tablets */
            @media only screen and (max-width: 1023px) {
                .slider-wrapper .slide-button {
                    display: none !important;
                }

                .slider-wrapper .image-list {
                    gap: 10px;
                    margin-bottom: 15px;
                    scroll-snap-type: x mandatory;
                }

                .slider-wrapper .image-list .image-item {
                    width: 280px;
                    height: 380px;
                }

                .slider-scrollbar .scrollbar-thumb {
                    width: 20%;
                }
            }
        </style>
        <script>
            const initSlider = () => { // khoi tao
                const imageList = document.querySelector(".slider-wrapper .image-list");
                const slideButtons = document.querySelectorAll(".slider-wrapper .slide-button");
                const sliderScrollbar = document.querySelector(".container .slider-scrollbar");
                const scrollbarThumb = sliderScrollbar.querySelector(".scrollbar-thumb");
                const maxScrollLeft = imageList.scrollWidth - imageList.clientWidth;

                scrollbarThumb.addEventListener("mousedown", (e) => { // keo thanh truot
                    const startX = e.clientX; // bat dau keo
                    const thumbPosition = scrollbarThumb.offsetLeft; // vi tri ban dau thanh truot
                    const maxThumbPosition = sliderScrollbar.getBoundingClientRect().width - scrollbarThumb.offsetWidth; // max 

                    const handleMouseMove = (e) => { // di chuot de chuyen
                        const deltaX = e.clientX - startX; // vi tri bat dau keo
                        const newThumbPosition = thumbPosition + deltaX; // vi tri moi


                        const boundedPosition = Math.max(0, Math.min(maxThumbPosition, newThumbPosition)); // ko < 0 va ko > max
                        const scrollPosition = (boundedPosition / maxThumbPosition) * maxScrollLeft; // ko vuọt qua max

                        scrollbarThumb.style.left = `${boundedPosition}px`; // cap nhat vi tri
                        imageList.scrollLeft = scrollPosition; // vi tri cuon cua slider
                    }


                    const handleMouseUp = () => {
                        document.removeEventListener("mousemove", handleMouseMove);
                        document.removeEventListener("mouseup", handleMouseUp);
                    }


                    document.addEventListener("mousemove", handleMouseMove);
                    document.addEventListener("mouseup", handleMouseUp);
                });


                slideButtons.forEach(button => { //nhan nut truot
                    button.addEventListener("click", () => {
                        const direction = button.id === "prev-slide" ? -1 : 1; // xd hướng cuộn
                        const scrollAmount = imageList.clientWidth * direction; 
                        imageList.scrollBy({left: scrollAmount, behavior: "smooth"});// cuộn hình ảnh
                    });
                });


                const handleSlideButtons = () => { // hien thi nut chuyển
                    slideButtons[0].style.display = imageList.scrollLeft <= 0 ? "none" : "flex";
                    slideButtons[1].style.display = imageList.scrollLeft >= maxScrollLeft ? "none" : "flex";
                }


                const updateScrollThumbPosition = () => { //cap nhat vi tri truot
                    const scrollPosition = imageList.scrollLeft;
                    const thumbPosition = (scrollPosition / maxScrollLeft) * (sliderScrollbar.clientWidth - scrollbarThumb.offsetWidth);
                    scrollbarThumb.style.left = `${thumbPosition}px`;
                }


                imageList.addEventListener("scroll", () => {
                    updateScrollThumbPosition();
                    handleSlideButtons();
                });
            }

            window.addEventListener("resize", initSlider);
            window.addEventListener("load", initSlider);
        </script>
        <style>
            img{
                max-height: 300px;
                max-width: 350px;
            }
        </style>
    </head>
    <body>
        
        <div class="container">
            <div class="slider-wrapper">
                <button id="prev-slide" class="slide-button material-symbols-rounded">
                    chevron_left
                </button>
                <ul class="image-list">
                   
                    <c:forEach items="${sliders}" var="s">     
                    <a href="slider-list"><img class="image-item" src="${s.image}" alt="slider image" /></a>
                    </c:forEach>
                     
                </ul>
                <button id="next-slide" class="slide-button material-symbols-rounded">
                </button>
            </div>
            <div class="slider-scrollbar">
                <div class="scrollbar-track">
                    <div class="scrollbar-thumb"></div>
                </div>
            </div>
        </div>
    </body>
</html>
<%-- 
    Document   : subHome
    Created on : 21-09-2023, 09:43:08
    Author     : admin
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <style>
            .brifInfor{
                white-space: nowrap;
                overflow: hidden;
                text-overflow: ellipsis;
                max-width: 100%;
            }
        </style>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Quikk</title>
        <link rel="icon" href="images/Quikk.png" type="image/png">
    </head>
    <body>
        <div id="page" class="container-fluid">
            <jsp:include page="Common/Header.jsp"></jsp:include>
                <div>
                    <div>
                        <div style="text-align: center">
                            <a href="${pageContext.request.contextPath}/" class="m-3 btn btn-primary" role="button">Learn more</a>
                    </div>
                </div>


                <div style="margin-top: 500px">
                    <jsp:include page="Slider.jsp"></jsp:include>
                    </div> 
                    <div class="container">
                       <h3 class="text-uppercase text-center" style="font-size: 2em; font-weight: bold; color: #333;">Course</h3>


                        <div class="row">
                        <c:forEach items="${requestScope.course}" var="C" varStatus="loop">
                            <c:if test="${loop.index < 3}">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 mb-4" style="text-align: center">
                                    <div class="w3-card-4 w3-margin w3-white">
                                        <div class="w3-container">
                                            <h5><span class="w3-opacity"></span></h5>
                                            <div class="w3-row">
                                                <div class="w3-col m6 s12">
                                                    <img src="${C.thumbnail}" alt="Nature" style="width: 300px; height: 200px;">
                                                </div>
                                                <div class="w3-col m6 s12">
                                                    <h4><b>${C.courseName}</b></h4>
                                                    <div class="w3-col m8 s12">
                                                        <a href="coursedetail?courseId=${C.courseId}" class="button">Read More</a>
                                                    </div> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </c:if>
                        </c:forEach>

                    </div>
                </div>

                <div class="container">
                    <h3 class="text-uppercase text-center" style="font-size: 2em; font-weight: bold; color: #333;">Blog</h3>


                    <div class="row">
                        <c:forEach items="${requestScope.Post}" var="P" varStatus="loop">
                            <c:if test="${loop.index < 3}">
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 mb-4" style="text-align: center">
                                    <div class="w3-card-4 w3-margin w3-white">
                                        <div class="w3-container">
                                            <h5><span class="w3-opacity"></span></h5>
                                            <div class="w3-row">
                                                <div class="w3-col m6 s12">
                                                    <img src="${P.thumbnail}" alt="Nature" style="width: 300px; height: 200px;">
                                                </div>
                                                <div class="w3-col m6 s12">
                                                    <h4><b>${P.title}</b></h4>
                                                    <p class="brifInfor">${P.brifInfor}.</p>
                                                    <div class="w3-row">
                                                        <div class="w3-col m8 s12">
                                                            <a href="blogdetail?postId=${P.postId}" class="button">Read More</a>
                                                        </div>
                                                        <div class="w3-col m4 w3-hide-small">
                                                            <p><span class="w3-padding-large w3-right"><b>Comments  </b> <span class="w3-tag">0</span></span></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </c:if>
                        </c:forEach>

                    </div>
                </div>

            </div>
            <jsp:include page="Common/Footer.jsp"></jsp:include>
            </div>

        <jsp:include page="Common/Sidebar.jsp"></jsp:include>



    </body>

</html>

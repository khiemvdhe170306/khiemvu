<%-- 
    Document   : Achievement
    Created on : Oct 24, 2023, 3:12:22 PM
    Author     : arans
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Achivement</title>
        <link rel="icon" href="images/Quikk.png" type="image/png">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <!-- Boxicons -->
        <link href='https://unpkg.com/boxicons@2.0.9/css/boxicons.min.css' rel='stylesheet'>

        <!-- My CSS -->
        <link rel="stylesheet" href="css/userLevel.css">
        <style>

            .todo-list progress {
                width: 100%;
                height: 15px;
                appearance: none;
            }

            .todo-list progress::-webkit-progress-value {
                background: #3C91E6;
                border-radius: 5px;
            }
            .xp-info {
                text-align: center;
            }
            .level-progress {
                display: flex;
                align-items: center;
                justify-content: space-between;
                width: 100%;
            }

            .level-progress p {
                margin: 0;
                white-space: nowrap; /* Prevent text from wrapping */
                flex: 1; /* Allow the text to take up available space */
            }

            .level-progress progress {
                flex: 4; /* Allow the progress bar to take up more space */
            }
            .course-container {
                padding-top: 50px;
                width: 80%;
                margin: 0 auto;
                text-align: center;
                font-size: 18px;
            }

            .chapter-list {
                list-style: none;
                padding: 0;
                margin: 10px 0;
                text-align: center; /* Align text to center */
            }

            .chapter-list li {
                margin: 10px 0;
            }

            .quiz-progress {
                display: flex;
                justify-content: center; /* Aligns the quiz items to the center */
                align-items: center;
                gap: 20px;
                margin-top: 20px;
                width: 80%; /* Adjust width if needed */
                margin: 0 auto; /* To center the container itself */
            }

            .quiz-circle {
                width: 40px;
                height: 40px;
                border-radius: 50%;
                background-color: #ccc;
                display: inline-block;
            }

            .completed {
                background-color: green; /* Change the color for completed quizzes */
            }
            .quiz-details-content {
                display: flex;
                flex-direction: row;
                justify-content: space-around;
            }

            .quiz-list {
                padding: 0;
                margin: 0;
            }


            .quiz-item h3 {
                margin: 0;
                font-size: 18px;
            }

            .quiz-info {
                display: flex;
                align-items: center;
                justify-content: space-between;
                margin-top: 10px;
            }

            .score {
                flex: 1; /* Score on the left */
            }

            .review {
                flex: 1; /* Review on the right */
                display: flex;
                justify-content: flex-end;
            }

            .review button {
                padding: 5px 10px;
                background-color: #3498db;
                color: #fff;
                border: none;
                border-radius: 4px;
            }
            .quiz-details {
                text-align: center;
            }

            .quiz-details .quiz-name {
                text-align: center;
            }

            .quiz-item {
                display: flex;
                justify-content: space-between;
                align-items: center;
                width: 50%; /* You can adjust the width as needed */
                text-align: left;
            }
            img {
                border: 1px solid #ddd;
                border-radius: 50px;
                padding: 5px;
                width: 150px;

            }

        </style>
        <title>User Hub</title>
    </head>
    <body onload="functionToCall()">

        <c:if test="${sessionScope.account != null}">
            <!-- SIDEBAR -->
            <section id="sidebar">
                <a href="#" class="brand">
                    <i class='bx bxs-smile'></i>
                    <span class="text">User Hub</span>
                </a>

                <ul class="side-menu top">
                    <li class="active">
                        <a href="userdashboard?">
                            <i class='bx bxs-dashboard' ></i>
                            <span class="text">Dashboard</span>
                        </a>
                    </li>
                    <li>
                        <a href="home">
                            <i class='bx bxs-shopping-bag-alt' ></i>
                            <span class="text">Home</span>
                        </a>
                    </li>
                    <c:if test = "${fn:contains(sessionScope.account.roleName.access, 'membership')}">
                        <li>
                            <a href="salechart">
                                <i class='bx bxs-doughnut-chart' ></i>
                                <span class="text">Sale Chart</span>
                            </a>
                        </li>
                    </c:if>
                    <c:if test = "${fn:contains(sessionScope.account.roleName.access, 'account')}">
                        <li >
                            <a href="userchart">
                                <i class='bx bxs-message-dots' ></i>
                                <span class="text">User Chart</span>
                            </a>
                        </li>
                    </c:if>
                    <c:if test = "${fn:contains(sessionScope.account.roleName.access, 'quiz')}">
                        <li >
                            <a href="ChartQuiz.jsp">
                                <i class='bx bxs-message-dots' ></i>
                                <span class="text">Website Chart</span>
                            </a>
                        </li>
                    </c:if>

                </ul>
            </section>
            <!-- SIDEBAR -->



            <!-- CONTENT -->
            <section id="content">
                <!-- NAVBAR -->
                <nav>
                    <i class='bx bx-menu' ></i>
                    <a href="#" class="nav-link">Categories</a>
                    <input type="checkbox" id="switch-mode" hidden>
                    <label for="switch-mode" class="switch-mode"></label>

                </nav>
                <!-- NAVBAR -->

                <!-- MAIN -->
                <main>
                    <div class="head-title">
                        <div class="left">
                            <h1>Course Enrolled</h1>
                        </div>
                    </div>
                    <h1 id="demo"></h1>
                    <c:forEach items="${sessionScope.account.course}" var="c">
                        <div class="table-data">
                            <div class="order">
                                <div class="head" id="courseHeader">
                                    <h3>${c.courseName} <i class="bx bx-chevron-down" id="dropdownIcon" onclick="courseShow(${c.courseId})"></i></h3>

                                </div>
                                <div class="btn btn-success">
                                    Complete the course to get Voucher 50%
                                </div>
                                    <br>
                                ${c.description}
                                <div id="courseContainer${c.courseId}">
                                    <!-- Existing HTML Code -->
                                    <div class="course-container" id="courseContainer" style="padding-bottom: 130px; cursor: pointer;" >

                                        <div class="quiz-progress horizontal" >
                                            <% int quizcount = 1; %>
                                            <c:forEach items="${requestScope.quizByCourse[c.courseId]}" var="qbc">
                                                <c:choose>
                                                    <c:when test="${qbc.score >= 7}">
                                                        <li class= "btn quiz-circle completed" onclick="Show(${qbc.quizId})" value="${qbc.quizId}"> <%= quizcount++ %>
                                                            <br><br>

                                                            <div class="btn" id="Quiz${qbc.quizId}" style="display: none;position: relative;right:450px">
                                                                <div class="btn" style="position: relative; right:30px">
                                                                    <img src="${qbc.image}" alt="alt"/> ${qbc.title}
                                                                </div>
                                                                <a class="btn" href="quizattempt?quizId=${qbc.quizId}" style="position: relative; left: 600px;padding: 5px 10px;background-color: #3498db;color: #fff;border: none;border-radius: 4px;">
                                                                    Review
                                                                </a>
                                                                <div class="btn" style="position: relative; left: 400px; top:50px">
                                                                    Score: ${qbc.score} / Total Attempts: ${qbc.totalAttempts}
                                                                </div>
                                                            </div>

                                                        </li>

                                                    </c:when>
                                                    <c:otherwise>
                                                        <li class="btn quiz-circle" onclick="Show(${qbc.quizId})" value="${qbc.quizId}"><%= quizcount++ %>
                                                            <br><br>
                                                            <div class="btn" id="Quiz${qbc.quizId}" style="display: none;position: relative;right:450px">
                                                                <div class="btn" style="position: relative; right:30px">
                                                                    <img src="${qbc.image}" alt="alt"/> ${qbc.title}
                                                                </div>
                                                                <a class="btn" href="quizattempt?quizId=${qbc.quizId}" style="position: relative; left: 600px;padding: 5px 10px;background-color: #3498db;color: #fff;border: none;border-radius: 4px;">
                                                                    Review
                                                                </a>
                                                                <div class="btn" style="position: relative; left: 400px; top:50px">
                                                                    Score: ${qbc.score} / Total Attempts: ${qbc.totalAttempts}
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </c:otherwise>

                                                </c:choose>
                                            </c:forEach>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </main>
                <!-- MAIN -->
            </section>
            <!-- CONTENT -->
        </c:if>


        <!--        <script src="js/userLevel.js"></script>-->

        
        <script>
            function changePadding(element) {
                if (element.style.paddingBottom === '30px') {
                    element.style.paddingBottom = '70px';
                } else {
                    element.style.paddingBottom = '30px';
                }
            }

            function Show(i) {
                functionToCall2();
                var z = document.getElementById("Quiz" + i);
                if (z.style.display === "none") {
                    z.style.display = "block";
                    z.style.paddingBottom = '100px';
                } else {
                    z.style.display = "none";
                }
                //functionToCall2();
            }
            function functionToCall2() {
                for (let ai = 1; ai < 16; ai++) {
                    var z = document.getElementById("Quiz" + ai);
                    if (z != null)
                        z.style.display = "none";
                }
            }
            function courseShow(i) {
                var z = document.getElementById("courseContainer" + i);
                if (z.style.display === "none") {
                    z.style.display = "block";
                } else {
                    z.style.display = "none";
                }
            }

            function functionToCall() {
                for (let ai = 0; ai < 30; ai++) {
                    var z = document.getElementById("courseContainer" + ai);
                    if (z != null)
                        z.style.display = "none";
                }
            }
//            document.addEventListener("DOMContentLoaded", function () {
//                const courseHeaders = document.querySelectorAll(".head");
//
//                courseHeaders.forEach(header => {
//                    const courseContainer = header.nextElementSibling;
//                    const dropdownIcon = header.querySelector("i");
//
//                    header.addEventListener("click", function () {
//                        if (courseContainer.style.display === "none") {
//                            courseContainer.style.display = "block";
//                            dropdownIcon.classList.remove("bx-chevron-down");
//                            dropdownIcon.classList.add("bx-chevron-up");
//                        } else {
//                            courseContainer.style.display = "none";
//                            dropdownIcon.classList.remove("bx-chevron-up");
//                            dropdownIcon.classList.add("bx-chevron-down");
//                        }
//                    });
//                });
//            });
        </script>
    </body>
</html>

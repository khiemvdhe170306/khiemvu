<%-- 
    Document   : lesson
    Created on : Sep 16, 2023, 8:49:08 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Freebies - Free Resource Sharing Bootstrap 5 HTML CSS Website Template</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="format-detection" content="telephone=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="author" content="">
        <meta name="keywords" content="">
        <meta name="description" content="">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
        <link id="theme" rel="stylesheet" type="text/css" href="css/greyson-theme.css">
        <link rel="stylesheet" type="text/css" href="css/style.css">
        <link rel="stylesheet" type="text/css" href="fonts/icomoon/icomoon.css">

        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Mulish:ital@0;1&family=Oswald:wght@500&display=swap" rel="stylesheet">
    </head>
    <body>

        <div class="pattern-bg"></div>
        <jsp:include page="Common/Header.jsp"></jsp:include>
<!--            <header id="header">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <div class="container-fluid">

                        <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#offcanvasNavbar"
                                aria-controls="offcanvasNavbar">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="offcanvas offcanvas-end" tabindex="-1" id="offcanvasNavbar"
                             aria-labelledby="offcanvasNavbarLabel">
                            <div class="offcanvas-header">
                                <h5 class="offcanvas-title" id="offcanvasNavbarLabel">Offcanvas</h5>
                                <button type="button" class="btn-close text-reset" data-bs-dismiss="offcanvas" aria-label="Close"></button>
                            </div>
                            <div class="offcanvas-body">
                                <ul class="navbar-nav justify-content-end fw-bold fs-6 text-uppercase flex-grow-1 pe-3 gap-3">
                                    <li class="nav-item">
                                        <a class="nav-link active" aria-current="page" href="index.html">Home</a>
                                    </li>

                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle" href="#" id="dropdownId" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Pages</a>
                                        <div class="dropdown-menu" aria-labelledby="dropdownId">
                                            <a class="dropdown-item" href="blog.html">Blog</a>
                                            <a class="dropdown-item" href="single-blog.html">Blog Single</a>
                                        </div>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">Blog</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">Contact</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </nav>
            </header>-->

            <section id="billboard" class="col-md-12 mt-5">
                <div class="container">
                    <div class="row">
                        <div class="text-content col-md-6 py-5">
                            <h1 class="display-4 fw-bold">OUR LESSONS</h1>

                            <form action="searchlesson" method="get" class="searchbar d-flex mt-5">
                                <input class="form-control form-control-lg rounded-0 rounded-start" type="text" name="txt" placeholder="What are you looking for?">
                                <button class="btn btn-primary rounded-0 rounded-end px-5" type="submit"><i class="icon icon-search"></i><span class="text-uppercase ps-2">search</span></button>
                            </form>
                        </div>	

                    </div>
                </div><!--container-->
            </section><!--billboard-->



            <section class="my-5">
                <div class="container">
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div class="tab-pane active" id="ecommerce" role="tabpanel" aria-labelledby="ecommerce-tab">
                            <div class="row row-cols-1 row-cols-md-3 g-5 mt-2">
                            <c:forEach items="${requestScope.lesson}" var="c" begin="${requestScope.page*3-3}" end="${requestScope.page*3-1}">
                                <div class="col">
                                    <a href="lessondetail?lessonId=${c.lessonId}"><img src="${c.image}" alt="" class="img-fluid"></a>
                                    <h6 class="my-2"><a href="lessondetail?lessonId=${c.lessonId}" class="text-dark text-decoration-none">${c.lessonName}</a></h6>

                                </div>
                            </c:forEach>

                        </div><!--row-->
                    </div>

                </div>
            </div>
        </section>

        <section class="latest-blogs py-5">
            <div class="container">
                <div class="row mb-2">
<!--                    <div class="col-md-6">
                        <h2>Our Blog</h2>
                        <p>Get latest news and updates about design Industry</p>
                    </div>-->

                    <div class="col-md-6 text-end">
                        <% int i=1; %>
                        <c:forEach items="${requestScope.lesson}" step="3">
                            <c:set var="i" value="<%=i%>"/>
                            <a href="lesson?page=<%=i%>&" class="btn btn-primary"><%=i%></a>
                            </li> 
                            <% i++;%>
                        </c:forEach>

                    </div>
                </div>
<!--                <div class="row">

                    <div class="col-md-4 post-item">
                        <figure>
                            <a href="#"><img src="images/postitem3.jpg" alt="postitem" class="img-fluid"></a>
                        </figure>
                        <div class="content">
                            <div class="category colored">
                                <a href="#" class="text-decoration-none">business</a>
                            </div>
                            <h4><a href="#" class="fw-bold text-decoration-none text-dark">Darkness they're, void great man one man from blessed.</a></h4>
                        </div>
                    </div>post-item
                </div>-->
            </div>
        </section>	
        <jsp:include page="Common/Sidebar.jsp"></jsp:include>
            <footer id="footer" class="mt-5">
            <jsp:include page="Common/Footer.jsp"></jsp:include>
        </footer>

        <script src="js/jquery-1.11.0.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe" crossorigin="anonymous"></script>
    </body>
</html>

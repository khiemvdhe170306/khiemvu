<%-- 
    Document   : home
    Created on : Sep 8, 2023, 2:13:16 PM
    Author     : Acer
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <title>Courses</title>
        <meta charset="utf-8">
        <link rel="icon" href="images/Quikk.png" type="image/png">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="format-detection" content="telephone=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="author" content="">
        <meta name="keywords" content="">
        <meta name="description" content="">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
        <link id="theme" rel="stylesheet" type="text/css" href="css/greyson-theme.css">
        <link rel="stylesheet" type="text/css" href="fonts/icomoon/icomoon.css">

        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Mulish:ital@0;1&family=Oswald:wght@500&display=swap" rel="stylesheet">
        <style>
            .notify-badge{
                position: absolute;
                left:20px;
                top:10px;
                background:red;
                text-align: center;
                border-radius: 30px 30px 30px 30px;
                color:white;
                padding:5px 10px;
                font-size:20px;
            }
        </style>
    </head>
    <body style="font-family: var(--poppins);">

        <div class="pattern-bg"></div>
        <jsp:include page="Common/Header.jsp"></jsp:include>



            <div style="display: flex;align-items: center;justify-content:  center;" class="row">
                <div class="text-content col-md-6 py-5">
                    <form action="searchcourse" method="get" class="searchbar d-flex mt-5">
                        <input class="form-control form-control-lg rounded-0 rounded-start" type="text" name="txt" placeholder="What are you looking for?">
                        <button class="btn btn-primary rounded-0 rounded-end px-5" type="submit"><i class="icon icon-search"></i><span class="text-uppercase ps-2">search</span></button>
                    </form>
                </div>	

            </div>
            <section class="my-5">
                <div class="container">
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item"><a style="font-family: var(--poppins);" class="nav-link fw-bold text-uppercase <c:if test="${param.subject eq 0 || param.subject eq null}">active</c:if>" id="All Subjects" href="courselist?subject=0">All Subjects</a></li>
                        <c:forEach items="${requestScope.subject}" var="s">
                        <li class="nav-item"><a class="nav-link fw-bold text- <c:if test="${param.subject eq s.subjectId}">active</c:if>" id="${s.title}" href="courselist?subject=${s.subjectId}">${s.title}</a></li>
                        </c:forEach> 
                </ul>

                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="tab-pane active" id="All-Subjects" role="tabpanel" aria-labelledby="All-Subjects-tab">
                        <div class="row row-cols-1 row-cols-md-3 g-5 mt-2">
                            <c:forEach items="${requestScope.courses}" var="c" begin="${requestScope.page*3-3}" end="${requestScope.page*3-1}">
                                <c:if test="${c.status eq true}">
                                    <div class="col">
                                        <a href="coursedetail?courseId=${c.courseId}&page=1"><c:if test="${c.level eq true}"><span class="notify-badge">VIP</span></c:if><img style="height: 200px" src="${c.thumbnail}" alt="" class="img-fluid"></a>
                                        <h6 class="my-2"><a href="coursedetail?courseId=${c.courseId}&page=1" class="text-dark text-decoration-none">${c.courseName}</a></h6>
                                    </div>
                                </c:if>
                            </c:forEach>
                        </div>
                    </div>                   

                </div>

            </div>
            <div class="col-md-6 text-end">
                <% int i=1; %>
                <c:forEach items="${requestScope.courses}" step="3">
                    <c:set var="i" value="<%=i%>"/>
                    <a href="courselist?page=<%=i%>&" class="btn btn-primary"><%=i%></a>
                    </li> 
                    <% i++;%>
                </c:forEach>

            </div>
        </section>

        <section class="latest-blogs py-5">
            <div class="container">
                <div class="row mb-2">
                    <div class="col-md-6">
                        <h2>Discover Our Blogs</h2>
                        <p>Get latest news and updates about our courses</p>
                    </div>


                </div>

                <div class="row">
                    <c:forEach items="${requestScope.post}" var = "c" begin="0" end="2">
                        <div class="col-md-4 post-item">
                            <figure>
                                <a href="blogdetail?postId=${c.postId}"><img src="${c.thumbnail}" alt="postitem" class="img-fluid"></a>
                            </figure>
                            <div class="content">
                                <div class="category colored">

                                </div>
                                <h4><a href="blogdetail?postId=${c.postId}" class="fw-bold text-decoration-none text-dark">${c.brifInfor}</a></h4>
                            </div>
                        </div><!--post-item-->
                    </c:forEach>
                </div>

            </div>
        </section>	
        <jsp:include page="Common/Sidebar.jsp"></jsp:include>
            <footer id="footer" class="mt-5">
            <jsp:include page="Common/Footer.jsp"></jsp:include>
        </footer>
        <script>
            var navLinks = document.querySelectorAll('.nav-link');

            navLinks.forEach(function (navLink) {
                navLink.addEventListener('click', function (event) {
                    // Remove the 'active' class from all nav links
                    navLinks.forEach(function (link) {
                        link.classList.remove('active');
                    });

                    // Add the 'active' class to the clicked nav link
                    navLink.classList.add('active');
                });
            });
        </script>

        <script src="js/jquery-1.11.0.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ENjdO4Dr2bkBIFxQpeoTz1HIcje39Wm4jDKdf19U8gI4ddQ3GYNS7NTKfAdVQSZe" crossorigin="anonymous"></script>
    </body>
</html>

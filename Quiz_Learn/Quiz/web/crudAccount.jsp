<%-- 
    Document   : crudAccount
    Created on : Sep 10, 2023, 6:11:07 PM
    Author     : khiem
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">


        <title>Manage Account</title>
        <link rel="icon" href="images/Quikk.png" type="image/png">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" rel="stylesheet">
        <style type="text/css">
            body{
                margin-top:20px;
                color: #1a202c;
                text-align: left;
                background-color: #e2e8f0;
            }
            .main-body {
                padding: 15px;
            }
            .card {
                box-shadow: 0 1px 3px 0 rgba(0,0,0,.1), 0 1px 2px 0 rgba(0,0,0,.06);
            }

            .card {
                position: relative;
                display: flex;
                flex-direction: column;
                min-width: 0;
                word-wrap: break-word;
                background-color: #fff;
                background-clip: border-box;
                border: 0 solid rgba(0,0,0,.125);
                border-radius: .25rem;
            }

            .card-body {
                flex: 1 1 auto;
                min-height: 1px;
                padding: 1rem;
            }

            .gutters-sm {
                margin-right: -8px;
                margin-left: -8px;
            }

            .gutters-sm>.col, .gutters-sm>[class*=col-] {
                padding-right: 8px;
                padding-left: 8px;
            }
            .mb-3, .my-3 {
                margin-bottom: 1rem!important;
            }

            .bg-gray-300 {
                background-color: #e2e8f0;
            }
            .h-100 {
                height: 100%!important;
            }
            .shadow-none {
                box-shadow: none!important;
            }
            .notify-badge{
                position: absolute;
                left:20px;
                top:10px;
                background-image: url('images/wing.png');
                background-size: 33px 25px;
                text-align: center;
                border-radius: 30px 30px 30px 30px;
                color:gold;
                padding:5px 10px;
                font-size:20px;
            }

        </style>
    </head>
    <body>
        <div class="container">
            <div class="main-body">

                <nav aria-label="breadcrumb" class="main-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="home">Home</a></li>
                        <li class="breadcrumb-item"><a href="accountmanagement">User</a></li>
                        <li class="breadcrumb-item active" aria-current="page">User Profile</li>
                    </ol>
                </nav>
                
                <div class="row gutters-sm">
                    <c:if test="${requestScope.action eq 'update'}">
                    <div class="col-md-12">
                            <c:if test="${requestScope.account.image eq null}">
                                <c:if test="${requestScope.account.vip}"><span class="notify-badge">VIP</span></c:if><img src="${requestScope.account.url}" width="200" class="rounded-circle">
                            </c:if>
                            <c:if test="${requestScope.account.image ne null}">
                                <c:if test="${requestScope.account.vip}"><span class="notify-badge">VIP</span></c:if><img src="data:image/jpg;base64,${requestScope.account.image}" data-index="0" width="200" height="200"class="rounded-circle">
                            </c:if>
                            <h5>Account:${requestScope.account.email}</h5>
                            <h5>Role:${requestScope.account.roleName.name}</h5>
                        </div>
                            </c:if>
                    <div class="col-md-8">
                        <div class="card mb-3">
                            <div class="card-body">

                                <c:if test="${requestScope.action eq 'update'}">
                                    <form action="crudaccount" method="post">
                                        <input type="hidden" name="action" value="${requestScope.action}">
                                        <input type="hidden" name="aid" value="${requestScope.account.aid}">

                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Full Name</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <input type="text" name="fullname" pattern="(?=.*[a-zA-Z])([a-zA-Z\s]){1,50}" value="${requestScope.account.accountinfo.fullname}" 
                                                       oninvalid="setCustomValidity('Name only contain alphabet character max 50 chracters')" oninput="setCustomValidity('')"       disabled>                                              
                                            </div>
                                        </div>
                                        <hr>

                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Phone</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <input type="tel" placeholder="phone number" pattern="[0-9]{10,11}" id="phone"  name="phone" value="${requestScope.account.accountinfo.phone}" 
                                                       oninvalid="setCustomValidity('Phone number should have 10 or 11 digits')" oninput="setCustomValidity('')"       disabled>                                           
                                            </div>
                                        </div>
                                        <hr>

                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Address</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <input type="text" name="address" pattern="(?=.*[a-zA-Z])([a-zA-Z0-9\s]){1,350}" value="${requestScope.account.accountinfo.address}" 
                                                       oninvalid="setCustomValidity('address not null')" oninput="setCustomValidity('')"       disabled>

                                            </div>
                                        </div>
                                        <hr>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Age</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <input type="number" min="13" name="age" value="${requestScope.account.accountinfo.age}" 
                                                      oninvalid="setCustomValidity('Minimum age of google account is 13')" oninput="setCustomValidity('')"       disabled>   

                                            </div>
                                        </div>
                                        <hr>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Gender</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <select name="gender" disabled>
                                                    <option value="1" ${requestScope.accountinfo.gender eq true?'selected':''}>Male</option>
                                                    <option value="0" ${requestScope.accountinfo.gender eq false?'selected':''}>Female</option>
                                                </select>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-12">
                                            </div>
                                        </div>
                                    </form>

                                </c:if>
                                <c:if test="${requestScope.action eq 'add'}">
                                    <form action="crudaccount" method="post">
                                        <input type="hidden" name="action" value="add">
                                        <hr>
                                        
                                             <h5 style="color: lightcoral;text-align: center" class="alert alert-info">${requestScope.status}</h5>

                                         <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Email</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <input type="email" name="email"value="${requestScope.email}" >                                            
                                            </div>
                                             
                                        </div>
                                        
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Password</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <input type="text" name="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" value="${requestScope.password}"
                                               oninvalid="setCustomValidity('password contains at least 1 digit,1 lowcase,1 uppercase character')" oninput="setCustomValidity('')"      >
                                            </div>
                                        </div><hr>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <button type="submit">ADD</button>
                                            </div>
                                        </div>
                                    </form>
                                </c:if>
                            </div>
                        </div>
                    </div>
                    <c:if test="${requestScope.action eq 'update'}">
                        <div class="col-md-4">
                            <div class="card mb-3">
                                <div class="card-body">
                                    <form action="crudaccount" method="post">
                                        <input type="hidden" name="action" value="changepassword">
                                        <div class="row">
                                            <hr>
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Email</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <input type="text" name="email1" value="${requestScope.account.email}" disabled>   
                                                <input type="hidden" name="email" value="${requestScope.account.email}">   
                                            </div>
                                            <hr>
                                        </div><hr>
                                        
                                    </form>
                                </div>
                            </div>
                        </div>
                    </c:if>

                </div>
            </div>
        </div>
    </div>
    <script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript">

    </script>
</body>
</html>

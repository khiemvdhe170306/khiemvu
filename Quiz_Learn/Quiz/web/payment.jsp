<%-- 
    Document   : payment
    Created on : Oct 20, 2023, 3:22:34 PM
    Author     : Acer
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="icon" href="images/Quikk.png" type="image/png">
        <title>Payment</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"/>
        <style>
            body {
                background-color: #eee
            }

            .card {
                width: 340px;
                border-radius: 35px;
                background-color: #fff;
                box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)
            }

            .total-amount {
                font-size: 22px;
                font-weight: 700;
                color: #383737
            }

            .amount-container {
                background-color: #e9eaeb;
                padding: 6px;
                padding-left: 15px;
                padding-right: 15px;
                border-radius: 8px
            }

            .amount-text {
                font-size: 20px;
                font-weight: 700;
                color: #673AB7
            }

            .dollar-sign {
                font-size: 20px;
                font-weight: 700;
                color: #000
            }

            .label-text {
                font-size: 16px;
                font-weight: 600;
                color: #b2b2b2
            }

            .credit-card-number {
                width: 290px;
                z-index: 28;
                border: 2px solid #ced4da;
                border-radius: 6px;
                font-weight: 600
            }

            .credit-card-number:focus {
                box-shadow: none;
                border: 2px solid #673AB7
            }

            .visa-icon {
                position: relative;
                top: 42px;
                right: 14px;
                z-index: 30
            }

            .expiry-class {
                width: 120px;
                border: 2px solid #ced4da;
                font-weight: 600;
                font-size: 12px;
                height: 48px
            }

            .expiry-class:focus {
                box-shadow: none;
                border: 2px solid #673AB7
            }

            .cvv-class {
                width: 120px;
                border: 2px solid #ced4da;
                font-weight: 600;
                font-size: 12px;
                height: 48px
            }

            .cvv-class:focus {
                box-shadow: none;
                border: 2px solid #673AB7
            }

            .payment-btn {
                background-color: #673AB7;
                padding: 15px;
                padding-left: 25px;
                padding-right: 25px;
                color: #fff;
                font-weight: 600;
                border-radius: 12px
            }

            .payment-btn:hover {
                box-shadow: none;
                color: #fff
            }

            .cancel-btn {
                background-color: #fff;
                color: #b2b2b2;
                padding: 0px;
                padding-top: 3px;
                padding-bottom: 3px;
                font-weight: 600;
                border-radius: 6px
            }

            .cancel-btn:hover {
                border: 2px solid #b2b2b2;
                color: #b2b2b2
            }

            .cancel-btn:focus {
                box-shadow: none
            }

            .label-text-cc-number {
                position: relative;
                top: 4px
            }
            .logo2{
                position:relative;
                left:50px;
                margin-top:150px;
                margin-bottom: 100px;
            }
        </style>
    </head>
    <body>
        <jsp:include page="Common/Header.jsp"></jsp:include>
            <div class="container-fliud mt-5 d-flex justify-content-center" style="background-image: url('https://i.imgur.com/lqv1pcl.png');height: 100%">

                <form action="authorize_payment" method="post">
                    <input type="hidden" name="shipping" value="0" />
                    <input type="hidden" name="tax" value="0" />
                    <input type="hidden" name="priceId" value="${requestScope.pricepack.priceId}" />
                <input type="hidden" name="subtotal" value="${requestScope.pricepack.price}" />
                <div class="card p-4" style="margin-top: 200px">
                    <div class="d-flex justify-content-between align-items-center">
                        <h5 class="total-amount">Total amount: </h5>
                        <input name="total" class="form-control credit-card-number" id="total-field" value="${requestScope.pricepack.price}"/>
                    </div>
                    <div class="pt-4">
                        <label class="d-flex justify-content-between">
                            <span class="label-text label-text-cc-number">Service:</span>
                        </label>
                        <input type="text" name="product" class="form-control credit-card-number" value="${requestScope.pricepack.name}">
                    </div>

                    <input type="hidden" name="priceId" value="${requestScope.pricepack.priceId}">
                    <c:if test="${not empty sessionScope.account.voucher}">
                        <div>
                            Choose a voucher:
                            <select name="voucher" id="voucher-select">
                                <option value="0">No Voucher</option>
                                <c:forEach items="${sessionScope.account.voucher}" var="c">

                                    <option value="${c.id}" data-percentage="${c.percentage}">${c.name}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </c:if>
                    <c:if test="${empty sessionScope.account.voucher}">
                        <p>You have no voucher to use!</p>
                    </c:if>
                    <div class="d-flex justify-content-between pt-5 align-items-center">

                        <a type="button" href="membership" class="btn cancel-btn">Cancel</a>
                        <button type="submit" onclick="handlePayment(event)" class="btn payment-btn">Make Payment</button>
                    </div>

                </div>
            </form>

        </div>
        <jsp:include page="Common/Sidebar.jsp"></jsp:include>

        <jsp:include page="Common/Footer.jsp"></jsp:include>
            <script>
                // Get the necessary elements
                const voucherSelect = document.getElementById('voucher-select');
                const totalField = document.getElementById('total-field');

                // Add event listener to the voucher select element
                voucherSelect.addEventListener('change', function () {
                    const selectedOption = voucherSelect.options[voucherSelect.selectedIndex];
                    const percentage = selectedOption.getAttribute('data-percentage');

                    // Calculate the new total amount based on the selected voucher percentage
                    const subtotal = parseFloat(document.getElementsByName('subtotal')[0].value);
                    const discount = (subtotal * percentage) / 100;
                    const newTotal = subtotal - discount;

                    // Update the total field value
                    totalField.value = newTotal.toFixed(2);
                });

                function handlePayment(event) {
                    event.preventDefault(); // Prevent the form from submitting immediately

                    if (${sessionScope.account.accountinfo eq null}) {
                        // Account info is null, show the notification
                        alert("Please fill in your account information before making the payment.");
                        window.location.href = "viewuser"; // Redirect to the user profile page
                    } else {
                        // Account info is not null, submit the form
                        event.target.closest('form').submit();
                    }
                }
        </script>
    </body>
</html>

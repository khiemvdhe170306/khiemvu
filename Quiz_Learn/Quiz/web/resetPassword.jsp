<%-- 
    Document   : check.jsp
    Created on : Sep 7, 2023, 7:16:30 PM
    Author     : khiem
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Reset Password</title>
        <link rel="icon" href="images/Quikk.png" type="image/png">
        <style>
            body{
                  background-image: url(https://cmshn.fpt.edu.vn/pluginfile.php/1/theme_trema/frontpagebanner/1697438386/sanhAlpha.jpg);

            }
            .mainDiv {
                display: flex;
                min-height: 100%;
                align-items: center;
                justify-content: center;
                font-family: 'Open Sans', sans-serif;
            }
            .cardStyle {
                width: 500px;
                border-color: white;
                background: #fff;
                padding: 36px 0;
                border-radius: 4px;
                margin: 30px 0;
                box-shadow: 0px 0 2px 0 rgba(0,0,0,0.25);
            }
            #signupLogo {
                max-height: 100px;
                margin: auto;
                display: flex;
                flex-direction: column;
            }
            .formTitle{
                font-weight: 600;
                margin-top: 20px;
                color: #2F2D3B;
                text-align: center;
            }
            .inputLabel {
                font-size: 12px;
                color: #555;
                margin-bottom: 6px;
                margin-top: 24px;
            }
            .inputDiv {
                width: 70%;
                display: flex;
                flex-direction: column;
                margin: auto;
            }
            input {
                height: 40px;
                font-size: 16px;
                border-radius: 4px;
                border: none;
                border: solid 1px #ccc;
                padding: 0 11px;
            }

            .buttonWrapper {
                margin-top: 40px;
            }
            .submitButton {
                width: 70%;
                height: 40px;
                margin: auto;
                display: block;
                color: #fff;
                background-color: #065492;
                border-color: #065492;
                text-shadow: 0 -1px 0 rgba(0, 0, 0, 0.12);
                box-shadow: 0 2px 0 rgba(0, 0, 0, 0.035);
                border-radius: 4px;
                font-size: 14px;
                cursor: pointer;
            }
        </style>
    </head>
    <body onload="functionToCall()">
        <input type="hidden" name="name" id="duration" value="300000">
        <c:if test="${sessionScope.resetpass eq null}">
            <h1 style="color: red">${requestScope.status}</h1>
            <div class="mainDiv">
                <div class="cardStyle">
                    <form action="resetpassword" >
                        <img src="" id="signupLogo"/>
                        <h2 class="formTitle">
                            Enter the your email to get code and reset password
                        </h2>
                        <div class="inputDiv">
                            <label class="inputLabel" for="email">your email</label>
                            <input type="text" name="email" required>
                        </div>
                        <h5 style="color: red;text-align: center;">${requestScope.ms}</h5>
                        <div class="buttonWrapper">
                            <button type="submit" class="submitButton pure-button pure-button-primary">
                                <span>Submit</span>

                            </button>
                        </div>
                    </form>
                </div>
            </div>

        </c:if>
        <c:if test="${sessionScope.resetpass ne null}">
            <div class="mainDiv">
                <div class="cardStyle">
                    <form action="resetpassword" method="post" name="signupForm" id="signupForm">

                        <img src="" id="signupLogo"/>

                        <h2 class="formTitle" id="title">
                            Enter the code and reset password
                        </h2>
                        <div id="codeForm">
                            <div class="inputDiv">
                                <label class="inputLabel" for="code">code</label>
                                <input type="text" id="code" name="code" required>
                            </div>
                            <p style="color: red;text-align: center;" id="demo"></p>                      
                            <p style="text-align: center;">A code has been sent to : <span style="color:blue;">${sessionScope.userName}</span></p>
                            <h5 style="color: red;text-align: center;" id="demo2"></h5>
                            <input type="hidden" id="code1" value="${sessionScope.codeMail}">
                            <h5 style="color: red;text-align: center;">${requestScope.status}</h5>
                            <div class="inputDiv">
                                <label class="inputLabel" for="password">New Password</label>
                                <input type="password" id="password" name="password" required>
                            </div>

                            <div class="inputDiv">
                                <label class="inputLabel" for="confirmPassword">Confirm Password</label>
                                <input type="password" id="confirmPassword" name="confirmPassword">
                            </div>      

                            <p style="color: red;text-align: center;" id="demo1"></p> 
                            <div class="buttonWrapper">
                                <input type="button" class="submitButton" onclick="matchPassword()" value="Submit" name="Submit">
                                <div>

                                </div>
                            </div>
                        </div>
                    </form>
                    <div style="display: flex;align-items: center;justify-content: center">
                        <form action="resetpassword" id="newcode" >
                            <input type="hidden" name="email"  value="${sessionScope.userName}">
                            <input type="hidden" name="password" value="111">
                            <input type="submit" value="Send a new code to ${sessionScope.userName}" />
                        </form>
                    </div>    
                </div>
            </div>
        </c:if>
        <script>

            function matchPassword() {
                document.getElementById("demo").innerHTML = "";
                document.getElementById("demo1").innerHTML = "";
                var pw1 = document.getElementById("password").value
                        , pw2 = document.getElementById("confirmPassword").value;
                if (!pw1 || !pw2) {
                    document.getElementById("demo1").innerHTML = "Please fill out the form";
                } else if (pw1 !== pw2) {
                    document.getElementById("demo1").innerHTML = "Not match password!!! Enter again";

                } else if (!checkPassword(pw1))
                {
                    //document.getElementById("demo").innerHTML = "";
                    document.getElementById("demo1").innerHTML = "New Password Must contain at least one  number and one uppercase and lowercase letter, and at least 8 or more character";
                } else {

                    matchCode();
                }
            }
            function functionToCall() {
                document.getElementById("newcode").style.display = "none";
            }
            function matchCode() {
                var pw1 = document.getElementById("code").value
                        , pw2 = document.getElementById("code1").value;
                if (!pw1 || !pw2) {
                    document.getElementById("demo").innerHTML = "Please fill out the form";
                } else if (pw1 !== pw2) {
                    document.getElementById("demo").innerHTML = "Code does not match!!! Enter again";

                } else {

                    document.getElementById("signupForm").submit();
                }
            }
            function checkPassword(str)
            {
                var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
                return re.test(str);
            }
            var duration_raw = document.getElementById("duration").value;
            let duration = parseInt(duration_raw);
            // Update the count down every 1 second
            var x = setInterval(function () {
                // Time calculations for days, hours, minutes and seconds
                var seconds = Math.floor((duration % (1000 * 60)) / 1000);
                var minutes = Math.floor((duration % (1000 * 60 * 60)) / (1000 * 60));
                // Output the result in an element with id="demo"
                document.getElementById("demo2").innerHTML = "Code will Expire in: " + minutes + "m "  + seconds + "s ";
                //document.getElementById("demo").innerHTML=duration;
                // If the count down is over, write some text 
                if (duration < 0) {
                    clearInterval(x);
                    document.getElementById("codeForm").style.display = "none";
                    document.getElementById("demo2").innerHTML = "";
                    document.getElementById("newcode").style.display = "block";
                    document.getElementById("title").innerHTML = "Your code is expired";
                }
                duration -= 1000;
                document.getElementById("d").value = duration.toString();
            }, 1000);


            document.getElementById('signupLogo').src = "https://s3-us-west-2.amazonaws.com/shipsy-public-assets/shipsy/SHIPSY_LOGO_BIRD_BLUE.png";

        </script>

    </body>
</html>

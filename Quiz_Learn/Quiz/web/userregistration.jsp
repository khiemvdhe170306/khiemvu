<%-- 
    Document   : userregistration
    Created on : Oct 29, 2023, 8:23:46 PM
    Author     : Acer
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <link rel="stylesheet" href="https://allyoucan.cloud/cdn/icofont/1.0.1/icofont.css" integrity="sha384-jbCTJB16Q17718YM9U22iJkhuGbS0Gd2LjaWb4YJEZToOPmnKDjySVa323U+W7Fv" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.3.1/dist/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </head>
    <body style="font-family: var(--poppins);">
        <jsp:include page="Common/Header.jsp"></jsp:include>
            <div class="container" style="margin-top: 100px; height: 100%">
                <div class="row">               
                    <div class="col-md-9">
                        <div class="osahan-account-page-right shadow-sm bg-white p-4 h-100">
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane  fade  active show" id="orders" role="tabpanel" aria-labelledby="orders-tab">
                                    <h4 class="font-weight-bold mt-0 mb-4">Registration</h4>
                                    <div class="bg-white card mb-4 order-list shadow-sm">

                                    <c:forEach items="${requestScope.courseU}" var="c">
                                        <div class="gold-members p-4">

                                            <a href="#">
                                            </a>
                                            <div class="media">
                                                <a href="">
                                                    <img style="width: 48px; height: 48px;" class="mr-4" src="${c.thumbnail}" alt="course-img">
                                                </a>
                                                <div class="media-body">
                                                    <a href="#">
                                                        <c:forEach items="${requestScope.regiscourse}" var="r">
                                                            <span class="float-right text-info"><c:if test="${c.courseId eq r.courseId}">Start Date: ${r.regisDate}</c:if><i class="icofont-check-circled text-success"></i></span>
                                                        </c:forEach>
                                                </a>
                                                <h6 class="mb-2">
                                                    <a href="#"></a>
                                                    <a href="#" class="text-black">${c.courseName}</a><!-- Course Name -->
                                                </h6>
                                                <p class="text-gray mb-1"><i class="icofont-location-arrow"></i> ${c.description}
                                                </p>
                                                <!--                                                    <p class="text-gray mb-3"><i class="icofont-list"></i> ORDER #25102589748 <i class="icofont-clock-time ml-2"></i> Mon, Nov 12, 6:26 PM</p>
                                                                                                    <p class="text-dark">Veg Masala Roll x 1, Veg Burger x 1, Veg Penne Pasta in Red Sauce x 1
                                                                                                    </p>-->
                                                <hr>
                                                <div class="float-right">
                                                    <a class="btn btn-sm btn-outline-primary" href="coursedetail?courseId=${c.courseId}&page=1"><i class="icofont-headphone-alt"></i> View Detail</a>
                                                    <a class="btn btn-sm btn-primary" href="usercoursedetail?courseId=${c.courseId}"><i class="icofont-refresh"></i> Continue Learning</a>
                                                </div>
                                                <p class="mb-0 text-black text-primary pt-2"><span class="text-black font-weight-bold"> Status:</span> Learning
                                                </p>
                                            </div>

                                        </div>

                                    </div>
                                </c:forEach>
                            </div> 

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <jsp:include page="Common/Sidebar.jsp"></jsp:include>
    <jsp:include page="Common/Footer.jsp"></jsp:include>
</body>
</html>

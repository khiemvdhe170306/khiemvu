<%-- 
    Document   : lessonManagement
    Created on : Sep 22, 2023, 9:24:58 PM
    Author     : Admin
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Lessons Management</title>
        <link rel="icon" href="images/Quikk.png" type="image/png">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="./css/lessonManagement.css"><!-- comment -->

        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

        <script>
            $(document).ready(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>
    </head>
    <body>
        <c:if test = "${fn:contains(sessionScope.account.roleName.access, 'course')}">
            <jsp:include page="Common/Management.jsp"></jsp:include>
                <div class="container-xl">
                    <div class="table-responsive">
                        <div class="table-wrapper">
                            <div class="table-title">
                                <div class="row">
                                    <div class="col-sm-5">
                                        <h2>Lesson <b>Management</b></h2>
                                    </div>
                                    <div class="col-sm-7">
                                        <a href="crudlesson?action=add" class="btn btn-secondary"><i class="material-icons">&#xE147;</i> <span>Add New Lesson</span></a>					
                                    </div>
                                </div>
                            </div>
                            <div class="message">
                            <c:if test="${not empty msg}">
                                <div class="alert alert-info">${msg}</div>
                            </c:if>
                        </div>
                        <!--                           filter                        -->
                        <form action="lessonmanagement"  id="filter">
                            <!--                                filter by subject-->
                            Subject: <select name="subject" onchange="subjectSubmit()">
                                <option value="0" id="allSubject" <c:if test="${param.subject eq 0}">selected</c:if>>All subject </option>

                                <c:forEach items="${requestScope.subjectList}" var="c">  
                                    <option value="${c.subjectId}" <c:if test="${param.subject eq c.subjectId}">selected</c:if>>${c.title}</option>
                                </c:forEach>  
                            </select>
                            <!--                                filter by course-->
                            Course: <select name="course" onchange="courseSubmit()">
                                <option value="0" id="allCourse" <c:if test="${param.course eq 0}">selected</c:if>>All course </option>

                                <c:forEach items="${requestScope.courseList}" var="c">  
                                    <option value="${c.courseId}" <c:if test="${param.course eq c.courseId}">selected</c:if>>${c.courseName}</option>
                                </c:forEach>  
                            </select>
                            <!--                            filter theo chap   -->
                            Chapter: <select name="chapter" onchange="chapterSubmit()">
                                <option  value="0" id="allChapter" <c:if test="${param.chapter eq 0}">selected</c:if>>All chapter </option>

                                <c:forEach items="${requestScope.chapterList}" var="c">
                                    <option value="${c.chapId}" <c:if test="${param.chapter eq c.chapId}">selected</c:if>>${c.chapName} </option>
                                </c:forEach>  
                            </select>
                            <p id="notAll" style="color:red;"></p>
                        </form>


                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Lesson Id</th>
                                    <th>Lesson Name</th>
                                    <th>Image</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:if test="${not empty requestScope.lessonlist}">

                                    <c:forEach items="${requestScope.lessonlist}" var="l" begin="${requestScope.page*6-6}" end="${requestScope.page*6-1}">
                                        <tr>
                                            <td>${l.lessonId}</td>
                                            <td>${l.lessonName}</td>
                                            <td> <img src="${l.image}" alt="alt" style="width: 100px;"/></td>

                                            <td>
                                                <a href="lessondetail?lessonId=${l.lessonId}" title="View" data-toggle="tooltip"><i class="fa fa-eye" style="font-size:22px;position:relative;top:-3px;color: #1aa3ff"></i></a>
                                                <a href="crudlesson?lessonId=${l.lessonId}&action=update" class="settings" title="Edit" data-toggle="tooltip"><i class="material-icons">&#xE8B8;</i></a>
                                                <a href="crudlesson?lessonId=${l.lessonId}&action=delete" class="disabled" title="Delete" data-toggle="tooltip" onclick="return confirm('Are you sure you want to delete this lesson?');"><i class="fa fa-trash-o"></i></a>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                </c:if>
                            </tbody>
                        </table>
                        <div class="clearfix">

                            <% int i=1; %>
                            <ul class="pagination">

                                <c:forEach items="${requestScope.lessonlist}" step="6">
                                    <c:set var="i" value="<%=i%>"/>
                                    <li class="page-item <c:if test="${requestScope.page eq i}">active</c:if>" ><a href="lessonmanagement?page=<%=i%><c:if test="${param.subject ne null}">&subject=</c:if>${param.subject}<c:if test="${param.course ne null}">&course=</c:if>${param.course}<c:if test="${param.chapter ne null}">&chapter=</c:if>${param.chapter}" class="page-link"><%=i%></a>
                                        </li> 
                                    <% i++;%>
                                </c:forEach>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </c:if>
    </body>
    <script>
        function subjectSubmit()
        {
            if (document.getElementById("allSubject").selected) {
                document.getElementById("allChapter").selected = true;
                document.getElementById("allCourse").selected = true;
            }
            document.getElementById("filter").submit();
        }
        function courseSubmit()
        {
            if (document.getElementById("allCourse").selected) {
                document.getElementById("allChapter").selected = true;
                document.getElementById("filter").submit();
            } else if (document.getElementById("allSubject").selected) {
                document.getElementById("notAll").innerHTML = "choose subject first";
                document.getElementById("allCourse").selected = true;
            } else {

                document.getElementById("filter").submit();
            }
        }
        function chapterSubmit()
        {
            if (document.getElementById("allCourse").selected) {
                document.getElementById("notAll").innerHTML = "choose course first";
                document.getElementById("allChapter").selected = true;
            } else {

                document.getElementById("filter").submit();
            }
        }
    </script>
</html>

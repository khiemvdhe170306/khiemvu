<%-- 
    Document   : crudAccount
    Created on : Sep 10, 2023, 6:11:07 PM
    Author     : khiem
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">


        <title>Profile</title>
        <link rel="icon" href="images/Quikk.png" type="image/png">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" rel="stylesheet">
        <style type="text/css">
            body{
                margin-top:20px;
                color: #1a202c;
                text-align: left;
                background-color: #e2e8f0;
            }
            .main-body {
                padding: 15px;
            }
            .card {
                box-shadow: 0 1px 3px 0 rgba(0,0,0,.1), 0 1px 2px 0 rgba(0,0,0,.06);
            }

            .card {
                position: relative;
                display: flex;
                flex-direction: column;
                min-width: 0;
                word-wrap: break-word;
                background-color: #fff;
                background-clip: border-box;
                border: 0 solid rgba(0,0,0,.125);
                border-radius: .25rem;
            }

            .card-body {
                flex: 1 1 auto;
                min-height: 1px;
                padding: 1rem;
            }

            .gutters-sm {
                margin-right: -8px;
                margin-left: -8px;
            }

            .gutters-sm>.col, .gutters-sm>[class*=col-] {
                padding-right: 8px;
                padding-left: 8px;
            }
            .mb-3, .my-3 {
                margin-bottom: 1rem!important;
            }

            .bg-gray-300 {
                background-color: #e2e8f0;
            }
            .h-100 {
                height: 100%!important;
            }
            .shadow-none {
                box-shadow: none!important;
            }
            
            .notify-badge{
                position: absolute;
                left:20px;
                top:10px;
                background-image: url('images/wing.png');
                background-size: 33px 25px;
                text-align: center;
                border-radius: 30px 30px 30px 30px;
                color:gold;
                padding:5px 10px;
                font-size:20px;
            }

        </style>
    </head>
    <body>
        <div class="container-fluid">
            <jsp:include page="Common/Header.jsp"></jsp:include>
            <br><br><br><br>
            <div class="main-body">

                <nav aria-label="breadcrumb" class="main-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="home">Home</a></li>
                        <li class="breadcrumb-item"><a href="cruduser?action=view">User Profile</a></li>
                    </ol>
                </nav>

                <div class="row gutters-sm">
                    <div class="col-md-12">
                            <c:if test="${sessionScope.account.image eq null}">

                                <c:if test="${sessionScope.account.vip}"><span class="notify-badge">VIP</span></c:if><img src="${sessionScope.account.url}" width="150" class="rounded-circle">

                                <form action="img" method="post" enctype="multipart/form-data">
                                    <input type="file" class="hidden" name="imageProfile" id="files"  onchange="this.form.submit()" />

                                    <input type="hidden" name="add" value="${sessionScope.account.aid}">
                                </form> 
                                <h5></h5>
                                

                            </c:if>
                                
                            <c:if test="${sessionScope.account.image ne null}">
                                <c:if test="${sessionScope.account.vip}"><span class="notify-badge">VIP</span></c:if><img src="data:image/jpg;base64,${sessionScope.account.image}" data-index="0" width="150" height="200"class="rounded-circle">
                                <form action="img" method="post" enctype="multipart/form-data">

                                    <input type="file" class="hidden" name="imageProfile" id="files"  onchange="this.form.submit()" />
                                    <input type="hidden" name="update" value="${sessionScope.account.aid}">
                                </form> 
                            </c:if>
                                <c:if test="${sessionScope.account.vip}">
                                 <h5><img width="50px" src="https://img.freepik.com/premium-vector/golden-vip-badges-luxury-elegant-business-icon-premium-vector_567423-318.jpg?size=338&ext=jpg&ga=GA1.1.1413502914.1696896000&semt=ais" alt="alt"/></h5>
                                </c:if>
                            <h5>Account:${sessionScope.account.email}</h5>
                            <h5>Role:${sessionScope.account.roleName.name}</h5>
                        </div>
                    
                    <c:if test="${requestScope.action eq 'update'}">
                        
                        
                        <div class="col-md-12">
                            <div class="card mb-3">
                                <div class="card-body">
                                    <form action="cruduser" method="post">
                                        <input type="hidden" name="action" value="${requestScope.action}">
                                        <input type="hidden" name="aid" value="${sessionScope.account.aid}">

                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Full Name</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <input type="text" name="fullname" pattern="[^\s][^0-9!@#$%&]+" value="${sessionScope.account.accountinfo.fullname}" 
                                                       oninvalid="setCustomValidity('Name only contain alphabet character')" oninput="setCustomValidity('')"       required>                                            
                                            </div>
                                        </div>
                                        <hr>

                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Phone</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <input type="tel" placeholder="phone number" pattern="[0-9]{10,11}" id="phone"  name="phone" value="${sessionScope.account.accountinfo.phone}" 
                                                       oninvalid="setCustomValidity('Phone number should have 10 or 11 digits')" oninput="setCustomValidity('')"       required>                                          
                                            </div>
                                        </div>
                                        <hr>

                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Address</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                           
                                                <input type="text" name="address" pattern="[^\s][^!@#$%&]+" value="${sessionScope.account.accountinfo.address}" 
                                                       oninvalid="setCustomValidity('address not null')" oninput="setCustomValidity('')"       required>

                                            </div>
                                        </div>
                                        <hr>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Age</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <input type="number" min="13" id="age" name="age" value="${sessionScope.account.accountinfo.age}"
                                                       oninvalid="setCustomValidity('Minimum age of google account is 13')" oninput="setCustomValidity('')"       required>                                          
                                            </div>
                                        </div>
                                        <hr>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Gender</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <select name="gender" required>
                                                    <option value="1" ${sessionScope.accountinfo.gender eq true?'selected':''}>Male</option>
                                                    <option value="0" ${sessionScope.accountinfo.gender eq false?'selected':''}>Female</option>
                                                </select>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <input type="submit" value="Update Profile" onclick="myFunction()">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </c:if>


                    <c:if test="${requestScope.action eq 'updatePassword'}">
                        
                        <div class="col-md-12">
                            <div class="card mb-3">
                                <div class="card-body">
                                    <form action="cruduser" method="post" id="signupForm">
                                        <input type="hidden" name="action" value="${requestScope.action}">
                                        <div class="row">
                                            <hr>
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Email</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <input type="text" name="email1" value="${sessionScope.account.email}" disabled>   
                                                <input type="hidden" name="email" value="${sessionScope.account.email}">   
                                            </div>
                                            <hr>
                                        </div><hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Password</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">

                                                <input type="hidden" name="oldpassword" id="oldpassword" value="${sessionScope.account.password}"> 
                                                <input type="password" name="newpassword" id="newpassword"  required> 
                                            </div>
                                        </div><hr>
                                        <p style="color: red" id="demo"></p>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">New Password</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <input type="password" id="password" name="password" required="">                                          
                                            </div>
                                        </div>
                                        <p style="color: red" id="demo1"></p>
                                        <hr>
                                        <input type="button" value="Update Account" onclick="matchCode()"/>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </c:if>
                    <c:if test="${requestScope.action eq 'view'}">

                        <div class="col-md-8">
                            <div class="card mb-3">
                                <div class="card-body">
                                    <form action="cruduser">
                                        <input type="hidden" name="action" value="update">


                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Full Name</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <input type="text" name="fullname" value="${sessionScope.account.accountinfo.fullname}" disabled>                                            
                                            </div>
                                        </div>
                                        <hr>

                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Phone</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <input type="text" name="phone" value="${sessionScope.account.accountinfo.phone}" disabled>                                          
                                            </div>
                                        </div>
                                        <hr>

                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Address</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <input type="text" name="address" value="${sessionScope.account.accountinfo.address}" disabled>  

                                            </div>
                                        </div>
                                        <hr>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Age</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <input type="text" name="age" value="${sessionScope.account.accountinfo.age}" disabled>  

                                            </div>
                                        </div>
                                        <hr>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Gender</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <select name="gender" disabled>
                                                    <option value="1" ${sessionScope.accountinfo.gender eq true?'selected':''}>Male</option>
                                                    <option value="0" ${sessionScope.accountinfo.gender eq false?'selected':''}>Female</option>
                                                </select>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <input type="submit" value="Update Profile" >
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="card mb-3">
                                <div class="card-body">
                                    <form action="cruduser" >
                                        <input type="hidden" name="action" value="updatePassword">
                                        <div class="row">
                                            <hr>
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Email</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <input type="text" name="email" value="${sessionScope.account.email}" disabled>   

                                            </div>
                                            <hr>
                                        </div><hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Password</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <input type="password" name="password" value="${sessionScope.account.password}" disabled>                                            
                                            </div>
                                        </div><hr>
                                        <input type="submit" value="Update Account" />
                                    </form>
                                </div>
                            </div>
                        </div>
                    </c:if>
                </div>
            </div>
        </div>
            <jsp:include page="Common/Sidebar.jsp"></jsp:include>

            <jsp:include page="Common/Footer.jsp"></jsp:include>
        <form action="logout" id="logout"></form>
    </div>
    <script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.bundle.min.js"></script>
    <script type="text/javascript">
                                    let i=0;
                                    function matchCode() {
                                        if(i>=4)
                                            {
                                                document.getElementById("logout").submit();
                                            }
                                        var pw1 = document.getElementById("oldpassword").value
                                                , pw2 = document.getElementById("newpassword").value,
                                                pw3 = document.getElementById("password").value;
                                        
                                        if (pw1 !== pw2) {
                                            i++;
                                            document.getElementById("demo").innerHTML = "Password isn' correct!!! Enter again "+i+"/5\n\
                                you will be forced to logout if you enter wrong 5 times in a row";
            
                                        } else if (!checkPassword(pw3))
                                        {
                                            document.getElementById("demo").innerHTML ="";
                                            document.getElementById("demo1").innerHTML = "New Password Must contain at least one  number and one uppercase and lowercase letter, and at least 8 or more character";
                                        } else
                                        {
                                            document.getElementById("signupForm").submit();
                                        }
                                    }
                                    function checkPassword(str)
                                    {
                                        var re = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
                                        return re.test(str);
                                    }

    </script>
</body>
</html>

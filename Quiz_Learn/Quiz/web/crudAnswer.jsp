<%-- 
    Document   : CrudSubject
    Created on : Sep 15, 2023, 10:36:57 AM
    Author     : Acer
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">


        <title>Manage Options</title>
        <link rel="icon" href="images/Quikk.png" type="image/png">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.ckeditor.com/ckeditor5/39.0.2/classic/ckeditor.js"></script>
        <style type="text/css">
            body{
                margin-top:20px;
                color: #1a202c;
                text-align: left;
                font-size: 13px;
                background-color: #e2e8f0;
                font-family: 'Varela Round', sans-serif;
            }
            .main-body {
                padding: 15px;
            }
            .card {
                box-shadow: 0 1px 3px 0 rgba(0,0,0,.1), 0 1px 2px 0 rgba(0,0,0,.06);
            }

            .card {
                position: relative;
                display: flex;
                flex-direction: column;
                min-width: 0;
                word-wrap: break-word;
                background-color: #fff;
                background-clip: border-box;
                border: 0 solid rgba(0,0,0,.125);
                border-radius: .25rem;
            }

            .card-body {
                flex: 1 1 auto;
                min-height: 1px;
                padding: 1rem;
            }

            .gutters-sm {
                margin-right: -8px;
                margin-left: -8px;
            }

            .gutters-sm>.col, .gutters-sm>[class*=col-] {
                padding-right: 8px;
                padding-left: 8px;
            }
            .mb-3, .my-3 {
                margin-bottom: 1rem!important;
            }

            .bg-gray-300 {
                background-color: #e2e8f0;
            }
            .h-100 {
                height: 100%!important;
            }
            .shadow-none {
                box-shadow: none!important;
            }

        </style>
    </head>
    <body>
        
        <div class="container">
            <div class="main-body">

               <nav aria-label="breadcrumb" class="main-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="home">Home</a></li>
                        <li class="breadcrumb-item"><a href="questionmanagement">Quiz</a></li>
                        <li class="breadcrumb-item"><a href="answermanagement?questionId=${sessionScope.question.questionId}">Question ${sessionScope.question.questionId}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Manage Answer ${requestScope.answer.aid}</li>
                    </ol>
                </nav>
                <div class="row gutters-sm">
                    <div class="col-md-12">
                        <div class="card mb-3">
                            <div class="card-body">

                                <c:if test="${requestScope.action eq 'update'}">
                                    <form action="editAnswer" method="post">
                                        <input type="hidden" name="action" value="${requestScope.action}">
                                        <input type="hidden" name="aid" value="${requestScope.answer.aid}">

                                         
                                       
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Content</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                
                                                
                                                <input type="text" name="content" pattern="(?=.*[a-zA-Z])([^!@#$%&])+" value="${requestScope.answer.content}" 
                                                       oninvalid="setCustomValidity('content not null')" oninput="setCustomValidity('')"       required>  
                                            </div>
                                        </div><hr>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Correct</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <select name="correct" >
                                                    <option value="1" ${requestScope.answer.correct eq true?'selected':''}>True</option>
                                                    <option value="0" ${requestScope.answer.correct eq false?'selected':''}>False</option>
                                                </select>
                                            </div>
                                        </div>
                                        <hr>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <input type="submit" value="Update">
                                            </div>
                                        </div>
                                    </form>

                                </c:if>
                                <c:if test="${requestScope.action eq 'add'}">
                                    <form action="editAnswer" method="post">
                                        <input type="hidden" name="action" value="add">
                                        
                                       
                                       
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Content</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                 
                                                
                                                <input type="text" name="content" pattern="(?=.*[a-zA-Z])([^!@#$%&])+" 
                                                       oninvalid="setCustomValidity('description not null')" oninput="setCustomValidity('')"       required>
                                            </div>
                                        </div><hr>
                                         <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Correct</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <select name="correct">
                                                    <option value="1" >True</option>
                                                    <option value="0" >False</option>
                                                </select>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <input type="submit" value="Add" />
                                            </div>
                                        </div>
                                    </form>
                                </c:if>
                            </div>
                        </div>
                    </div>                   

                </div>
            </div>
        </div>
    </div>
    <script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.bundle.min.js"></script>
    <script>
        
    </script>
</body>
</html>


<%-- 
    Document   : header
    Created on : Jun 19, 2023, 3:02:58 PM
    Author     : khiem
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 


<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

        <title>Electro - HTML Ecommerce Template</title>

        <!-- Google font -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">

        <!-- Bootstrap -->
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>

        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <style>
            .navbar-nav{
                flex-direction: row;
            }
        </style>
    </head>
    <body>

        <!-- NAVIGATION -->
        <nav id="navigation">
            <!-- container -->
            <div class="container">
                <!-- responsive-nav -->
                <div id="responsive-nav">
                    <!-- NAV -->
                    <ul class="main-nav nav navbar-nav">
                        <li><a href="home">Home</a></li>
                            <c:if test = "${fn:contains(sessionScope.account.roleName.access, 'blog')}">
                            <li <%if((request.getRequestURI().contains("blogCrud.jsp"))||(request.getRequestURI().contains("blogManagement.jsp"))){%> class="active" <%}%> ><a href="blogmanagement">Manage Blog</a></li>

                        </c:if>
<c:if test = "${fn:contains(sessionScope.account.roleName.access, 'slider')}">
                            <li  class="active" ><a href="slider">Manage Slider</a></li>

                        </c:if>
                        <c:if test = "${fn:contains(sessionScope.account.roleName.access, 'course')}">                        
                            <li <%if((request.getRequestURI().contains("crudCourse.jsp"))||(request.getRequestURI().contains("courseManagement.jsp"))){%> class="active" <%}%> ><a href="coursemanagement">Manage Course</a></li>
                            <li <%if((request.getRequestURI().contains("crudChapter.jsp"))||(request.getRequestURI().contains("chapterManagement.jsp"))){%> class="active" <%}%> ><a href="chaptermanagement">Manage Chapter</a></li>
                            <li <%if((request.getRequestURI().contains("crudLesson.jsp"))||(request.getRequestURI().contains("lessonManagement.jsp"))){%> class="active" <%}%> ><a href="lessonmanagement">Manage Lesson</a></li>

                        </c:if>
                        <c:if test = "${fn:contains(sessionScope.account.roleName.access, 'subject')}">
                            <li <%if((request.getRequestURI().contains("crudSubject.jsp"))||(request.getRequestURI().contains("subjectManagement.jsp"))){%> class="active" <%}%> ><a href="subjectmanagement">Manage Subject</a></li>
                            </c:if> 
                            <c:if test = "${fn:contains(sessionScope.account.roleName.access, 'account')}">
                            <li <%if((request.getRequestURI().contains("crudAccount.jsp"))||(request.getRequestURI().contains("accountManagement.jsp"))){%> class="active" <%}%> ><a href="accountmanagement">Manage Account</a></li>                          
                            <li><a href="pricepackagemanagement">Manage Package</a></li>

                            </c:if>
                                <c:if test = "${fn:contains(sessionScope.account.roleName.access, 'quiz')}">
                                                            <li <%if((request.getRequestURI().contains("crudQuiz.jsp"))||(request.getRequestURI().contains("quizManagement.jsp"))){%> class="active" <%}%> ><a href="quizmanagement">Manage Quiz</a></li>
                                </c:if>
                                <c:if test = "${fn:contains(sessionScope.account.roleName.access, 'membership')}">
                                                            <li><a href="regismembermanagement">Manage Membership</a></li>
                                                            <li><a href="pricepackagemanagement">Manage PricePackage</a></li>
                                </c:if>

                    </ul>
                    <!-- /NAV -->
                </div>
                <!-- /responsive-nav -->
            </div>
            <!-- /container -->
        </nav>
        <!-- /NAVIGATION -->
        <!-- jQuery Plugins -->
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/slick.min.js"></script>
        <script src="js/nouislider.min.js"></script>
        <script src="js/jquery.zoom.min.js"></script>
        <script src="js/main.js"></script>
    </body>
</html>

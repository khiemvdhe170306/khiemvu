<link rel="stylesheet" type="text/css" href="https://cmshn.fpt.edu.vn/theme/styles.php/trema/1684246329_1/all">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<footer id="page-footer">
    <div class="container">
        <div class="row gtr-200 gtr-uniform">
            <section class="col-12 text-center col-12-medium col-12-xsmall footer-info">
                <div class="tool_usertours-resettourcontainer"></div>
                <nav class="nav navbar-nav d-md-none">
                    <ul class="list-unstyled pt-3">
                    </ul>
                </nav>
            </section>

            <section class="col-12 footer-info">

            </section>

            <section class="col-12 text-center col-12-medium col-12-xsmall footer-info smalltext">
                <c:if test="${sessionScope.account != null}">
                    <div class="logininfo">You are logged in as <a
                            href="editProfile" title="View profile">${sessionScope.account.email}</a> (<a
                            href="logout">Log out</a>)</div>
                    </c:if>
                    <c:if test="${sessionScope.account == null}">
                    <div class="logininfo">You are not logged in. (<a href="login.jsp">Log in</a>)</div>
                </c:if>
                <script type="text/javascript">
                    //<![CDATA[
                    var require = {
                        baseUrl: 'https://cmshn.fpt.edu.vn/lib/requirejs.php/1566968257/',
                        // We only support AMD modules with an explicit define() statement.
                        enforceDefine: true,
                        skipDataMain: true,
                        waitSeconds: 0,

                        paths: {
                            jquery: 'https://cmshn.fpt.edu.vn/lib/javascript.php/1566968257/lib/jquery/jquery-3.2.1.min',
                            jqueryui: 'https://cmshn.fpt.edu.vn/lib/javascript.php/1566968257/lib/jquery/ui-1.12.1/jquery-ui.min',
                            jqueryprivate: 'https://cmshn.fpt.edu.vn/lib/javascript.php/1566968257/lib/requirejs/jquery-private'
                        },

                        // Custom jquery config map.
                        map: {
                            // '*' means all modules will get 'jqueryprivate'
                            // for their 'jquery' dependency.
                            '*': {jquery: 'jqueryprivate'},
                            // Stub module for 'process'. This is a workaround for a bug in MathJax (see MDL-60458).
                            '*': {process: 'core/first'},

                            // 'jquery-private' wants the real jQuery module
                            // though. If this line was not here, there would
                            // be an unresolvable cyclic dependency.
                            jqueryprivate: {jquery: 'jquery'}
                        }
                    };

                    //]]>
                </script>
                <script type="text/javascript"
                src="https://cmshn.fpt.edu.vn/lib/javascript.php/1566968257/lib/requirejs/require.min.js"></script>
                <script type="text/javascript">
                    //<![CDATA[
                    require(['core/first'], function () {
                        ;
                        require(["media_videojs/loader"], function (loader) {
                            loader.setUp(function (videojs) {
                                videojs.options.flash.swf = "https://cmshn.fpt.edu.vn/media/player/videojs/videojs/video-js.swf";
                                videojs.addLanguage("en", {
                                    "Audio Player": "Audio Player",
                                    "Video Player": "Video Player",
                                    "Play": "Play",
                                    "Pause": "Pause",
                                    "Replay": "Replay",
                                    "Current Time": "Current Time",
                                    "Duration Time": "Duration Time",
                                    "Remaining Time": "Remaining Time",
                                    "Stream Type": "Stream Type",
                                    "LIVE": "LIVE",
                                    "Loaded": "Loaded",
                                    "Progress": "Progress",
                                    "Progress Bar": "Progress Bar",
                                    "progress bar timing: currentTime={1} duration={2}": "{1} of {2}",
                                    "Fullscreen": "Fullscreen",
                                    "Non-Fullscreen": "Non-Fullscreen",
                                    "Mute": "Mute",
                                    "Unmute": "Unmute",
                                    "Playback Rate": "Playback Rate",
                                    "Subtitles": "Subtitles",
                                    "subtitles off": "subtitles off",
                                    "Captions": "Captions",
                                    "captions off": "captions off",
                                    "Chapters": "Chapters",
                                    "Descriptions": "Descriptions",
                                    "descriptions off": "descriptions off",
                                    "Audio Track": "Audio Track",
                                    "Volume Level": "Volume Level",
                                    "You aborted the media playback": "You aborted the media playback",
                                    "A network error caused the media download to fail part-way.": "A network error caused the media download to fail part-way.",
                                    "The media could not be loaded, either because the server or network failed or because the format is not supported.": "The media could not be loaded, either because the server or network failed or because the format is not supported.",
                                    "The media playback was aborted due to a corruption problem or because the media used features your browser did not support.": "The media playback was aborted due to a corruption problem or because the media used features your browser did not support.",
                                    "No compatible source was found for this media.": "No compatible source was found for this media.",
                                    "The media is encrypted and we do not have the keys to decrypt it.": "The media is encrypted and we do not have the keys to decrypt it.",
                                    "Play Video": "Play Video",
                                    "Close": "Close",
                                    "Close Modal Dialog": "Close Modal Dialog",
                                    "Modal Window": "Modal Window",
                                    "This is a modal window": "This is a modal window",
                                    "This modal can be closed by pressing the Escape key or activating the close button.": "This modal can be closed by pressing the Escape key or activating the close button.",
                                    ", opens captions settings dialog": ", opens captions settings dialog",
                                    ", opens subtitles settings dialog": ", opens subtitles settings dialog",
                                    ", opens descriptions settings dialog": ", opens descriptions settings dialog",
                                    ", selected": ", selected",
                                    "captions settings": "captions settings",
                                    "subtitles settings": "subititles settings",
                                    "descriptions settings": "descriptions settings",
                                    "Text": "Text",
                                    "White": "White",
                                    "Black": "Black",
                                    "Red": "Red",
                                    "Green": "Green",
                                    "Blue": "Blue",
                                    "Yellow": "Yellow",
                                    "Magenta": "Magenta",
                                    "Cyan": "Cyan",
                                    "Background": "Background",
                                    "Window": "Window",
                                    "Transparent": "Transparent",
                                    "Semi-Transparent": "Semi-Transparent",
                                    "Opaque": "Opaque",
                                    "Font Size": "Font Size",
                                    "Text Edge Style": "Text Edge Style",
                                    "None": "None",
                                    "Raised": "Raised",
                                    "Depressed": "Depressed",
                                    "Uniform": "Uniform",
                                    "Dropshadow": "Dropshadow",
                                    "Font Family": "Font Family",
                                    "Proportional Sans-Serif": "Proportional Sans-Serif",
                                    "Monospace Sans-Serif": "Monospace Sans-Serif",
                                    "Proportional Serif": "Proportional Serif",
                                    "Monospace Serif": "Monospace Serif",
                                    "Casual": "Casual",
                                    "Script": "Script",
                                    "Small Caps": "Small Caps",
                                    "Reset": "Reset",
                                    "restore all settings to the default values": "restore all settings to the default values",
                                    "Done": "Done",
                                    "Caption Settings Dialog": "Caption Settings Dialog",
                                    "Beginning of dialog window. Escape will cancel and close the window.": "Beginning of dialog window. Escape will cancel and close the window.",
                                    "End of dialog window.": "End of dialog window."
                                });

                            });
                        });
                        ;

                        require(['jquery'], function ($) {
                            $('#single_select649d04d21b3d04').change(function () {
                                var ignore = $(this).find(':selected').attr('data-ignore');
                                if (typeof ignore === typeof undefined) {
                                    $('#single_select_f649d04d21b3d03').submit();
                                }
                            });
                        });
                        ;

                        require(
                                [
                                    'jquery',
                                    'core_message/message_popover'
                                ],
                                function (
                                        $,
                                        Popover
                                        ) {
                                    var toggle = $('#message-drawer-toggle-649d04d21df2d649d04d21b3d06');
                                    Popover.init(toggle);
                                });
                        ;

                        require(['jquery', 'message_popup/notification_popover_controller'], function ($, controller) {
                            var container = $('#nav-notification-popover-container');
                            var controller = new controller(container);
                            controller.registerEventListeners();
                            controller.registerListNavigationEventListeners();
                        });
                        ;

                        require(['jquery', 'core_message/message_drawer'], function ($, MessageDrawer) {
                            var root = $('#message-drawer-649d04d22a6ec649d04d21b3d016');
                            MessageDrawer.init(root);
                        });
                        ;

                        require(['theme_boost/loader']);
                        require(['theme_boost/drawer'], function (mod) {
                            mod.init();
                        });
                        ;
                        require(["core/notification"], function (amd) {
                            amd.init(2, []);
                        });
                        ;
                        require(["core/log"], function (amd) {
                            amd.setConfig({"level": "warn"});
                        });
                        ;
                        require(["core/page_global"], function (amd) {
                            amd.init();
                        });
                    });
                    //]]>
                </script>
                <script type="text/javascript">
                    //<![CDATA[
                    M.str = {"moodle": {"lastmodified": "Last modified", "name": "Name", "error": "Error", "info": "Information", "yes": "Yes", "no": "No", "cancel": "Cancel", "collapseall": "Collapse all", "expandall": "Expand all", "confirm": "Confirm", "areyousure": "Are you sure?", "closebuttontitle": "Close", "unknownerror": "Unknown error"}, "repository": {"type": "Type", "size": "Size", "invalidjson": "Invalid JSON string", "nofilesattached": "No files attached", "filepicker": "File picker", "logout": "Logout", "nofilesavailable": "No files available", "norepositoriesavailable": "Sorry, none of your current repositories can return files in the required format.", "fileexistsdialogheader": "File exists", "fileexistsdialog_editor": "A file with that name has already been attached to the text you are editing.", "fileexistsdialog_filemanager": "A file with that name has already been attached", "renameto": "Rename to \"{$a}\"", "referencesexist": "There are {$a} alias\/shortcut files that use this file as their source", "select": "Select"}, "admin": {"confirmdeletecomments": "You are about to delete comments, are you sure?", "confirmation": "Confirmation"}};
                    //]]>
                </script>
                <script type="text/javascript">
                    //<![CDATA[
                    (function () {
                        Y.use("moodle-filter_mathjaxloader-loader", function () {
                            M.filter_mathjaxloader.configure({"mathjaxconfig": "\nMathJax.Hub.Config({\n    config: [\"Accessible.js\", \"Safe.js\"],\n    errorSettings: { message: [\"!\"] },\n    skipStartupTypeset: true,\n    messageStyle: \"none\"\n});\n", "lang": "en"});
                        });
                        M.util.help_popups.setup(Y);
                        Y.use("moodle-course-categoryexpander", function () {
                            Y.Moodle.course.categoryexpander.init();
                        });
                        M.util.js_pending('random649d04d21b3d017');
                        Y.on('domready', function () {
                            M.util.js_complete("init");
                            M.util.js_complete('random649d04d21b3d017');
                        });
                    })();
                    //]]>
                </script>

            </section>
        </div>
    </div>
    <div class="copyleft"><span>©</span>Theme Trema</div>
</footer>

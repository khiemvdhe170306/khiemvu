<%-- 
    Document   : Sidebar
    Created on : 15-09-2023, 16:10:36
    Author     : admin
--%>


<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="https://cmshn.fpt.edu.vn/theme/styles.php/trema/1684246329_1/all">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<body>
    <div id="nav-drawer" data-region="drawer" class="d-print-none moodle-has-zindex " style="background-color: black" aria-hidden="false"
     tabindex="-1">
    <nav class="list-group">
        <a class="list-group-item list-group-item-action active" href="${pageContext.request.contextPath}/"
           data-key="home" data-isexpandable="0" data-indent="0" data-showdivider="0" data-type="1"
           data-nodetype="1" data-collapse="0" data-forceopen="1" data-isactive="1" data-hidden="0"
           data-preceedwithhr="0">
            <div class="m-l-0">
                <div class="media">
                    <span class="media-left">
                        <i class="icon fa fa-home fa-fw " aria-hidden="true"></i>
                    </span>
                    <span class="media-body font-weight-bold">Home</span>
                </div>
            </div>
        </a>
        <a class="list-group-item list-group-item-action active" href="blog"
           data-key="home" data-isexpandable="0" data-indent="0" data-showdivider="0" data-type="1"
           data-nodetype="1" data-collapse="0" data-forceopen="1" data-isactive="1" data-hidden="0"
           data-preceedwithhr="0">
            <div class="m-l-0">
                <div class="media">
                    <span class="media-left">
                        <i class="icon fa fa-pencil fa-fw " aria-hidden="true"></i>
                    </span>
                    <span class="media-body font-weight-bold">Blog</span>
                </div>
            </div>           

        </a>
        <a class="list-group-item list-group-item-action active" href="courselist"
           data-key="home" data-isexpandable="0" data-indent="0" data-showdivider="0" data-type="1"
           data-nodetype="1" data-collapse="0" data-forceopen="1" data-isactive="1" data-hidden="0"
           data-preceedwithhr="0">
            <div class="m-l-0">
                <div class="media">
                    <span class="media-left">
                        <i class="icon fa fa-book " aria-hidden="true"></i>
                    </span>
                    <span class="media-body font-weight-bold">Courses</span>
                </div>
            </div>
        </a>
<!--        <a class="list-group-item list-group-item-action active" href="lesson"
           data-key="home" data-isexpandable="0" data-indent="0" data-showdivider="0" data-type="1"
           data-nodetype="1" data-collapse="0" data-forceopen="1" data-isactive="1" data-hidden="0"
           data-preceedwithhr="0">
            <div class="m-l-0">
                <div class="media">
                    <span class="media-left">
                        <i class="icon fa fa-book " aria-hidden="true"></i>
                    </span>
                    <span class="media-body font-weight-bold">Lesson</span>
                </div>
            </div>
        </a>-->
        <c:if test="${sessionScope.account != null}">
            <div class="list-group-item" data-key="mycourses" data-isexpandable="1" data-indent="0" data-showdivider="0" data-type="0" data-nodetype="1" data-collapse="0" data-forceopen="1" data-isactive="0" data-hidden="0" data-preceedwithhr="0" data-parent-key="home">
                <div class="m-l-0">
                    <div class="media">
                        <span class="media-left">
                            <i class="icon fa fa-graduation-cap fa-fw " aria-hidden="true"></i>
                        </span>
                        <span class="media-body">My courses</span>
                    </div>
                </div>
            </div>
            <%--<c:forEach items="${sessionScope.listCourse}" var="item">
                <a class="list-group-item list-group-item-action " href="testcourse?courseId=${item.courseId}" data-key="4493" data-isexpandable="1" data-indent="1" data-showdivider="0" data-type="20" data-nodetype="1" data-collapse="0" data-forceopen="0" data-isactive="0" data-hidden="0" data-preceedwithhr="0" data-parent-key="mycourses">
                    <div class="m-l-1">
                        <div class="media">
                            <span class="media-left">
                                <i class="icon fa fa-graduation-cap fa-fw " aria-hidden="true"></i>
                            </span>
                            <span class="media-body ">${item.courseName}</span>
                        </div>
                    </div>
                </a>
            </c:forEach>    --%>  
            <c:forEach items="${sessionScope.account.course}" var="item">
                <a class="list-group-item list-group-item-action " href="usercoursedetail?courseId=${item.courseId}" data-key="4493" data-isexpandable="1" data-indent="1" data-showdivider="0" data-type="20" data-nodetype="1" data-collapse="0" data-forceopen="0" data-isactive="0" data-hidden="0" data-preceedwithhr="0" data-parent-key="mycourses">
                    <div class="m-l-1">
                        <div class="media">
                            <span class="media-left">
                                <i class="icon fa fa-graduation-cap fa-fw " aria-hidden="true"></i>
                            </span>
                            <span class="media-body ">${item.courseName}</span>
                        </div>
                    </div>
                </a>
            </c:forEach>   
            
        </c:if>
           
    </nav>
</div>
           
</body>

<%-- 
    Document   : coursejectdetail
    Created on : Sep 10, 2023, 2:39:03 PM
    Author     : Acer
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %> 
<!DOCTYPE html>
<html lang="en">
    <head>

        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" href="images/Quikk.png" type="image/png">
        <title>${requestScope.coursedetail.courseName}</title>
        <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,700" rel="stylesheet">

        <!-- Slick -->
        <link type="text/css" rel="stylesheet" href="css/slick.css"/>
        <link type="text/css" rel="stylesheet" href="css/slick-theme.css"/>

        <!-- nouislider -->
        <link type="text/css" rel="stylesheet" href="css/nouislider.min.css"/>

        <!-- Font Awesome Icon -->
        <link rel="stylesheet" href="css/font-awesome.min.css">

        <!-- Custom stlylesheet -->
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <script src="https://cdn.ckeditor.com/ckeditor5/39.0.2/classic/ckeditor.js"></script>
        <style>

            /*****************globals*************/
            body {
                font-family: 'open sans';
                overflow-x: hidden;
            }
            .amount-old {
                text-decoration: line-through;
            }

            img {
                max-width: 100%;
            }
            .rating-avg {
                font-size: 24px;
                font-weight: 700;
                margin-bottom: 15px;
            }

            .rating-avg .rating-stars {
                margin-left: 10px;
            }

            .rating-avg .rating-stars, .rating .rating-stars {
                display: inline-block;
            }

            .rating-avg .rating-stars>i, .rating .rating-stars>i {
                color: #E4E7ED;
            }

            .rating-avg .rating-stars>i.fa-star, .rating .rating-stars>i.fa-star {
                color: #D10024;
            }

            .rating li {
                margin: 5px 0px;
            }

            .rating .rating-progress {
                position: relative;
                display: inline-block;
                height: 9px;
                background-color: #E4E7ED;
                width: 120px;
                margin: 0px 10px;
                border-radius: 5px;
            }

            .rating .rating-progress>div {
                background-color: #D10024;
                position: absolute;
                left: 0;
                top: 0;
                bottom: 0;
                border-radius: 5px;
            }

            .rating .sum {
                display: inline-block;
                font-size: 12px;
                color: #8D99AE;
            }

            /*-- Reviews --*/

            .reviews li {
                position: relative;
                padding-left: 145px;
                margin-bottom: 30px;
            }

            .reviews .review-heading {
                position: absolute;
                width: 130px;
                left: 0;
                top: 0;
                height: 70px;
            }

            .reviews .review-body {
                min-height: 70px;
            }

            .reviews .review-heading .name {
                margin-bottom: 5px;
                margin-top: 0px;
            }

            .reviews .review-heading .date {
                color: #8D99AE;
                font-size: 10px;
                margin: 0;
            }

            .reviews .review-heading .review-rating {
                margin-top: 5px;
            }

            .reviews .review-heading .review-rating>i {
                color: #E4E7ED;
            }

            .reviews .review-heading .review-rating>i.fa-star {
                color: #D10024;
            }

            .reviews-pagination {
                text-align: center;
            }

            .reviews-pagination li {
                display: inline-block;
                width: 35px;
                height: 35px;
                line-height: 35px;
                text-align: center;
                background-color: #FFF;
                border: 1px solid #E4E7ED;
                -webkit-transition: 0.2s all;
                transition: 0.2s all;
            }

            .reviews-pagination li:hover {
                background-color: #E4E7ED;
                color: #D10024;
            }

            .reviews-pagination li.active {
                background-color: #D10024;
                border-color: #D10024;
                color: #FFF;
                cursor: default;
            }

            .reviews-pagination li a {
                display: block;
            }


            @media screen and (min-width: 997px) {
                .wrapper {
                    display: -webkit-box;
                    display: -webkit-flex;
                    display: -ms-flexbox;
                    display: flex;
                }
            }

            @-webkit-keyframes opacity {
                0% {
                    opacity: 0;
                    -webkit-transform: scale(3);
                    transform: scale(3);
                }
                100% {
                    opacity: 1;
                    -webkit-transform: scale(1);
                    transform: scale(1);
                }
            }

            @keyframes opacity {
                0% {
                    opacity: 0;
                    -webkit-transform: scale(3);
                    transform: scale(3);
                }
                100% {
                    opacity: 1;
                    -webkit-transform: scale(1);
                    transform: scale(1);
                }
            }

            /*# sourceMappingURL=style.css.map */

            body {
                background-color: #f5f5f5;
                font-family: Arial, sans-serif;
                margin: 0;
                padding: 20px;
            }

            .rating-avg {
                font-size: 24px;
                font-weight: 700;
                margin-bottom: 15px;
            }

            .rating-avg .rating-stars {
                margin-left: 10px;
            }

            .rating-avg .rating-stars, .rating .rating-stars {
                display: inline-block;
            }

            .rating-avg .rating-stars>i, .rating .rating-stars>i {
                color: #E4E7ED;
            }

            .rating-avg .rating-stars>i.fa-star, .rating .rating-stars>i.fa-star {
                color: #D10024;
            }

            .rating li {
                margin: 5px 0px;
            }

            .rating .rating-progress {
                position: relative;
                display: inline-block;
                height: 9px;
                background-color: #E4E7ED;
                width: 120px;
                margin: 0px 10px;
                border-radius: 5px;
            }

            .rating .rating-progress>div {
                background-color: #D10024;
                position: absolute;
                left: 0;
                top: 0;
                bottom: 0;
                border-radius: 5px;
            }

            .rating .sum {
                display: inline-block;
                font-size: 12px;
                color: #8D99AE;
            }

            /*-- Reviews --*/

            .reviews li {
                position: relative;
                padding-left: 145px;
                margin-bottom: 30px;
            }

            .reviews .review-heading {
                position: absolute;
                width: 130px;
                left: 0;
                top: 0;
                height: 70px;
            }

            .reviews .review-body {
                min-height: 70px;
            }

            .reviews .review-heading .name {
                margin-bottom: 5px;
                margin-top: 0px;
            }

            .reviews .review-heading .date {
                color: #8D99AE;
                font-size: 10px;
                margin: 0;
            }

            .reviews .review-heading .review-rating {
                margin-top: 5px;
            }

            .reviews .review-heading .review-rating>i {
                color: #E4E7ED;
            }

            .reviews .review-heading .review-rating>i.fa-star {
                color: #D10024;
            }

            .reviews-pagination {
                text-align: center;
            }

            .reviews-pagination li {
                display: inline-block;
                width: 35px;
                height: 35px;
                line-height: 35px;
                text-align: center;
                background-color: #FFF;
                border: 1px solid #E4E7ED;
                -webkit-transition: 0.2s all;
                transition: 0.2s all;
            }

            .reviews-pagination li:hover {
                background-color: #E4E7ED;
                color: #D10024;
            }

            .reviews-pagination li.active {
                background-color: #D10024;
                border-color: #D10024;
                color: #FFF;
                cursor: default;
            }

            .reviews-pagination li a {
                display: block;
            }

            /*-- Review Form --*/

            .review-form .input {
                margin-bottom: 15px;
            }

            .review-form .input-rating {
                margin-bottom: 15px;
            }

            .review-form .input-rating .stars {
                display: inline-block;
                vertical-align: top;
            }

            .review-form .input-rating .stars input[type="radio"] {
                display: none;
            }

            .review-form .input-rating .stars>label {
                float: right;
                cursor: pointer;
                padding: 0px 3px;
                margin: 0px;
            }

            .review-form .input-rating .stars>label:before {
                content: "\f006";
                font-family: FontAwesome;
                color: #E4E7ED;
                -webkit-transition: 0.2s all;
                transition: 0.2s all;
            }

            .review-form .input-rating .stars>label:hover:before, .review-form .input-rating .stars>label:hover~label:before {
                color: #D10024;
            }

            .review-form .input-rating .stars>input:checked label:before, .review-form .input-rating .stars>input:checked~label:before {
                content: "\f005";
                color: #D10024;
            }
            .icon-hover:hover {
                border-color: #3b71ca !important;
                background-color: white !important;
                color: #3b71ca !important;
            }

            .icon-hover:hover i {
                color: #3b71ca !important;
            }
        </style>

    </head>

    <body style="background-color: white;">
        <jsp:include page="Common/Header.jsp"></jsp:include>
            <!-- content -->
            <section class="py-5">
                <div class="container">
                    <div class="row gx-5">
                        <aside class="col-lg-6">
                            <div class="border rounded-4 mb-3 d-flex justify-content-center">
                                <a data-fslightbox="mygalley" class="rounded-4" target="_blank" data-type="image" href="https://bootstrap-ecommerce.com/bootstrap5-ecommerce/images/items/detail1/big.webp">
                                    <img style=" max-height: 100vh; margin: auto; height: 240px;" class="rounded-4 fit" src="${requestScope.coursedetail.thumbnail}" />
                            </a>
                        </div>
                    </aside>
                    <main class="col-lg-6">
                        <div class="ps-lg-3">
                            <h4 class="title text-dark">
                                ${requestScope.coursedetail.courseName}
                            </h4>
                            <div class="d-flex flex-row my-3">
                                <div class="text-warning mb-1 me-2">

                                    <div class="rating-stars">
                                        <c:forEach varStatus="loop" begin="1" end="${requestScope.totalrate}" step="1">
                                            <i class="fa fa-star"></i>
                                        </c:forEach> 
                                        <c:forEach varStatus="loop" begin="1" end="${5-requestScope.totalrate}" step="1">
                                            <i class="fa fa-star-o"></i>
                                        </c:forEach> 
                                    </div>
                                    <span class="ms-1">${requestScope.totalrate}.0</span>
                                </div>                      
                            </div>
                            <p>
                                ${requestScope.coursedetail.description}
                            </p>

                            <div class="row">
                                <dt class="col-3">Subject:</dt>
                                <dd class="col-9">Regular</dd>
                            </div>

                            <hr />
                            <c:if test="${requestScope.regiscourse eq null && requestScope.coursedetail.level && !sessionScope.account.vip}">
                                <div class="action">
                                    <form action="" method="get">
                                        <input type="hidden" name="courseId" value="${requestScope.coursedetail.courseId}">

                                        <button class="btn btn-primary shadow-0" <c:if test="${requestScope.regiscourse eq null && sessionScope.account.vip}">type="hidden"</c:if> type ="button"><a style="color: white;" href="membership">Buy Member</a></button>
                                        </form>                                
                                    </div>
                            </c:if>
                            <c:if test="${requestScope.regiscourse ne null}">
                                <div class="action">
                                    <form action="courseregistration" method="post">
                                        <input type="hidden" name="courseId" value="${requestScope.coursedetail.courseId}">
                                        <input class="btn btn-primary shadow-0" type="submit" value="Unenroll">
                                    </form> 
                                    <a href="usercoursedetail?courseId=${requestScope.coursedetail.courseId}" class="btn btn-warning shadow-0"> Start </a>
                                </div>
                            </c:if>
                            <c:if test="${(requestScope.regiscourse eq null && !requestScope.coursedetail.level) || (requestScope.regiscourse eq null && sessionScope.account.vip)}">
                                <div class="action">
                                    <form action="courseregistration" method="get">
                                        <input type="hidden" name="courseId" value="${requestScope.coursedetail.courseId}">
                                        <input class="btn btn-primary shadow-0" type="submit" value="Enroll Now">
                                    </form>                                
                                </div>
                            </c:if>
                            <c:if test="${sessionScope.account eq null || requestScope.regiscourse eq null}">
                                <a href="usercoursedetail?courseId=${requestScope.coursedetail.courseId}" class="btn btn-warning shadow-0"> Preview </a>
                            </c:if>
                        </div>
                    </main>
                </div>
            </div>
        </section>
        <!-- content -->

        <section class="bg-light border-top py-4">
            <div class="container">
                <div class="row gx-4">
                    <div class="col-lg-8 mb-4">
                        <div class="border rounded-2 px-3 py-2 bg-white">
                            <h2>Introduction Video</h2>
                            <iframe width="420" height="345" src="${requestScope.coursedetail.video}">
                            </iframe>
                            <!-- Pills content -->
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="px-0 border rounded-2 shadow-0">
                            <div class="card">
                                <div class="card-body">
                                    <h5 class="card-title">Related Courses</h5>

                                    <c:forEach items="${requestScope.course}" var="c">
                                        <c:if test="${c.subjectId eq requestScope.coursedetail.subjectId && c.courseId ne requestScope.coursedetail.courseId}">
                                            <div class="d-flex mb-3">
                                                <div class="info">
                                                    <a href="coursedetail?courseId=${c.courseId}&page=1" class="nav-link mb-1"> ${c.courseName} </a>
                                                </div>
                                            </div>
                                        </c:if>
                                    </c:forEach>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <h1 align="center" style="color: red;">RATE THIS COURSE NOW</h1>
        <div class="comment" style="margin:50px auto;width: 80%;margin-bottom: 0px">

            <div id="tab3" class="tab-pane fade in" >
                <div class="row">
                    <c:if test="${requestScope.rating ne null}">
                        <!-- Rating -->
                        <div class="col-md-3">
                            <div style="position: relative;bottom: 50px;"> <h2>Comment</h2></div>
                            <div id="rating">
                                <div class="rating-avg">
                                    <span>${requestScope.totalrate}.0</span>
                                    <div class="rating-stars">
                                        <c:forEach varStatus="loop" begin="1" end="${requestScope.totalrate}" step="1">
                                            <i class="fa fa-star"></i>
                                        </c:forEach> 
                                        <c:forEach varStatus="loop" begin="1" end="${5-requestScope.totalrate}" step="1">
                                            <i class="fa fa-star-o"></i>
                                        </c:forEach> 
                                    </div>
                                </div>
                                <ul class="rating">
                                    <li>
                                        <div class="rating-stars">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                        </div>
                                        <div class="rating-progress">
                                            <div style="width: ${requestScope.ratedetail[5]/fn:length(requestScope.rating)*100}%;"></div>
                                        </div>
                                        <span class="sum">${requestScope.ratedetail[5]/fn:length(requestScope.rating)*100}%</span>
                                    </li>
                                    <li>
                                        <div class="rating-stars">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                        </div>
                                        <div class="rating-progress">
                                            <div style="width: ${requestScope.ratedetail[4]/fn:length(requestScope.rating)*100}%;"></div>
                                        </div>
                                        <span class="sum">${requestScope.ratedetail[4]/fn:length(requestScope.rating)*100}%</span>
                                    </li>
                                    <li>
                                        <div class="rating-stars">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                        </div>
                                        <div class="rating-progress">
                                            <div style="width: ${requestScope.ratedetail[3]/fn:length(requestScope.rating)*100}%;"></div>
                                        </div>
                                        <span class="sum">${requestScope.ratedetail[3]/fn:length(requestScope.rating)*100}%</span>
                                    </li>
                                    <li>
                                        <div class="rating-stars">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                        </div>
                                        <div class="rating-progress">
                                            <div style="width: ${requestScope.ratedetail[2]/fn:length(requestScope.rating)*100}%;"></div>
                                        </div>
                                        <span class="sum">${requestScope.ratedetail[2]/fn:length(requestScope.rating)*100}%</span>
                                    </li>
                                    <li>
                                        <div class="rating-stars">
                                            <i class="fa fa-star"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                            <i class="fa fa-star-o"></i>
                                        </div>
                                        <div class="rating-progress">
                                            <div style="width: ${requestScope.ratedetail[1]/fn:length(requestScope.rating)*100}%;"></div>
                                        </div>
                                        <span class="sum">${requestScope.ratedetail[1]/fn:length(requestScope.rating)*100}%</span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <!-- /Rating -->

                        <!-- Reviews -->
                        <div class="col-md-9">
                            <div id="reviews">
                                <ul class="reviews" style="border:1px solid #888; padding: 5px;border-radius: 10px;margin-bottom: 10px;">
                                    <h3 style="color:red">${requestScope.error}</h3>
                                    <c:forEach items="${requestScope.rating}" var="r" begin="${param.page*4-4}" end="${requestScope.page*4-1}">
                                        <li >
                                            <div class="review-heading">
                                                <div style="display: flex;"> 
                                                    <c:if test="${r.a.accountinfo.fullname ne null}">
                                                        <h5 class="name" style="color:${r.a.email eq sessionScope.account.email?'red':''}">${r.a.email eq sessionScope.account.email?'Me':r.a.accountinfo.fullname}</h5></div> 
                                                    </c:if>
                                                    <c:if test="${r.a.accountinfo.fullname eq null}">
                                                    <h5 class="name" style="color:${r.a.email eq sessionScope.account.email?'red':''}">${r.a.email eq sessionScope.account.email?'Me':'Anonymous'}</h5></div> 
                                                </c:if>
                                            <p class="date">${r.date}</p>
                                            <div class="review-rating">
                                                <c:set var="a" value="${r.rating}"></c:set>
                                                <c:set var="b" value="${5 - r.rating}"></c:set>
                                                <c:forEach varStatus="loop" begin="1" end="${a}" step="1">
                                                    <i class="fa fa-star"></i>
                                                </c:forEach> 
                                                <c:forEach varStatus="loop" begin="1" end="${b}" step="1">
                                                    <i class="fa fa-star-o empty"></i>
                                                </c:forEach> 

                                            </div>
                                            </div>
                                            <div class="review-body">
                                                <p>${r.comment}</p>
                                            </div>
                                        </li>
                                    </c:forEach>
                                </ul>
                                <% int i=1; %>
                                <ul class="reviews-pagination">
                                    <c:if test="${param.page>1}"><li class="page-item "><a href="coursedetail?courseId=${requestScope.coursedetail.courseId}&page=${param.page-1}">Pre</a></li></c:if>
                                        <c:forEach items="${requestScope.rating}" var="o" step="4" >
                                            <c:set var="i" value="<%=i%>"/>
                                        <li<c:if test="${param.page eq i}"> class="page-item active"</c:if>>
                                            <a href="coursedetail?courseId=${requestScope.coursedetail.courseId}&page=<%=i%>" class="page-link"><%=i%></a></li> 
                                            <% i++;%>
                                        </c:forEach>
                                        <c:if test="${fn:length(requestScope.rating)>=(param.page*4+1)}">
                                        <li class="page-item"><a href="coursedetail?courseId=${requestScope.coursedetail.courseId}&page=${param.page+1}" class="page-link">Next</a></li></c:if>
                                    </ul>
                                </div>
                            </div>
                    </c:if>
                    <c:if test="${requestScope.rating eq null}">
                        <!-- Rating -->
                        <div class="col-md-3">

                            <div id="rating">
                                <div class="rating-avg">
                                    <span>0.0</span>
                                    <div class="rating-stars">
                                        <i class="fa fa-star-o"></i>
                                        <i class="fa fa-star-o"></i>
                                        <i class="fa fa-star-o"></i>
                                        <i class="fa fa-star-o"></i>
                                        <i class="fa fa-star-o"></i>
                                    </div>
                                </div>
                                <ul class="rating">
                                    <c:forEach var="i" begin="0" end="4">
                                        <li>
                                            <div class="rating-stars">
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                                <i class="fa fa-star"></i>
                                            </div>
                                            <div class="rating-progress">
                                                <div ></div>
                                            </div>
                                            <span class="sum">0</span>
                                        </li>
                                    </c:forEach>



                                </ul>
                            </div>
                        </div>
                        <!-- /Rating -->

                        <!-- Reviews -->
                        <div class="col-md-6">
                            <div id="reviews">
                                <ul class="reviews">
                                    <li><div class="review-heading">
                                            <h5 class="name"></h5>
                                            <p class="date"></p>
                                            <div class="review-rating">

                                            </div>
                                        </div>
                                        <div class="review-body">
                                            <h3>Be the first person to comment</h3>
                                        </div>
                                    </li>

                                </ul>
                            </div>
                        </div>
                    </c:if>





                    <!-- /Reviews -->
                    <!-- Review Form -->
                    <div class="col-md-10" style="margin: 50px 0px;">
                        <div id="review-form">
                            <form class="review-form" action="rating">
                                <input type="hidden" name="courseId" value="${param.courseId}">
                                <input type="hidden" name="aid" value="${sessionScope.account.aid}">
                                <textarea class="input" name="comment" id="editor" placeholder="Your Review"></textarea>   
                                <div class="input-rating">
                                    <span>Your Rating: </span>
                                    <div class="stars">
                                        <input id="star5" name="rating" value="5" type="radio" required><label for="star5"></label>
                                        <input id="star4" name="rating" value="4" type="radio" required><label for="star4"></label>
                                        <input id="star3" name="rating" value="3" type="radio" required><label for="star3"></label>
                                        <input id="star2" name="rating" value="2" type="radio" required><label for="star2"></label>
                                        <input id="star1" name="rating" value="1" type="radio" required><label for="star1"></label>
                                    </div>
                                </div>
                                <c:if test="${sessionScope.account eq null}">
                                    <a href="login">Login to comment</a>    
                                </c:if>
                                <c:if test="${sessionScope.account ne null}">
                                    <button type="submit" class="primary-btn">Submit</button>    
                                </c:if>

                            </form>
                        </div>
                    </div>
                    <!-- /Review Form -->

                </div>
            </div>




            <jsp:include page="Common/Sidebar.jsp"></jsp:include>
            <jsp:include page="Common/Footer.jsp"></jsp:include>
            <script>
                function openForm() {
                    document.getElementById("popupForm").style.display = "block";
                }
                $('.sum').each(function () {
                    const originalPercentage = parseFloat($(this).text());
                    const roundedPercentage = Math.round(originalPercentage);

                    $(this).text(roundedPercentage + '%');
                });
            </script>
            <script>
                ClassicEditor
                        .create(document.querySelector('#editor'))
                        .catch(error => {
                            console.error(error);
                        });
            </script>
    </body>
</html>



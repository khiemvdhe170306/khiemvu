<%-- 
    Document   : chapterManagement
    Created on : Sep 10, 2023, 1:46:27 PM
    Author     : khiem
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<!DOCTYPE html>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Chapter Management</title>
        <link rel="icon" href="images/Quikk.png" type="image/png">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <style>
            body {
                color: #566787;
                background: #f5f5f5;
                font-family: 'Varela Round', sans-serif;
                font-size: 13px;
            }
            .table-responsive {
                margin: 30px 0;
            }
            .table-wrapper {
                min-width: 1000px;
                background: #fff;
                padding: 20px 25px;
                border-radius: 3px;
                box-shadow: 0 1px 1px rgba(0,0,0,.05);
            }
            .table-title {
                padding-bottom: 15px;
                background: #299be4;
                color: #fff;
                padding: 16px 30px;
                margin: -20px -25px 10px;
                border-radius: 3px 3px 0 0;
            }
            .table-title h2 {
                margin: 5px 0 0;
                font-size: 24px;
            }
            .table-title .btn {
                color: #566787;
                float: right;
                font-size: 13px;
                background: #fff;
                border: none;
                min-width: 50px;
                border-radius: 2px;
                border: none;
                outline: none !important;
                margin-left: 10px;
            }
            .table-title .btn:hover, .table-title .btn:focus {
                color: #566787;
                background: #f2f2f2;
            }
            .table-title .btn i {
                float: left;
                font-size: 21px;
                margin-right: 5px;
            }
            .table-title .btn span {
                float: left;
                margin-top: 2px;
            }
            table.table tr th, table.table tr td {
                border-color: #e9e9e9;
                padding: 12px 15px;
                vertical-align: middle;
            }
            table.table tr th:first-child {
                width: 60px;
            }
            table.table tr th:last-child {
                width: 100px;
            }
            table.table-striped tbody tr:nth-of-type(odd) {
                background-color: #fcfcfc;
            }
            table.table-striped.table-hover tbody tr:hover {
                background: #f5f5f5;
            }
            table.table th i {
                font-size: 13px;
                margin: 0 5px;
                cursor: pointer;
            }
            table.table td:last-child i {
                opacity: 0.9;
                font-size: 22px;
                margin: 0 5px;
            }
            table.table td a {
                font-weight: bold;
                color: #566787;
                display: inline-block;
                text-decoration: none;
            }
            table.table td a:hover {
                color: #2196F3;
            }
            table.table td a.settings {
                color: #2196F3;
            }
            table.table td a.delete {
                color: #F44336;
            }
            table.table td i {
                font-size: 19px;
            }
            table.table .avatar {
                border-radius: 50%;
                vertical-align: middle;
                margin-right: 10px;
            }
            .status {
                font-size: 30px;
                margin: 2px 2px 0 0;
                display: inline-block;
                vertical-align: middle;
                line-height: 10px;
            }
            .text-success {
                color: #10c469;
            }
            .text-info {
                color: #62c9e8;
            }
            .text-warning {
                color: #FFC107;
            }
            .text-danger {
                color: #ff5b5b;
            }
            .pagination {
                float: right;
                margin: 0 0 5px;
            }
            .pagination li a {
                border: none;
                font-size: 13px;
                min-width: 30px;
                min-height: 30px;
                color: #999;
                margin: 0 2px;
                line-height: 30px;
                border-radius: 2px !important;
                text-align: center;
                padding: 0 6px;
            }
            .pagination li a:hover {
                color: #666;
            }
            .pagination li.active a, .pagination li.active a.page-link {
                background: #03A9F4;
            }
            .pagination li.active a:hover {
                background: #0397d6;
            }
            .pagination li.disabled i {
                color: #ccc;
            }
            .pagination li i {
                font-size: 16px;
                padding-top: 6px
            }
            .hint-text {
                float: left;
                margin-top: 10px;
                font-size: 13px;
            }
        </style>
        <script>
            $(document).ready(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>
    </head>
    <body>
        <c:if test = "${fn:contains(sessionScope.account.roleName.access, 'course')}">
            <jsp:include page="Common/Management.jsp"></jsp:include>
                <div class="container-xl">
                    <div class="table-responsive">
                        <div class="table-wrapper">
                            <div class="table-title">
                                <div class="row">
                                    <div class="col-sm-5">
                                        <h2>Chapter <b>Management</b></h2>
                                        <h5></h5>
                                        <form action="chaptermanagement" id="myForm">
                                            <input style="width: 400px" type="text" class="form-control" placeholder="Search by Name" name="search" id="pw">

                                        </form>
                                    </div>
                                    <div class="col-sm-7">
                                        <a href="crudchapter?aid=0&action=add" class="btn btn-secondary"><i class="material-icons">&#xE147;</i> <span>Add New Chapter</span></a>

                                    </div>

                                </div>
                            </div>
                            <div class="message">
                            <c:if test="${not empty msg}">
                                <div class="alert alert-info">${msg}</div>
                            </c:if>
                        </div>
                        <form action="chaptermanagement"  id="filter">
                            <!--                                filter by subject-->
                            Subject: <select name="subject" onchange="subjectSubmit()">
                                <option value="0" id="allSubject" <c:if test="${param.subject eq 0}">selected</c:if>>All subject </option>

                                <c:forEach items="${requestScope.subjectList}" var="c">  
                                    <option value="${c.subjectId}" <c:if test="${param.subject eq c.subjectId}">selected</c:if>>${c.title}</option>
                                </c:forEach>  
                            </select>

                            <!--                                filter by course-->
                            Course: <select name="course" onchange="courseSubmit()">
                                <option value="0" id="allCourse" <c:if test="${param.course eq 0}">selected</c:if>>All course </option>

                                <c:forEach items="${requestScope.courseList}" var="c">  
                                    <option value="${c.courseId}" <c:if test="${param.course eq c.courseId}">selected</c:if>>${c.courseName}</option>
                                </c:forEach>  
                            </select>


                            <p id="notAll" style="color:red;"></p>
                        </form>
                        <c:if test="${requestScope.ms ne null}">
                            <h5 style="color: green;text-align: center" class="alert alert-info">${requestScope.ms}</h5>
                        </c:if>
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>Chapter ID</th>
                                    <th>Description</th>
                                    <th>Chapter Name</th>						
                                    <th>Course Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${requestScope.chapterlist}" var="a" begin="${requestScope.page*6-6}" end="${requestScope.page*6-1}">
                                    <tr>
                                        <td>${a.chapId}</td>
                                        <td>${a.description} description</td>
                                        <td>${a.chapName}</td>                                                                
                                        <td>${a.course.courseName}</td> 
                                        <% int a=1; %>
                                        
                                        <td>
                                            <a href="crudchapter?action=update&ChapterId=${a.chapId}" class="settings" title="Edit" data-toggle="tooltip"><i class="material-icons">&#xE8B8;</i></a>
                                            <a href="crudchapter?action=delete&ChapterId=${a.chapId}" class="disabled" title="Delete" data-toggle="tooltip" onclick="return confirm('Are you sure you want to delete this Chapter?');"><i class="fa fa-trash-o"></i></a>

                                        </td>
                                    </tr>
                                </c:forEach>


                            </tbody>
                        </table>
                        <div class="clearfix">
                            <div class="hint-text">Showing <b>5</b> out of <b>25</b> entries</div>
                            <% int i=1; %>
                            <ul class="pagination">

                                <c:forEach items="${requestScope.chapterlist}" step="6">
                                    <c:set var="i" value="<%=i%>"/>
                                    <li class="page-item <c:if test="${requestScope.page eq i}">active</c:if>" ><a href="chaptermanagement?page=<%=i%><c:if test="${param.subject ne null}">&subject=</c:if>${param.subject}<c:if test="${param.course ne null}">&course=</c:if>${param.course}<c:if test="${param.search ne null}">&search=</c:if>${param.search}" class="page-link"><%=i%></a>
                                        </li> 
                                    <% i++;%>
                                </c:forEach>

                            </ul>
                        </div>
                    </div>
                </div>
            </div> 
        </c:if>

    </body>
    <script>
        function subjectSubmit()
        {
            if (document.getElementById("allSubject").selected) {
                document.getElementById("allCourse").selected = true;
            }

            document.getElementById("filter").submit();
        }
        function courseSubmit()
        {
            if (document.getElementById("allSubject").selected) {
                document.getElementById("notAll").innerHTML = "choose subject first";
                document.getElementById("allCourse").selected = true;
            } else {

                document.getElementById("filter").submit();}
        }
    </script>
</html>

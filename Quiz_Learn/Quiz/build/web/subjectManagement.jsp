<%-- 
    Document   : crudSubject
    Created on : Sep 14, 2023, 8:23:46 AM
    Author     : Acer
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Subjects Management</title>
        <link rel="icon" href="images/Quikk.png" type="image/png">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="./css/lessonManagement.css"><!-- comment -->

        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>


        <script>
            $(document).ready(function () {
                // Activate tooltip
                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>
    </head>
    <body>
        <c:if test = "${fn:contains(sessionScope.account.roleName.access, 'subject')}">
            <jsp:include page="Common/Management.jsp"></jsp:include>
                <div class="container-xl">
                    <div class="table-responsive">
                        <div class="table-wrapper">
                            <div class="table-title">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <h2>Manage <b>Subject</b></h2>
                                    </div>
                                    <div class="col-sm-6">
                                        <a href="crudsubject?action=add" class="btn btn-success"><i class="material-icons">&#xE147;</i> <span>Add New Subject</span></a>
                                        <!--                                <a href="#deleteEmployeeModal" class="btn btn-danger" data-toggle="modal"><i class="material-icons">&#xE15C;</i> <span>Delete</span></a>						-->
                                    </div>
                                </div>
                            </div>
                            <div class="message">
                            <c:if test="${not empty msg}">
                                <div class="alert alert-info">${msg}</div>
                            </c:if>
                        </div>
                        <table class="table table-striped table-hover">
                            <thead>
                                <tr>                               
                                    <th>ID</th>
                                    <th>Title</th>
                                    <th>Description</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${requestScope.subject}" var="c" begin="${requestScope.page*5-5}" end="${requestScope.page*5-1}">
                                    <tr>

                                        <td>${c.subjectId}</td>

                                        <td>${c.title}</td>
                                        <td>${c.description}</td>
                                        <td>
                                            <a href="crudsubject?subjectId=${c.subjectId}&action=update" class="settings" title="Edit" data-toggle="tooltip"><i class="material-icons">&#xE8B8;</i></a>
                                            <a href="crudsubject?subjectId=${c.subjectId}&action=delete" class="disabled" title="Delete" data-toggle="tooltip" onclick="return confirm('Are you sure you want to delete this subject?');"><i class="material-icons">&#xE5C9;</i></a>
                                        </td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                        <div class="clearfix">
                            <% int i=1; %>
                            <ul class="pagination">
                                <c:forEach items="${requestScope.subject}" step="5">
                                    <c:set var="i" value="<%=i%>"/>
                                    <li class="page-item <c:if test="${requestScope.page eq i}">active</c:if>" ><a href="subjectmanagement?page=<%=i%>" class="page-link"><%=i%></a>
                                        </li> 
                                    <% i++;%>
                                </c:forEach>
                            </ul>
                        </div>
                    </div>
                </div>        
            </div>           
        </c:if>
    </body>
</html>

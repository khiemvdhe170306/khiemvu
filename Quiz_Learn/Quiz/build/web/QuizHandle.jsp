<%-- 
    Document   : home
    Created on : Sep 20, 2023, 7:46:52 PM
    Author     : khiem
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!doctype html>
<html>

    <head>
        <meta charset='utf-8'>
        <meta name='viewport' content='width=device-width, initial-scale=1'>
        <title>Snippet - BBBootstrap</title>
        <link rel="icon" href="images/Quikk.png" type="image/png">
        <link href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css' rel='stylesheet'>
        <link href='#' rel='stylesheet'>
        <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
        <style>
            html * {

                font-family: 'Montserrat', sans-serif;
            }
            ::-webkit-scrollbar {
                width: 8px;
            }

            /* Track */
            ::-webkit-scrollbar-track {
                background: #f1f1f1;
            }

            /* Handle */
            ::-webkit-scrollbar-thumb {
                background: #888;
            }

            /* Handle on hover */
            ::-webkit-scrollbar-thumb:hover {
                background: #555;
            }

            body {
                background-color: #616161;
            }

            label.btn {
                padding: 18px 60px;
                white-space: normal;
                -webkit-transform: scale(1.0);
                -moz-transform: scale(1.0);
                -o-transform: scale(1.0);
                -webkit-transition-duration: .3s;
                -moz-transition-duration: .3s;
                -o-transition-duration: .3s
            }

            label.btn:hover {
                text-shadow: 0 3px 2px rgba(0, 0, 0, 0.4);
                -webkit-transform: scale(1.1);
                -moz-transform: scale(1.1);
                -o-transform: scale(1.1)
            }

            label.btn-block {
                text-align: left;
                position: relative
            }
/*            mau cua mui ten*/
            label .btn-label {
                position: absolute;
                left: 0;
                top: 0;
                display: inline-block;
                padding: 0 10px;
                background: #000000;
                height: 100%
            }

            label .glyphicon {
                top: 34%
            }

            .element-animation1 {
                animation: animationFrames ease .8s;
                animation-iteration-count: 1;
                transform-origin: 50% 50%;
                -webkit-animation: animationFrames ease .8s;
                -webkit-animation-iteration-count: 1;
                -webkit-transform-origin: 50% 50%;
                -ms-animation: animationFrames ease .8s;
                -ms-animation-iteration-count: 1;
                -ms-transform-origin: 50% 50%
            }

            .element-animation2 {
                animation: animationFrames ease 1s;
                animation-iteration-count: 1;
                transform-origin: 50% 50%;
                -webkit-animation: animationFrames ease 1s;
                -webkit-animation-iteration-count: 1;
                -webkit-transform-origin: 50% 50%;
                -ms-animation: animationFrames ease 1s;
                -ms-animation-iteration-count: 1;
                -ms-transform-origin: 50% 50%
            }

            .element-animation3 {
                animation: animationFrames ease 1.2s;
                animation-iteration-count: 1;
                transform-origin: 50% 50%;
                -webkit-animation: animationFrames ease 1.2s;
                -webkit-animation-iteration-count: 1;
                -webkit-transform-origin: 50% 50%;
                -ms-animation: animationFrames ease 1.2s;
                -ms-animation-iteration-count: 1;
                -ms-transform-origin: 50% 50%
            }

            .element-animation4 {
                animation: animationFrames ease 1.4s;
                animation-iteration-count: 1;
                transform-origin: 50% 50%;
                -webkit-animation: animationFrames ease 1.4s;
                -webkit-animation-iteration-count: 1;
                -webkit-transform-origin: 50% 50%;
                -ms-animation: animationFrames ease 1.4s;
                -ms-animation-iteration-count: 1;
                -ms-transform-origin: 50% 50%
            }

            @keyframes animationFrames {
                0% {
                    opacity: 0;
                    transform: translate(-1500px, 0px)
                }

                60% {
                    opacity: 1;
                    transform: translate(30px, 0px)
                }

                80% {
                    transform: translate(-10px, 0px)
                }

                100% {
                    opacity: 1;
                    transform: translate(0px, 0px)
                }
            }

            @-webkit-keyframes animationFrames {
                0% {
                    opacity: 0;
                    -webkit-transform: translate(-1500px, 0px)
                }

                60% {
                    opacity: 1;
                    -webkit-transform: translate(30px, 0px)
                }

                80% {
                    -webkit-transform: translate(-10px, 0px)
                }

                100% {
                    opacity: 1;
                    -webkit-transform: translate(0px, 0px)
                }
            }

            .modal-header {
                background-color: transparent;
                color: inherit
            }

            .modal-body {
                min-height: 300px
            }
/*            mau luc chua an*/
            .btn-danger{

                background-color: white !important

            }
/*            mau luc an vao*/
            .btn-danger:hover, .btn-danger:focus, .btn-danger.focus, .btn-danger:active, .btn-danger.active, .open>.dropdown-toggle.btn-danger{
                background-color: #d1e8ff !important;
            }
            div #1{
                display: none !important;
            }
            .border{
                border:none;
            }
        </style>
    </head>

    <body className='snippet-body' onload="functionToCall()">
        <jsp:include page="Common/Header.jsp"></jsp:include>
        <%int a=1,answer=1;%>


        <div class="container" style="margin-top:100px">
            <div style="display: flex;flex-direction: row;align-items: center;justify-content: center">
                <img src="https://img.freepik.com/premium-vector/timer-clock-stopwatch-countdown-timer-symbol-icon-label-cooking-symbols-fast-time-stop-watch_476325-1096.jpg" width="50px" alt="alt"/>
                <h1 id="demo"></h1>

            </div>
            <h3 id="warning" style="color:#ff0000"></h3>

            <form action="quizreview" id="quizResult">
                <input type="hidden" name="name" id="duration" value="${requestScope.duration}">
                <c:forEach items="${requestScope.quizList}" var="q" >
                    <div id="<%=a%>">
                        <div class="modal-content" >

                            <div class="modal-header">
                                <h3>Question <%=a%>: ${q.qContent} </h3>
                                <h5>Choose ${q.numberOfTrue} option</h5>
                            </div>
                            <div class="modal-body">
                                <div class="col-xs-3 5"> </div>
                                <div class="quiz" id="quiz" data-toggle="buttons">
                                    <input type="hidden" name="question<%=a%>" value="${q.questionId}">
                                    <c:forEach items="${q.listAnswer}" var="a">
                                        
                                        
                                        <label class="element-animation1 btn btn-lg btn-danger btn-block" style="color:black;border: #888 solid thin ;border-radius: 2px" ><span class="btn-label"><i
                                                    class="glyphicon glyphicon-chevron-right" > </i></span> <input type="checkbox"
                                                 name="Answer${a.aid}"  class="question<%=a%>"  value="${a.aid}" >  ${a.content}</label>  

                                        <%answer++;%>
                                    </c:forEach>
                                </div>
                            </div>
                        </div>
                    </div>
                    <%a++;%>
                </c:forEach>
                <input type="hidden" name="lengthAnswer" value="<%=answer%>">
            </form>
            <%int i=1;%>
            <div class="modal-content" style="display: flex;align-items: center;justify-content:  center;height: 100px;flex-direction: row">
                <button onclick="Back()" class="border" onclick="changeAnswer()" id="Back" style="width:40px; height: 50px;border-radius: 10px;margin:10px;color:black" >
                    Back
                </button>
                <c:forEach items="${requestScope.quizList}">
                    <input type="button" name="page" class="border" id="page<%=i%>" value="<%=i%>" onclick="myFunction(<%=i%>)" style="width:40px; height: 50px;border-radius: 10px;background-color: white;margin:10px;color:black"  />
                    <%i++;%>
                </c:forEach>
                <button onclick="Next()" class="border" onclick="changeAnswer()" id="Next" style="width:40px; height: 50px;border-radius: 10px;margin:10px;color:black" >
                    Next
                </button>
            </div>
            <input type="button" style="width:180px; height: 50px;border-radius: 2px;background-color: whitesmoke;margin:10px;color:black;margin: 20px 500px;font-size: medium" onclick="submitForm()" value="Submit All And Finish"  />
        </div>

        <input type="hidden" name="list" id="list" value="${fn:length(requestScope.quizList)-1}">
        <input type="hidden" name="page" id="page">
        <jsp:include page="Common/QuizSider.jsp"></jsp:include>

        <jsp:include page="Common/Footer.jsp"></jsp:include>
    </body>
    <script type='text/javascript' src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js'></script>


    <script>
                function submitForm() {
                    document.getElementById("quizResult").submit();

                }

                var y = parseInt(document.getElementById("list").value) + 2;
                function functionToCall() {

                    for (let ai = 1; ai < y; ai++) {
                        var z = document.getElementById(ai.toString());
                        z.style.display = "none";
                    }
                    document.getElementById("1").style.display = "block";
                    document.getElementById("page").value = 1;
                }
                function changeAnswer()
                {
                    for (let a = 1; a < y; a++) {
                        var elements = document.getElementsByClassName("question" + a);
                        var test=0;
                        for (var i = 0; i < elements.length; i++) {
                            if (elements[i].checked)
                            {
                                test++;
                            }
                        }
                        if(test>0){
                            document.getElementById("page"+a).style.backgroundColor = "gray";
                            continue;
                        }
                        document.getElementById("page"+a).style.backgroundColor = "white";
                    }



                }
                function myFunction(i) {
                    functionToCall();
                    document.getElementById("1").style.display = "none";
                    var x = document.getElementById(i.toString());
                    x.style.display = "block";
                    document.getElementById("page").value = i;
                    changeAnswer();
                }
                function Back() {
                    var page = document.getElementById("page").value;
                    if (parseInt(page) === 1) {
                        myFunction(y - 1);
                    } else {
                        myFunction(parseInt(page) - 1);
                    }
                }
                function Next() {
                    var page = document.getElementById("page").value;
                    if (parseInt(page) === (y - 1)) {
                        myFunction(1);
                    } else {
                        myFunction(parseInt(page) + 1);
                    }
                }
                var duration_raw = document.getElementById("duration").value;
                let duration = parseInt(duration_raw);
                // Update the count down every 1 second
                var x = setInterval(function () {
                    var hours = Math.floor((duration % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
                    var minutes = Math.floor((duration % (1000 * 60 * 60)) / (1000 * 60));
                    var seconds = Math.floor((duration % (1000 * 60)) / 1000);

                    // Output the result in an element with id="demo"
                    document.getElementById("demo").innerHTML = minutes + "m " + seconds + "s ";
                    //document.getElementById("demo").innerHTML=duration;
                    if (minutes < 2)
                    {
                        document.getElementById("warning").innerHTML = "Timer is gonna be up !You should check your answer again before submitting";
                        document.getElementById("demo").style.color = "#ff0000";
                    }

                    // If the count down is over, write some text 
                    if (duration < 0) {
                        clearInterval(x);
                        //document.getElementById("demo").innerHTML = "EXPIRED";
                        document.getElementById("quizResult").submit();
                    }
                    duration -= 1000;
                }, 1000);

    </script>



</body>

</html>
<!doctype html>
<html class="no-js" lang="zxx">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Courses | Education</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="manifest" href="site.webmanifest">
        <link rel="shortcut icon" type="images/x-icon" href="images/favicon.ico">

        <!-- CSS here -->
        <link rel="stylesheet" href="css/bootstrap.min.css">


        <link rel="stylesheet" href="css/magnific-popup.css">
        <link rel="stylesheet" href="css/fontawesome-all.min.css">
        <link rel="stylesheet" href="css/themify-icons.css">
        <link rel="stylesheet" href="css/slick2.css">
        <link rel="stylesheet" href="css/nice-select.css">

        <link rel="stylesheet" href="css/style1.css">
        <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <script>
            const initSlider = () => { // khoi tao
                const imageList = document.querySelector(".slider-wrapper .image-list");
                const slideButtons = document.querySelectorAll(".slider-wrapper .slide-button");
                const sliderScrollbar = document.querySelector(".container .slider-scrollbar");
                const scrollbarThumb = sliderScrollbar.querySelector(".scrollbar-thumb");
                const maxScrollLeft = imageList.scrollWidth - imageList.clientWidth;

                scrollbarThumb.addEventListener("mousedown", (e) => { // keo thanh truot
                    const startX = e.clientX; // bat dau keo
                    const thumbPosition = scrollbarThumb.offsetLeft; // vi tri ban dau thanh truot
                    const maxThumbPosition = sliderScrollbar.getBoundingClientRect().width - scrollbarThumb.offsetWidth; // max 

                    const handleMouseMove = (e) => { // di chuot de chuyen
                        const deltaX = e.clientX - startX; // vi tri bat dau keo
                        const newThumbPosition = thumbPosition + deltaX; // vi tri moi


                        const boundedPosition = Math.max(0, Math.min(maxThumbPosition, newThumbPosition)); // ko < 0 va ko > max
                        const scrollPosition = (boundedPosition / maxThumbPosition) * maxScrollLeft; // ko vu?t qua max

                        scrollbarThumb.style.left = `${boundedPosition}px`; // cap nhat vi tri
                        imageList.scrollLeft = scrollPosition; // vi tri cuon cua slider
                    }


                    const handleMouseUp = () => {
                        document.removeEventListener("mousemove", handleMouseMove);
                        document.removeEventListener("mouseup", handleMouseUp);
                    }


                    document.addEventListener("mousemove", handleMouseMove);
                    document.addEventListener("mouseup", handleMouseUp);
                });


                slideButtons.forEach(button => { //nhan nut truot
                    button.addEventListener("click", () => {
                        const direction = button.id === "prev-slide" ? -1 : 1; // xd h??ng cu?n
                        const scrollAmount = imageList.clientWidth * direction;
                        imageList.scrollBy({left: scrollAmount, behavior: "smooth"});// cu?n h�nh ?nh
                    });
                });


                const handleSlideButtons = () => { // hien thi nut chuy?n
                    slideButtons[0].style.display = imageList.scrollLeft <= 0 ? "none" : "flex";
                    slideButtons[1].style.display = imageList.scrollLeft >= maxScrollLeft ? "none" : "flex";
                }


                const updateScrollThumbPosition = () => { //cap nhat vi tri truot
                    const scrollPosition = imageList.scrollLeft;
                    const thumbPosition = (scrollPosition / maxScrollLeft) * (sliderScrollbar.clientWidth - scrollbarThumb.offsetWidth);
                    scrollbarThumb.style.left = `${thumbPosition}px`;
                }


                imageList.addEventListener("scroll", () => {
                    updateScrollThumbPosition();
                    handleSlideButtons();
                });
            }
            document.addEventListener("DOMContentLoaded", function () {
                let currentSlide = 0;
                const slides = document.querySelectorAll('.slider-item');

                function showSlides(index) {
                    slides.forEach((slide, i) => {
                        slide.style.display = i >= index && i < index + 3 ? 'block' : 'none';
                    });
                }

                function nextSlides() {
                    currentSlide = (currentSlide + 3) % slides.length;
                    showSlides(currentSlide);
                }

                function prevSlides() {
                    currentSlide = (currentSlide - 3 + slides.length) % slides.length;
                    showSlides(currentSlide);
                }

                document.getElementById('prev-slide').addEventListener('click', prevSlides);
                document.getElementById('next-slide').addEventListener('click', nextSlides);

                setInterval(nextSlides, 3000); // Chuy?n slide m?i 5 gi�y
            });
            window.addEventListener("resize", initSlider);
            window.addEventListener("load", initSlider);
        </script>

        <style>
            img{
                max-height: 500px;
                max-width: 100%;
            }

            .slider-wrapper {
                position: relative;
                width : 100%;
            }

            .slider-wrapper .slide-button {
                position: absolute;
                top: 50%;
                outline: none;
                border: none;
                height: 50px;
                width: 50px;
                z-index: 5;
                color: #fff;
                display: flex;
                cursor: pointer;
                font-size: 2.2rem;
                background: #000;
                align-items: center;
                justify-content: center;
                border-radius: 50%;
                transform: translateY(-50%);
            }

            .slider-wrapper .slide-button:hover {
                background: #404040;
            }

            .slider-wrapper .slide-button#prev-slide {
                left: -25px;
                display: none;
            }

            .slider-wrapper .slide-button#next-slide {
                right: -25px;
            }

            .slider-wrapper .image-list {
                display: grid;
                grid-template-columns: repeat(12, 1fr);
                gap: 18px;
                font-size: 1.5;
                list-style: none;
                margin-bottom: 30px;
                overflow-x: auto;
                scrollbar-width: none;
                width : 100% !important;
            }
            body{
                width:100% !important;
                background-color: #dd5f4 !important;
            }
            .slider-wrapper .image-list::-webkit-scrollbar {
                display: none;

            }

            .slider-wrapper .image-list .image-item {
                width: 100%;
                height: 500px;
                object-fit: cover;
            }
            .container .slider-scrollbar {
                height: 24px;
                width: 100%;
                display: flex;
                align-items: center;
            }

            .slider-scrollbar .scrollbar-track {
                background: #ccc;
                width: 100%;
                height: 2px;
                display: flex;
                align-items: center;
                border-radius: 4px;
                position: relative;
            }

            .slider-scrollbar:hover .scrollbar-track {
                height: 4px;
            }

            .slider-scrollbar .scrollbar-thumb {
                position: absolute;
                background: #000;
                top: 0;
                bottom: 0;
                width: 50%;
                height: 100%;
                cursor: grab;
                border-radius: inherit;
            }

            .slider-scrollbar .scrollbar-thumb:active {
                cursor: grabbing;
                height: 8px;
                top: -2px;
            }

            .slider-scrollbar .scrollbar-thumb::after {
                content: "";
                position: absolute;
                left: 0;
                right: 0;
                top: -10px;
                bottom: -10px;
            }
            @media only screen and (max-width: 1023px) {
                .slider-wrapper .slide-button {
                    display: none !important;
                }

                .slider-wrapper .image-list {
                    gap: 10px;
                    margin-bottom: 15px;
                    scroll-snap-type: x mandatory;
                }



                .slider-scrollbar .scrollbar-thumb {
                    width: 20%;
                }
            }
            .slider-item {
                border: 2px solid #ccc;
                padding: 10px;
                margin: 10px;
                border-radius: 10px;
                background-color: #f9f9f9;
                height: 575px;
                width : 575px;

            }
            button{
                padding:none !important;
            }
        </style>
    </head>

    <body>


        <jsp:include page="Common/Header.jsp"></jsp:include>
            <main>
                <!--? slider Area Start-->


                <!-- Courses area start -->
                <div class="courses-area section-padding40 fix">

                    <h1 class="text-center" style="font-size: 5em;font-family: Serif;">Favourite courses</h1>

                    <div class="slider-wrapper">
                        <button id="prev-slide" class="slide-button material-symbols-rounded">
                            <
                        </button>
                        <ul class="image-list">
                        <c:forEach items="${requestScope.sliders}" var="s" varStatus="status">
                            <a href="slider-details?id=${status.index + 1}">
                                <div class="slider-item">
                                    <img class="image-item" src="${s.image}" alt="slider image" />
                                    <p style="text-align: center;font-size: 1.5em;padding-top: 10px">${s.title}</p>
                                </div>
                            </a>
                        </c:forEach>
                    </ul>


                    <button id="next-slide" class="slide-button material-symbols-rounded">
                        >
                    </button>
                </div>


            </div>
            <!-- ? services-area -->
            <div class="services-area">
                <div class="container">
                    <div class="row justify-content-sm-center">
                        <div class="col-lg-4 col-md-6 col-sm-8">
                            <div class="single-services mb-30">
                                <div class="features-icon">
                                    <img src="img/icon/icon1.svg" alt="">
                                </div>
                                <div class="features-caption">
                                    <h3>Many interesting course</h3>
                                    <p>The automated process all your website tasks.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-8">
                            <div class="single-services mb-30">
                                <div class="features-icon">
                                    <img src="img/icon/icon2.svg" alt="">
                                </div>
                                <div class="features-caption">
                                    <h3>Expert instructors</h3>
                                    <p>The automated process all your website tasks.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-8">
                            <div class="single-services mb-30">
                                <div class="features-icon">
                                    <img src="img/icon/icon3.svg" alt="">
                                </div>
                                <div class="features-caption">
                                    <h3>Rating, Grading</h3>
                                    <p>You can see your rating and grading due for your process</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Courses area End -->
            <!--? About Area-1 Start -->
            <section class="about-area1 fix pt-10">
                <div class="support-wrapper align-items-center">
                    <div class="left-content1">
                        <div class="about-icon">
                            <img src="img/icon/about.svg" alt="">
                        </div>
                        <!-- section tittle -->
                        <div class="section-tittle section-tittle2 mb-55">
                            <div class="front-text">
                                <h2 class="">Learn new skills online with top educators</h2>
                                <p>The objective of this website is to provide an interactive platform for users to enhance their knowledge through quizzes.</p>
                            </div>
                        </div>
                        <div class="single-features">
                            <div class="features-icon">
                                <img src="img/icon/right-icon.svg" alt="">
                            </div>
                            <div class="features-caption">
                                <p>Users can choose from a wide selection of quizzes designed to cater to different learning interests and levels.</p>
                            </div>
                        </div>
                        <div class="single-features">
                            <div class="features-icon">
                                <img src="img/icon/right-icon.svg" alt="">
                            </div>
                            <div class="features-caption">
                                <p>This website offers a diverse range of quizzes covering various topics and subjects.</p>
                            </div>
                        </div>

                        <div class="single-features">
                            <div class="features-icon">
                                <img src="img/icon/right-icon.svg" alt="">
                            </div>
                            <div class="features-caption">
                                <p>Each quiz is carefully crafted to challenge and engage learners, providing valuable insights their understanding of the chosen subject matter.</p>
                            </div>
                        </div>
                    </div>
                    <div class="right-content1">
                        <!-- img -->
                        <div class="right-img">
                           <img src="images/Quikk.png" alt="Mieu ta anh" style="width: 500px; height: 500px;">

                        </div>
                    </div>
                </div>
            </section>
            <!-- About Area End -->

            <!--? About Area-3 Start -->
            <section class="about-area3 fix">
                <div class="support-wrapper align-items-center">
                    <div class="right-content3">
                        <!-- img -->
                        <div class="right-img">
                            <img src="img/gallery/about3.png" alt="">
                        </div>
                    </div>
                    <div class="left-content3">
                        <!-- section tittle -->
                        <div class="section-tittle section-tittle2 mb-20">
                            <div class="front-text">
                                <h2 class="">Learner outcomes on courses you will take</h2>
                            </div>
                        </div>
                        <div class="single-features">
                            <div class="features-icon">
                                <img src="img/icon/right-icon.svg" alt="">
                            </div>
                            <div class="features-caption">
                                <p>It sets clear expectations and goals, allowing learners to track their progress and measure their achievements.</p>
                            </div>
                        </div>
                        <div class="single-features">
                            <div class="features-icon">
                                <img src="img/icon/right-icon.svg" alt="">
                            </div>
                            <div class="features-caption">
                                <p>It ensures that the time and energy invested in the course are directed towards achieving specific learning goals.</p>
                            </div>
                        </div>
                        <div class="single-features">
                            <div class="features-icon">
                                <img src="img/icon/right-icon.svg" alt="">
                            </div>
                            <div class="features-caption">
                                <p>It empowers learners to actively engage with the material, seek help when needed, and take ownership of their educational journey.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- About Area End -->
            <!--? Team -->

            <!-- Services End -->
            <!--? About Area-2 Start -->
            <section class="about-area2 fix pb-padding">
                <div class="support-wrapper align-items-center">
                    <div class="right-content2">
                        <!-- img -->
                        <div class="right-img">
                            <img src="img/gallery/about2.png" alt="">
                        </div>
                    </div>
                    <div class="left-content2">
                        <!-- section tittle -->
                        <div class="section-tittle section-tittle2 mb-20">
                            <div class="front-text">
                                <h2 class="">Take the next step
                                    toward your personal
                                    and professional goals
                                    with us.</h2>
                                <p>The automated process all your website tasks. Discover tools and techniques to engage effectively with vulnerable children and young people.</p>
                                <a href="http://localhost:9999/Quiz/membership" class="btn">Join now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- About Area End -->
        </main>



        <!-- Scroll Up -->

        <!-- JS here -->


        <!-- Jquery Plugins, main Jquery -->	


    </body>
    <jsp:include page="Common/Sidebar.jsp"></jsp:include>
    <jsp:include page="Common/Footer.jsp"></jsp:include>
</html>
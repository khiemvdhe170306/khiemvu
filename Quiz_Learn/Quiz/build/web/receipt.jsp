<%-- 
    Document   : receipt
    Created on : Oct 24, 2023, 1:30:47 PM
    Author     : Acer
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Receipt</title>
        <link rel="icon" href="images/Quikk.png" type="image/png">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"/>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"/>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css"/>
        <style type="text/css">
            .body-main {
                background: #ffffff;
                border-bottom: 15px solid #1E1F23;
                border-top: 15px solid #1E1F23;
                margin-top: 30px;
                margin-bottom: 30px;
                padding: 40px 30px !important;
                position: relative ;
                box-shadow: 0 1px 21px #808080;
                font-size:10px;



            }

            .main thead {
                background: #1E1F23;
                color: #fff;
            }
            .img{
                height: 100px;
            }
            h1{
                text-align: center;
            }


        </style>
    </head>
    <body>
        <jsp:include page="Common/Header.jsp"></jsp:include>
            <div class="container" style="background-image: url('images/Quikk.png');height: 100%">
                <div class="container" style="margin-top: 100px;">
                    <div class="row">
                        <div class="col-md-6 col-md-offset-3 body-main">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-4">
                                        <img class="img" alt="Invoce Template" src="images/Quikk.png" />
                                    </div>
                                    <div class="col-md-8 text-right">
                                        <h4 style="color: #F81D2D;"><strong>Quikk</strong></h4>
                                        
                                    </div>
                                </div>
                                <br />
                                <div class="row">
                                    <div class="col-md-12 text-center">
                                        <h2>Receipt</h2>
                                        <h5>Payment Success! You are one of our members</h5>
                                    </div>
                                </div>
                                <br />
                                <div>
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th><h5>Pack</h5></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td class="col-md-9">${transaction.description}</td>
                                            
                                        </tr>                                      
                                        <tr style="color: #F81D2D;">
                                            <td class="text-right"><h4><strong>Total:</strong></h4></td>
                                            <td class="text-left"><h4><strong><i class="fas fa-dollar-sign" area-hidden="true"></i> ${transaction.amount.total} USD </strong></h4></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div>
                                <div class="col-md-12">
                                    <a href="home">Back to Home</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <jsp:include page="Common/Sidebar.jsp"></jsp:include>
        <jsp:include page="Common/Footer.jsp"></jsp:include>
    </body>
</html>

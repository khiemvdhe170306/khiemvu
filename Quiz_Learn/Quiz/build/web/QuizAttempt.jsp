<%-- 
    Document   : chapterManagement
    Created on : Sep 10, 2023, 1:46:27 PM
    Author     : khiem
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>${sessionScope.quiz.title}</title>
        <link rel="icon" href="images/Quikk.png" type="image/png">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <style>
            html * {

                font-family: 'Montserrat', sans-serif;

            }
            body {
                color: #566787;
                background: #f5f5f5;
/*                font-family: 'Varela Round', sans-serif;
                font-size: 13px;*/
            }
            .table-responsive {
                margin: 30px 0;
            }
            .table-wrapper {
                min-width: 1000px;
                background: #fff;
                padding: 20px 25px;
                border-radius: 3px;
                box-shadow: 0 1px 1px rgba(0,0,0,.05);
            }
            .table-title {
                padding-bottom: 15px;
                background: #299be4;
                color: #fff;
                padding: 16px 30px;
                margin: -20px -25px 10px;
                border-radius: 3px 3px 0 0;
            }
            .table-title h2 {
                margin: 5px 0 0;
                font-size: 24px;
            }
            .table-title .btn {
                color: #566787;
                float: right;
                font-size: 13px;
                background: #fff;
                border: none;
                min-width: 50px;
                border-radius: 2px;
                border: none;
                outline: none !important;
                margin-left: 10px;
            }
            .table-title .btn:hover, .table-title .btn:focus {
                color: #566787;
                background: #f2f2f2;
            }
            .table-title .btn i {
                float: left;
                font-size: 21px;
                margin-right: 5px;
            }
            .table-title .btn span {
                float: left;
                margin-top: 2px;
            }
            table.table tr th, table.table tr td {
                border-color: #e9e9e9;
                padding: 12px 15px;
                vertical-align: middle;
            }
            table.table tr th:first-child {
                width: 60px;
            }
            table.table tr th:last-child {
                width: 100px;
            }
            table.table-striped tbody tr:nth-of-type(odd) {
                background-color: #fcfcfc;
            }
            table.table-striped.table-hover tbody tr:hover {
                background: #f5f5f5;
            }
            table.table th i {
                font-size: 13px;
                margin: 0 5px;
                cursor: pointer;
            }
            table.table td:last-child i {
                opacity: 0.9;
                font-size: 22px;
                margin: 0 5px;
            }
            table.table td a {
                font-weight: bold;
                color: #566787;
                display: inline-block;
                text-decoration: none;
            }
            table.table td a:hover {
                color: #2196F3;
            }
            table.table td a.settings {
                color: #2196F3;
            }
            table.table td a.delete {
                color: #F44336;
            }
            table.table td i {
                font-size: 19px;
            }
            table.table .avatar {
                border-radius: 50%;
                vertical-align: middle;
                margin-right: 10px;
            }
            .status {
                font-size: 30px;
                margin: 2px 2px 0 0;
                display: inline-block;
                vertical-align: middle;
                line-height: 10px;
            }
            .text-success {
                color: #10c469;
            }
            .text-info {
                color: #62c9e8;
            }
            .text-warning {
                color: #FFC107;
            }
            .text-danger {
                color: #ff5b5b;
            }
            .pagination {
                float: right;
                margin: 0 0 5px;
            }
            .pagination li a {
                border: none;
                font-size: 13px;
                min-width: 30px;
                min-height: 30px;
                color: #999;
                margin: 0 2px;
                line-height: 30px;
                border-radius: 2px !important;
                text-align: center;
                padding: 0 6px;
            }
            .pagination li a:hover {
                color: #666;
            }
            .pagination li.active a, .pagination li.active a.page-link {
                background: #03A9F4;
            }
            .pagination li.active a:hover {
                background: #0397d6;
            }
            .pagination li.disabled i {
                color: #ccc;
            }
            .pagination li i {
                font-size: 16px;
                padding-top: 6px
            }
            .hint-text {
                float: left;
                margin-top: 10px;
                font-size: 13px;
            }
            .button:hover {background-color: #cccccc}
        </style>
        <script>
            $(document).ready(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>
    </head>
    <body>
        <jsp:include page="Common/Header.jsp"></jsp:include>
            
            <div class="container-xl">
                <div style="height: 100px">
                    
                </div>
                <div class="table-responsive" style="margin:100px 0px;">
                    <nav aria-label="breadcrumb" class="main-breadcrumb" style="margin-bottom: 15px;font-size: large">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="home">Home</a></li>
                        <li class="breadcrumb-item"><a href="usercoursedetail?courseId=${sessionScope.course.courseId}">${sessionScope.course.courseName}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Quiz : ${sessionScope.quiz.title}</li>
                    </ol>
                    </nav>
                    <div class="row" style="display: flex;justify-content:center;align-items: center;flex-direction: column;margin-bottom: 50px">
                        <h4>Time Limit :${sessionScope.quiz.duration} minutes</h4>
                        <h4>Number of question: ${sessionScope.quiz.numberOfQuestion}</h4>
                    </div>
                        
                    <div class="table-wrapper">
                        <div class="table-title">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h1>History of attempt</h1>
                                <h5></h5>
                                
                            </div>
                            

                        </div>
                    </div>

                    <h5 style="color: green;text-align: center">${requestScope.ms}</h5>
                    <table class="table table-striped table-hover">
                        <thead>
                            <tr>
                                
                                <th>Attempt</th>
                                <th>Start</th>
                                <th>End</th>
                                <th>Time taken</th>
                                <th>True Question</th>
                                <th>Grade/10</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach items="${requestScope.attemptList}" var="a" >
                                <tr>
                                    
                                    <td>${a.attempt}</td>
                                    <td><fmt:formatDate pattern = "yyyy-MM-dd HH:mm" value = "${a.start}" /></td>
                                    <td><fmt:formatDate pattern = "yyyy-MM-dd HH:mm" value = "${a.end}" /></td>
                                    <td>${a.time}</td>
                                    <td>${a.mark}</td> 
                                    <td>${a.grade}</td>
                                    <td><a href="historyreview?historyId=${a.id}">Review</a></td>


                                </tr>
                                
                            </c:forEach>


                        </tbody>
                    </table>
                    
                    
                </div>
                   <c:if test="${requestScope.high >0}"> <h3 style="margin: 20px 0px;float:left">Highest Score: ${requestScope.high}</h3>
                   
                   <c:if test="${requestScope.high >=7}"><h2 style="margin: 20px 0px;float:right;color: green">PASS</h2></c:if>
                   </c:if>
            </div>
                    
                   
                        <form action="quiz" style="display: flex;justify-content:center;align-items: center;flex-direction: column;margin-bottom: 50px;">
                            <input style="width: 175px;height: 45px;border-radius:5px;background-color: #999999" type="submit" value="Attempt Quiz" />
                        </form> 
                    
                    
        </div> 
                 
        <jsp:include page="Common/QuizSider.jsp"></jsp:include>
        <jsp:include page="Common/Footer.jsp"></jsp:include>
       
    </body>
     
    <script>

    </script>
</html>

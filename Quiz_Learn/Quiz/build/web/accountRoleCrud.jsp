<%-- 
    Document   : crudAccount
    Created on : Sep 10, 2023, 6:11:07 PM
    Author     : khiem
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">


        <title>Profile</title>
        <link rel="icon" href="images/Quikk.png" type="image/png">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.ckeditor.com/ckeditor5/39.0.2/classic/ckeditor.js"></script>
        <style type="text/css">
            body{
                margin-top:20px;
                color: #1a202c;
                text-align: left;
                background-color: #e2e8f0;
            }
            .main-body {
                padding: 15px;
            }
            .card {
                box-shadow: 0 1px 3px 0 rgba(0,0,0,.1), 0 1px 2px 0 rgba(0,0,0,.06);
            }

            .card {
                position: relative;
                display: flex;
                flex-direction: column;
                min-width: 0;
                word-wrap: break-word;
                background-color: #fff;
                background-clip: border-box;
                border: 0 solid rgba(0,0,0,.125);
                border-radius: .25rem;
            }

            .card-body {
                flex: 1 1 auto;
                min-height: 1px;
                padding: 1rem;
            }

            .gutters-sm {
                margin-right: -8px;
                margin-left: -8px;
            }

            .gutters-sm>.col, .gutters-sm>[class*=col-] {
                padding-right: 8px;
                padding-left: 8px;
            }
            .mb-3, .my-3 {
                margin-bottom: 1rem!important;
            }

            .bg-gray-300 {
                background-color: #e2e8f0;
            }
            .h-100 {
                height: 100%!important;
            }
            .shadow-none {
                box-shadow: none!important;
            }

        </style>
    </head>
    <body>
        <div class="container">
            <div class="main-body">

                <nav aria-label="breadcrumb" class="main-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="home">Home</a></li>
                        <li class="breadcrumb-item"><a href="rolemanagement">Role Management</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Blog Edit</li>
                    </ol>
                </nav>
                <div class="row gutters-sm">
                    <div class="col-md-8">
                        <div class="card mb-3">
                            <div class="card-body">
                                <c:if test="${requestScope.action eq 'update'}">
                                    <form action="crudrole" method="post">
                                        <input type="hidden" name="action" value="update">
                                        <input type="hidden" name="roleid" value="${requestScope.role.role}">

                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Role Name update</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">

                                                <input type="text" name="name" pattern="(?=.*[a-zA-Z])([^!@#$%&])+" value="${requestScope.role.name}" 
                                                       oninvalid="setCustomValidity('Name only contain alphabet character')" oninput="setCustomValidity('')"       required>
                                            </div>
                                        </div>
                                        <c:if test="${requestScope.ms ne null}">
                                            <p style="color: green;text-align: center" class="alert alert-info">${requestScope.ms}</p>
                                        </c:if>
                                        <hr>

                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Description</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">

                                                <input type="text" name="description" pattern="(?=.*[a-zA-Z0-9])([^!@#$%&])+" value="${requestScope.role.description}" 
                                                       oninvalid="setCustomValidity('description not null')" oninput="setCustomValidity('')"       required>
                                            </div>
                                        </div>
                                        <hr>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Authority</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <input type="checkbox" id="1" name="blog" value="blog" <c:if test = "${fn:contains(requestScope.role.access, 'blog')}">checked</c:if> >
                                                    <label for="1">Manage Blog</label><br>
                                                    <input type="checkbox" id="2" name="subject" value="subject" <c:if test = "${fn:contains(requestScope.role.access, 'subject')}">checked</c:if>>
                                                    <label for="2">Manage Subject</label><br>
                                                    <input type="checkbox" id="3" name="course" value="course" <c:if test = "${fn:contains(requestScope.role.access, 'course')}">checked</c:if>>
                                                    <label for="3">Manage Course</label><br>
                                                    <input type="checkbox" id="4" name="quiz" value="quiz" <c:if test = "${fn:contains(requestScope.role.access, 'quiz')}">checked</c:if>>
                                                    <label for="4">Manage Quiz</label><br>
                                                    <input type="checkbox" id="5" name="account" value="account" <c:if test = "${fn:contains(requestScope.role.access, 'account')}">checked</c:if>>
                                                    <label for="5">Manage Account</label><br>
                                                    <input type="checkbox" id="6" name="slider" value="slider" <c:if test = "${fn:contains(requestScope.role.access, 'slider')}">checked</c:if>>
                                                    <label for="6">Manage Slider</label><br>
                                                    <input type="checkbox" id="7" name="slider" value="membership" <c:if test = "${fn:contains(requestScope.role.access, 'membership')}">checked</c:if> >                                                
                                                <label for="7">Manage Membership</label><br>
                                                    <br>                                          
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="row">
                                                <div class="col-sm-12">
                                                    <input type="submit" value="Save">
                                                </div>
                                            </div>
                                        </form>
                                </c:if>
                                <c:if test="${requestScope.action eq 'add'}">
                                    <form action="crudrole" method="post">
                                        <input type="hidden" name="action" value="add">

                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Role Name</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <input type="text" name="name" pattern="(?=.*[a-zA-Z])([^!@#$%&])+" value="${requestScope.name}" 
                                                       oninvalid="setCustomValidity('Name only contain alphabet character')" oninput="setCustomValidity('')"       required>                                           
                                            </div>
                                        </div>
                                        <c:if test="${requestScope.ms ne null}">
                                            <p style="color: green;text-align: center" class="alert alert-info">${requestScope.ms}</p>
                                        </c:if>
                                        <hr>

                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Description</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">

                                                <input type="text" name="description" pattern="(?=.*[a-zA-Z0-9])([^!@#$%&])+" value="${requestScope.description}" 
                                                       oninvalid="setCustomValidity('desctiption not null')" oninput="setCustomValidity('')"       required>    
                                            </div>
                                        </div>
                                        <hr>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Authority</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <input type="checkbox" id="1" name="blog" value="blog" >                                               
                                                <label for="1">Manage Blog</label><br>
                                                <input type="checkbox" id="2" name="subject" value="subject" >
                                                <label for="2">Manage Subject</label><br>
                                                <input type="checkbox" id="3" name="course" value="course" >                                                
                                                <label for="3">Manage Course</label><br>
                                                <input type="checkbox" id="4" name="quiz" value="quiz" >                                              
                                                <label for="4">Manage Quiz</label><br>
                                                <input type="checkbox" id="5" name="account" value="account" >
                                                <label for="5">Manage Account</label><br>
                                                <input type="checkbox" id="6" name="slider" value="slider" >                                                
                                                <label for="6">Manage Slider</label><br>
                                                <input type="checkbox" id="7" name="slider" value="membership" >                                                
                                                <label for="7">Manage Membership</label><br>
                                                <br>                                          
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <input type="submit" value="Save">
                                            </div>
                                        </div>
                                    </form>
                                </c:if>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.bundle.min.js"></script>


</html>

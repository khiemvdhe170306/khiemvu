<%-- 
    Document   : Header
    Created on : 15-09-2023, 16:01:17
    Author     : admin
--%>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" type="text/css" href="https://cmshn.fpt.edu.vn/theme/yui_combo.php?rollup/3.17.2/yui-moodlesimple-min.css">
<link rel="stylesheet" type="text/css" href="https://cmshn.fpt.edu.vn/theme/styles.php/trema/1684246329_1/all">

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<nav class="fixed-top navbar navbar-light bg-white navbar-expand moodle-has-zindex" style="font-size: 20px !important; background-color: #f8f7f1 !important;">

    <div data-region="drawer-toggle" class="d-inline-block mr-3">
        <button aria-expanded="true" aria-controls="nav-drawer" type="button"
                class="btn nav-link float-sm-left mr-1 btn-secondary" data-action="toggle-drawer" data-side="left"
                data-preference="drawer-open-nav"><i class="icon fa fa-bars fa-fw " aria-hidden="true"></i><span
                class="sr-only">Side panel</span></button>
    </div>

    <a href="home" class="navbar-brand has-logo">
        <span class="logo d-inline">
                                <img src="images/Quikk.png" style="width: 35px;height: 35px"  alt="alt"/>

        </span>
    </a>
    <c:if test="${sessionScope.account != null}">
        <ul class="navbar-nav d-none d-md-flex">
            <!-- custom_menu -->
            <li class="dropdown nav-item">
                <a class="dropdown-toggle nav-link" id="drop-down-649d04d21cf01649d04d21b3d02"
                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" href="#"
                   aria-controls="drop-down-menu-649d04d21cf01649d04d21b3d02">
                    <i class="fa fa-graduation-cap mr-1" aria-hidden="true"></i> My courses
                </a>
                <div class="dropdown-menu" role="menu" id="drop-down-menu-649d04d21cf01649d04d21b3d02"
                     aria-labelledby="drop-down-649d04d21cf01649d04d21b3d02">
                    <%--<c:forEach items="${sessionScope.account.listCourse}" var="item">
                        <a class="dropdown-item" role="menuitem" href="${pageContext.request.contextPath}/course?id=${item.id}">${item.name}</a>
                    </c:forEach>--%>
                </div>
            </li>
            <!-- page_heading_menu -->

        </ul>
    </c:if>
    <ul class="nav navbar-nav ml-auto">
        <div class="d-none d-lg-block">

        </div>
        <!-- user_menu -->
        <li class="nav-item d-flex align-items-center">
            <div class="usermenu">
                <c:if test="${sessionScope.account != null}">
                    <div class="action-menu moodle-actionmenu nowrap-items d-inline" id="action-menu-1"
                         data-enhance="moodle-core-actionmenu">

                        <div class="menubar d-flex " id="action-menu-1-menubar" role="menubar">
                            <div class="action-menu-trigger">
                                <div class="dropdown">
                                    <a href="#" tabindex="0" class=" dropdown-toggle icon-no-margin" id="dropdown-1"
                                       aria-label="User menu" data-toggle="dropdown" role="button"
                                       aria-haspopup="true" aria-expanded="false"
                                       aria-controls="action-menu-1-menu">

                                        <span class="userbutton"><span class="usertext mr-1">${sessionScope.account.email}</span><span class="avatars"><span
                                                    class="avatar current"><img
                                                        <c:if test="${sessionScope.account.image eq null}">
                                                            src="${sessionScope.account.url}"
                                                        </c:if>
                                                        <c:if test="${sessionScope.account.image ne null}">
                                                            src="data:image/jpg;base64,${sessionScope.account.image}" 
                                                        </c:if>

                                                        class="userpicture" width="35" height="35"
                                                        role="presentation" aria-hidden="true"></span></span></span>

                                        <b class="caret"></b>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right menu  align-tr-br"
                                         id="action-menu-1-menu" data-rel="menu-content"
                                         aria-labelledby="action-menu-toggle-1" role="menu" data-align="tr-br">
                                        <div class="dropdown-divider" role="presentation"><span
                                                class="filler">&nbsp;</span></div>
                                        <a href="viewuser"
                                           class="dropdown-item menu-action" role="menuitem"
                                           data-title="profile,moodle" aria-labelledby="actionmenuaction-2">
                                            <i class="icon fa fa-user fa-fw " aria-hidden="true"></i>
                                            <span class="menu-action-text" id="actionmenuaction-2">
                                                Profile
                                            </span>
                                        </a>
                                        <div class="dropdown-divider" role="presentation"><span
                                                class="filler">&nbsp;</span></div>
                                        <a href="userdashboard"
                                           class="dropdown-item menu-action" role="menuitem"
                                           data-title="profile,moodle" aria-labelledby="actionmenuaction-2">
                                            <i class="icon fa fa-user fa-fw " aria-hidden="true"></i>
                                            <span class="menu-action-text" id="actionmenuaction-2">
                                                User Dashboard
                                            </span>
                                        </a>
                                        <c:if test = "${fn:contains(sessionScope.account.roleName.access, 'blog')}">
                                            <div class="dropdown-divider" role="presentation"><span
                                                    class="filler">&nbsp;</span></div>
                                            <a href="blogmanagement"
                                               class="dropdown-item menu-action" role="menuitem"
                                               data-title="profile,moodle" aria-labelledby="actionmenuaction-2">
                                                <i class="icon fa fa-pencil fa-fw " aria-hidden="true"></i>
                                                <span class="menu-action-text" id="actionmenuaction-2">
                                                    Blog Management
                                                </span>
                                            </a>
                                        </c:if>
                                        <c:if test = "${fn:contains(sessionScope.account.roleName.access, 'quiz')}">
                                            <div class="dropdown-divider" role="presentation"><span
                                                    class="filler">&nbsp;</span></div>
                                            <a href="quizmanagement"
                                               class="dropdown-item menu-action" role="menuitem"
                                               data-title="profile,moodle" aria-labelledby="actionmenuaction-2">
                                                <i class="icon fa fa-book  " aria-hidden="true"></i>
                                                <span class="menu-action-text" id="actionmenuaction-2">
                                                    Quiz Management
                                                </span>
                                            </a>
                                        </c:if>
                                        <c:if test = "${fn:contains(sessionScope.account.roleName.access, 'course')}">
                                            <div class="dropdown-divider" role="presentation"><span
                                                    class="filler">&nbsp;</span></div>
                                            <a href="coursemanagement"
                                               class="dropdown-item menu-action" role="menuitem"
                                               data-title="profile,moodle" aria-labelledby="actionmenuaction-2">
                                                <i class="icon fa fa-graduation-cap fa-fw  " aria-hidden="true"></i>
                                                <span class="menu-action-text" id="actionmenuaction-2">
                                                    Course Management
                                                </span>
                                            </a>
                                        </c:if>
                                        <c:if test = "${fn:contains(sessionScope.account.roleName.access, 'account')}">
                                            <div class="dropdown-divider" role="presentation"><span
                                                    class="filler">&nbsp;</span></div>
                                            <a href="accountmanagement"
                                               class="dropdown-item menu-action" role="menuitem"
                                               data-title="profile,moodle" aria-labelledby="actionmenuaction-2">
                                                <i class="icon fa fa-user fa-fw " aria-hidden="true"></i>
                                                <span class="menu-action-text" id="actionmenuaction-2">
                                                    Account Management
                                                </span>
                                            </a>
                                        </c:if>
                                        
                                        <c:if test = "${fn:contains(sessionScope.account.roleName.access, 'membership')}">
                                            <div class="dropdown-divider" role="presentation"><span
                                                    class="filler">&nbsp;</span></div>
                                            <a href="regismembermanagement"
                                               class="dropdown-item menu-action" role="menuitem"
                                               data-title="profile,moodle" aria-labelledby="actionmenuaction-2">
                                                <i class="icon fa fa-user fa-fw " aria-hidden="true"></i>
                                                <span class="menu-action-text" id="actionmenuaction-2">
                                                    Membership Management
                                                </span>
                                            </a>
                                        </c:if>

                                        <div class="dropdown-divider" role="presentation"><span
                                                class="filler">&nbsp;</span></div>
                                        <a href="logout"
                                           class="dropdown-item menu-action" role="menuitem"
                                           data-title="logout,moodle" aria-labelledby="actionmenuaction-6">
                                            <i class="icon fa fa-sign-out fa-fw " aria-hidden="true"></i>
                                            <span class="menu-action-text" id="actionmenuaction-6">
                                                Log out
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>

                        </div>

                    </div>
                </c:if>
                <c:if test="${sessionScope.account == null}">
                    <span class="login">You are not logged in. (<a href="login">Log in</a>)</span>
                </c:if>
            </div>
        </li>
    </ul>
    <!-- search_box -->
</nav>
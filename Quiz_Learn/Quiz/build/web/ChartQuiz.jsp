<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <!-- Boxicons -->
        <link href='https://unpkg.com/boxicons@2.0.9/css/boxicons.min.css' rel='stylesheet'>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
        <!-- My CSS -->
        <link rel="stylesheet" href="css/userLevel.css">
        <style>
            .chart-container {
                display: flex;
                flex-direction: column;
                align-items: center;
                width: 400px; /* or whatever width you want */
                margin: auto; /* centers the container horizontally */
            }
        </style>
        <style>
            .chart-container2 {
                display: flex;
                flex-direction: column;
                align-items: center;
                width: 1200px; /* or whatever width you want */
                margin: auto; /* centers the container horizontally */
            }
        </style>

        <meta charset="UTF-8">
        <title>Chart Quiz</title>

    </head>
    <body>
        <section id="sidebar">
            <a href="#" class="brand">
                <i class='bx bxs-smile'></i>
                <span class="text">User Hub</span>
            </a>
            <ul class="side-menu top">
                <li >
                    <a href="userdashboard?">
                        <i class='bx bxs-dashboard' ></i>
                        <span class="text">Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class='bx bxs-shopping-bag-alt' ></i>
                        <span class="text">Home</span>
                    </a>
                </li>
                <c:if test = "${fn:contains(sessionScope.account.roleName.access, 'membership')}">
                    <li>
                        <a href="salechart">
                            <i class='bx bxs-doughnut-chart' ></i>
                            <span class="text">Sale Chart</span>
                        </a>
                    </li>
                </c:if>
                <c:if test = "${fn:contains(sessionScope.account.roleName.access, 'account')}">
                    <li >
                        <a href="userchart">
                            <i class='bx bxs-message-dots' ></i>
                            <span class="text">User Chart</span>
                        </a>
                    </li>
                </c:if>
                <c:if test = "${fn:contains(sessionScope.account.roleName.access, 'quiz')}">
                    <li class="active">
                        <a href="ChartQuiz.jsp">
                            <i class='bx bxs-message-dots' ></i>
                            <span class="text">Website Chart</span>
                        </a>
                    </li>
                </c:if>
            </ul>
        </section>
        <section id="content">
            <main>
                <div class="head-title">
                    <div class="left">
                        <h1>Dashboard</h1>
                        <ul class="breadcrumb">
                            <li>
                                <a href="#">Dashboard</a>
                            </li>
                            <li><i class='bx bx-chevron-right' ></i></li>
                            <li>
                                <a class="active" href="#">Home</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="table-data">
                    <div class="chart-container">
                        <canvas id="postByCourseChart" width="400" height="100"></canvas>
                        <span>Posts by Course</span>
                    </div>
                    <div class="chart-container">
                        <canvas id="SliderByCourseChart" width="400" height="100"></canvas>
                        <span>Slider By Course</span>
                    </div>


                    <div class="chart-container">
                        <canvas id="CourseBySubjectData" width="400" height="200"></canvas>
                        <span>Course by Subject</span>
                    </div>
                </div>
                <div class="table-data">

                    <!--                    <div class="chart-container2">
                                            <canvas id="lessonByCourseChart" style="width:100%;max-width:800px;font-size: smaller"></canvas> 
                                            <span>Lesson By Course</span>
                                        </div>-->
                    <div class="chart-container2">

                        <canvas id="ChapterByCourseData" style="width:100%;max-width:800px;font-size: smaller"></canvas> 
                        <span>Chapter By Course </span>
                    </div>
                    <div class="chart-container2">                
                        <canvas id="lessonByChapterChart" style="width:100%;max-width:1500px;font-size: smaller"></canvas> 
                        <span>Lesson By Chapter</span>
                    </div> 
                    <div class="chart-container2">                
                        <canvas id="QuizByChapterData" style="width:100%;max-width:1500px;font-size: smaller"></canvas> 
                        <span>Quiz By Chapter</span>
                    </div>  
                </div>

            </main>
        </section>
        <script type="text/javascript">
            // wat for loading page 
            document.addEventListener('DOMContentLoaded', function () {

                //setting http request 
                var xhr = new XMLHttpRequest();
                xhr.open('GET', 'ChartQuizData', true);
                xhr.setRequestHeader('Content-Type', 'application/json');
                // wait for request to server 
                xhr.onload = function () {
                    //check status response
                    if (xhr.status >= 200 && xhr.status < 300) {
                        var response = JSON.parse(xhr.responseText);
                        // Chart 1: Lessons by Course
                        // checking data
                        if (response.lessonByCourseData) {
                            //shadow copy data
                            const data = {
                                ... response.lessonByCourseData
                            }

                            //draw chart
                            var lessonByCourseCanvas = document.getElementById('lessonByCourseChart');
                            var lessonByCourseChart = new Chart(lessonByCourseCanvas, {
                                type: 'bar',
                                data: response.lessonByCourseData,
                                //add option
                                options: {
                                    legend: {display: false},
                                    title: {
                                        display: true,
                                        text: "Title here"
                                    },
                                    scales: {
                                        yAxes: [{ticks: {min: 0}}],
                                    },
                                    tooltips: {
                                        mode: 'label',
                                        callbacks: {
                                            label: function (tooltipItem, data) {
                                                var dataset = data.datasets[tooltipItem.datasetIndex];
                                                var meta = dataset._meta[Object.keys(dataset._meta)[0]];
                                                var total = meta.total;
                                                var currentValue = dataset.data[tooltipItem.index];
                                                var percentage = parseFloat((currentValue / total * 100).toFixed(1));
                                                return currentValue + ' (' + percentage + '%)';
                                            },
                                            title: function (tooltipItem, data) {
                                                return data.labels[tooltipItem[0].index];
                                            }
                                        }
                                    }, animation: {
                                        duration: 1000, // Độ dài của hiệu ứng (ms)
                                        easing: 'easeOutBounce' // Loại hiệu ứng (có thể thay đổi)
                                    }, plugins: {
                                        colorschemes: {
                                            scheme: 'brewer.RdPu4' // Thay đổi màu sắc
                                        }
                                    }

                                }

                            });
                        } else {
                            console.log("Error : response data is required")
                        }



                        // Chart 3: Posts by Course
                        if (response.postByCourseData) {
                            const data = {
                                ... response.postByCourseData
                            };
                            var postByCourseCanvas = document.getElementById('postByCourseChart');
                            var postByCourseChart = new Chart(postByCourseCanvas, {
                                type: 'pie',
                                data,
                                options: {

                                    legend: {
                                        display: false
                                    },
                                    title: {
                                        display: true,
                                        text: "Title here"
                                    },
                                    scales: {
                                        yAxes: [{ticks: {min: 0}}],
                                    },
                                    tooltips: {
                                        mode: 'label',
                                        callbacks: {
                                            label: function (tooltipItem, data) {
                                                var dataset = data.datasets[tooltipItem.datasetIndex];
                                                var meta = dataset._meta[Object.keys(dataset._meta)[0]];
                                                var total = meta.total;
                                                var currentValue = dataset.data[tooltipItem.index];
                                                var percentage = parseFloat((currentValue / total * 100).toFixed(1));
                                                return currentValue + ' (' + percentage + '%)';
                                            },
                                            title: function (tooltipItem, data) {
                                                return data.labels[tooltipItem[0].index];
                                            }
                                        }
                                    },
                                    animation: {
                                        duration: 1000, // Độ dài của hiệu ứng (ms)
                                        easing: 'easeOutBounce' // Loại hiệu ứng (có thể thay đổi)
                                    },
                                    elements: {
                                        arc: {
                                            backgroundColor: ['pink', 'orange', 'green', 'blue']
                                        }
                                    },
                                    plugins: {
                                        colorschemes: {
                                            scheme: 'brewer.Set3' // Sử dụng màu hồng từ bảng màu Set3
                                        }
                                    }
                                }
                            });

                        } else {
                            console.log("Error: response course data is required");
                        }

                        // Chart 4: Lesson by Chapter
                        if (response.LessonByChapterData) {
                            const data = {
                                ... response.LessonByChapterData
                            }
                            var lessonByChapterCanvas = document.getElementById('lessonByChapterChart');
                            var lessonByChapterChart = new Chart(lessonByChapterCanvas, {
                                type: 'bar',
                                data,
                                options: {
                                    backgroundColor: 'pink',
                                    legend: {
                                        display: false
                                    },
                                    title: {
                                        display: true,
                                        text: "Title here"
                                    },
                                    scales: {
                                        yAxes: [{ticks: {min: 0}}],
                                    },
                                    tooltips: {
                                        mode: 'label',
                                        callbacks: {
                                            label: function (tooltipItem, data) {
                                                var dataset = data.datasets[tooltipItem.datasetIndex];
                                                var meta = dataset._meta[Object.keys(dataset._meta)[0]];
                                                var total = meta.total;
                                                var currentValue = dataset.data[tooltipItem.index];
                                                var percentage = parseFloat((currentValue / total * 100).toFixed(1));
                                                return currentValue + ' (' + percentage + '%)';
                                            },
                                            title: function (tooltipItem, data) {
                                                return data.labels[tooltipItem[0].index];
                                            }
                                        }
                                    }, animation: {
                                        duration: 1000, // Độ dài của hiệu ứng (ms)
                                        easing: 'easeOutBounce' // Loại hiệu ứng (có thể thay đổi)
                                    }, plugins: {
                                        colorschemes: {
                                            scheme: 'brewer.Paired12' // Thay đổi màu sắc
                                        }
                                    }
                                }
                            });
                        } else {
                            console.log("Error : response course data is required")
                        }
                        // Chart 5 : Slider by Course 

                        if (response.SliderByCourseData) {
                            const data = {
                                ... response.SliderByCourseData
                            }
                            var SliderByCourseCanvas = document.getElementById('SliderByCourseChart');
                            var SliderByCourseChart = new Chart(SliderByCourseCanvas, {
                                type: 'pie',
                                data,
                                options: {
                                    legend: {
                                        display: false
                                    },
                                    title: {
                                        display: true,
                                        text: "Title here"
                                    },
                                    scales: {
                                        yAxes: [{ticks: {min: 0}}],
                                    },
                                    tooltips: {
                                        mode: 'label',
                                        callbacks: {
                                            label: function (tooltipItem, data) {
                                                var dataset = data.datasets[tooltipItem.datasetIndex];
                                                var meta = dataset._meta[Object.keys(dataset._meta)[0]];
                                                var total = meta.total;
                                                var currentValue = dataset.data[tooltipItem.index];
                                                var percentage = parseFloat((currentValue / total * 100).toFixed(1));
                                                return currentValue + ' (' + percentage + '%)';
                                            },
                                            title: function (tooltipItem, data) {
                                                return data.labels[tooltipItem[0].index];
                                            }
                                        }
                                    }, animation: {
                                        duration: 1000, // Độ dài của hiệu ứng (ms)
                                        easing: 'easeOutBounce' // Loại hiệu ứng (có thể thay đổi)
                                    },
                                    elements: {
                                        arc: {
                                            backgroundColor: ['orange', 'yellow', 'red']
                                        }
                                    }, plugins: {
                                        colorschemes: {
                                            scheme: 'brewer.Paired12' // Thay đổi màu sắc
                                        }
                                    }
                                }
                            });
                        } else {
                            console.log("Error : response course data is required")
                        }

                        // Chart 6 : Chapter by Course 

                        if (response.ChapterByCourseData) {
                            const data = {
                                ... response.ChapterByCourseData
                            }
                            var ChapterByCourseData = document.getElementById('ChapterByCourseData');
                            var ChapterByCourseData = new Chart(ChapterByCourseData, {
                                type: 'bar',
                                data,
                                options: {
                                    legend: {
                                        display: false
                                    },
                                    title: {
                                        display: true,
                                        text: "Title here"
                                    },
                                    scales: {
                                        yAxes: [{ticks: {min: 0}}],
                                    },
                                    tooltips: {
                                        mode: 'label',
                                        callbacks: {
                                            label: function (tooltipItem, data) {
                                                var dataset = data.datasets[tooltipItem.datasetIndex];
                                                var meta = dataset._meta[Object.keys(dataset._meta)[0]];
                                                var total = meta.total;
                                                var currentValue = dataset.data[tooltipItem.index];
                                                var percentage = parseFloat((currentValue / total * 100).toFixed(1));
                                                return currentValue + ' (' + percentage + '%)';
                                            },
                                            title: function (tooltipItem, data) {
                                                return data.labels[tooltipItem[0].index];
                                            }
                                        }
                                    }, animation: {
                                        duration: 1000, // Độ dài của hiệu ứng (ms)
                                        easing: 'easeOutBounce' // Loại hiệu ứng (có thể thay đổi)
                                    }, plugins: {
                                        colorschemes: {
                                            scheme: 'tableau.Classic20' // Thay đổi màu sắc
                                        }
                                    }
                                }
                            });
                        } else {
                            console.log("Error : response course data is required")
                        }

                        if (response.CourseBySubjectData) {
                            const data = {
                                ... response.CourseBySubjectData
                            }
                            var CourseBySubjectData = document.getElementById('CourseBySubjectData');
                            var CourseBySubjectData = new Chart(CourseBySubjectData, {
                                type: 'pie',
                                data,
                                options: {
                                    legend: {
                                        display: false
                                    },
                                    title: {
                                        display: true,
                                        text: "Title here"
                                    },
                                    scales: {
                                        yAxes: [{ticks: {min: 0}}],
                                    }, tooltips: {
                                        mode: 'label',
                                        callbacks: {
                                            label: function (tooltipItem, data) {
                                                var dataset = data.datasets[tooltipItem.datasetIndex];
                                                var meta = dataset._meta[Object.keys(dataset._meta)[0]];
                                                var total = meta.total;
                                                var currentValue = dataset.data[tooltipItem.index];
                                                var percentage = parseFloat((currentValue / total * 100).toFixed(1));
                                                return currentValue + ' (' + percentage + '%)';
                                            },
                                            title: function (tooltipItem, data) {
                                                return data.labels[tooltipItem[0].index];
                                            }
                                        }
                                    }, animation: {
                                        duration: 1000, // Độ dài của hiệu ứng (ms)
                                        easing: 'easeOutBounce' // Loại hiệu ứng (có thể thay đổi)
                                    }, plugins: {
                                        colorschemes: {
                                            scheme: 'brewer.Paired12' // Thay đổi màu sắc
                                        }
                                    }
                                }
                            });
                        } else {
                            console.log("Error : response course data is required")
                        }
                        // Chart 8 : Quiz By Chapter
                        //check data 
                        if (response.QuizByChapterData) {
                            // copy data
                            const data = {
                                ... response.QuizByChapterData
                            }
                            //draw chart
                            var QuizByChapterData = document.getElementById('QuizByChapterData');
                            var QuizByChapterData = new Chart(QuizByChapterData, {
                                // type 
                                type: 'bar',
                                data,
                                //add option 
                                options: {
                                    backgroundColor: 'rgb(139, 69, 19, 0.5)',
                                    legend: {
                                        display: false
                                    },
                                    title: {
                                        display: true,
                                        text: "Title here"
                                    },
                                    scales: {
                                        yAxes: [{ticks: {min: 0}}],
                                    },
                                    tooltips: {
                                        mode: 'label',
                                        callbacks: {
                                            label: function (tooltipItem, data) {
                                                var dataset = data.datasets[tooltipItem.datasetIndex];
                                                var meta = dataset._meta[Object.keys(dataset._meta)[0]];
                                                var total = meta.total;
                                                var currentValue = dataset.data[tooltipItem.index];
                                                var percentage = parseFloat((currentValue / total * 100).toFixed(1));
                                                return currentValue + ' (' + percentage + '%)';
                                            },
                                            title: function (tooltipItem, data) {
                                                return data.labels[tooltipItem[0].index];
                                            }
                                        }
                                    }, animation: {
                                        duration: 1000, // Độ dài của hiệu ứng (ms)
                                        easing: 'easeOutBounce' // Loại hiệu ứng (có thể thay đổi)
                                    }, plugins: {
                                        colorschemes: {
                                            scheme: 'brewer.Paired12' // Thay đổi màu sắc
                                        }
                                    }
                                }
                            });
                        } else {
                            console.log("Error : response subject data is required")
                        }
                    }
                };
                xhr.send();
            });

        </script>
    </body>
</html>
<%-- 
    Document   : usercoursedetail
    Created on : Oct 5, 2023, 2:58:16 PM
    Author     : Acer
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>About Course</title>
        <link rel="icon" href="images/Quikk.png" type="image/png">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.4/css/all.min.css">
        <style>
            /* Set default font family and size */
            body {
                font-family: 'Open Sans', sans-serif;
                font-size: 16px;
                margin: 0;
                padding: 0;
            }
            /* Style the logo */
            .logo {
                font-size: 24px;
                font-weight: bold;
                text-transform: uppercase;
            }

            /* Style the navigation */
            nav {
                display: flex;
            }

            nav a {
                color: #fff;
                text-decoration: none;
                margin-left: 20px;
                font-weight: bold;
                text-transform: uppercase;
                transition: all 0.3s ease;
            }

            nav a:hover {
                color: #f2f2f2;
            }

            /* Style the chapter heading */
            h1 {
                font-size: 36px;
                font-weight: bold;
                margin: 50px 0 20px;
                text-align: center;
                text-transform: uppercase;
            }

            /* Style the lesson text */
            p {
                font-size: 20px;
                margin-bottom: 10px;
                line-height: 1.5;
            }

            /* Style the quiz section */
            .quiz {
                background-color: #f2f2f2;
                padding: 20px;
                margin-bottom: 50px;
            }

            /* Style the quiz title */
            .quiz h2 {
                font-size: 24px;
                font-weight: bold;
                margin-bottom: 20px;
                text-align: center;
                text-transform: uppercase;
            }

            /* Style the quiz list */
            .quiz ol {
                list-style-type: decimal;
                margin-left: 20px;
            }

            /* Style the quiz list item */
            .quiz li {
                margin-bottom: 10px;
            }
            .content {
                margin: 0 auto;
                max-width: 800px;
                padding: 20px;
                margin-top: 30px;
            }
            .gray {
                color: gray;
            }

            .green {
                color: green;
            }
        </style>
    </head>
    <body>
        <jsp:include page="Common/Header.jsp"></jsp:include>
            <div class="content">
            <c:forEach items="${requestScope.chapter}" var="c">
                <h1>Chapter ${c.chapName}</h1>
                <input type="hidden" name="chapId" value="${c.chapId}">
                <div class="lesson">
                    <h2>Lesson</h2>
                    <c:forEach items="${c.lesson}" var="l">
                        <c:if test="${sessionScope.account eq null}">
                            <p>Lesson ${l.lessonName}</p>
                        </c:if>
                        <c:if test="${sessionScope.account ne null}">
                            <a href="lessondetail?lessonId=${l.lessonId}"><p>Lesson ${l.lessonName}</p>  </a>
                        </c:if>
                    </c:forEach>
                </div>
                <c:if test="${sessionScope.account ne null}">
                    <div class="quiz">
                        <h2>Quiz</h2>
                        <c:forEach items="${c.quiz}" var="q">
                            <ol>
                                <li><i class="fas fa-check-circle"></i><a href="quizattempt?quizId=${q.quizId}"> ${q.title}</a></li>
                            </ol>
                        </c:forEach>
                    </div>
                </c:if>
            </c:forEach>
        </div>
        <jsp:include page="Common/Sidebar.jsp"></jsp:include>
        <jsp:include page="Common/Footer.jsp"></jsp:include>
    </body>
</html>

<%-- 
    Document   : CrudSubject
    Created on : Sep 15, 2023, 10:36:57 AM
    Author     : Acer
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">


        <title>Manage Members</title>
        <link rel="icon" href="images/Quikk.png" type="image/png">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/css/bootstrap.min.css" rel="stylesheet">
        <script src="https://cdn.ckeditor.com/ckeditor5/39.0.2/classic/ckeditor.js"></script>
        <style type="text/css">
            body{
                margin-top:20px;
                color: #1a202c;
                text-align: left;
                font-size: 13px;
                background-color: #e2e8f0;
                font-family: 'Varela Round', sans-serif;
            }
            .main-body {
                padding: 15px;
            }
            .card {
                box-shadow: 0 1px 3px 0 rgba(0,0,0,.1), 0 1px 2px 0 rgba(0,0,0,.06);
            }

            .card {
                position: relative;
                display: flex;
                flex-direction: column;
                min-width: 0;
                word-wrap: break-word;
                background-color: #fff;
                background-clip: border-box;
                border: 0 solid rgba(0,0,0,.125);
                border-radius: .25rem;
            }

            .card-body {
                flex: 1 1 auto;
                min-height: 1px;
                padding: 1rem;
            }

            .gutters-sm {
                margin-right: -8px;
                margin-left: -8px;
            }

            .gutters-sm>.col, .gutters-sm>[class*=col-] {
                padding-right: 8px;
                padding-left: 8px;
            }
            .mb-3, .my-3 {
                margin-bottom: 1rem!important;
            }

            .bg-gray-300 {
                background-color: #e2e8f0;
            }
            .h-100 {
                height: 100%!important;
            }
            .shadow-none {
                box-shadow: none!important;
            }

        </style>
    </head>
    <body>
        
        <div class="container">
            <div class="main-body">

               <nav aria-label="breadcrumb" class="main-breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="home">Home</a></li>
                        <li class="breadcrumb-item"><a href="accountmanagement">Account Management</a></li>
                        <li class="breadcrumb-item"><a href="regismembermanagement<c:if test="${sessionScope.regisUserId ne null}">?userId=${sessionScope.regisUserId}</c:if>">Registration member ${sessionScope.regisUserId}</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Manage registration </li>
                    </ol>
                </nav>
                <div class="row gutters-sm">
                    <div class="col-md-12">
                        <div class="card mb-3">
                            <div class="card-body">

                                <c:if test="${requestScope.action eq 'update'}">
                                    <form action="crudregismember" method="post">
                                        <input type="hidden" name="action" value="${requestScope.action}">
                                        <input type="hidden" name="userId" value="${sessionScope.regisUserId eq null ? requestScope.regismember.userId:sessionScope.regisUserId}">  
                                        <input type="hidden" name="regisId" value="${requestScope.regismember.regisId}">
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">User Id</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <input type="number" name="user" value="${sessionScope.regisUserId eq null ? requestScope.regismember.userId:sessionScope.regisUserId}" disabled>                                        
                                            </div>
                                        </div><hr>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Price Package</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <label for="cars">Choose a Price Package:${requestScope.regismember.pricePackage.priceId}</label>
                                                <select name="pricepackage"  >
                                                    <c:forEach items="${requestScope.pricepackageList}" var="sl">
                                                        <option value="${sl.priceId}" 
                                                                <c:if test="${requestScope.regismember.pricePackage.priceId eq sl.priceId}">
                                                                    selected
                                                                </c:if>
                                                                > ${sl.name} </option>
                                                        
                                                    </c:forEach>

                                                </select>

                                            </div>
                                        </div>
                                        <hr>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Voucher</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <label for="cars">Choose a Voucher:${requestScope.regismember.voucher.id}</label>
                                                <select name="voucher"  >
                                                    <c:forEach items="${requestScope.voucherList}" var="sl">
                                                        <option value="${sl.id}" 
                                                                <c:if test="${requestScope.regismember.voucher.id eq sl.id}">
                                                                    selected
                                                                </c:if>
                                                                > ${sl.name} </option>
                                                        
                                                    </c:forEach>

                                                </select>

                                            </div>
                                        </div>
                                        <hr>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <input type="submit" value="Update">
                                            </div>
                                        </div>
                                    </form>

                                </c:if>
                                <c:if test="${requestScope.action eq 'add'}">
                                    <form action="crudregismember" method="post">
                                        <input type="hidden" name="action" value="add">
                                        
                                       
                                        <p style="color:lightcoral;text-align: center">${requestScope.ms}</p>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">User Id</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <input type="number" name="userId" value="${requestScope.userId}" required>                                        
                                            </div>
                                        </div><hr>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Price Package</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <label for="cars">Choose a Price Package:</label>
                                                <select name="pricepackage"  >
                                                    <c:forEach items="${requestScope.pricepackageList}" var="sl">
                                                        <option value="${sl.priceId}"                                                             
                                                                > ${sl.name} </option>                                                       
                                                    </c:forEach>

                                                </select>

                                            </div>
                                        </div>
                                        <hr>
                                        <hr>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <h6 class="mb-0">Voucher</h6>
                                            </div>
                                            <div class="col-sm-9 text-secondary">
                                                <label for="cars">Choose a Voucher:</label>
                                                <select name="voucher"  >
                                                    
                                                    <c:forEach items="${requestScope.voucherList}" var="sl">
                                                        <option value="${sl.id}"                                                             
                                                                > ${sl.name} </option>                                                       
                                                    </c:forEach>

                                                </select>

                                            </div>
                                        </div>
                                        <hr>
                                        
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <input type="submit" value="Add" />
                                            </div>
                                        </div>
                                    </form>
                                </c:if>
                            </div>
                        </div>
                    </div>                   

                </div>
            </div>
        </div>
    </div>
    <script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.bundle.min.js"></script>
    <script>
        ClassicEditor
                .create(document.querySelector('#editor'))
                .catch(error => {
                    console.error(error);
                });
    </script>
</body>
</html>



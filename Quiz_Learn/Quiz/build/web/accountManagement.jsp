<%-- 
    Document   : accountManagement
    Created on : Sep 10, 2023, 1:46:27 PM
    Author     : khiem
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Account Management</title>
        <link rel="icon" href="images/Quikk.png" type="image/png">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
        <style>
            body {
                color: #566787;
                background: #f5f5f5;
                font-family: 'Varela Round', sans-serif;
                font-size: 13px;
            }
            .table-responsive {
                margin: 30px 0;
            }
            .table-wrapper {
                min-width: 1000px;
                background: #fff;
                padding: 20px 25px;
                border-radius: 3px;
                box-shadow: 0 1px 1px rgba(0,0,0,.05);
            }
            .table-title {
                padding-bottom: 15px;
                background: #299be4;
                color: #fff;
                padding: 16px 30px;
                margin: -20px -25px 10px;
                border-radius: 3px 3px 0 0;
            }
            .table-title h2 {
                margin: 5px 0 0;
                font-size: 24px;
            }
            .table-title .btn {
                color: #566787;
                float: right;
                font-size: 13px;
                background: #fff;
                border: none;
                min-width: 50px;
                border-radius: 2px;
                border: none;
                outline: none !important;
                margin-left: 10px;
            }
            .table-title .btn:hover, .table-title .btn:focus {
                color: #566787;
                background: #f2f2f2;
            }
            .table-title .btn i {
                float: left;
                font-size: 21px;
                margin-right: 5px;
            }
            .table-title .btn span {
                float: left;
                margin-top: 2px;
            }
            table.table tr th, table.table tr td {
                border-color: #e9e9e9;
                padding: 12px 15px;
                vertical-align: middle;
            }
            table.table tr th:first-child {
                width: 60px;
            }
            table.table tr th:last-child {
                width: 100px;
            }
            table.table-striped tbody tr:nth-of-type(odd) {
                background-color: #fcfcfc;
            }
            table.table-striped.table-hover tbody tr:hover {
                background: #f5f5f5;
            }
            table.table th i {
                font-size: 13px;
                margin: 0 5px;
                cursor: pointer;
            }
            table.table td:last-child i {
                opacity: 0.9;
                font-size: 22px;
                margin: 0 5px;
            }
            table.table td a {
                font-weight: bold;
                color: #566787;
                display: inline-block;
                text-decoration: none;
            }
            table.table td a:hover {
                color: #2196F3;
            }
            table.table td a.settings {
                color: #2196F3;
            }
            table.table td a.delete {
                color: #F44336;
            }
            table.table td i {
                font-size: 19px;
            }
            table.table .avatar {
                border-radius: 50%;
                vertical-align: middle;
                margin-right: 10px;
            }
            .status {
                font-size: 30px;
                margin: 2px 2px 0 0;
                display: inline-block;
                vertical-align: middle;
                line-height: 10px;
            }
            .text-success {
                color: #10c469;
            }
            .text-info {
                color: #62c9e8;
            }
            .text-warning {
                color: #FFC107;
            }
            .text-danger {
                color: #ff5b5b;
            }
            .pagination {
                float: right;
                margin: 0 0 5px;
            }
            .pagination li a {
                border: none;
                font-size: 13px;
                min-width: 30px;
                min-height: 30px;
                color: #999;
                margin: 0 2px;
                line-height: 30px;
                border-radius: 2px !important;
                text-align: center;
                padding: 0 6px;
            }
            .pagination li a:hover {
                color: #666;
            }
            .pagination li.active a, .pagination li.active a.page-link {
                background: #03A9F4;
            }
            .pagination li.active a:hover {
                background: #0397d6;
            }
            .pagination li.disabled i {
                color: #ccc;
            }
            .pagination li i {
                font-size: 16px;
                padding-top: 6px
            }
            .hint-text {
                float: left;
                margin-top: 10px;
                font-size: 13px;
            }
        </style>
        <script>
            $(document).ready(function () {
                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>
    </head>
    <body>
        <c:if test="${fn:contains(sessionScope.account.roleName.access, 'account')}">
            <jsp:include page="Common/Management.jsp"></jsp:include>
                <div class="container-xl">
                    <div class="table-responsive">
                        <div class="table-wrapper">
                            <div class="table-title">
                                <div class="row">
                                    <div class="col-sm-5">
                                        <h2>User <b>Management</b></h2>
                                        <h5></h5>
                                        <form action="accountmanagement" id="myForm">
                                            <input style="width: 400px" type="text" class="form-control" placeholder="Search by Name" name="search" id="pw">

                                        </form>
                                    </div>
                                    <div class="col-sm-7">
                                        <a href="crudaccount?aid=0&action=add" class="btn btn-secondary"><i class="material-icons">&#xE147;</i> <span>Add New User</span></a>

                                    </div>

                                </div>
                            </div>
                        <c:if test="${requestScope.ms ne null}">
                            <h5 style="color: green;text-align: center" class="alert alert-info">${requestScope.ms}</h5>
                        </c:if>
                        <form action="accountmanagement">
                            <c:if test="${param.sort ne null}"><input type="hidden" name="sort" value="${param.sort}"></c:if>
                                <select name="vip" onchange="this.form.submit()" style="font-size: large">
                                    <option value="0" <c:if test="${param.vip eq 0}">selected</c:if>>All</option>
                                <option value="1" <c:if test="${param.vip eq 1}">selected</c:if>>VIP</option>
                                <option value="2" <c:if test="${param.vip eq 2}">selected</c:if>>NO VIP</option>
                                </select>
                            </form>

                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>Account ID</th>
                                        <th>Level</th>
                                        <th>Email</th>						
                                        <th>Role<a href="rolemanagement" class="settings" title="Edit" data-toggle="tooltip"><i class="material-icons">&#xE8B8;</i></a></th>
                                        <th>Status</th>
                                        <th>Member registered<a href="regismembermanagement" target="_blank" class="settings" title="Edit" data-toggle="tooltip"><i class="material-icons">&#xE8B8;</i></a></th>
                                        <th>Money Spend<c:if test="${param.sort eq 1}"><a href="accountmanagement?sort=2<c:if test="${param.vip ne null}">&vip=${param.vip}</c:if>"><i class="fa fa-sort" aria-hidden="true"></i></a></c:if><c:if test="${param.sort eq 2 || param.sort eq null}"><a href="accountmanagement?sort=1<c:if test="${param.vip ne null}">&vip=${param.vip}</c:if>"><i class="fa fa-sort" aria-hidden="true"></i></a></c:if></th>
                                            <th>Voucher<a href="vouchermanagement" class="settings" title="Edit" data-toggle="tooltip"><i class="material-icons">&#xE8B8;</i></a></th>
                                            <th>Action</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                <c:forEach items="${requestScope.account}" var="a" begin="${requestScope.page*6-6}" end="${requestScope.page*6-1}">
                                    <tr>
                                        <td>${a.aid} 
                                            <c:if test="${a.vip}">
                                                <img width="30px" src="https://img.freepik.com/premium-vector/golden-vip-badges-luxury-elegant-business-icon-premium-vector_567423-318.jpg?size=338&ext=jpg&ga=GA1.1.1413502914.1696896000&semt=ais" alt="alt"/>
                                            </c:if></td>
                                        <td>${a.level.currentLevel}</td>
                                        <td>${a.email}</td>                        
                                        <td>
                                            <form action="crudaccount" method="post">
                                                <input type="hidden" name="action" value="changeRole">
                                                <input type="hidden" name="aid" value="${a.aid}">

                                                <select name="role" onchange="this.form.submit()" <c:if test="${a.role eq 2}">disabled</c:if>>
                                                    <c:forEach items="${requestScope.role}" var="r">
                                                        <option value="${r.role}" <c:if test="${a.role eq r.role}">selected</c:if>>${r.name}</option>
                                                    </c:forEach>
                                                </select>
                                            </form>


                                        </td>
                                        <td><c:choose>
                                                <c:when test = "${a.status}"><span class="status text-success">&bull;</span>Active</c:when> 
                                                <c:otherwise><span class="status text-warning">&bull;</span>Unactive</c:otherwise>
                                            </c:choose></td>
                                        <td><a href="regismembermanagement?userId=${a.aid}" target="_blank">${a.member[fn:length(a.member)-1].regisId}</a></td>
                                        <td>${a.money} $</td> 
                                        <td><c:if test="${a.role eq 1}">
                                                <c:forEach items="${a.voucher}" var="v">${v.name}, </c:forEach><h5><a  href="voucheruser?aid=${a.aid}"><i class="material-icons"  style="color: green ">&#xE147;</i> </a></h5>
                                                    <form action="plusVoucher">                                               
                                                    </form>
                                            </c:if>      
                                        </td>
                                        <td>

                                            <a href="crudaccount?aid=${a.aid}&action=update" title="View" data-toggle="tooltip"><i class="fa fa-eye" style="font-size:22px;position:relative;top:-3px;color: #1aa3ff"></i></a>
                                            <c:if test="${a.role ne 2}">
                                                <a href="crudaccount?aid=${a.aid}&action=changestatus<c:if test="${param.page ne null}">&page=</c:if>${param.page}" class="disabled" title="ChangeStatus" data-toggle="tooltip"><i class="material-icons">&#xE5C9;</i></a>
                                            </c:if>
                                        </td>
                                    </tr>
                                </c:forEach>


                            </tbody>
                        </table>
                        <div class="clearfix">
                            <% int i=1; %>
                            <ul class="pagination">

                                <c:forEach items="${requestScope.account}" step="6">
                                    <c:set var="i" value="<%=i%>"/>
                                    <li class="page-item <c:if test="${requestScope.page eq i}">active</c:if>" ><a href="accountmanagement?page=<%=i%><c:if test="${param.sort ne null}">&sort=</c:if>${param.sort}<c:if test="${param.search ne null}">&search=</c:if>${param.search}<c:if test="${param.vip ne null}">&vip=${param.vip}</c:if>" class="page-link"><%=i%></a>
                                        </li> 
                                    <% i++;%>
                                </c:forEach>

                            </ul>
                        </div>
                    </div>
                </div>
            </div> 
        </c:if>

    </body>

</html>

<%-- 
    Document   : lessonDetail
    Created on : Sep 19, 2023, 11:15:41 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">
    <head>
        <title>${lessondetail.lessonName}</title>
        <link rel="icon" href="images/Quikk.png" type="image/png">
        <meta name="viewport" content="width=device-width, initial-scale=1">     
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="format-detection" content="telephone=no">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="author" content="">
        <meta name="keywords" content="">
        <meta name="description" content="">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KK94CHFLLe+nY2dmCWGMq91rCGa5gtU4mk92HdvYe+M/SXH301p5ILy+dN9+nJOZ" crossorigin="anonymous">
        <link id="theme" rel="stylesheet" type="text/css" href="css/greyson-theme.css">
        <link rel="stylesheet" type="text/css" href="fonts/icomoon/icomoon.css">

        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link href="https://fonts.googleapis.com/css2?family=Mulish:ital@0;1&family=Oswald:wght@500&display=swap" rel="stylesheet">
        
        <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
        <link rel="stylesheet" href="./css/lessonDetail.css">
       
        <style>
            body,h2,h3,h4,h5 {
                font-family: sans-serif;
            }
            h1{
                font-family: "Manhattan";
            }
            .container {
                padding: 20px;
            }
            .user-details {
                display: flex;
                align-items: center; /* Vertically align items in the container */
                gap: 10px; /* Adjust the space between the elements as needed */
            }
            .post-date {
                margin-left: auto; /* This will push "Uploaded Date" to the right */
            }
        </style>
    </head>
    <body class="w3-light-grey">
        <div class="pattern-bg"></div>
        <jsp:include page="Common/Header.jsp"></jsp:include>
        <%@include file="Common/blogHeader.jsp" %>


        <!-- w3-content defines a container for fixed size centered content, 
        and is wrapped around the whole page content, except for the footer in this example -->
        <div class="w3-content" style="max-width:1400px">

            <!-- Header -->
            <header class="w3-container w3-center w3-padding-32"> 
                <h1><b>LESSON</b></h1>
            </header>

            <!-- Grid -->
            <div class="w3-row">
                <!-- Lesson section-->
                <section class="py-5">
                    <div class="container px-4 px-lg-5 my-5">
                        <div class="row gx-4 gx-lg-5 align-items-center">
                            <h1 class="display-5 fw-bolder">${lessondetail.lessonName}</h1>
                            <div class="col-md-12"><img class="card-img-top mb-5 mb-md-0" src="${lessondetail.image}" alt="..." /></div>
                            <div class="col-md-12">
                                
                                <p class="lead">${lessondetail.content}</p>
                                
                            </div>
                        </div>
                    </div>
                    
                </section>
            </div><br>

        </div>

        <jsp:include page="Common/Sidebar.jsp"></jsp:include>

        <jsp:include page="Common/Footer.jsp"></jsp:include>

    </body>
    
</html>

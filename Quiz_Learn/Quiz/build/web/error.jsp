<%-- 
    Document   : error
    Created on : Oct 24, 2023, 1:45:57 PM
    Author     : Acer
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Error</title>
        <link rel="icon" href="images/Quikk.png" type="image/png">
    </head>
    <body>
        <div align="center">
            <h1>Payment Error</h1>
            <br/>
            <h3 style="color: red;">${errorMessage}</h3>
            <a href="home"><button>Back to Home</button></a>
            <br/>
        </div>
    </body>
</html>

<%-- 
    Document   : courses
    Created on : Nov 11, 2023, 3:41:14 PM
    Author     : Acer
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!doctype html>
<html class="no-js" lang="zxx">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Courses | Education</title>
        <link rel="icon" href="images/Quikk.png" type="image/png">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="manifest" href="site.webmanifest">
        <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.ico">

        <!-- CSS here -->
        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/owl.carousel.min.css">
        <link rel="stylesheet" href="assets/css/slicknav.css">
        <link rel="stylesheet" href="assets/css/flaticon.css">
        <link rel="stylesheet" href="assets/css/progressbar_barfiller.css">
        <link rel="stylesheet" href="assets/css/gijgo.css">
        <link rel="stylesheet" href="assets/css/animate.min.css">
        <link rel="stylesheet" href="assets/css/animated-headline.css">
        <link rel="stylesheet" href="assets/css/magnific-popup.css">
        <link rel="stylesheet" href="assets/css/fontawesome-all.min.css">
        <link rel="stylesheet" href="assets/css/themify-icons.css">
        <link rel="stylesheet" href="assets/css/slick.css">
        <link rel="stylesheet" href="assets/css/nice-select.css">
        <link rel="stylesheet" href="assets/css/style.css">
    </head>

    <body>
        <jsp:include page="Common/Header.jsp"></jsp:include>
        <jsp:include page="Common/Sidebar.jsp"></jsp:include>
            <main>
                <!--? slider Area Start-->                
                <!-- Courses area start -->
                <div class="courses-area section-padding40 fix">
                    <div class="container">
                        <div class="row justify-content-center">
                            <div class="col-xl-7 col-lg-8">
                                <div class="section-tittle text-center mb-55">
                                    <h2>Our featured courses</h2>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                        <c:forEach items="${requestScope.courses}" var="c" begin="${requestScope.page*3-3}" end="${requestScope.page*3-1}">
                            <c:if test="${c.status eq true}">
                                <div class="col-lg-4">
                                    <div class="properties properties2 mb-30">
                                        <div class="properties__card">
                                            <div class="properties__img overlay1">
                                                <a href="coursedetail?courseId=${c.courseId}&page=1""><img style="height: 200px;" src="${c.thumbnail}" alt=""></a>
                                            </div>
                                            <div class="properties__caption">
                                                <p><c:forEach items="${requestScope.subject}" var="s"><c:if test="${c.subjectId eq s.subjectId}">${s.title}</c:if></c:forEach></p>
                                                <h3><a href="coursedetail?courseId=${c.courseId}&page=1">${c.courseName}</a></h3>

                                                <div class="properties__footer d-flex justify-content-between align-items-center">
                                                    <div class="restaurant-name">                                                     
                                                    </div>
                                                    <div class="price">
                                                        <span><c:if test="${c.level eq true}">Vip</c:if></span>
                                                        </div>
                                                    </div>
                                                    <a href="coursedetail?courseId=${c.courseId}&page=1" class="border-btn border-btn2">See more</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </c:if>
                        </c:forEach>                                                                  
                    </div>
                    <!--Pagination-->
                    <div class="col-md-6 text-end">
                        <% int i=1; %>
                        <c:forEach items="${requestScope.courses}" step="3">
                            <c:set var="i" value="<%=i%>"/>
                            <a href="courselist?page=<%=i%>&" class="btn btn-primary"><%=i%></a>
                            </li> 
                            <% i++;%>
                        </c:forEach>

                    </div>
                </div>
            </div>
            <!-- Courses area End -->
            <!--? top subjects Area Start -->
            <div class="topic-area">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-xl-7 col-lg-8">
                            <div class="section-tittle text-center mb-55">
                                <h2>Explore top subjects</h2>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <c:forEach items="${requestScope.subject}" var="s">
                            <div class="col-lg-3 col-md-4 col-sm-6">
                                <div class="single-topic text-center mb-30">
                                    <div class="topic-img">
                                        <img src="assets/img/gallery/topic1.png" alt="">
                                        <div class="topic-content-box">
                                            <div class="topic-content">
                                                <h3><a href="courselist?subject=${s.subjectId}">${s.title}</a></h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </div>               
                </div>
            </div>
            <!-- top subjects End -->  
            <!-- ? services-area -->    

            <jsp:include page="Common/Footer.jsp"></jsp:include>
        </main>

        <!-- Scroll Up -->
        <div id="back-top" >
            <a title="Go to Top" href="#"> <i class="fas fa-level-up-alt"></i></a>
        </div>



    </body>
</html>

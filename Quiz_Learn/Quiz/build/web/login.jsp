<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c"  uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Login to Quikk</title>
        <link rel="icon" href="images/Quikk.png" type="image/png">
        <script type="module" src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.esm.js"></script>
        <script nomodule src="https://unpkg.com/ionicons@5.5.2/dist/ionicons/ionicons.js"></script><link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
        <link rel='stylesheet' href='https://fonts.googleapis.com/css2?family=Poppins:ital,wght@0,100;0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,100;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&amp;display=swap'><link rel="stylesheet" href="css/style.css">
        <style>
            .google-btn {
                margin:10px 0 0 0;
                width: 190px;
                height: 35px;
                background-color: #4285f4;
                border-radius: 2px;
                box-shadow: 0 3px 4px 0 rgba(0,0,0,.25);
                cursor: pointer;
                padding: 0.5em;
                background: #89cff3;
                color: white;
                border: none;
                border-radius: 30px;
                font-weight: 100;
            }
            .google-icon-wrapper {
                position: absolute;
                margin-top: 1px;
                margin-left: 1px;
                width: 40px;
                height: 40px;
                border-radius: 2px;
                background-color: #fff;
            }
            .google-icon {
                position: absolute;
                margin-top: 11px;
                margin-left: 11px;
                width: 18px;
                height: 18px;
                border-radius: 20px
            }
            .logo2{
                position:relative;
                left:50px;
                margin-top:150px;
            }
            .btn-text {
                float: right;
                margin: 11px 11px 0 0;
                color: #fff;
                letter-spacing: 0.2px;
            }
        </style>
    </head>
    <body>
        <!-- partial:index.partial.html -->
        <div class="screen-1">
            <form action="login" method="post">
                
                <div class="logo2" >
                    <img src="images/Quikk.png" style="width: 200px;border-radius:100px;"  alt="alt"/>
                </div>
                <div style="display: flex;justify-content: center;align-items: center;flex-direction: column;position: relative;bottom:20px">
                    <h5 style="color:green">${requestScope.status}</h5>
                    <h5 style="color:#DF2E38">${requestScope.ms}</h5>
                </div>
                <div class="email">
                    <label for="email">Email Address</label>
                    <div class="sec-2">
                        <ion-icon name="mail-outline"></ion-icon>
                        <input type="email" name="email" placeholder="Username@gmail.com"/>
                    </div>
                </div>
                <br>
                <div class="password">
                    <label for="password">Password</label>
                    <div class="sec-2">
                        <ion-icon name="lock-closed-outline"></ion-icon>
                        <input class="pas" type="password" name="password" placeholder="············"/>
                    </div>
                </div>
                <br><br>
                <div style="display: flex;justify-content: center;align-items: center;flex-direction: column;"><button class="login" style="width:200px" type="submit" >Login </button>
                    <div class="google-btn" onclick="myFunction()" >
                        <div class="google-icon-wrapper" style=" background-color: #89cff3">
                            <img class="google-icon" src="https://upload.wikimedia.org/wikipedia/commons/5/53/Google_%22G%22_Logo.svg" style="border-radius:20px; background-color: white"/>
                        </div>
                        <p class="btn-text"><b>Google Login</b></p>
                    </div>
                </div>



            </form>


                <div class="footer"><span><a href="signup.jsp" style="text-decoration: none">Signup</a></span><span><a href="forgetpassword"style="text-decoration: none">Forgot Password?</a></span></div>
        </div>
        <!-- partial -->
        <script>
            function myFunction() {
                location.replace("https://accounts.google.com/o/oauth2/auth?scope=email%20profile%20openid&redirect_uri=http://localhost:9999/Quiz/LoginGoogleHandler&response_type=code&client_id=716342073717-cavrgrtgu5k8gjkl2eto4qu8fh1a9h0u.apps.googleusercontent.com&approval_prompt=force");
            }
        </script>

    </body>
</html>

<%-- 
    Document   : userLevel
    Created on : Oct 17, 2023, 8:04:46 PM
    Author     : arans
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <!-- Boxicons -->
        <link href='https://unpkg.com/boxicons@2.0.9/css/boxicons.min.css' rel='stylesheet'>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!-- My CSS -->
        <link rel="stylesheet" href="css/userLevel.css">
        <style>


            .notify-badge{
                position: relative;
                left:35px;
                bottom:80px;
                background-image: url('images/wing.png');
                background-size: 13px 10px;
                text-align: center;
                border-radius: 30px 30px 30px 30px;
                color:gold;
                padding:5px 10px;
                font-size:10px;
            }
            img{
                border-radius: 50px;
                height: 100px;
            }
            .email-name{
                position: relative;
                left:150px;
                bottom:75px;
            }
            .transaction-name{
                position: relative;
                left:40px;
                bottom:20px;
            }
        </style>
        <title>User Hub</title>
    </head>
    <body>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js"></script>
        <!-- SIDEBAR -->
        <section id="sidebar">
            <a href="#" class="brand">
                <i class='bx bxs-smile'></i>
                <span class="text">User Hub</span>
            </a>
            <ul class="side-menu top">
                <li >
                    <a href="userdashboard?">
                        <i class='bx bxs-dashboard' ></i>
                        <span class="text">Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class='bx bxs-shopping-bag-alt' ></i>
                        <span class="text">Home</span>
                    </a>
                </li>
                <c:if test = "${fn:contains(sessionScope.account.roleName.access, 'membership')}">
                <li>
                    <a href="salechart">
                        <i class='bx bxs-doughnut-chart' ></i>
                        <span class="text">Sale Chart</span>
                    </a>
                </li>
                </c:if>
                 <c:if test = "${fn:contains(sessionScope.account.roleName.access, 'account')}">
                <li class="active">
                    <a href="userchart">
                        <i class='bx bxs-message-dots' ></i>
                        <span class="text">User Chart</span>
                    </a>
                </li>
                </c:if>
                <c:if test = "${fn:contains(sessionScope.account.roleName.access, 'quiz')}">
                <li >
                    <a href="ChartQuiz.jsp">
                        <i class='bx bxs-message-dots' ></i>
                        <span class="text">Website Chart</span>
                    </a>
                </li>
                </c:if>
            </ul>

            
        </section>
        <!-- SIDEBAR -->



        <!-- CONTENT -->
        <section id="content">

            <!-- NAVBAR -->
            <nav>
                <i class='bx bx-menu' ></i>

                <input type="checkbox" id="switch-mode" hidden>
                <label for="switch-mode" class="switch-mode"></label>

                <a href="#" class="profile" style="position: absolute;left: 1300px">
                    <c:if test="${sessionScope.account.image eq null}">
                        <c:if test="${sessionScope.account.vip}"><span class="notify-badge">VIP</span></c:if><img src="${sessionScope.account.url}" width="100" >
                    </c:if> 
                    <c:if test="${sessionScope.account.image ne null}">
                        <c:if test="${sessionScope.account.vip}"><span class="notify-badge">VIP</span></c:if><img src="data:image/jpg;base64,${sessionScope.account.image}"  data-index="0" width="100" height="100">

                    </c:if>
                </a>
            </nav>
            <!-- NAVBAR -->

            <!-- MAIN -->
            <main>
                <div class="head-title">
                    <div class="left">
                        <h1>Dashboard</h1>
                        <ul class="breadcrumb">
                            <li>
                                <a href="#">Dashboard</a>
                            </li>
                            <li><i class='bx bx-chevron-right' ></i></li>
                            <li>
                                <a class="active" href="#">Home</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <form action="userchart" id="filter" style="font-size: large">
                    Year <select name="year" onchange="yearSubmit()" style="width: 150px">
                        <option value="0"id="allYear" ${param.year eq 0?'selected':''}>All</option>
                        <option value="2023" ${param.year eq 2023?'selected':''}>2023</option>
                        <option value="2022" ${param.year eq 2022?'selected':''}>2022</option>  
                    </select>&nbsp;&nbsp;&nbsp;
                    Month <select name="month" onchange="monthSubmit()" style="width: 150px">
                        <option value="0" id="allMonth" ${param.month eq 0?'selected':''}>All</option>
                        <option value="1" ${param.month eq 1?'selected':''}>January</option>
                        <option value="2" ${param.month eq 2?'selected':''}>February</option>
                        <option value="3" ${param.month eq 3?'selected':''}>March</option>
                        <option value="4" ${param.month eq 4?'selected':''}>April</option>
                        <option value="5" ${param.month eq 5?'selected':''}>May</option>
                        <option value="6" ${param.month eq 6?'selected':''}>June</option>
                        <option value="7" ${param.month eq 7?'selected':''}>July</option>
                        <option value="8" ${param.month eq 8?'selected':''}>August</option>
                        <option value="9" ${param.month eq 9?'selected':''}>September</option>
                        <option value="10" ${param.month eq 10?'selected':''}>October</option>
                        <option value="11" ${param.month eq 11?'selected':''}>November</option>
                        <option value="12" ${param.month eq 12?'selected':''}>December</option>           
                    </select>
                    &nbsp;&nbsp;&nbsp;
                    <a href="userchart"><button type="button">ALL</button></a>

                </form><p id="notAll" style="color:red;"></p>
                <ul class="box-info">
                    <li>
                        <i class='bx bxs-calendar-check' ></i>
                        <span class="text">
                            <h3>${requestScope.totalSignUp}
                                <c:if test="${requestScope.signUpChange>0}">
                                    <div style="color:green;"><fmt:formatNumber type="number" maxFractionDigits = "2" value="${requestScope.signUpChange}" />%<i class="material-icons" >arrow_upward</i></div>
                                </c:if>
                                <c:if test="${requestScope.signUpChange<0}">
                                    <div style="color:red;"><fmt:formatNumber type="number" maxFractionDigits = "2" value="${-requestScope.signUpChange}" />%<i class="material-icons" >arrow_downward</i></div>
                                </c:if>
                                <c:if test="${requestScope.signUpChange==0}">
                                    <div style="color:green;">0%<i class="material-icons" >dehaze</i></div>
                                </c:if>
                            </h3>
                            <p>New User</p>
                        </span>
                    </li>
                    <li>
                        <i class='bx bx-medal'></i>
                        <span class="text">
                            <h3>${requestScope.totalAccess}
                                <c:if test="${requestScope.accessChange>0}">
                                    <div style="color:green;"><fmt:formatNumber type="number" maxFractionDigits = "2" value="${requestScope.accessChange}" />%<i class="material-icons" >arrow_upward</i></div>
                                </c:if>
                                <c:if test="${requestScope.accessChange<0}">
                                    <div style="color:red;"><fmt:formatNumber type="number" maxFractionDigits = "2" value="${-requestScope.accessChange}" />%<i class="material-icons" >arrow_downward</i></div>
                                </c:if>
                                <c:if test="${requestScope.accessChange==0}">
                                    <div style="color:green;">0%<i class="material-icons" >dehaze</i></div>
                                </c:if>
                            </h3>
                            <p>Total Access Web</p>
                        </span>
                    </li>
                    
                </ul>

                <!--                            Table DATA 0-->      
                <div class="table-data">
                    <div class="order" >
                        <div>
                            <div>
                                <c:forEach items="${requestScope.chartSignUp}" var="c" begin="0" end="10">
                                    <input type="hidden" name="xSignUp" value="${c.month}">
                                    <input type="hidden" name="ySignUp" value="${c.y}">
                                </c:forEach>
                            </div>
                            <div><canvas id="signUpChart" style="width:100%;"></canvas></div>
                            
                            
                            
                        </div>

                    </div>
                    <div class="todo">
                        <div>
                                <c:forEach items="${requestScope.ageSignUp}" var="c" >
                                    <input type="hidden" name="xAgeSignUp" value="${c.month} years old">
                                    <input type="hidden" name="yAgeSignUp" value="${c.y}">
                                </c:forEach>
                            </div><!-- comment -->
                              <div style="margin-left:20%"><canvas id="ageSignUpChart" style="width:100%;max-width:800px;font-size: smaller"></canvas></div>

                            <div>
                                <c:forEach items="${requestScope.genderSignUp}" var="c" >
                                    <input type="hidden" name="xGenderSignUp" value="${c.month}">
                                    <input type="hidden" name="yGenderSignUp" value="${c.y}">
                                </c:forEach>
                            </div>
                            <div style="margin-left:20%"><canvas id="genderSignUpChart" style="width:100%;max-width:800px;font-size: smaller"></canvas></div>
                    </div>
                </div>
                <!--                            Table DATA 1-->
                <div class="table-data">
                    <div class="order">
                        <div>
                            <c:forEach items="${requestScope.age}" var="c" >
                                <input type="hidden" name="xAge" value="${c.month} years old">
                                <input type="hidden" name="yAge" value="${c.y}">
                            </c:forEach>
                        </div>
                        <div><canvas id="ageChart" style="width:100%;max-width:800px;font-size: smaller"></canvas></div>
                        <div>
                            <c:forEach items="${requestScope.gender}" var="c" >
                                <input type="hidden" name="xGender" value="${c.month}">
                                <input type="hidden" name="yGender" value="${c.y}">
                            </c:forEach>
                        </div>
                        <div style="color: greenyellow;width: 100%;display: flex;justify-content: center;align-items:center;"><canvas id="genderChart" style="width:100%;max-width:800px;font-size: smaller"></canvas></div>
                    </div>

                    <div class="todo">
                        <div class="head">
                            <h3>Top - Member Spending</h3>
                        </div>
                        <table>
                            <thead>
                                <tr>
                                    <th style="width: 300px">User</th>
                                    <th>Total Money:</th>

                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${requestScope.user}" var="p" begin="0" end="4">
                                    <tr>
                                        <td >
                                            <c:if test="${p.account.image eq null}">
                                                <c:if test="${p.account.vip}"><span class="notify-badge">VIP</span></c:if><img src="${p.account.url}" width="100" >
                                            </c:if> 
                                            <c:if test="${p.account.image ne null}">
                                                <c:if test="${p.account.vip}"><span class="notify-badge">VIP</span></c:if><img src="data:image/jpg;base64,${p.account.image}"  data-index="0" width="100" height="100">

                                            </c:if>
                                            <p class="email-name">${p.account.email}</p>
                                        </td>
                                        <td><p class="transaction-name">${p.y} $</p></td>

                                    </tr>
                                </c:forEach>



                            </tbody>
                        </table>
                        <c:if test="${fn:length(requestScope.user) eq 0}"><h3>No member registration</h3></c:if>

                        </div>

                    </div>
                    <!--                            Table DATA 2-->
                    <div class="table-data">
                        <div class="order">

                            <div>
                            <c:forEach items="${requestScope.ageAccess}" var="c" >
                                <input type="hidden" name="xAgeAccess" value="${c.month} years old">
                                <input type="hidden" name="yAgeAccess" value="${c.y}">
                            </c:forEach>
                        </div>
                        <div><canvas id="ageAccessChart" style="width:100%;max-width:800px;font-size: smaller"></canvas></div>

                        <div>
                            <c:forEach items="${requestScope.genderAccess}" var="c" >
                                <input type="hidden" name="xGenderAccess" value="${c.month}">
                                <input type="hidden" name="yGenderAccess" value="${c.y}">
                            </c:forEach>
                        </div>
                        <div ><canvas id="genderAccessChart" style="width:100%;max-width:800px;font-size: smaller"></canvas></div>
                    </div>

                    <div class="todo">
                        <div class="head">
                            <h3>Top - Member Access Website</h3>
                        </div>
                        <table>
                            <thead>
                                <tr>
                                    <th style="width: 300px">User</th>
                                    <th>Number of traffic:</th>

                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach items="${requestScope.accessuser}" var="p" begin="0" end="4">
                                    <tr>
                                        <td >
                                            <c:if test="${p.account.image eq null}">
                                                <c:if test="${p.account.vip}"><span class="notify-badge">VIP</span></c:if><img src="${p.account.url}" <c:if test="${!p.account.vip}">style="margin-left:30px;"</c:if> data-index="0" width="100" height="100" >
                                            </c:if> 
                                            <c:if test="${p.account.image ne null}">
                                                <c:if test="${p.account.vip}"><span class="notify-badge">VIP</span></c:if><img <c:if test="${!p.account.vip}">style="margin-left:30px;"</c:if> src="data:image/jpg;base64,${p.account.image}"  data-index="0" width="100" height="100">

                                            </c:if>
                                            <p class="email-name">${p.account.email}</p>
                                        </td>
                                        <td><p class="transaction-name">${p.y}</p></td>

                                    </tr>
                                </c:forEach>
                                <c:if test="${fn:length(requestScope.accessuser) eq 0}"><h3>No user login</h3></c:if>


                                </tbody>
                            </table>
                            <li>
                                <a href="#" class="logout">
                                    <i class='bx bx-chevrons-left'></i>
                                    <span class="text">SEE MORE</span>
                                </a>
                            </li>
                        </div>
                        


            </main>
            <!-- MAIN -->
        </section>
        <!-- CONTENT -->


        <script src="js/userLevel.js"></script>
        <script>
                        var barColors2 = [
                            "yellow",
                            "blue",
                            "green",
                            "red",
                            "#1e7145"
                        ];

                        var barColors1 = [
                            "green",
                            "pink",
                            "#2b5797",
                            "#e8c3b9",
                            "#1e7145"
                        ];
                        //line chart: signUp
                        //chart signUp
                        let xSignUps = [];
                        let ySignUps = [];
                        var xSignUp = document.getElementsByName("xSignUp");
                        var ySignUp = document.getElementsByName("ySignUp");
                        //document.getElementById("demo").innerHTML = x[0].value;
                        for (i = 0; i < xSignUp.length; i++) {
                            xSignUps.push(xSignUp[i].value);
                            ySignUps.push(ySignUp[i].value);
                        }

                        new Chart("signUpChart", {
                            type: "line",
                            data: {
                                labels: xSignUps,
                                datasets: [{
                                        fill: false,
                                        lineTension: 0,
                                        backgroundColor: "rgba(0,50,255,1.0)",
                                        borderColor: "rgba(0,50,255,0.1)",
                                        data: ySignUps
                                    }]
                            },
                            options: {
                                legend: {display: false},
                                title: {
                                    display: true,
                                    text: "DATA ANALASIS FOR NEW USER"
                                },
                                scales: {
                                    yAxes: [{ticks: {min: 0}}]
                                }
                            }
                        });

                        //pie chart : Age
                        var xAges = [];
                        var yAges = [];
                        var xAge = document.getElementsByName("xAge");
                        var yAge = document.getElementsByName("yAge");
                        //document.getElementById("demo").innerHTML = x[0].value;
                        for (i = 0; i < xAge.length; i++) {
                            xAges.push(xAge[i].value);
                            yAges.push(yAge[i].value);
                        }

                        new Chart("ageChart", {
                            type: "pie",
                            data: {
                                labels: xAges,
                                datasets: [{
                                        backgroundColor: barColors2,
                                        data: yAges
                                    }]
                            },
                            options: {
                                title: {
                                    display: true,
                                    text: "Age of Vip Registration "
                                }
                            }
                        });
                        //gender chart
                        var xGenders = [];
                        var yGenders = [];
                        var xGender = document.getElementsByName("xGender");
                        var yGender = document.getElementsByName("yGender");
                        //document.getElementById("demo").innerHTML = x[0].value;
                        for (i = 0; i < xGender.length; i++) {
                            xGenders.push(xGender[i].value);
                            yGenders.push(yGender[i].value);
                        }

                        new Chart("genderChart", {
                            type: "pie",
                            data: {
                                labels: xGenders,
                                datasets: [{
                                        backgroundColor: barColors1,
                                        data: yGenders
                                    }]
                            },
                            options: {
                                title: {
                                    display: true,
                                    text: "Gender of Vip Registration "
                                }
                            }
                        });
                        //pie chart access : Age
                        var xAgeAccesss = [];
                        var yAgeAccesss = [];
                        var xAgeAccess = document.getElementsByName("xAgeAccess");
                        var yAgeAccess = document.getElementsByName("yAgeAccess");
                        //document.getElementById("demo").innerHTML = x[0].value;
                        for (i = 0; i < xAgeAccess.length; i++) {
                            xAgeAccesss.push(xAgeAccess[i].value);
                            yAgeAccesss.push(yAgeAccess[i].value);
                        }

                        new Chart("ageAccessChart", {
                            type: "pie",
                            data: {
                                labels: xAgeAccesss,
                                datasets: [{
                                        backgroundColor: barColors2,
                                        data: yAgeAccesss
                                    }]
                            },
                            options: {
                                title: {
                                    display: true,
                                    text: "Age of Access"
                                }
                            }
                        });

                        //gender chart
                        var xGenderAccesss = [];
                        var yGenderAccesss = [];
                        var xGenderAccess = document.getElementsByName("xGenderAccess");
                        var yGenderAccess = document.getElementsByName("yGenderAccess");
                        //document.getElementById("demo").innerHTML = x[0].value;
                        for (i = 0; i < xGenderAccess.length; i++) {
                            xGenderAccesss.push(xGenderAccess[i].value);
                            yGenderAccesss.push(yGenderAccess[i].value);
                        }

                        new Chart("genderAccessChart", {
                            type: "pie",
                            data: {
                                labels: xGenderAccesss,
                                datasets: [{
                                        backgroundColor: barColors1,
                                        data: yGenderAccesss
                                    }]
                            },
                            options: {
                                title: {
                                    display: true,
                                    text: "Gender of Access "
                                }
                            }
                        });
                        //pie chart SignUp : Age
                        var xAgeSignUps = [];
                        var yAgeSignUps = [];
                        var xAgeSignUp = document.getElementsByName("xAgeSignUp");
                        var yAgeSignUp = document.getElementsByName("yAgeSignUp");
                        //document.getElementById("demo").innerHTML = x[0].value;
                        for (i = 0; i < xAgeSignUp.length; i++) {
                            xAgeSignUps.push(xAgeSignUp[i].value);
                            yAgeSignUps.push(yAgeSignUp[i].value);
                        }

                        new Chart("ageSignUpChart", {
                            type: "pie",
                            data: {
                                labels: xAgeSignUps,
                                datasets: [{
                                        backgroundColor: barColors2,
                                        data: yAgeSignUps
                                    }]
                            },
                            options: {
                                title: {
                                    display: true,
                                    text: "Age of New User"
                                }
                            }
                        });

                        //gender chart
                        var xGenderSignUps = [];
                        var yGenderSignUps = [];
                        var xGenderSignUp = document.getElementsByName("xGenderSignUp");
                        var yGenderSignUp = document.getElementsByName("yGenderSignUp");
                        //document.getElementById("demo").innerHTML = x[0].value;
                        for (i = 0; i < xGenderSignUp.length; i++) {
                            xGenderSignUps.push(xGenderSignUp[i].value);
                            yGenderSignUps.push(yGenderSignUp[i].value);
                        }

                        new Chart("genderSignUpChart", {
                            type: "pie",
                            data: {
                                labels: xGenderSignUps,
                                datasets: [{
                                        backgroundColor: barColors1,
                                        data: yGenderSignUps
                                    }]
                            },
                            options: {
                                title: {
                                    display: true,
                                    text: "Gender of New User "
                                }
                            }
                        });


                        function monthSubmit()
                        {
                            if (document.getElementById("allYear").selected) {
                                document.getElementById("notAll").innerHTML = "choose year first";
                                document.getElementById("allMonth").selected = true;
                            } else {
                                document.getElementById("filter").submit();
                            }
                        }
                        function yearSubmit()
                        {
                            if (document.getElementById("allYear").selected) {
                                document.getElementById("allMonth").selected = true;
                            }
                            document.getElementById("filter").submit();
                        }
        </script>
    </body>
</html>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

        <title>CRUD Slider</title>
        <link rel="icon" href="images/Quikk.png" type="image/png">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto|Varela+Round">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>      
     <style>
            body {
                color: #566787;
                background: #f5f5f5;
                font-family: 'Varela Round', sans-serif;
                font-size: 13px;
            }
            .table-responsive {
                margin: 30px 0;
            }
            .table-wrapper {
                min-width: 1000px;
                background: #fff;
                padding: 20px 25px;
                border-radius: 3px;
                box-shadow: 0 1px 1px rgba(0,0,0,.05);
            }
            .table-title {
                padding-bottom: 15px;
                background: #299be4;
                color: #fff;
                padding: 16px 30px;
                margin: -20px -25px 10px;
                border-radius: 3px 3px 0 0;
            }
            .table-title h2 {
                margin: 5px 0 0;
                font-size: 24px;
            }
            .table-title .btn {
                color: #566787;
                float: right;
                font-size: 13px;
                background: #fff;
                border: none;
                min-width: 50px;
                border-radius: 2px;
                border: none;
                outline: none !important;
                margin-left: 10px;
            }
            .table-title .btn:hover, .table-title .btn:focus {
                color: #566787;
                background: #f2f2f2;
            }
            .table-title .btn i {
                float: left;
                font-size: 21px;
                margin-right: 5px;
            }
            .table-title .btn span {
                float: left;
                margin-top: 2px;
            }
            table.table tr th, table.table tr td {
                border-color: #e9e9e9;
                padding: 12px 15px;
                vertical-align: middle;
            }
            table.table tr th:first-child {
                width: 60px;
            }
            table.table tr th:last-child {
                width: 100px;
            }
            table.table-striped tbody tr:nth-of-type(odd) {
                background-color: #fcfcfc;
            }
            table.table-striped.table-hover tbody tr:hover {
                background: #f5f5f5;
            }
            table.table th i {
                font-size: 13px;
                margin: 0 5px;
                cursor: pointer;
            }
            table.table td:last-child i {
                opacity: 0.9;
                font-size: 22px;
                margin: 0 5px;
            }
            table.table td a {
                font-weight: bold;
                color: #566787;
                display: inline-block;
                text-decoration: none;
            }
            table.table td a:hover {
                color: #2196F3;
            }
            table.table td a.settings {
                color: #2196F3;
            }
            table.table td a.delete {
                color: #F44336;
            }
            table.table td i {
                font-size: 19px;
            }
            table.table .avatar {
                border-radius: 50%;
                vertical-align: middle;
                margin-right: 10px;
            }
            .status {
                font-size: 30px;
                margin: 2px 2px 0 0;
                display: inline-block;
                vertical-align: middle;
                line-height: 10px;
            }
            .text-success {
                color: #10c469;
            }
            .text-info {
                color: #62c9e8;
            }
            .text-warning {
                color: #FFC107;
            }
            .text-danger {
                color: #ff5b5b;
            }
            .pagination {
                float: right;
                margin: 0 0 5px;
            }
            .pagination li a {
                border: none;
                font-size: 13px;
                min-width: 30px;
                min-height: 30px;
                color: #999;
                margin: 0 2px;
                line-height: 30px;
                border-radius: 2px !important;
                text-align: center;
                padding: 0 6px;
            }
            .pagination li a:hover {
                color: #666;
            }
            .pagination li.active a, .pagination li.active a.page-link {
                background: #03A9F4;
            }
            .pagination li.active a:hover {
                background: #0397d6;
            }
            .pagination li.disabled i {
                color: #ccc;
            }
            .pagination li i {
                font-size: 16px;
                padding-top: 6px
            }
            .hint-text {
                float: left;
                margin-top: 10px;
                font-size: 13px;
            }
        </style>
        <script>
            $(document).ready(function () {
                // Activate tooltip
                $('[data-toggle="tooltip"]').tooltip();

                // Select/Deselect checkboxes
                var checkbox = $('table tbody input[type="checkbox"]');
                $("#selectAll").click(function () {
                    if (this.checked) {
                        checkbox.each(function () {
                            this.checked = true;
                        });
                    } else {
                        checkbox.each(function () {
                            this.checked = false;
                        });
                    }
                });
                checkbox.click(function () {
                    if (!this.checked) {
                        $("#selectAll").prop("checked", false);
                    }
                });
            });
           
        </script>
         <script>
            $(document).ready(function () {
                // Activate tooltip
                $('[data-toggle="tooltip"]').tooltip();
            });
        </script>
         <jsp:include page="Common/Management.jsp"></jsp:include>
    </head>
    <body>
        <div class="container-xl">
            <div class="table-responsive">
                <!-- Your CRUD table here -->
                <form action="deleteSlider" method="post">
                    <div class="table-wrapper">
                        <div class="table-title">
                            <div class="row">
                                <div class="col-sm-6">
                                    <h2>Slider <b>Manager</b></h2>
                                </div>
                                <div class="col-sm-6">
                                    <a href="#addEmployeeModal" class="btn btn-success" data-toggle="modal"><i class="material-icons">&#xE147;</i> <span>Add</span></a>
                                    					
                                </div>
                            </div>
                        </div>

                            <c:if test="${not empty param.mess}">
                                <div style="text-align: center; color: green">
                                    ${param.mess}
                                </div>
                            </c:if>

                            <table class="table table-striped table-hover">
                                <thead>
                                    <tr>
                                        <th>[sliderId]</th>
                                        <th>[title]</th>
                                        <th>[content]</th>
                                        <th>[courseId]</th>
                                        <th>[image]</th>
                                        <th>[Action]</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach items="${requestScope.sliders}" var="item">
                                        <tr>
                                            <<td>${item.sliderId}</td>
                                            <td>${item.getTitle()}</td>
                                            <td>${item.getContent()}</td>
                                            <td>${item.courseId}</td>
                                            <td>
                                                <img style="max-height: 50px; max-width: 50px" src="${item.getImage()}" alt="Image "/>
                                            </td>
                                            <td>
                                                <a href="#editEmployeeModal" class="edit" data-toggle="modal" onclick="populateEditModal(${item.sliderId}, '${item.courseId}',
                                                            '${item.title}', '${item.content}', '${item.notes}', '${item.image}')"> 
                                                    <i class="material-icons" data-toggle="tooltip" title="Edit">&#xE254;</i> </a>
                                                <a href="deleteSlider?id=${item.getSliderId()}" class="delete" data-id="${item.getSliderId()}">
                                                    <i class="material-icons" data-toggle="tooltip" title="Delete">&#xE872;</i>
                                                </a>
                                            </td>
                                        </tr>
                                    </c:forEach>

                            </table>
                        <div class="clearfix">
                            <c:if test="${requestScope.totalSlider < 5}">
                                <div class="hint-text">Showing <b>${requestScope.totalSlider}</b> out of <b>${requestScope.totalSlider}</b> entries</div>
                            </c:if>
                            <c:if test="${requestScope.totalSlider > 5}">
                                <div class="hint-text">Showing <b>10</b> out of <b>${requestScope.totalSlider}</b> entries</div>
                            </c:if>                                                 
                            <ul class="pagination">
                                <li class="page-item disabled">
                                <c:if test="${requestScope.pageNum != 1}">
                                   </li>                            
                                    <c:forEach var="i" begin="1" end="${requestScope.pageNum}">
                                        <li class="page-item">
                                            <a href="slider?page=${i}" class="page-link">${i}</a>
                                        </li>
                                    </c:forEach>  
                                    <c:if test="${requestScope.currentPage != requestScope.pageNum}">
                                    
                                    </c:if> 
                                </c:if>   
                            </ul>

                        </div>
                    </div>
                </form>
                <div id="addEmployeeModal" class="modal fade">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <form action="add-sider" method="post">
                                <input type="hidden" name="action" value="add"> <!-- Th�m tr??ng ?n ?? ghi nh?n action -->

                                <div class="modal-header">						
                                    <h4 class="modal-title">Add Slider</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                </div>

                                <div class="modal-body">					
                                    <div class="form-group">
                                        <label>courseId</label>
                                        <input type="text" class="form-control" name="courseId" required>
                                    </div>

                                    <div class="form-group">
                                        <label>Title</label>
                                        <input type="text" class="form-control" name="title" required>
                                    </div>

                                    <div class="form-group">
                                        <label>Content</label>
                                        <textarea class="form-control" name="content" required></textarea>
                                    </div>


                                    <div class="form-group">
                                        <label>Image</label>
                                        <input type="text" class="form-control" name="image" placeholder="Input the image link" required>
                                    </div>
                                </div>

                                <div class="modal-footer">
                                    <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                                    <input type="submit" class="btn btn-success" value="Add">
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
                <!-- Edit Modal HTML -->
                <div id="editEmployeeModal" 

                     class="modal fade"> <div class="modal-dialog">
                        <div class="modal-content"> 
                            <form action="edit-slider" method="POST"> 
                                <div class="modal-header"> 
                                    <h4 class="modal-title">Edit Slider</h4> 
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                </div>
                                <div class="form-group">
                                    <input type="hidden" id="editSliderId" name="sliderId">
                                    <label>courseId</label> <input type="text" id="editcourseId"name="courseId" class="form-control" required>
                                </div>
                                <div class="form-group"> <label>Title</label> <input type="text" name="title"id="editTitle" class="form-control" > </div>
                                <div class="form-group"> <label>Content</label> <textarea name="content" id="editContent" class="form-control" required></textarea>
                                </div>
                                <div class="form-group"> <label>Image</label> <input type="text" name="image" id="editImage" class="form-control"
                                                                                     placeholder="Input the image link"> </div>
                        <div class="modal-footer"> <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel"> 
                            <input
                                type="submit" class="btn btn-info" value="Update">
                        </div>
                        </form>
                        </div>
                        
                    </div>
                </div>
            </div>
            <!-- Delete Modal HTML -->
            <div id="deleteEmployeeModal" class="modal fade">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <form>3
                            <div class="modal-header">						
                                <h4 class="modal-title">Delete Employee</h4>
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                            </div>
                            <div class="modal-body">					
                                <p>Are you sure you want to delete these Records?</p>
                                <p class="text-warning"><small>This action cannot be undone.</small></p>
                            </div>
                            <div class="modal-footer">
                                <input type="button" class="btn btn-default" data-dismiss="modal" value="Cancel">
                                <input type="submit" class="btn btn-danger" value="Delete">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

 <script>
                                        const ctx = document.getElementById('barchart');
                                        const barchart = new Chart(ctx, {
                                            type: 'bar',
                                            data: {
                                                labels: ['Quiz', 'Lession', 'Subject', 'Course', 'Blog'],
                                                datasets: [{
                                                        label: '# of Votes',
                                                        data: [12, 19, 3, 5, 2, 3],
                                                        borderWidth: 1
                                                    }]
                                            },
                                            options: {
                                                scales: {
                                                    y: {
                                                        beginAtZero: true
                                                    }
                                                }
                                            }
                                        });

                                        const ctx2 = document.getElementById('doughnut');
                                        const doughnut = new Chart(ctx2, {
                                            type: 'doughnut',
                                            data: {
                                                labels: ['Quiz', 'Lession', 'Subject', 'Course', 'Blog'],
                                                datasets: [{
                                                        label: '# of Votes',
                                                        data: [12, 19, 3, 5, 2, 3],
                                                        borderWidth: 1
                                                    }]
                                            },
                                            options: {
                                                scales: {
                                                    y: {
                                                        beginAtZero: true
                                                    }
                                                }
                                            }
                                        });
                                        function populateEditModal(sliderId, courseId, title, content, notes, image) {
    console.log('Received sliderId:', sliderId);
    // Set the values in the edit modal
    document.getElementById('editSliderId').value = sliderId;
    document.getElementById('editcourseId').value = courseId;
    document.getElementById('editTitle').value = title;
    document.getElementById('editContent').value = content;
    document.getElementById('editNotes').value = notes;
    document.getElementById('editImage').value = image;
}
    </script>
    <script data-cfasync="false" src="/cdn-cgi/scripts/5c5dd728/cloudflare-static/email-decode.min.js"></script><script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.4.1/dist/js/bootstrap.bundle.min.js"></script>
    <script>
                                        ClassicEditor
                                                .create(document.querySelector('#editor'))
                                                .catch(error => {
                                                    console.error(error);
                                                });
    </script>
</body>
</html>

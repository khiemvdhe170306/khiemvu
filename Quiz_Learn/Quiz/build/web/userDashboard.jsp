<%-- 
    Document   : userLevel
    Created on : Oct 17, 2023, 8:04:46 PM
    Author     : arans
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Dashboard</title>
        <link rel="icon" href="images/Quikk.png" type="image/png">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <!-- Boxicons -->
        <link href='https://unpkg.com/boxicons@2.0.9/css/boxicons.min.css' rel='stylesheet'>
        <!-- My CSS -->
        <link rel="stylesheet" href="css/userLevel.css">
        <style>

            .todo-list progress {
                width: 100%;
                height: 15px;
                appearance: none;
            }

            .todo-list progress::-webkit-progress-value {
                background: #3C91E6;
                border-radius: 5px;
            }
            .xp-info {
                text-align: center;
            }
            .level-progress {
                display: flex;
                align-items: center;
                justify-content: space-between;
                width: 100%;
            }

            .level-progress p {
                margin: 0;
                white-space: nowrap; /* Prevent text from wrapping */
                flex: 1; /* Allow the text to take up available space */
            }

            .level-progress progress {
                flex: 4; /* Allow the progress bar to take up more space */
            }
        </style>
        <title>User Hub</title>
    </head>
    <body>


        <!-- SIDEBAR -->
        <section id="sidebar">
            <a href="#" class="brand">
                <i class='bx bxs-smile'></i>
                <span class="text">User Hub</span>
            </a>
            <ul class="side-menu top">
                <li class="active">
                    <a href="userdashboard?">
                        <i class='bx bxs-dashboard' ></i>
                        <span class="text">Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="home">
                        <i class='bx bxs-shopping-bag-alt' ></i>
                        <span class="text">Home</span>
                    </a>
                </li>
                <c:if test = "${fn:contains(sessionScope.account.roleName.access, 'membership')}">
                    <li>
                        <a href="salechart">
                            <i class='bx bxs-doughnut-chart' ></i>
                            <span class="text">Sale Chart</span>
                        </a>
                    </li>
                </c:if>
                <c:if test = "${fn:contains(sessionScope.account.roleName.access, 'account')}">
                    <li >
                        <a href="userchart">
                            <i class='bx bxs-message-dots' ></i>
                            <span class="text">User Chart</span>
                        </a>
                    </li>
                </c:if>
                <c:if test = "${fn:contains(sessionScope.account.roleName.access, 'quiz')}">
                    <li >
                        <a href="ChartQuiz.jsp">
                            <i class='bx bxs-message-dots' ></i>
                            <span class="text">Website Chart</span>
                        </a>
                    </li>
                </c:if>

            </ul>

        </section>
        <!-- SIDEBAR -->



        <!-- CONTENT -->
        <section id="content">
            <!-- NAVBAR -->
            <nav>
                <i class='bx bx-menu' ></i>
                <input type="checkbox" id="switch-mode" hidden>
                <label for="switch-mode" class="switch-mode"></label>

            </nav>
            <!-- NAVBAR -->

            <!-- MAIN -->
            <main>
                <div class="head-title">
                    <div class="left">
                        <h1>Dashboard</h1>
                    </div>
                </div>

                <ul class="box-info">
                    <li>
                        <i class='bx bxs-calendar-check' ></i>
                        <span class="text">
                            <h3>${numberOfCourseRegistration.getTotalCourseCount()}/${totalNumberOfCourse}</h3>
                            <p>Courses</p>
                        </span>
                    </li>
                    <li>
                        <i class='bx bxs-check-square' ></i>
                        <span class="text">
                            <c:choose>
                                <c:when test="${currentLevel eq 0}">
                                    <h3>No attempt have been done</h3>
                                </c:when>
                                <c:otherwise>
                                    <h3>${numberOfQuizDone.getNumberOfQuizDone()}</h3>
                                </c:otherwise>
                            </c:choose>
                            Attempts
                        </span>
                    </li>
                    <li>
                        <i class='bx bx-chevrons-right'></i>
                        <a href="achievement" class="logout">
                            <span class="text">
                                <h3>See More</h3>
                                <p>Explore about your progress</p>
                            </span>
                        </a>
                    </li>
                </ul>


                <div class="table-data">
                    <div class="order">
                        <div class="head">
                            <h3>Ranking</h3>
                        </div>
                        <table>
                            <thead>
                                <tr>
                                    <th>User</th>
                                    <th>Ranking</th>
                                    <th>Level</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <% int count = 1; %>
                                <c:forEach items="${requestScope.rList}" var="rank" varStatus="loop">
                                    <c:if test="${loop.index < 10}">
                                        <c:set var="highlightRank" value="false" />
                                        <c:if test="${rank.user.email eq sessionScope.account.email}">
                                            <c:set var="highlightRank" value="true" />
                                        </c:if>
                                        <tr>
                                            <td>
                                                <c:if test="${rank.user.url ne null}">
                                                    <img src="${rank.user.url}" width="100" height="100">
                                                </c:if>
                                                <c:if test="${rank.user.url eq null}">
                                                    <img src="img/people.png">
                                                </c:if>
                                                <p ${highlightRank ? "style='color: red;'" : ""}>
                                                    ${fn:split(rank.user.email, '@')[0]}
                                                </p>
                                            </td>
                                            <td>
                                                <c:choose>
                                                    <c:when test="${rank.level.currentLevel eq 0}">
                                                        Non-ranked
                                                    </c:when>
                                                    <c:otherwise>
                                                        <%= count++ %>
                                                    </c:otherwise>
                                                </c:choose>
                                            </td>
                                            <td>${rank.level.currentLevel}</td>
                                            <td>
                                                <c:if test="${highlightRank}">
                                                    Me
                                                </c:if>
                                            </td>
                                        </tr>
                                        <c:if test="${highlightRank}">
                                            <c:set var="count" value="${loop.index + 1}" />
                                        </c:if>
                                    </c:if>
                                </c:forEach>
                            </tbody>
                        </table>

                        <li>
                            <a href="rank" class="logout">
                                <i class='bx bx-chevrons-left'></i>
                                <span class="text">SEE MORE</span>
                            </a>
                        </li>
                    </div>
                    <div class="todo">
                        <div class="head">
                            <h3>My Level</h3>
                        </div>
                        <ul class="todo-list">
                            <li class="completed">
                                <div class="level-progress">
                                    <p>Level ${currentLevel} </p>
                                    <progress max="${requiredXP}" value="${currentXP}"></progress>
                                </div>
                            </li>
                        </ul>
                        <div class="xp-info">
                            <p class="xp-paragraph">My XP to next level: ${currentXP}/${requiredXP}</p>
                        </div>
                        <div class="head">
                            <h3>My Voucher</h3>
                        </div>
                        <ul>
                            <c:choose>
                                <c:when test="${empty voucherByAccount}">
                                    <li>No voucher</li>
                                    </c:when>
                                    <c:otherwise>
                                        <c:forEach var="voucher" items="${voucherByAccount}">
                                        <li>
                                            Voucher: ${voucher.name} (${voucher.percentage}% off price)
                                        </li>
                                    </c:forEach>
                                </c:otherwise>
                            </c:choose>
                            <li>
                                <a href="courselist" class="logout">
                                    <i class='bx bx-chevrons-right'></i>
                                    <span class="text">Go Enroll Course</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </main>
            <!-- MAIN -->
        </section>
        <!-- CONTENT -->


        <script src="js/userLevel.js"></script>
    </body>
</html>
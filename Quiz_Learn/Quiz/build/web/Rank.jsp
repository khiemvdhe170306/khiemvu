<%-- 
    Document   : Rank
    Created on : Oct 20, 2023, 9:39:33 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <!-- Boxicons -->
        <link href='https://unpkg.com/boxicons@2.0.9/css/boxicons.min.css' rel='stylesheet'>
        <!-- My CSS -->
        <link rel="stylesheet" href="css/userLevel.css">
        <style>

            .todo-list progress {
                width: 100%;
                height: 15px;
                appearance: none;
            }

            .todo-list progress::-webkit-progress-value {
                background: #3C91E6;
                border-radius: 5px;
            }
            .xp-info {
                text-align: center;
            }
            .level-progress {
                display: flex;
                align-items: center;
                justify-content: space-between;
                width: 100%;
            }

            .level-progress p {
                margin: 0;
                white-space: nowrap; /* Prevent text from wrapping */
                flex: 1; /* Allow the text to take up available space */
            }

            .level-progress progress {
                flex: 4; /* Allow the progress bar to take up more space */
            }
        </style>
        <title>User Hub</title>
    </head>
    <body>


        <!-- SIDEBAR -->
        <section id="sidebar">
            <a href="#" class="brand">
                <i class='bx bxs-smile'></i>
                <span class="text">User Hub</span>
            </a>
            <ul class="side-menu top">
                <li class="active">
                    <a href="#">
                        <i class='bx bxs-dashboard' ></i>
                        <span class="text">Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class='bx bxs-shopping-bag-alt' ></i>
                        <span class="text">Home</span>
                    </a>
                </li>
                <li>
                    <a href="salechart">
                        <i class='bx bxs-doughnut-chart' ></i>
                        <span class="text">Sale Chart</span>
                    </a>
                </li>
                <li>
                    <a href="userchart">
                        <i class='bx bxs-message-dots' ></i>
                        <span class="text">User Chart</span>
                    </a>
                </li>
                <li >
                    <a href="ChartQuiz.jsp">
                        <i class='bx bxs-message-dots' ></i>
                        <span class="text">Quiz Chart</span>
                    </a>
                </li>

            </ul>

        </section>
        <!-- SIDEBAR -->



        <!-- CONTENT -->
        <section id="content">
            <!-- NAVBAR -->
            <nav>
                <i class='bx bx-menu' ></i>
                <a href="#" class="nav-link">Categories</a>
                <input type="checkbox" id="switch-mode" hidden>
                <label for="switch-mode" class="switch-mode"></label>

            </nav>
            <!-- NAVBAR -->

            <!-- MAIN -->
            <main>
                <div class="head-title">
                    <div class="left">
                        <h1>Dashboard</h1>
                        <ul class="breadcrumb">
                            <li>
                                <a href="#">Dashboard</a>
                            </li>
                            <li><i class='bx bx-chevron-right' ></i></li>
                            <li>
                                <a class="active" href="#">Home</a>
                            </li>
                        </ul>
                    </div>
                </div>

                


                <div class="table-data">
                    <div class="order">
                        <div class="head">
                            <h3>Ranking</h3>
                        </div>
                        <table>
                            <thead>
                                <tr>
                                    <th>User</th>
                                    <th>Ranking</th>
                                    <th>Level</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <% int count = 1; %>
                                <c:forEach items="${requestScope.rankList}" var="rank" varStatus="loop">
                                    <c:if test="${loop.index < 10}">
                                        <c:set var="highlightRank" value="false" />
                                        <c:if test="${rank.user.email eq sessionScope.account.email}">
                                            <c:set var="highlightRank" value="true" />
                                        </c:if>
                                        <tr>
                                            <td>
                                                <c:if test="${rank.user.url ne null}">
                                                    <img src="${rank.user.url}" width="100" height="100">
                                                </c:if>
                                                <c:if test="${rank.user.url eq null}">
                                                    <img src="img/people.png">
                                                </c:if>
                                                <p ${highlightRank ? "style='color: red;'" : ""}>${rank.user.email}</p>
                                            </td>
                                            <td>
                                                <c:choose>
                                                    <c:when test="${rank.level.currentLevel eq 0}">
                                                        Non-ranked
                                                    </c:when>
                                                    <c:otherwise>
                                                        <%= count++ %>
                                                    </c:otherwise>
                                                </c:choose>
                                            </td>
                                            <td>${rank.level.currentLevel}</td>
                                            <td>
                                                <c:if test="${highlightRank}">
                                                    Me
                                                </c:if>
                                            </td>
                                        </tr>
                                        <c:if test="${highlightRank}">
                                            <c:set var="count" value="${loop.index + 1}" />
                                        </c:if>
                                    </c:if>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="table-data">
                    <div class="order">
                        <div class="head">
                            <h3>Score Ranking</h3>
                        </div>
                        <table>
                            <thead>
                                <tr>
                                    <th>User</th>
                                    <th>Ranking</th>
                                    <th>Score</th>
                                    <th>Quiz</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <% int count1 = 1; %>
                                <c:forEach items="${requestScope.scoreList}" var="rank" varStatus="loop">

                                    <tr>
                                        <td>
                                            <c:if test="${rank.account.url ne null}">
                                                <img src="${rank.account.url}" width="100" height="100">
                                            </c:if>
                                            <c:if test="${rank.account.url eq null}">
                                                <img src="img/people.png">
                                            </c:if>
                                            ${rank.account.email}
                                        </td>
                                        <td>
                                            <c:choose>
                                                <c:when test="${rank.mark eq 0}">
                                                    Non-ranked
                                                </c:when>
                                                <c:otherwise>
                                                    <%= count1++ %>
                                                </c:otherwise>
                                            </c:choose>
                                        </td>
                                        <td>${rank.mark}</td>
                                        <td>
                                            ${rank.quizId}
                                        </td>
                                    </tr>


                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
            </main>
            <!-- MAIN -->
        </section>
        <!-- CONTENT -->


        <script src="js/userLevel.js"></script>
    </body>
</html>
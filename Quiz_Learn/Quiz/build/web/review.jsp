<%-- 
    Document   : review
    Created on : Oct 24, 2023, 1:27:04 PM
    Author     : Acer
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Review Payment</title>
        <link rel="icon" href="images/Quikk.png" type="image/png">
        <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <script src="//netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
        <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
        <style type="text/css">
            .submit-button {
                margin-top: 10px;
            }
        </style>
    </head>
    <body style="font-family: var(--poppins);">      
        <jsp:include page="Common/Header.jsp"></jsp:include>
            <div class="container" style="height: 100%; margin-top: 200px;" >
                <h1 class="col-md-12 text-center" style="color: blue; font-size: 36px">Please review before making a payment</h1>
                <div class='row'>
                    <div class='col-md-4'></div>
                    <div class='col-md-4'>                       
                        <script src='https://js.stripe.com/v2/' type='text/javascript'></script>
                        <form accept-charset="UTF-8" action="execute_payment" class="require-validation" data-cc-on-file="false" data-stripe-publishable-key="pk_bQQaTxnaZlzv4FnnuZ28LFHccVSaj" id="payment-form" method="post"><div style="margin:0;padding:0;display:inline"><input name="utf8" type="hidden" value="✓" /><input name="_method" type="hidden" value="PUT" /><input name="authenticity_token" type="hidden" value="qLZ9cScer7ZxqulsUWazw4x3cSEzv899SP/7ThPCOV8=" /></div>
                            <input type="hidden" name="paymentId" value="${param.paymentId}" />
                        <input type="hidden" name="PayerID" value="${param.PayerID}" />
                        <div class='form-row'>
                            <div class='col-xs-12 form-group required'>
                                <label style="font-size: 30px" class='control-label'>Description:</label>
                                <input style="font-size: 24px" class='form-control' size='4' type='text' value="${transaction.description}">
                            </div>
                        </div>
                        <div class='form-row'>
                            <div class='col-xs-12 form-group card required'>
                                <label style="font-size: 30px" class='control-label'>Price</label>
                                <input style="font-size: 24px" autocomplete='off' class='form-control card-number' value="${transaction.amount.total} USD" size='20' type='text'>
                            </div>
                        </div>
                        <h1 class="col-md-12 text-center" style="color: blue; font-size: 36px">Payer's Information</h1>
                        <div class='form-row'>
                            <div class='col-xs-12 form-group required'>
                                <label style="font-size: 30px" class='control-label'>First name</label>
                                <input style="font-size: 24px" class='form-control' type='text' value="${payer.firstName}">
                            </div>
                        </div>
                        <div class='form-row'>
                            <div class='col-xs-12 form-group required'>
                                <label style="font-size: 30px" class='control-label'>Last name</label>
                                <input style="font-size: 24px" class='form-control' type='text' value="${payer.lastName}">
                            </div>
                        </div>
                        <div class='form-row'>
                            <div class='col-xs-12 form-group required'>
                                <label style="font-size: 30px" class='control-label'>Email</label>
                                <input style="font-size: 24px" class='form-control' type='text' value="${payer.email}">
                            </div>
                        </div>
                        <div class='form-row'>
                            <div class='col-md-12'>
                                <div style="font-size: 24px" class='form-control total btn btn-info'>
                                    Total:
                                    <span class='amount'>$${transaction.amount.total}</span>
                                </div>
                            </div>
                        </div>
                        <div class='form-row'>
                            <div class='col-md-12 form-group'>
                                <button style="font-size: 24px" class='form-control btn btn-primary submit-button' onclick="this.form.submit()">Pay »</button>
                            </div>
                        </div>
                        <div class='form-row'>
                            <div class='col-md-12 error form-group hide'>
                                <div class='alert-danger alert'>
                                    Please correct the errors and try again.
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class='col-md-4'></div>
            </div>
        </div>
        <jsp:include page="Common/Sidebar.jsp"></jsp:include>
        <jsp:include page="Common/Footer.jsp"></jsp:include>
    </body>
</html>

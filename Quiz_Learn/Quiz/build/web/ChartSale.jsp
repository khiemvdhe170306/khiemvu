<%-- 
    Document   : userLevel
    Created on : Oct 17, 2023, 8:04:46 PM
    Author     : arans
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <!-- Boxicons -->
        <link href='https://unpkg.com/boxicons@2.0.9/css/boxicons.min.css' rel='stylesheet'>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!-- My CSS -->
        <link rel="stylesheet" href="css/userLevel.css">
        <style>


            .notify-badge{
                position: relative;
                left:35px;
                bottom:80px;
                background-image: url('images/wing.png');
                background-size: 13px 10px;
                text-align: center;
                border-radius: 30px 30px 30px 30px;
                color:gold;
                padding:5px 10px;
                font-size:10px;
            }
            img{
                border-radius: 50px;
                height: 100px;
            }
            .email-name{
                position: relative;
                left:150px;
                bottom:75px;
            }
            .transaction-name{
                position: relative;
                left:40px;
                bottom:20px;
            }
        </style>
        <title>User Hub</title>
    </head>
    <body>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.js"></script>
        <!-- SIDEBAR -->
        <section id="sidebar">
            <a href="#" class="brand">
                <i class='bx bxs-smile'></i>
                <span class="text">User Hub</span>
            </a>
            <ul class="side-menu top">
                <li >
                    <a href="userdashboard?">
                        <i class='bx bxs-dashboard' ></i>
                        <span class="text">Dashboard</span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <i class='bx bxs-shopping-bag-alt' ></i>
                        <span class="text">Home</span>
                    </a>
                </li>
                <c:if test = "${fn:contains(sessionScope.account.roleName.access, 'membership')}">
                <li class="active">
                    <a href="salechart">
                        <i class='bx bxs-doughnut-chart' ></i>
                        <span class="text">Sale Chart</span>
                    </a>
                </li>
                </c:if>
                 <c:if test = "${fn:contains(sessionScope.account.roleName.access, 'account')}">
                <li >
                    <a href="userchart">
                        <i class='bx bxs-message-dots' ></i>
                        <span class="text">User Chart</span>
                    </a>
                </li>
                </c:if>
                <c:if test = "${fn:contains(sessionScope.account.roleName.access, 'quiz')}">
                <li >
                    <a href="ChartQuiz.jsp">
                        <i class='bx bxs-message-dots' ></i>
                        <span class="text">Website Chart</span>
                    </a>
                </li>
                </c:if>
                
            </ul>

            
        </section>
        <!-- SIDEBAR -->



        <!-- CONTENT -->
        <section id="content">

            <!-- NAVBAR -->
            <nav>
                <i class='bx bx-menu' ></i>

                <input type="checkbox" id="switch-mode" hidden>
                <label for="switch-mode" class="switch-mode"></label>

                <a href="#" class="profile" style="position: absolute;left: 1300px">
                    <c:if test="${sessionScope.account.image eq null}">
                        <c:if test="${sessionScope.account.vip}"><span class="notify-badge">VIP</span></c:if><img src="${sessionScope.account.url}" width="100" >
                    </c:if> 
                    <c:if test="${sessionScope.account.image ne null}">
                        <c:if test="${sessionScope.account.vip}"><span class="notify-badge">VIP</span></c:if><img src="data:image/jpg;base64,${sessionScope.account.image}"  data-index="0" width="100" height="100">

                    </c:if>
                </a>
            </nav>
            <!-- NAVBAR -->

            <!-- MAIN -->
            <main>
                <div class="head-title">
                    <div class="left">
                        <h1>Dashboard</h1>
                        <ul class="breadcrumb">
                            <li>
                                <a href="#">Dashboard</a>
                            </li>
                            <li><i class='bx bx-chevron-right' ></i></li>
                            <li>
                                <a class="active" href="#">Home</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <form action="salechart" id="filter" style="font-size: large">
                    Year <select name="year" onchange="yearSubmit()" style="width: 150px">
                        <option value="0"id="allYear" ${param.year eq 0?'selected':''}>All</option>
                        <option value="2023" ${param.year eq 2023?'selected':''}>2023</option>
                        <option value="2022" ${param.year eq 2022?'selected':''}>2022</option>  
                    </select>&nbsp;&nbsp;&nbsp;
                    Month <select name="month" onchange="monthSubmit()" style="width: 150px">
                        <option value="0" id="allMonth" ${param.month eq 0?'selected':''}>All</option>
                        <option value="1" ${param.month eq 1?'selected':''}>January</option>
                        <option value="2" ${param.month eq 2?'selected':''}>February</option>
                        <option value="3" ${param.month eq 3?'selected':''}>March</option>
                        <option value="4" ${param.month eq 4?'selected':''}>April</option>
                        <option value="5" ${param.month eq 5?'selected':''}>May</option>
                        <option value="6" ${param.month eq 6?'selected':''}>June</option>
                        <option value="7" ${param.month eq 7?'selected':''}>July</option>
                        <option value="8" ${param.month eq 8?'selected':''}>August</option>
                        <option value="9" ${param.month eq 9?'selected':''}>September</option>
                        <option value="10" ${param.month eq 10?'selected':''}>October</option>
                        <option value="11" ${param.month eq 11?'selected':''}>November</option>
                        <option value="12" ${param.month eq 12?'selected':''}>December</option>           
                    </select>
                    &nbsp;&nbsp;&nbsp;
                    <a href="salechart"><button type="button">ALL</button></a>

                </form><p id="notAll" style="color:red;"></p>


                <ul class="box-info">
                    <li>
                        <i class='bx bxs-calendar-check' ></i>
                        <span class="text">
                            <h3>${requestScope.totalMoney} $
                                <c:if test="${requestScope.moneyChange>0}">
                                    <div style="color:green;"><fmt:formatNumber type="number" maxFractionDigits = "2" value="${requestScope.moneyChange}" />%<i class="material-icons" >arrow_upward</i></div>
                                </c:if>
                                <c:if test="${requestScope.moneyChange<0}">
                                    <div style="color:red;"><fmt:formatNumber type="number" maxFractionDigits = "2" value="${-requestScope.moneyChange}" />%<i class="material-icons" >arrow_downward</i></div>
                                </c:if>
                                <c:if test="${requestScope.moneyChange==0}">
                                    <div style="color:green;">0%<i class="material-icons" >dehaze</i></div>
                                </c:if>
                            </h3>
                            <p>Total Money</p>
                        </span>
                    </li>
                    <li>
                        <i class='bx bx-medal'></i>
                        <span class="text">
                            <h3>${requestScope.totalSale}
                                <c:if test="${requestScope.saleChange>0}">
                                    <div style="color:green;"><fmt:formatNumber type="number" maxFractionDigits = "2" value="${requestScope.saleChange}" />%<i class="material-icons" >arrow_upward</i></div>
                                </c:if>
                                <c:if test="${requestScope.saleChange<0}">
                                    <div style="color:red;"><fmt:formatNumber type="number" maxFractionDigits = "2" value="${-requestScope.saleChange}" />%<i class="material-icons" >arrow_downward</i></div>
                                </c:if>
                                <c:if test="${requestScope.saleChange==0}">
                                    <div style="color:green;">0%<i class="material-icons" >dehaze</i></div>
                                </c:if>
                            </h3>
                            <p>Membership Package Sold</p>
                        </span>
                    </li>
                    <li>
                        <i class='bx bxs-check-square' ></i>
                        <span class="text">
                            <h3>${requestScope.totalRegis}
                                <c:if test="${requestScope.courseChange>0}">
                                    <div style="color:green;"><fmt:formatNumber type="number" maxFractionDigits = "2" value="${requestScope.courseChange}" />%<i class="material-icons" >arrow_upward</i></div>
                                </c:if>
                                <c:if test="${requestScope.courseChange<0}">
                                    <div style="color:red;"><fmt:formatNumber type="number" maxFractionDigits = "2" value="${-requestScope.courseChange}" />%<i class="material-icons" >arrow_downward</i></div>
                                </c:if>
                                <c:if test="${requestScope.courseChange==0}">
                                    <div style="color:green;">0%<i class="material-icons" >dehaze</i></div>
                                </c:if>
                            </h3>
                            </h3>
                            <p>Course registration</p>
                        </span>
                    </li>
                </ul>
                <!--                            Table DATA 2-->
                <div class="table-data">
                    <div class="order" >
                        <div>
                            <div>
                                <c:forEach items="${requestScope.chart}" var="c" begin="0" end="10">
                                    <input type="hidden" name="xSale" value="${c.month}">
                                    <input type="hidden" name="ySale" value="${c.y}">
                                </c:forEach>
                            </div>
                            <div><canvas id="saleChart" style="width:100%;"></canvas></div>
                        </div>

                    </div>

                </div>
                <!--                            Table DATA 1-->
                <div class="table-data">
                    <div class="order" >
                        <div>
                            <c:forEach items="${requestScope.course}" var="c" begin="0" end="10">
                                <input type="hidden" name="xCourse" value="${c.course.courseName}">
                                <input type="hidden" name="yCourse" value="${c.y}">
                            </c:forEach>
                        </div>
                        <div ><canvas id="courseChart" style="width:100%;max-width:800px;font-size: smaller"></canvas></div>
                        <div>
                            <c:forEach items="${requestScope.price}" var="c" begin="0" end="10">
                                <input type="hidden" name="xPrice" value="${c.pricePackage.name}">
                                <input type="hidden" name="yPrice" value="${c.y}">
                            </c:forEach>
                        </div>
                        <div ><canvas id="priceChart" style="width:100%;max-width:800px;font-size: smaller"></canvas></div>

                    </div>
                    <div class="todo" >
                        <div class="head">
                            <h3>Top - Course </h3>
                        </div>
                        <table>
                            <thead>
                                <tr>
                                    <th style="width: 500px">User</th>
                                    <th>Registration:</th>

                                </tr>
                            </thead>

                            <tbody>
                                <c:forEach items="${requestScope.topcourse}" var="p" begin="0" end="4">
                                    <tr>
                                        <td >
                                            <a target="_blank" href="coursedetail?courseId=${p.course.courseId}"> <img src="${p.course.thumbnail}" alt="alt" width="100" height="100"/></a> 
                                            <p class="email-name">${p.course.courseName}</p>
                                        </td>
                                        <td><p class="transaction-name">${p.y}</p></td>

                                    </tr>
                                </c:forEach>



                            </tbody>
                        </table>
                        <c:if test="${fn:length(requestScope.topcourse) eq 0}"><h3>No product Sold</h3></c:if>

                    </div>

                </div>


            </main>
            <!-- MAIN -->
        </section>
        <!-- CONTENT -->


        <script src="js/userLevel.js"></script>
        <script>
                        //chart sale
                        let xSales = [];
                        let ySales = [];
                        var xSale = document.getElementsByName("xSale");
                        var ySale = document.getElementsByName("ySale");
                        //document.getElementById("demo").innerHTML = x[0].value;
                        for (i = 0; i < xSale.length; i++) {
                            xSales.push(xSale[i].value);
                            ySales.push(ySale[i].value);
                        }

                        new Chart("saleChart", {
                            type: "line",
                            data: {
                                labels: xSales,
                                datasets: [{
                                        fill: false,
                                        lineTension: 0,
                                        backgroundColor: "rgba(0,50,255,1.0)",
                                        borderColor: "rgba(0,50,255,0.1)",
                                        data: ySales
                                    }]
                            },
                            options: {
                                legend: {display: false},
                                title: {
                                    display: true,
                                    text: "DATA ANALASIS FOR TOTAL SALE GROWTH($)"
                                },
                                scales: {
                                    yAxes: [{ticks: {min: 0}}]
                                }
                            }
                        });


                        //chart course
                        var xCourses = [];
                        var yCourses = [];
                        var barColors = ["red", "green", "blue", "brown", "orange", "purple", "black", "yellow"];
                        var xCourse = document.getElementsByName("xCourse");
                        var yCourse = document.getElementsByName("yCourse");
                        for (i = 0; i < xCourse.length; i++) {
                            xCourses.push(xCourse[i].value);
                            yCourses.push(yCourse[i].value);
                        }


                        new Chart("courseChart", {
                            type: "bar",
                            data: {
                                labels: xCourses,
                                datasets: [{
                                        backgroundColor: barColors,
                                        data: yCourses
                                    }]
                            },
                            options: {
                                legend: {display: false},
                                title: {
                                    display: true,
                                    text: "Course Registration"
                                },
                                scales: {
                                    yAxes: [{ticks: {min: 0}}],

                                }
                            }
                        });


                        //chart price
                        var xPrices = [];
                        var yPrices = [];
                        var barColors = ["red", "green", "blue", "brown", "orange", "purple", "black", "yellow"];
                        var xPrice = document.getElementsByName("xPrice");
                        var yPrice = document.getElementsByName("yPrice");
                        for (i = 0; i < xPrice.length; i++) {
                            xPrices.push(xPrice[i].value);
                            yPrices.push(yPrice[i].value);
                        }


                        new Chart("priceChart", {
                            type: "bar",
                            data: {
                                labels: xPrices,
                                datasets: [{
                                        backgroundColor: barColors,
                                        data: yPrices
                                    }]
                            },
                            options: {
                                legend: {display: false},
                                title: {
                                    display: true,
                                    text: "Sale Distribution of Each Member PricePackage"
                                },
                                scales: {
                                    yAxes: [{ticks: {min: 0}}],

                                }
                            }
                        });
                        function monthSubmit()
                        {
                            if (document.getElementById("allYear").selected) {
                                document.getElementById("notAll").innerHTML = "choose year first";
                                document.getElementById("allMonth").selected = true;
                            } else {
                                document.getElementById("filter").submit();
                            }
                        }
                        function yearSubmit()
                        {
                            if (document.getElementById("allYear").selected) {
                                document.getElementById("allMonth").selected = true;
                            }
                            document.getElementById("filter").submit();
                        }
        </script>
    </body>
</html>
<%-- 
    Document   : result
    Created on : Sep 11, 2023, 10:24:53 PM
    Author     : khiem
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<!DOCTYPE html>
<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Review</title>
        <link rel="icon" href="images/Quikk.png" type="image/png">
        <link href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css' rel='stylesheet'>
        <script type='text/javascript' src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js'></script>
        <style>
            html * {

                font-family: 'Montserrat', sans-serif;

            }
            ::-webkit-scrollbar {
                width: 8px;
            }

            /* Track */
            ::-webkit-scrollbar-track {
                background: #f1f1f1;
            }

            /* Handle */
            ::-webkit-scrollbar-thumb {
                background: #888;
            }

            /* Handle on hover */
            ::-webkit-scrollbar-thumb:hover {
                background: #555;
            }

            body {
                background-color: #616161 !important;
            }

            label.btn {
                padding: 18px 60px;
                white-space: normal;
                -webkit-transform: scale(1.0);
                -moz-transform: scale(1.0);
                -o-transform: scale(1.0);
                -webkit-transition-duration: .3s;
                -moz-transition-duration: .3s;
                -o-transition-duration: .3s
            }

            label.btn:hover {
                text-shadow: 0 3px 2px rgba(0, 0, 0, 0.4);
                -webkit-transform: scale(1.1);
                -moz-transform: scale(1.1);
                -o-transform: scale(1.1)
            }
            
/*            doi mau ten*/
            label.btn-block {
                color: black;
                text-align: left;
                position: relative;
                border: #888 solid thin ;
                border-radius: 2px
            }

            label .btn-label {
                position: absolute;
                left: 0;
                top: 0;
                display: inline-block;
                padding: 0 10px;
                background: #000000;
                height: 100%
            }

            label .glyphicon {
                top: 34%
            }
            .element-animation1 {
                animation: animationFrames ease .8s;
                animation-iteration-count: 1;
                transform-origin: 50% 50%;
                -webkit-animation: animationFrames ease .8s;
                -webkit-animation-iteration-count: 1;
                -webkit-transform-origin: 50% 50%;
                -ms-animation: animationFrames ease .8s;
                -ms-animation-iteration-count: 1;
                -ms-transform-origin: 50% 50%
            }

            .element-animation2 {
                animation: animationFrames ease 1s;
                animation-iteration-count: 1;
                transform-origin: 50% 50%;
                -webkit-animation: animationFrames ease 1s;
                -webkit-animation-iteration-count: 1;
                -webkit-transform-origin: 50% 50%;
                -ms-animation: animationFrames ease 1s;
                -ms-animation-iteration-count: 1;
                -ms-transform-origin: 50% 50%
            }

            .element-animation3 {
                animation: animationFrames ease 1.2s;
                animation-iteration-count: 1;
                transform-origin: 50% 50%;
                -webkit-animation: animationFrames ease 1.2s;
                -webkit-animation-iteration-count: 1;
                -webkit-transform-origin: 50% 50%;
                -ms-animation: animationFrames ease 1.2s;
                -ms-animation-iteration-count: 1;
                -ms-transform-origin: 50% 50%
            }

            .element-animation4 {
                animation: animationFrames ease 1.4s;
                animation-iteration-count: 1;
                transform-origin: 50% 50%;
                -webkit-animation: animationFrames ease 1.4s;
                -webkit-animation-iteration-count: 1;
                -webkit-transform-origin: 50% 50%;
                -ms-animation: animationFrames ease 1.4s;
                -ms-animation-iteration-count: 1;
                -ms-transform-origin: 50% 50%
            }

            @keyframes animationFrames {
                0% {
                    opacity: 0;
                    transform: translate(-1500px, 0px)
                }

                60% {
                    opacity: 1;
                    transform: translate(30px, 0px)
                }

                80% {
                    transform: translate(-10px, 0px)
                }

                100% {
                    opacity: 1;
                    transform: translate(0px, 0px)
                }
            }

            @-webkit-keyframes animationFrames {
                0% {
                    opacity: 0;
                    -webkit-transform: translate(-1500px, 0px)
                }

                60% {
                    opacity: 1;
                    -webkit-transform: translate(30px, 0px)
                }

                80% {
                    -webkit-transform: translate(-10px, 0px)
                }

                100% {
                    opacity: 1;
                    -webkit-transform: translate(0px, 0px)
                }
            }

            .modal-header {
                background-color: transparent;
                color: inherit
            }

            .modal-body {
                min-height: 300px
            }
            /*            mau luc chua an*/
            .btn-danger{
                
                background-color: white !important

            }
/*            mau luc an vao*/
            .btn-danger:hover, .btn-danger:focus, .btn-danger.focus, .btn-danger:active, .btn-danger.active, .open>.dropdown-toggle.btn-danger{
                
                background-color: #d1e8ff !important;
            }
            div #1{
                display: none !important;
            }
            td{
                padding: 10px;
            }
        </style>
    </head>
    <body onload="load()">

        <jsp:include page="Common/Header.jsp"></jsp:include>
            <div class="container-fluid" style="background-color: #F8F6F4 !important;">
                <div style="margin-top:100px;" class="col-md-12"></div>
                <div class="col-md-8">
                <%int a=1;%>
                <c:if test="${requestScope.message!=null}">
                    <div style="display: flex;justify-content: center;justify-items: center;margin-bottom: 20px">
                        <div class="btn btn-success" style="border-radius: 10px">
                            <h5>${requestScope.message} </h5>
                            <a style="color: green;font-size: medium" href="userdashboard">View<img src="https://img.freepik.com/premium-vector/gift-voucher-template-isolated-gray-background-discount-coupon-50-off-promotion-sale_97458-853.jpg?w=996" width="50px" alt="alt"/>
                               </a> 
                          </div>
                    </div>
                     
                          
                         
                              
                               
                                           
                        </c:if>
                <table border="3" style="width: 95%;margin-left: 30px;margin-right: 30px; margin-bottom: 50px;font-size: large;border-radius: 5px;background-color: white ">
                    <tbody style="margin:10px">
                        <tr>
                            <td>Started on</td>

                            <td><fmt:formatDate pattern = "yyyy-MM-dd HH:mm" value = "${requestScope.start}" /></td>
                        </tr>
                        <tr>
                            <td>Completed on</td>
                            <td><fmt:formatDate pattern = "yyyy-MM-dd HH:mm" value = "${requestScope.end}" /></td>
                        </tr>
                        <tr>
                            <td>Time Taken</td>
                            <td>${requestScope.time_taken}</td>
                        </tr>
                        <tr>
                            <td>Marks</td>
                            <td>${requestScope.marks}/${fn:length(requestScope.question)}</td>
                        </tr>
                        <tr>
                            <td>Grade</td>
                            <c:set var="Time"><fmt:formatNumber type="number" maxIntegerDigits = "1" maxFractionDigits = "1" value="${requestScope.grade}" /></c:set>
                            <td>${Time},0 out of 10,00</td>
                        </tr>
                    </tbody>
                </table>
                
                
                <c:forEach items="${requestScope.question}" var="q" >


                    <div id="<%=a%>" style="width: 95%;margin-bottom: 20px" >
                        <div class="modal-content" style="margin-left:30px" >

                            <div class="modal-header">
                                <h3>Question <%=a%>: ${q.qContent} 
                                    <c:if test="${q.qValue}"><span style="margin:0px 20px;color: #5D9C59">Correct</span></c:if><c:if test="${!q.qValue}"><span  style="margin:0px 20px;color: #DF2E38">Incorrect</span></c:if></h3>

                                </div>

                                <div class="modal-body">
                                    <div class="col-xs-3 5"> </div>
                                    <div class="quiz" id="quiz" data-toggle="buttons">

                                    <c:forEach items="${q.listAnswer}" var="a">
                                        <label class=" btn btn-lg btn-danger  btn-block"  <c:if test="${a.choice && a.correct}">style="background-color: #5D9C59 !important;"</c:if>
                                               <c:if test="${a.choice && !a.correct}">style="background-color: #DF2E38 !important;"</c:if>><span class="btn-label"><i
                                                           class="" > </i></span> <input type="checkbox" 
                                                                                 disabled   > ${a.content}</label>  


                                    </c:forEach>
                                </div>
                            </div>
                            <h3 style="display: flex;flex-direction: row"> True Answer:<c:forEach items="${q.choice}" var="choice"><p style="margin:0px 10px">${choice.content}</p></c:forEach></h3>


                            </div>
                        </div>
                    <%a++;%>

                    <c:if test="${sessionScope.account.vip}">
                    <div  style="width: 98%;margin-bottom: 10px;padding: 10px" >
                        <div class="modal-content" style="margin-left:20px" >
                            
                            <h3  onclick="Explain(<%=a%>)" style="display: flex;justify-content: center;justify-items: center;padding: 10px;"><button><i class="fa fa-question-circle" style="font-size:30px;color:#F7D060;"></i> <h5  id="Click">Click to see explanation</h5></button></h3>
                            
                            <h3 id="explain<%=a%>" style="padding: 10px;margin-left: 5px">Explanation ${q.qExplainContent}</h3>
                        </div>
                    </div>
                    </c:if>
                    <c:if test="${!sessionScope.account.vip}">
                    <div  style="width: 98%;margin-bottom: 10px;padding: 10px" >
                        <div class="modal-content" style="margin-left:20px" >  
                            <a target="_blank" href="membership" style="display: flex;justify-content: center;justify-items: center;padding: 10px;"><button><i class="fa fa-question-circle" style="font-size:30px;color:green"></i> <h5 >Register to see explanation</h5></button></a>
                            
                        </div>
                    </div>
                    </c:if>
                </c:forEach>               
            </div>
            <input type="hidden" name="list" id="list" value="${fn:length(requestScope.question)-1}">
            <div class="col-md-3" style="height: 300px;background-color: white;border-radius: 8px;border: 1px solid #000000;box-shadow: 0px 4px 6px rgba(0, 0, 0, 0.2);" >
                <%int i=1;%>
                <div style="margin-left:  10px;" >
                    <c:forEach items="${requestScope.question}" var="q">
                        <input type="button" name="page" id="page<%=i%>" value="<%=i%>" onclick="myFunction(<%=i%>)" style="width:40px; height: 50px;border-radius: 10px;margin:10px;color:black;background-color: #5D9C59; <c:if test="${q.qValue}">color: black;</c:if><c:if test="${!q.qValue}">color:white;background-color: #DF2E38;</c:if>"  />
                        <%i++;%>
                    </c:forEach>

                </div>
                <form action="quizattempt" style="display: flex;justify-content:center;align-items: center;flex-direction: column;margin-bottom: 50px">
                    <input style="width: 175px;height: 45px;border-radius:5px;background-color: #F9FCFB" type="submit" value="Finish Attempt" />
                    <input type="hidden" name="quizId" value="${sessionScope.quiz.quizId}">
                </form> 
            </div>
        </div>


        <jsp:include page="Common/QuizSider.jsp"></jsp:include> 
        <jsp:include page="Common/Footer.jsp"></jsp:include>

    </body>
    <script type='text/javascript' src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js'></script>
    <script >
                            var y = parseInt(document.getElementById("list").value) + 2;
                            function load()
                            {
                                
                                for (let ai = 2; ai < y; ai++) {
                                    var z = document.getElementById("explain"+ai);
                                    z.style.display = "none";
                                }
                                

                            }
                            function myFunction(i)
                            {
                                var x = document.getElementById(i.toString());
                                x.scrollIntoView({behavior: 'smooth', block: 'center'});
                            }
                            

                            function Explain(i)
                            {
                                var z = document.getElementById("explain" + i);
                                if (z.style.display === "none")
                                {
                                    z.style.display = "block";
                                    document.getElementById("Click").innerHTML = "Click to close explaination";
                                } else {
                                    z.style.display = "none";
                                    document.getElementById("Click").innerHTML = "Click to see explaination";
                                }
                            }
    </script>
</html>

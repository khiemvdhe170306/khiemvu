/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package question;

import dal.AnswerDAO;
import dal.QuestionDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.Answer;
import model.Question;


/**
 *
 * @author khiem
 */
@WebServlet(name="EditAnswer", urlPatterns={"/editAnswer"})
public class EditAnswer extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Testing</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Testing at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        AnswerDAO ad = new AnswerDAO();
        QuestionDAO qd = new QuestionDAO();
        String answer_raw = request.getParameter("aid");
        HttpSession session = request.getSession();
        Question q = (Question) session.getAttribute("question");
        int aid = 0;
        if (answer_raw != null) {
            try {
                aid = Integer.parseInt(answer_raw);
            } catch (Exception e) {
            }
        }
        String action = request.getParameter("action");
        if (action.equalsIgnoreCase("update")) {//update
            //check duplicate content
            Answer a = ad.getAnswerById(aid);
            request.setAttribute("answer", a);
            request.setAttribute("action", "update");
            request.getRequestDispatcher("crudAnswer.jsp").forward(request, response);
        } else if (action.equalsIgnoreCase("add")) {
            request.setAttribute("action", "add");
            request.getRequestDispatcher("crudAnswer.jsp").forward(request, response);
        } else if (action.equalsIgnoreCase("delete")) {//delete
            Answer a = ad.getAnswerById(aid);
            //get question from session

            //check if it's only true answer or not to delete
            if (ad.checkAnswerCorrect(a, qd.getQuestionById(a.getQuestionId()))) {
                request.setAttribute("status", "delete answer successfully");
                ad.deleteAnswer(aid);
                //check if it's correct = true then update question num of true
                if (a.isCorrect()) {
                    ad.updateAnswerTrue(q.getQuestionId(), false);
                }
            } else {
                request.setAttribute("status", "cannot delete the only true answer");
            }

            request.getRequestDispatcher("answermanagement?questionId=" + q.getQuestionId()).forward(request, response);

        } else if (action.equalsIgnoreCase("deleteAll")) {
            ad.deleteAll(q.getQuestionId());
            request.getRequestDispatcher("answermanagement?questionId=" + q.getQuestionId()).forward(request, response);
        }

    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        AnswerDAO ad = new AnswerDAO();
        QuestionDAO qd = new QuestionDAO();
        String action = request.getParameter("action");
        String content = request.getParameter("content").trim();
        String correct_raw = request.getParameter("correct");
        boolean correct = false;
        if (correct_raw.equalsIgnoreCase("1")) {
            correct = true;
        }
        HttpSession session = request.getSession();
        Question q = (Question) session.getAttribute("question");
        if (action.equalsIgnoreCase("add")) {
            
            //check content duplicate
            if (ad.getAnswerByContent(content,q.getQuestionId()) == 0) {
                out.println("null");
                ad.addAnswer(content, correct, q.getQuestionId());
                if (correct) {
                    ad.updateAnswerTrue(q.getQuestionId(), true);
                }
                request.setAttribute("status", "add answer successfully");
                request.getRequestDispatcher("answermanagement").forward(request, response);
                
            } else {
                out.println("not null");
                request.setAttribute("status", "duplicate answer! Unable to add");
                request.getRequestDispatcher("answermanagement").forward(request, response);
            }
            //change
        } else if (action.equalsIgnoreCase("update")) {
            String answer_raw = request.getParameter("aid");
            int aid = 0;
            if (answer_raw != null) {
                try {
                    aid = Integer.parseInt(answer_raw);
                } catch (Exception e) {
                }
            }
            Answer a = ad.getAnswerById(aid);
            //check content duplicate
            if (ad.getAnswerByContent(content,q.getQuestionId()) ==1&&!content.equalsIgnoreCase(a.getContent())) {
                request.setAttribute("status", "duplicate answer! Unable to update");
                request.getRequestDispatcher("answermanagement").forward(request, response);
            }else{
            
            if (ad.updateAnswerToFalse(a, correct)) {
                if (!a.isCorrect() && correct) {
                    ad.updateAnswerTrue(q.getQuestionId(), true);
                } else if (a.isCorrect() && !correct) {
                    ad.updateAnswerTrue(q.getQuestionId(), false);
                }
                ad.updateAnswer(aid, content, correct);
                request.setAttribute("status", "update answer successfully");
            } else {
                request.setAttribute("status", "cannot change the only true answer to false");
            }
            request.getRequestDispatcher("answermanagement").forward(request, response);
        }}

    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

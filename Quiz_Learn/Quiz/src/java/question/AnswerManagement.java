/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package question;

import dal.AnswerDAO;
import dal.QuestionDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.Answer;
import model.Question;

/**
 *
 * @author khiem
 */
@WebServlet(name="AnswerManagement", urlPatterns={"/answermanagement"})
public class AnswerManagement extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AnswerManagement</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AnswerManagement at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {        
        PrintWriter out = response.getWriter();     
        //get question Id
        String question_raw=request.getParameter("questionId");
        String page_raw=request.getParameter("page");
        
        int questionId=1;
        if(question_raw!=null)
            try {
            questionId=Integer.parseInt(question_raw);
        } catch (Exception e) {
            out.println(e);
        }    
        int page=1;
        if(page_raw!=null)
            try {
            page=Integer.parseInt(page_raw);
        } catch (Exception e) {
            out.println(e);
        }
           
        QuestionDAO qd=new QuestionDAO();
        //AnswerDAO ad=new AnswerDAO();       
        Question q=qd.getQuestionById(questionId);
        //List<Answer> alist=q.getListAnswer();
        
        HttpSession session = request.getSession();
        session.setAttribute("question", q);
        request.setAttribute("page", page);
        //request.setAttribute("answer", alist);
        request.getRequestDispatcher("answerManagement.jsp").forward(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        HttpSession session = request.getSession();
        Question q=(Question) session.getAttribute("question");
        QuestionDAO qd=new QuestionDAO();
        int qid=q.getQuestionId();
        session.setAttribute("question", qd.getQuestionById(qid));
        request.setAttribute("page", 1);
         request.getRequestDispatcher("answerManagement.jsp").forward(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

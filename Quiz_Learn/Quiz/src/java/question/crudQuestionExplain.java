/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package question;

import dal.AnswerDAO;
import dal.QuestionDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import static java.lang.System.out;
import java.util.List;
import model.Account;
import model.Question;

/**
 *
 * @author arans
 */
@WebServlet(name = "crudQuestionExplain", urlPatterns = {"/crudquestionexplain"})
public class crudQuestionExplain extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet crudQuestionExplain</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet crudQuestionExplain at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        Account a = (Account) session.getAttribute("acc");
        //get old question
        String action = request.getParameter("action");
        QuestionDAO qd = new QuestionDAO();

        if (action.equalsIgnoreCase("add")) {
            int questionId = Integer.parseInt(request.getParameter("questionId"));
            Question q = qd.getQuestionById(questionId);
            request.setAttribute("action", action);
            request.setAttribute("question", q);

            request.getRequestDispatcher("crudQuestionExplain.jsp").forward(request, response);

        } else if (action.equalsIgnoreCase("update")) {
            int questionId = Integer.parseInt(request.getParameter("questionId"));
            Question q = qd.getQuestionExplainById(questionId);
            request.setAttribute("action", action);

            request.setAttribute("question", q);
            request.getRequestDispatcher("crudQuestionExplain.jsp").forward(request, response);
            //delete
         } else {
            int questionId = Integer.parseInt(request.getParameter("questionId"));
            Question q = qd.deleteQuestionExplain(questionId);
            request.setAttribute("action", action);
            request.getRequestDispatcher("answermanagement").forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        QuestionDAO qd = new QuestionDAO();
        String action = request.getParameter("action");
        HttpSession session = request.getSession(false);

        QuestionDAO ql = new QuestionDAO();
        String qExplainContent = request.getParameter("qExplainContent");
        Account a = (Account) session.getAttribute("account");
        if (action.equalsIgnoreCase("add")) {
            int questionId = Integer.parseInt(request.getParameter("questionId"));
            out.print(questionId);
            out.print(qExplainContent);
            qd.addQuestionExplain(questionId, qExplainContent);
            request.setAttribute("action", action);
            out.print(qd);
            request.getRequestDispatcher("answermanagement").forward(request, response);

        } else if (action.equalsIgnoreCase("update")) {
            int questionId = Integer.parseInt(request.getParameter("questionId"));
            Question q = qd.getQuestionExplainById(questionId);
            request.setAttribute("action", action);
            q = qd.updateQuestionExplain(questionId, qExplainContent);
            request.getRequestDispatcher("answermanagement").forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

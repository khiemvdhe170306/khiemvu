/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package question;

import dal.AnswerDAO;
import dal.QuestionDAO;
import dal.QuizDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.Account;
import model.Question;
import model.Quiz;

/**
 *
 * @author arans
 */
@WebServlet(name = "crudQuestion", urlPatterns = {"/crudquestion"})
public class crudQuestion extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet crudQuestion</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet crudQuestion at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        Account a = (Account) session.getAttribute("acc");
        //get old question
        String action = request.getParameter("action");
        String message = "";
        QuestionDAO qd = new QuestionDAO();

        QuizDAO quiz = new QuizDAO();
        List<Quiz> quizList = quiz.getAllQuiz();

        if (action.equalsIgnoreCase("add")) {
            request.setAttribute("action", action);
            request.setAttribute("quizList", quizList);
            out.print(quizList);
            request.getRequestDispatcher("crudQuestion.jsp").forward(request, response);

        } else if (action.equalsIgnoreCase("update")) {
            int questionId = Integer.parseInt(request.getParameter("questionId"));
            Question q = qd.getQuestionById(questionId);
            request.setAttribute("action", action);

            request.setAttribute("question", q);
            request.setAttribute("quizList", quizList);
            request.getRequestDispatcher("crudQuestion.jsp").forward(request, response);
            //delete
        } else {
            int questionId = Integer.parseInt(request.getParameter("questionId"));
            int deleteStatus = qd.deleteQuestion(questionId);
            if (deleteStatus == 1) {
                message = "Question deleted successfully";
            } else {
                message = "Failed to delete question (there are answers in this question, you must delete them first)";
            }

            request.setAttribute("message", message); // Pass the message to the JSP
            request.getRequestDispatcher("questionmanagement").forward(request, response);
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //update to new question
        QuestionDAO qd = new QuestionDAO();
        AnswerDAO ad = new AnswerDAO();
        String action = request.getParameter("action");
        HttpSession session = request.getSession();
        String page_raw = request.getParameter("page");
        QuestionDAO ql = new QuestionDAO();
        List<Question> questionlist = ql.getAllQuestion();
        int page = 1;
        if (page_raw != null) {
            page = Integer.parseInt(page_raw);
        }
        long millis = System.currentTimeMillis();
        // creating a new object of the class Date  
        PrintWriter out = response.getWriter();
        java.sql.Date date = new java.sql.Date(millis);
        String qContent = request.getParameter("qContent");
        String quizId = request.getParameter("quizId");
        System.out.println("123" + qContent);
        Quiz quizz = (Quiz) session.getAttribute("quiz");
        Account a = (Account) session.getAttribute("account");
        if (action.equalsIgnoreCase("add")) {
            qd.addQuestion(qContent, quizz.getQuizId());
            out.print(quizz.getQuizId());
            ad.addAnswerTrue(qd.getQuestionId());
            String message = "Question added successfully";
            request.setAttribute("message", message);
            request.setAttribute("questionlist", questionlist);
            request.setAttribute("page", page);

            if (quizz.getQuizId() > 0) {
                response.sendRedirect("questionmanagement?quiz=" + quizz.getQuizId());
            } else {
                response.sendRedirect("questionmanagement");
            }

        } else if (action.equalsIgnoreCase("update")) {
            int questionId = Integer.parseInt(request.getParameter("questionId"));
            Question q = qd.getQuestionById(questionId);

            request.setAttribute("action", action);
            q = qd.updateQuestion(questionId, qContent, quizz.getQuizId());
            String message = "Question updated successfully";
            request.setAttribute("message", message);
            request.setAttribute("questionlist", questionlist);
            request.setAttribute("page", page);
            if (quizz.getQuizId() > 0) {
                response.sendRedirect("questionmanagement?quiz=" + quizz.getQuizId());
            } else {
                response.sendRedirect("questionmanagement");
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

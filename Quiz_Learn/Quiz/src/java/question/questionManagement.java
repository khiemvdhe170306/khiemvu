/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package question;

import dal.QuestionDAO;
import dal.QuizDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import static java.lang.System.out;
import java.util.List;
import model.Question;
import model.Quiz;

/**
 *
 * @author arans
 */
@WebServlet(name = "questionManagement", urlPatterns = {"/questionmanagement"})
public class questionManagement extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet questionManagement</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet questionManagement at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String searchq = request.getParameter("searchq");
        String page_raw = request.getParameter("page");
        String quiz_raw = request.getParameter("quiz");
        QuestionDAO qd = new QuestionDAO();
        QuizDAO quizd = new QuizDAO();
        List<Quiz> quizList = quizd.getAllQuiz();
        List<Question> questionList = qd.getAllQuestion();
        int page = 1;
        if (page_raw != null) {
            page = Integer.parseInt(page_raw);
        }
        if (searchq != null) {
            questionList = qd.searchQuestionByContent(searchq);
        }
        for (Question quest : questionList) {
            out.println(quest.getQuiz().getTitle());
        }
        if (quiz_raw != null) {
            if (quiz_raw.equalsIgnoreCase("0"))//get all course
            {
            } else {
                int quiz = Integer.parseInt(quiz_raw);
                questionList = qd.getAllQuestionByQuiz(quiz);
            }
        }
        if (searchq != null && quiz_raw != null) {
            int quiz = Integer.parseInt(quiz_raw);
            questionList = qd.getAllQuestionByQuizAndContent(quiz, searchq);
        }
        if (quiz_raw != null) {
            int quiz = Integer.parseInt(quiz_raw);
            Quiz quizmodel = quizd.getQuizById(quiz);
            HttpSession session = request.getSession();
            session.setAttribute("quiz", quizmodel);
        }
        request.setAttribute("questionList", questionList);
        request.setAttribute("page", page);
        request.setAttribute("quizList", quizList);
        request.getRequestDispatcher("questionManagement.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        QuestionDAO qd = new QuestionDAO();
        QuizDAO quizd = new QuizDAO();
        List<Quiz> quizList = quizd.getAllQuiz();
        List<Question> questionList = qd.getAllQuestion();
        request.setAttribute("questionList", questionList);
        request.setAttribute("page", 1);
        request.setAttribute("quizList", quizList);
        request.getRequestDispatcher("questionManagement.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

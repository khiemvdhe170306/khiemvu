package controller;

import java.io.IOException;
import java.util.List;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Slider;
import dal.SliderDAO;

@WebServlet(name = "SliderController", urlPatterns = {"/slider"})
public class SliderController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        SliderDAO sliderDAO = new SliderDAO(); 
        String raw_page = request.getParameter("page");
        int currentPage = 0;
        String sliderIdParam = request.getParameter("sliderId");
        double totalSlider = sliderDAO.getTotalSlider();
             
    if (sliderIdParam != null && !sliderIdParam.isEmpty()) {
        int sliderId = Integer.parseInt(sliderIdParam);
        SliderDAO dao = new SliderDAO();
        Slider slider = dao.getSliderById(sliderId);
        request.setAttribute("slider", slider);
        request.getRequestDispatcher("crudSlider.jsp").forward(request, response);
    }
            

            // Redirect or forward to the appropriate editing page with the sliderId
            // For example:
            // request.getRequestDispatcher("/edit-slider.jsp?id=" + sliderId).forward(request, response);
       
        if(raw_page == null){
            currentPage = 1;
        }else {
            currentPage = Integer.parseInt(raw_page);
        }
        int pageNum = (int) Math.ceil(totalSlider / 5); // chia trang
        int offset = (currentPage - 1) * 5; // vi tri ban ghi bat dau
        int fetch = 5; // so luong
        List<Slider> sliders = sliderDAO.getSliderPagination(offset, fetch);
        request.setAttribute("totalSlider", totalSlider);
        request.setAttribute("sliders", sliders);
        request.setAttribute("pageNum", pageNum);
        request.setAttribute("currentPage", currentPage);
        request.getRequestDispatcher("crudSlider.jsp").forward(request, response);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
//        
//        String action = request.getParameter("action");
//        System.out.println("Actiojn "+ action);
//        if (action != null) {
//            if (action.equals("add")) {
//                
//                // Xử lý thêm Slider
//               
//            } else if (action.equals("edit")) {
//                // Xử lý sửa Slider
//                System.out.println("Go to editt");
//                int id = Integer.parseInt(request.getParameter("id"));
//                String title = request.getParameter("title");
//                String content = request.getParameter("content");
//                String notes = request.getParameter("notes");
//                String status = request.getParameter("status");
//                
//                Slider slider = new Slider();
//                slider.setSliderId(id);
//                slider.setTitle(title);
//                slider.setContent(content);
//                slider.setNotes(notes);
//                slider.setStatus(true);
//                
//                SliderDAO sliderDAO = new SliderDAO();
//                sliderDAO.updateSlider(slider);
//                
//                // Sau khi sửa xong, chuyển hướng về trang danh sách Slider
//                response.sendRedirect(request.getContextPath() + "/slider?action=list");
//            } else if (action.equals("delete")) {
//                // Xử lý xóa Slider
//                int id = Integer.parseInt(request.getParameter("id"));
//                
//                SliderDAO sliderDAO = new SliderDAO();
//                sliderDAO.deleteSlider(id);
//                
//                // Sau khi xóa xong, chuyển hướng về trang danh sách Slider
//                response.sendRedirect(request.getContextPath() + "/slider?action=list");
//            }
//        }
    }

    @Override
    public String getServletInfo() {
        return "Slider Controller";
    }
}

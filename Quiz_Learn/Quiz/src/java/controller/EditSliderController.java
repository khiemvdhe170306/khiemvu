package controller;

import dal.SliderDAO;
import java.io.IOException;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Slider;

public class EditSliderController extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        int sliderId;
        int courseId;
        try {
            String sliderIdStr = request.getParameter("sliderId");
            System.out.println("sliderIdStr "+ sliderIdStr);
            String courseIdStr = request.getParameter("courseId");
            System.out.println("courseIdStr "+ courseIdStr);

            sliderId = Integer.parseInt(sliderIdStr);
            System.out.println("sliderId "+ sliderId);
            courseId = Integer.parseInt(courseIdStr);
        } catch (NumberFormatException e) {
            // Log the error for debugging purposes
            e.printStackTrace();
            // Inform the user about the incorrect format
            response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Invalid input format for sliderId or courseId");
            return;
        }

        String title = request.getParameter("title");
        String content = request.getParameter("content");
        String notes = request.getParameter("note");
        String image = request.getParameter("image");

        // Create a Slider object with the updated information
        Slider slider = new Slider();
        slider.setSliderId(sliderId);
        slider.setCourseId(courseId);
        slider.setTitle(title);
        slider.setContent(content);
        slider.setNotes(notes);
        slider.setImage(image);

        // Update the slider in the database using SliderDAO
        SliderDAO sliderDAO = new SliderDAO();
        sliderDAO.updateSlider(slider);

        // Redirect to the slider listing page
        response.sendRedirect(request.getContextPath() + "/slider?mess=Update successfully");
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }
}

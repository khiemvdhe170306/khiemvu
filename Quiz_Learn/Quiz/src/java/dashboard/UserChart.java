/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package dashboard;

import dal.ChartDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.Chart;

/**
 *
 * @author khiem
 */
@WebServlet(name="UserChart", urlPatterns={"/userchart"})
public class UserChart extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UserChart</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UserChart at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        ChartDAO cd=new ChartDAO();
        String year_raw = request.getParameter("year");
        int year = 0,month=0;
        PrintWriter out = response.getWriter();
        //year
        if (request.getParameter("year") != null) {
            try {
                year = Integer.parseInt(year_raw);
            } catch (Exception e) {
            }
        }       
        //month
        if (request.getParameter("month") != null) {
            try {
                month = Integer.parseInt(request.getParameter("month"));
            } catch (Exception e) {
            }
        } 
        //test
        out.println(year + " " + month);
        List<Chart> topUserChart=cd.getTopAccount(year, month);
        List<Chart> ageChart=cd.getAgeChartCircle(year, month);
        List<Chart> genderChart=cd.getGenderChartCircle(year, month);
        List<Chart> topAccessChart=cd.getAccessAccount(year, month);
        List<Chart> ageAccess=cd.getAgeAccessCircle(year, month);
        List<Chart> genderAccess=cd.getGenderAccessCircle(year, month);
        List<Chart> ageSignUp=cd.getAgeSignUpChartCircle(year, month);
        List<Chart> genderSignUp=cd.getGenderSignUpChartCircle(year, month);
        List<Chart> LineChartSignUp=cd.getUserSignUpChart(year, month);
        int totalSignUp=cd.getTotalSignUp(year, month);
        double SignUpChange=cd.getSignUpDifference(year, month);
        int totalAccess=cd.getTotalAccess(year, month);
        double AccessChange=cd.getAccessDifference(year, month);
        
                
        request.setAttribute("age", ageChart);
        request.setAttribute("gender", genderChart);
        request.setAttribute("ageAccess", ageAccess);
        request.setAttribute("genderAccess", genderAccess);
        request.setAttribute("ageSignUp", ageSignUp);
        request.setAttribute("genderSignUp", genderSignUp);
        request.setAttribute("user", topUserChart);
        request.setAttribute("accessuser", topAccessChart);
        request.setAttribute("chartSignUp", LineChartSignUp);
        //total
        request.setAttribute("totalSignUp", totalSignUp);
        request.setAttribute("totalAccess", totalAccess);
        request.setAttribute("signUpChange", SignUpChange);
        request.setAttribute("accessChange", AccessChange);
        for(Chart c:genderAccess)
        {
            out.println(c.getMonth()+" "+c.getY());
        }
        request.getRequestDispatcher("ChartUser.jsp").forward(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

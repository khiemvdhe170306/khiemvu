/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dashboard;

import dal.ChartDAO;
import dal.RankDAO;
import static java.lang.System.out;
import java.util.List;
import model.Chart;
import model.Rank;

/**
 *
 * @author khiem
 */
public class NewClass {

    public static void main(String[] args) {
        ChartDAO cd = new ChartDAO();
        RankDAO rd=new RankDAO();
        List<Chart> TopCourse = cd.getUserSignUpChart(0, 0);
        //System.out.println(cd.getTotalSaleMembership(2022, 3)+" "+cd.getTotalSaleMembership(2022, 4)+" diff:"+cd.getSaleMembershipDifference(2022, 4));
        List<Rank> rList=rd.getListRank();
        
        rList=rd.rank(rList,1);
        System.out.println("//////////");
        for(Rank r:rList)
        {
            System.out.println(r.getLevel().getCurrentLevel()+","+r.getUser().getEmail());
            
        }
        rList=rd.rank(rList,2);
        System.out.println("//////////");
        for(Rank r:rList)
        {
            System.out.println(r.getLevel().getCurrentLevel()+","+r.getUser().getEmail());
            
        }
//        System.out.println(cd.getSaleYear(2022, 0, 12));
    }

}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package dashboard;

import dal.CourseDAO;
import dal.LevelDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import model.Account;
import model.Course;
import model.Level;
import model.Quiz;

/**
 *
 * @author arans
 */
@WebServlet(name = "Achievement", urlPatterns = {"/achievement"})
public class Achievement extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Achievement</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Achievement at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        Account a = (Account) session.getAttribute("account");
        int userId = a.getAid();
        CourseDAO courseDAO = new CourseDAO();
        LevelDAO levelDAO = new LevelDAO();
        Level numberOfQuizDone = levelDAO.NumberOfQuizDone(userId);
        List<Course> courseList = courseDAO.getAllCourse();
        List<Course> courseByUser = courseDAO.getUserRegisCourse(userId);
        Map<Integer, List<Quiz>> quizByCourse = new HashMap<>();

        for (Course course : courseList) {
            int courseId = course.getCourseId();
            List<Quiz> quizzesForCourse = levelDAO.getAllQuizByCourse(courseId);
            quizByCourse.put(courseId, quizzesForCourse);

            // Calculate and store the scores for each quiz
            for (Quiz quiz : quizzesForCourse) {
                int quizId = quiz.getQuizId();
                int score = levelDAO.getHighestGradeOfEachQuiz(quizId, userId);
                quiz.setScore(score);
                int totalAttempts = levelDAO.getTotalAttemptsForUserAndQuiz(userId, quizId);
                quiz.setTotalAttempts(totalAttempts);
            }
        }
        
        //show list of course
        request.setAttribute("courseList", courseList);
        //show list course by user
        request.setAttribute("courseByUser", courseByUser);
        //show list of all quiz in course
        request.setAttribute("quizByCourse", quizByCourse);
        //show list of quiz have done
        request.setAttribute("numberOfQuizDone", numberOfQuizDone);
        
        request.getRequestDispatcher("Achievement.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

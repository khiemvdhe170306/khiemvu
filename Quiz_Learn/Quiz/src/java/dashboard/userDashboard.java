/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package dashboard;

import dal.LevelDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.Account;
import model.CourseRegistration;
import model.Level;
import model.Rank;
import model.Voucher;

/**
 *
 * @author arans
 */
@WebServlet(name = "userDashboard", urlPatterns = {"/userdashboard"})
public class userDashboard extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet userDashboard</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet userDashboard at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        Account a = (Account) session.getAttribute("account");
//        if (a != null) {
        int userId = a.getAid();
        LevelDAO levelDAO = new LevelDAO();
        Level l = levelDAO.getLevel(userId);
        Level numberOfQuizDone = levelDAO.NumberOfQuizDone(userId);
        if (numberOfQuizDone == null) {
            numberOfQuizDone = new Level(); // Create an empty Level object
        }
        List<Voucher> voucherByAccount = levelDAO.getListVoucherByAccountId(userId);
        int totalNumberOfCourse = levelDAO.getTotalCourse();
        CourseRegistration numberOfCourseRegistration = levelDAO.NumberOfCourseRegistration(userId);
        //get the level of user
        request.setAttribute("currentLevel", l.getCurrentLevel());
        request.setAttribute("requiredXP", l.getRequireXP());
        request.setAttribute("currentXP", l.getCurrentXP());
        out.print(l.getCurrentLevel());
        out.print(l.getRequireXP());
        out.print(l.getCurrentXP());
        //get the number of course registration
        request.setAttribute("numberOfCourseRegistration", numberOfCourseRegistration);
        //get the total number of course
        request.setAttribute("totalNumberOfCourse", totalNumberOfCourse);
        //get the number of quiz done
        request.setAttribute("numberOfQuizDone", numberOfQuizDone);
        out.print(numberOfQuizDone.getNumberOfQuizDone());
        //get the voucher of account
        request.setAttribute("voucherByAccount", voucherByAccount);
        //get list of rank
        List<Rank> rList = levelDAO.getListRank();
        rList = levelDAO.rank(rList, 1);
        System.out.println("//////////");
        for (Rank r : rList) {
            System.out.println(r.getLevel().getCurrentLevel() + "," + r.getUser().getEmail());

        }
        request.setAttribute("rList", rList);

//        } else {
//            out.print("Account not found in session.");
//        }
        request.getRequestDispatcher("userDashboard.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

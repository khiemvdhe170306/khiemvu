/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package dashboard;

import dal.ChartDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.Chart;

/**
 *
 * @author khiem
 */
@WebServlet(name="SaleChart", urlPatterns={"/salechart"})
public class SaleChart extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SaleChart</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SaleChart at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        ChartDAO cd=new ChartDAO();
        String year_raw = request.getParameter("year");
        int year = 0,month=0;
        PrintWriter out = response.getWriter();
        //year
        if (request.getParameter("year") != null) {
            try {
                year = Integer.parseInt(year_raw);
            } catch (Exception e) {
            }
        }       
        //month
        if (request.getParameter("month") != null) {
            try {
                month = Integer.parseInt(request.getParameter("month"));
            } catch (Exception e) {
            }
        } 
        //test
        out.println(year + " " + month);
        
        List<Chart> SaleChart=cd.getSaleChart(year, month);
        List<Chart> CourseRegistration=cd.getSaleChartEachCourse(year, month);
        List<Chart> PricePackage=cd.getSaleChartEachPricePackage(year, month);
        List<Chart> TopCourse=cd.getTopCourse(year, month);
        int totalmoney=cd.getTotalMoneyMembership(year, month);
        int totalSale=cd.getTotalSaleMembership(year, month);
        int totalRegis=cd.getTotalCourseMembership(year, month);
        double SaleChange=cd.getSaleMembershipDifference(year, month);
        double MoneyChange=cd.getMoneyMembershipDifference(year, month);
        double CourseChange=cd.getCourseMembershipDifference(year, month);
        
        
        out.println(totalmoney);
        //requestScope Attribute
        request.setAttribute("saleChange", SaleChange);
        request.setAttribute("moneyChange", MoneyChange);
        request.setAttribute("courseChange", CourseChange);
        request.setAttribute("totalMoney", totalmoney);
        request.setAttribute("totalSale", totalSale);
        request.setAttribute("totalRegis",totalRegis);
        request.setAttribute("chart", SaleChart);
        request.setAttribute("course", CourseRegistration);
        request.setAttribute("topcourse", TopCourse);
        request.setAttribute("price", PricePackage);
        request.getRequestDispatcher("ChartSale.jsp").forward(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

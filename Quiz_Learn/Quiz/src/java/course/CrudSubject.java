/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package course;

import dal.CourseDAO;
import dal.SubjectDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Subject;

/**
 *
 * @author Admin
 */
@WebServlet(name = "CrudSubject", urlPatterns = {"/crudsubject"})
public class CrudSubject extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CrudSubject</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CrudSubject at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String subject_raw = request.getParameter("subjectId");
        SubjectDAO sd = new SubjectDAO();
        CourseDAO cd = new CourseDAO();
        int subjectId = 0;
        try {
            subjectId = Integer.parseInt(subject_raw);
        } catch (Exception e) {
        }
        String action = request.getParameter("action");
        if (action.equalsIgnoreCase("update")) {//update
            Subject s = sd.getSubjectById(subjectId);
            request.setAttribute("subject", s);
            request.setAttribute("action", "update");
            request.getRequestDispatcher("crudSubject.jsp").forward(request, response);
        } else if (action.equalsIgnoreCase("add")) {
            request.setAttribute("action", "add");
            request.getRequestDispatcher("crudSubject.jsp").forward(request, response);
        } else if (action.equalsIgnoreCase("delete")) {//delete
            if (!cd.getCourseBySubjectId(subjectId).isEmpty()) {
                request.setAttribute("msg", "This subject still contains courses!Cannot Delete!");
                request.getRequestDispatcher("subjectmanagement").forward(request, response);
            } else {
                sd.deleteSubject(subjectId);
                request.setAttribute("msg", "Delete Successfully!");
                request.getRequestDispatcher("subjectmanagement").forward(request, response);
            }
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        SubjectDAO sd = new SubjectDAO();
        String action = request.getParameter("action");
        String title = request.getParameter("title");
        String subject_raw = request.getParameter("subjectId");
        String description = request.getParameter("description");
        if (action.equalsIgnoreCase("add")) {
            sd.addSubject(title, description);
            request.setAttribute("msg", "Add Successfully!");
            request.getRequestDispatcher("subjectmanagement").forward(request, response);
            //change
        } else if (action.equalsIgnoreCase("update")) {
            int subjectId = 0;
            try {
                subjectId = Integer.parseInt(subject_raw);
            } catch (Exception e) {
            }
            sd.updateSubject(subjectId, title, description);
            request.setAttribute("msg", "Update Successfully!");
            request.getRequestDispatcher("subjectmanagement").forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

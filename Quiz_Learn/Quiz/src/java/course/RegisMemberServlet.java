/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package course;

import dal.PricePackageDAO;
import dal.UserDAO;
import dal.VoucherDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.Account;
import model.PricePackage;
import model.Voucher;

/**
 *
 * @author Acer
 */
@WebServlet(name = "RegisMemberServlet", urlPatterns = {"/regismember"})
public class RegisMemberServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet RegisMemberServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet RegisMemberServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String priceID_raw = request.getParameter("priceId");
        HttpSession session = request.getSession();
        Account ac = (Account) session.getAttribute("account");
        int priceId = 0;
        int percent = 1;
        try {
            priceId = Integer.parseInt(priceID_raw);
        } catch (Exception e) {
        }
        PrintWriter out = response.getWriter();
        out.println(priceId);
        PricePackageDAO pd = new PricePackageDAO();
        UserDAO ud = new UserDAO();
        if (ac == null) {
            response.sendRedirect("login.jsp");
        } else {
            PricePackage pp = pd.getPricePackageById(priceId);

            request.setAttribute("pricepack", pp);
            request.setAttribute("cc", percent);
            request.getRequestDispatcher("payment.jsp").forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String priceID_raw = request.getParameter("priceId");
        String voucher_raw = request.getParameter("voucher");
        HttpSession session = request.getSession();
        Account ac = (Account) session.getAttribute("account");
        int priceId = 0, voucher = 0;
        int percent = 1;
        UserDAO ud = new UserDAO();
        PricePackageDAO pd = new PricePackageDAO();
        try {
            priceId = Integer.parseInt(priceID_raw);
            voucher = Integer.parseInt(voucher_raw);
        } catch (Exception e) {
        }
        PrintWriter out = response.getWriter();
        out.print(priceId + ", " + voucher);
        VoucherDAO vd = new VoucherDAO();
        Voucher v = vd.getVoucherById(voucher);
        percent = v.getPercentage() / 100;
        ud.addRegistrationMember(priceId, ac.getAid(), voucher);
        //set lai account with new regis member vip
        Account a = ud.getAccountByAid(ac.getAid());
        PricePackage pp = pd.getPricePackageById(priceId);

        request.setAttribute("pricepack", pp);
        request.setAttribute("cc", percent);
        session.removeAttribute("account");
        //request.getRequestDispatcher("payment.jsp").forward(request, response);
        session.setAttribute("account", a);
        response.sendRedirect("courselist");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

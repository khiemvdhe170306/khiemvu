/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package course;

import dal.ChapterDAO;
import dal.CourseDAO;
import dal.LessonDAO;
import dal.LevelDAO;
import dal.QuizDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.Account;
import model.Attempt;
import model.Chapter;
import model.Lesson;
import model.Quiz;

/**
 *
 * @author Acer
 */
@WebServlet(name="UserCourseDetailServlet", urlPatterns={"/usercoursedetail"})
public class UserCourseDetailServlet extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet UserCourseDetailServlet</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet UserCourseDetailServlet at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String courseId_raw = request.getParameter("courseId");
        String chapId_raw = request.getParameter("chapId");
        String quizId_raw = request.getParameter("quizId");
        int courseId = 0, chapId = 0, quizId = 0;
        try {
            courseId = Integer.parseInt(courseId_raw);
            chapId = Integer.parseInt(chapId_raw);
            quizId = Integer.parseInt(quizId_raw);
        } catch (Exception e) {
        }
        ChapterDAO cd = new ChapterDAO();
        LessonDAO ld = new LessonDAO();
        QuizDAO qd = new QuizDAO();
        CourseDAO cod=new CourseDAO();
        LevelDAO ed = new LevelDAO();
        HttpSession session = request.getSession();
        Account ac = (Account) session.getAttribute("account");
        //List<Attempt> list = ed.getListOfAttemptByQuizId(ac.getAid(), quizId);
        //double grade = ed.getHighestAttempt(list);
        List<Chapter> cList = cd.getChapterByCourseId(courseId);
        //List<Lesson> lList = ld.getAllLessonByCourse(courseId, chapId);
        //List<Quiz> qList = qd.getAllQuizByCourse(courseId, chapId);
        request.setAttribute("chapter", cList);
//        request.setAttribute("lesson", lList);
//        request.setAttribute("quiz", qList);
        PrintWriter out = response.getWriter();
        //out.print(qList);
        session.setAttribute("course", cod.getCourseById(courseId));
        //request.setAttribute("grade", grade);
        request.getRequestDispatcher("usercoursedetail.jsp").forward(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package course;

import dal.CourseDAO;
import dal.SubjectDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import model.Course;
import model.Subject;

/**
 *
 * @author Acer
 */
@WebServlet(name="CourseManagementServlet", urlPatterns={"/coursemanagement"})
public class CourseManagementServlet extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CourseManagementServlet</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CourseManagementServlet at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        String page_raw=request.getParameter("page");
        String subject_raw = request.getParameter("subject");
        String name = request.getParameter("txt");
        SubjectDAO sd = new SubjectDAO();
        List<Subject> sList = sd.getAllSubject();
        int page = 1;
        try {
            page = Integer.parseInt(page_raw);
        } catch (Exception e) {
        }
        CourseDAO d = new CourseDAO();
        PrintWriter out = response.getWriter();
        List <Course> a=  d.getAllCourse();
        
        if (subject_raw != null) {
            if (subject_raw.equalsIgnoreCase("0"))//get all course and chapter 
            {
                ;
            } else {
                int subject = Integer.parseInt(subject_raw);
                a = d.getCourseBySubjectId(subject);
            }
       }
        if(name!=null){
            a = d.getCourseByName(name);
        }
        for(Course s:a){
            out.println(s.toString());
        }
        request.setAttribute("subjectList", sList);
        request.setAttribute("course", a);
        request.setAttribute("page", page);
        request.getRequestDispatcher("courseManagement.jsp").forward(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        doGet(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

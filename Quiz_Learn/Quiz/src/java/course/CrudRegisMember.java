/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package course;

import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.PricePackage;
import model.Registration;
import model.Voucher;

/**
 *
 * @author khiem
 */
@WebServlet(name = "CrudRegisMember", urlPatterns = {"/crudregismember"})
public class CrudRegisMember extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CrudRegisMember</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CrudRegisMember at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        UserDAO ud = new UserDAO();
        String regis_raw = request.getParameter("regisId");
        String action = request.getParameter("action");
        PrintWriter out = response.getWriter();
        List<Voucher> vL = ud.getAllVoucher();
        List<PricePackage> ppL = ud.getAllPricePackage();
        request.setAttribute("pricepackageList", ppL);
        //out.println(ppL.get(0).getPriceId());
        request.setAttribute("voucherList", vL);
        if (action.equalsIgnoreCase("update")) {//update
            int regisId = Integer.parseInt(regis_raw);
            Registration regismember = ud.getRegistrationByRegisId(regisId);
            request.setAttribute("regismember", regismember);
            request.setAttribute("action", "update");
            request.getRequestDispatcher("crudRegisMember.jsp").forward(request, response);
        } else if (action.equalsIgnoreCase("add")) {

            request.setAttribute("action", "add");
            request.getRequestDispatcher("crudRegisMember.jsp").forward(request, response);
        } else if (action.equalsIgnoreCase("delete")) {//delete
            int regisId = Integer.parseInt(regis_raw);
            ud.deleteRegistrationMember(regisId);
            HttpSession session = request.getSession();
            String acc = (String) session.getAttribute("regisUserId");
            if (acc != null) {
                request.getRequestDispatcher("regismembermanagement?userId=" + acc).forward(request, response);
            } else {
                request.getRequestDispatcher("regismembermanagement").forward(request, response);
            }
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        String user_raw = request.getParameter("userId");
        UserDAO ud = new UserDAO();
        String price_raw = request.getParameter("pricepackage");
        String regis_raw = request.getParameter("regisId");
        String voucher_raw = request.getParameter("voucher");
        int voucher = Integer.parseInt(voucher_raw);
        int priceId = Integer.parseInt(price_raw);
        PrintWriter out = response.getWriter();
        if (action.equalsIgnoreCase("update")) {//update 

            int regisId = Integer.parseInt(regis_raw);
            out.println(regisId + " " + voucher + " " + priceId+" "+user_raw);
            ud.updateRegistrationMember(regisId, priceId, voucher);
            request.setAttribute("ms", "update registration member successfully");
            int user = Integer.parseInt(user_raw);
            request.getRequestDispatcher("regismembermanagement?userId=" + user).forward(request, response);
        } else if (action.equalsIgnoreCase("add")) {
            int user = Integer.parseInt(user_raw);
            if (ud.getAccountByAid(user) == null) {
                List<Voucher> vL = ud.getAllVoucher();
                List<PricePackage> ppL = ud.getAllPricePackage();
                request.setAttribute("pricepackageList", ppL);
                request.setAttribute("voucherList", vL);
                request.setAttribute("ms", "User id " + user + " is not found");
                request.setAttribute("userId", user);
                request.setAttribute("action", "add");
                request.getRequestDispatcher("crudRegisMember.jsp").forward(request, response);
            } else if (ud.vip2(ud.getAccountByAid(user))) {
                List<Voucher> vL = ud.getAllVoucher();
                List<PricePackage> ppL = ud.getAllPricePackage();
                request.setAttribute("pricepackageList", ppL);
                request.setAttribute("voucherList", vL);
                request.setAttribute("ms", "User have remaining registration! You need to delete before add another one");
                request.setAttribute("userId", user);
                request.setAttribute("action", "add");
                request.getRequestDispatcher("crudRegisMember.jsp").forward(request, response);
            } else {
                ud.addRegistrationMember(priceId, user, voucher);
                request.setAttribute("ms", "add registration member successfully");
            }
        }
        if (user_raw != null) {
            int user = Integer.parseInt(user_raw);
            request.getRequestDispatcher("regismembermanagement?userId=" + user).forward(request, response);
        }
        request.getRequestDispatcher("regismembermanagement").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

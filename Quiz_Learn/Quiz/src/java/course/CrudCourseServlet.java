/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package course;

import dal.CourseDAO;
import dal.SubjectDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import model.Course;
import model.Subject;

/**
 *
 * @author Acer
 */
@WebServlet(name = "CrudCourseServlet", urlPatterns = {"/crudcourse"})
public class CrudCourseServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CrudCourseServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CrudCourseServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        CourseDAO d = new CourseDAO();
        SubjectDAO sd = new SubjectDAO();
        int courseId = 0;
        try {
            courseId = Integer.parseInt(request.getParameter("courseId"));
        } catch (Exception e) {
        }
        List<Subject> a = sd.getAllSubject();
        String action = request.getParameter("action");
        out.println(courseId + " " + action);
        if (action.equalsIgnoreCase("update")) {//update
            Course s = d.getCourseById(courseId);
            request.setAttribute("course", s);
            request.setAttribute("subject", a);
            request.setAttribute("action", "update");
            request.getRequestDispatcher("crudCourse.jsp").forward(request, response);
        } else if (action.equalsIgnoreCase("add")) {
            request.setAttribute("subject", a);
            request.setAttribute("action", "add");
            request.getRequestDispatcher("crudCourse.jsp").forward(request, response);
        } else if (action.equalsIgnoreCase("delete")) {//delete
            if (!d.getChapterByCourseId(courseId).isEmpty()) {
                request.setAttribute("msg", "This course still contain chapters! Cannot delete!");
                request.getRequestDispatcher("coursemanagement").forward(request, response);
            } else {
                d.deleteCourse(courseId);
                request.setAttribute("msg", "Delete Sucessfully!");
                request.getRequestDispatcher("coursemanagement").forward(request, response);
            }
        } else if (action.equalsIgnoreCase("change")) {
            int status;
            if (d.getCourseById(courseId).isStatus() == true) {
                status = 0;
            } else {
                status = 1;
            }
            d.changeStatus(courseId, status);
            request.setAttribute("msg", "Change status successful!");
            request.getRequestDispatcher("coursemanagement").forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        CourseDAO d = new CourseDAO();
        PrintWriter out = response.getWriter();
        String action = request.getParameter("action");
        String courseName = request.getParameter("courseName");
        String thumbnail = request.getParameter("thumbnail");
        String courseId_raw = request.getParameter("courseId");
        String description = request.getParameter("description");
        String subjectId_raw = request.getParameter("subjectId");
        String level_raw = request.getParameter("level");
        String video_raw = request.getParameter("video");
        String video = video_raw.replace("watch?v=", "embed/");
        int courseId = 0;
        int subjectId = 0;
        int level = 0;
        if (courseId_raw != null) {
            courseId = Integer.parseInt(courseId_raw);
        }
        if (subjectId_raw != null) {
            subjectId = Integer.parseInt(subjectId_raw);
        }
        if (level_raw != null) {
            level = Integer.parseInt(level_raw);
        }
//DAO 
        //add
        if (action.equalsIgnoreCase("add")) {
            if (d.checkDupCourseName(courseName) != null) {
                request.setAttribute("msg", "Course Name already existed!");
                request.getRequestDispatcher("crudCourse.jsp").forward(request, response);
            } else {
                out.println(d.addCourse(courseId, courseName, subjectId, 1, thumbnail, description, level, video));
                request.setAttribute("msg", "Add Successfully!");
                request.getRequestDispatcher("coursemanagement").forward(request, response);
            }
            //change
        } else if (action.equalsIgnoreCase("update")) {
            out.println(d.updateCourse(courseId, courseName, subjectId, thumbnail, description, level, video));
            request.setAttribute("msg", "Update Successfully!");
            request.getRequestDispatcher("coursemanagement").forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package course;

import dal.CourseDAO;
import dal.PricePackageDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.PricePackage;

/**
 *
 * @author Acer
 */
@WebServlet(name="CrudPricePackageServlet", urlPatterns={"/crudpricepackage"})
public class CrudPricePackageServlet extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CrudPricePackageServlet</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CrudPricePackageServlet at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        CourseDAO d = new CourseDAO();
        PricePackageDAO sd = new PricePackageDAO();
        int priceId = 0;
        try {
            priceId = Integer.parseInt(request.getParameter("priceId"));
        } catch (Exception e) {
        }
        List<PricePackage> a = d.getAllPricePackage();
        String action = request.getParameter("action");
        if (action.equalsIgnoreCase("update")) {//update
            PricePackage s = sd.getPricePackageById(priceId);
            request.setAttribute("pricepack", s);
            request.setAttribute("action", "update");
            request.getRequestDispatcher("crudPricePackage.jsp").forward(request, response);
        } else if (action.equalsIgnoreCase("add")) {
            request.setAttribute("subject", a);
            request.setAttribute("action", "add");
            request.getRequestDispatcher("crudPricePackage.jsp").forward(request, response);
        } else if (action.equalsIgnoreCase("delete")) {//delete
            sd.deletePricePackage(priceId);
            request.setAttribute("msg", "Delete Successfully!");
            request.getRequestDispatcher("pricepackagemanagement").forward(request, response);
        }
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        PricePackageDAO d = new PricePackageDAO();
        PrintWriter out = response.getWriter();
        String priceId_raw = request.getParameter("priceId");
        String action = request.getParameter("action");
        String name = request.getParameter("name");
        String dur_raw = request.getParameter("accessDuration");
        String price_raw = request.getParameter("price");
        String description = request.getParameter("description");
        int dur = 0;
        double price = 0;
        int priceId = 0;
        if (dur_raw != null) {
            dur = Integer.parseInt(dur_raw);
        }
        if (price_raw != null) {
            price = Double.parseDouble(price_raw);
        }
        if (priceId_raw != null) {
            priceId = Integer.parseInt(priceId_raw);
        }
//DAO 
        //add
        if (action.equalsIgnoreCase("add")) {
            out.println(d.addPricePackage(name, dur, price, description));
            request.setAttribute("msg", "Add Successfully!");
            request.getRequestDispatcher("pricepackagemanagement").forward(request, response);
            //change
        } else if (action.equalsIgnoreCase("update")) {
            out.println(d.updatePricePackage(name, dur, price, description, priceId));
            request.setAttribute("msg", "Update Successfully!");
            request.getRequestDispatcher("pricepackagemanagement").forward(request, response);
        }
    }    

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

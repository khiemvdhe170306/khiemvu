/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package course;

import dal.CourseDAO;
import dal.PricePackageDAO;
import dal.SubjectDAO;
import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;
import model.Account;
import model.Course;
import model.CourseRegistration;
import model.PricePackage;
import model.Registration;
import model.Subject;

/**
 *
 * @author Acer
 */
@WebServlet(name = "CourseDetailServlet", urlPatterns = {"/coursedetail"})
public class CourseDetailServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SubjectDetailServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SubjectDetailServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String id_raw = request.getParameter("courseId");
        String page_raw = request.getParameter("page");
        int courseId = 0, page = 0;
        try {
            courseId = Integer.parseInt(id_raw);
            page = Integer.parseInt(page_raw);
        } catch (Exception e) {
        }
        CourseDAO d = new CourseDAO();
        UserDAO ud = new UserDAO();
        HttpSession session = request.getSession();
        Account ac = (Account) session.getAttribute("account");
        PricePackageDAO pd = new PricePackageDAO();
        List<PricePackage> priceList = pd.getAllPack();

        if (ac == null) {
            ArrayList<Course> a = (ArrayList<Course>) d.getAllCourse();
            Course sd = d.getCourseById(courseId);
            request.setAttribute("coursedetail", sd);
            request.setAttribute("course", a);
            request.setAttribute("pricepack", priceList);
            request.setAttribute("page", page);
            request.getRequestDispatcher("coursecomment").forward(request, response);
        } else {
            ArrayList<Course> a = (ArrayList<Course>) d.getAllCourse();
            List<Registration> regisList = ud.getRegistrationByAccountId(ac.getAid());
            CourseRegistration cr = d.getRegisCourseByUserId(ac.getAid(), courseId);
            Course sd = d.getCourseById(courseId);
            request.setAttribute("coursedetail", sd);
            request.setAttribute("course", a);
            request.setAttribute("regiscourse", cr);
            request.setAttribute("pricepack", priceList);
            request.setAttribute("memberList", regisList);
            request.setAttribute("page", page);
            request.getRequestDispatcher("coursecomment").forward(request, response);
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

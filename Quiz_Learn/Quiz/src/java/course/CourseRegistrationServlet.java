/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package course;

import dal.CourseDAO;
import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.Date;
import java.util.List;
import model.Account;
import model.CourseRegistration;

/**
 *
 * @author Acer
 */
@WebServlet(name = "CourseRegistrationServlet", urlPatterns = {"/courseregistration"})
public class CourseRegistrationServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CourseRegistrationServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CourseRegistrationServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        UserDAO ud = new UserDAO();
        String courseId_raw = request.getParameter("courseId");
        HttpSession session = request.getSession();
        Account ac = (Account) session.getAttribute("account");

        long millis = System.currentTimeMillis();
        java.sql.Date date = new java.sql.Date(millis);
        int courseId = 0;
        try {
            courseId = Integer.parseInt(courseId_raw);
        } catch (Exception e) {
        }
        PrintWriter out = response.getWriter();
        out.print(courseId);
        CourseDAO d = new CourseDAO();
        if (ac == null) {
            response.sendRedirect("login.jsp");
        } else {
            CourseRegistration cr = d.getRegisCourseByUserId(ac.getAid(), courseId);
            d.regisCourse(date, courseId, ac.getAid());
            Account a = ud.getAccountByAid(ac.getAid());
            session.setAttribute("account", a);
            request.setAttribute("regiscourse", cr);
            request.setAttribute("msg", "Enroll Successfully! You can start learning by clicking Start!");
            request.getRequestDispatcher("coursedetail?courseId=" + courseId).forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String courseId_raw = request.getParameter("courseId");
        HttpSession session = request.getSession();
        Account ac = (Account) session.getAttribute("account");
        UserDAO ud = new UserDAO();
        int courseId = 0;
        try {
            courseId = Integer.parseInt(courseId_raw);
        } catch (Exception e) {
        }
        PrintWriter out = response.getWriter();
        out.print(courseId);
        CourseDAO d = new CourseDAO();
        CourseRegistration cr = d.getRegisCourseByUserId(ac.getAid(), courseId);
        out.print(cr);
        out.println(ac.getAid());
        d.unEnroll(ac.getAid(), courseId);
        Account a = ud.getAccountByAid(ac.getAid());
        session.setAttribute("account", a);
        request.setAttribute("regiscourse", cr);
        request.setAttribute("msg", "Unenroll Succesfully!");
        request.getRequestDispatcher("coursedetail?courseId=" + courseId).forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

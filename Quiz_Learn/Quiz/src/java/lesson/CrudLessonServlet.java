/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package lesson;

import dal.ChapterDAO;
import dal.CourseDAO;
import dal.LessonDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.Chapter;
import model.Course;
import model.Lesson;

/**
 *
 * @author Admin
 */
@WebServlet(name="CrudLessonServlet", urlPatterns={"/crudlesson"})
public class CrudLessonServlet extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CrudLessonServlet</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CrudLessonServlet at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        HttpSession session = request.getSession();
        String action = request.getParameter("action");
        LessonDAO ld = new LessonDAO();
        ChapterDAO chapter = new ChapterDAO();
        List<Chapter> chapterlist = chapter.getAllChapter();

        if (action.equalsIgnoreCase("add")) {
            request.setAttribute("action", action);
            request.setAttribute("chapterlist", chapterlist);
            request.getRequestDispatcher("crudLesson.jsp").forward(request, response);
            
        } else if (action.equalsIgnoreCase("update")) {
            int lessonId = Integer.parseInt(request.getParameter("lessonId"));
            Lesson l = ld.getLessonById(lessonId);
            request.setAttribute("action", action);
            request.setAttribute("lesson", l);
            request.setAttribute("chapterlist", chapterlist);
            request.getRequestDispatcher("crudLesson.jsp").forward(request, response);
            //delete
        } else {
            int lessonId = Integer.parseInt(request.getParameter("lessonId"));
            ld.deleteLesson(lessonId);
            request.setAttribute("msg", "Delete Successfully!");
            request.getRequestDispatcher("lessonmanagement").forward(request, response);
        }
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        LessonDAO ld = new LessonDAO();
        String action = request.getParameter("action");
        HttpSession session = request.getSession(false);
        String page_raw = request.getParameter("page");
        LessonDAO ll = new LessonDAO();
        List<Lesson> lessonlist = ll.getAllLesson();
        int page = 1;
        if (page_raw != null) {
            page = Integer.parseInt(page_raw);
        }
        PrintWriter out = response.getWriter();
        String lessonName = request.getParameter("lessonName");        
        String content = request.getParameter("content");
        String image = request.getParameter("image");
        String chapId = request.getParameter("chapId");
        
        //add
        if (action.equalsIgnoreCase("add")) {
            ld.addLesson(Integer.parseInt(chapId), lessonName, content, image);
            request.setAttribute("lessonlist", lessonlist);
            request.setAttribute("page", page);
            request.setAttribute("msg", "Add Successfully!");
            request.getRequestDispatcher("lessonmanagement").forward(request, response);

        //update
        } else if (action.equalsIgnoreCase("update")) {
            int lessonId = Integer.parseInt(request.getParameter("lessonId"));
            Lesson l = ld.getLessonById(lessonId);
            
            request.setAttribute("action", action);
            l = ld.updateLesson(lessonId, lessonName, content, image, Integer.parseInt(chapId));
            
            request.setAttribute("lessonlist", lessonlist);
            request.setAttribute("page", page);
            request.setAttribute("msg", "Update Successfully!");
            request.getRequestDispatcher("lessonmanagement").forward(request, response);
        }
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}





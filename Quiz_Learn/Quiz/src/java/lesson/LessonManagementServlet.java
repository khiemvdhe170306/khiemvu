/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package lesson;

import dal.ChapterDAO;
import dal.CourseDAO;
import dal.LessonDAO;
import dal.PostDAO;
import dal.SubjectDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.Chapter;
import model.Course;
import model.Lesson;
import model.Post;
import model.Subject;

/**
 *
 * @author Admin
 */
@WebServlet(name = "LessonManagementServlet", urlPatterns = {"/lessonmanagement"})
public class LessonManagementServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet LessonManagementServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet LessonManagementServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        //get parameter to filter
        //get id course to filter
        String course_raw = (String) request.getParameter("course");
        //get id chapter to filter
        String chapter_raw = request.getParameter("chapter");
        //get page phan trang
        String subject_raw = request.getParameter("subject");
        String page_raw = request.getParameter("page");
        LessonDAO l = new LessonDAO();
        CourseDAO cd = new CourseDAO();
        ChapterDAO ch = new ChapterDAO();
        SubjectDAO sd=new SubjectDAO();
        List<Course> cList = cd.getAllCourse();
        List<Chapter> chList = ch.getAllChapter();
        List<Lesson> lessonlist = l.getAllLesson();
        List<Subject> sList=sd.getAllSubject();
        int page = 1;
        if (page_raw != null) {
            page = Integer.parseInt(page_raw);
        }
        int subject=0;
        if (subject_raw != null) {
            
            if (subject_raw.equalsIgnoreCase("0")) {
                ;
            } else {
                subject=Integer.parseInt(subject_raw);
                if (course_raw != null) {
                    if (course_raw.equalsIgnoreCase("0"))//get all course and chapter 
                    {
                        lessonlist = l.getAllLessonByCourse(subject, 0, 0);
                        cList=cd.getCourseBySubjectId(subject);
                        chList=cd.getChapterByCourseIdAndSubjectId(subject, 0);
                    } else {
                        int course = Integer.parseInt(course_raw);
                        chList = cd.getChapterByCourseId(course);
                        int chapter;
                        if (chapter_raw == null)//filter with course param where chapter is null
                        {
                            chapter = 0;
                            lessonlist = l.getAllLessonByCourse(subject, course, 0);
                        } else {
                            chapter = Integer.parseInt(chapter_raw);//filter with chapter and course param
                            lessonlist = l.getAllLessonByCourse(subject, course, chapter);
                        }
                    }
                }
            }
        }
        request.setAttribute("subjectList", sList);
        request.setAttribute("chapterList", chList);
        request.setAttribute("lessonlist", lessonlist);
        request.setAttribute("courseList", cList);
        request.setAttribute("page", page);
        for (Lesson a : lessonlist) {
            out.println(a.getLessonName());
        }
//        for (Chapter b : chList) {
//            out.print(b.getChapName());
//        }
//        for (Subject c:sList) {
//            out.print(c.getSubjectId());
//        }
//        for (Course d:cList) {
//            out.print(d.getCourseName());
//        }

        request.getRequestDispatcher("lessonManagement.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        LessonDAO ld = new LessonDAO();
        CourseDAO cd = new CourseDAO();
        ChapterDAO chd = new ChapterDAO();
        List<Course> cList = cd.getAllCourse();
        List<Chapter> chList = chd.getAllChapter();
        List<Lesson> lList = ld.getAllLesson();

        request.setAttribute("lessonlist", lList);
        request.setAttribute("page", 1);
        request.setAttribute("courseList", cList);
        request.setAttribute("chapterlist", chList);
        request.getRequestDispatcher("lessonManagement.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

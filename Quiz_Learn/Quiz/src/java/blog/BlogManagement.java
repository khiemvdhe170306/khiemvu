/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package blog;

import dal.CourseDAO;
import dal.PostDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.Course;
import model.Post;

/**
 *
 * @author arans
 */
@WebServlet(name = "BlogManagement", urlPatterns = {"/blogmanagement"})
public class BlogManagement extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet BlogManagement</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet BlogManagement at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        String title = request.getParameter("txt");
        String page_raw = request.getParameter("page");
        String course_raw = request.getParameter("course");
        PostDAO p = new PostDAO();
        CourseDAO cd = new CourseDAO();
        List<Course> cList = cd.getAllCourse();
        List<Post> postlist = p.getAllPost();
        int page = 1;
        if (page_raw != null) {
            page = Integer.parseInt(page_raw);
        }
        
        if(title != null){
            postlist = p.searchPostByTitle(title);
        }
        for(Post po:postlist)
        {
            out.println(po.getCourse().getCourseName());
        }
       if (course_raw != null) {
            if (course_raw.equalsIgnoreCase("0"))//get all course
            {
            } else {
                int course = Integer.parseInt(course_raw);
                postlist = cd.getPostByCourseId(course);
            }
       }
        request.setAttribute("postlist", postlist);
        request.setAttribute("page", page);
        request.setAttribute("courseList", cList);
        request.getRequestDispatcher("blogManagement.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PostDAO p = new PostDAO();
        CourseDAO cd = new CourseDAO();
        List<Course> cList = cd.getAllCourse();
        List<Post> postlist = p.getAllPost();
        request.setAttribute("postlist", postlist);
        request.setAttribute("page", 1);
        request.setAttribute("courseList", cList);
        request.getRequestDispatcher("blogManagement.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

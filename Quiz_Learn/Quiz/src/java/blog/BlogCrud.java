/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package blog;

import dal.PostDAO;
import dal.CourseDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.Account;
import model.Post;
import model.Course;

/**
 *
 * @author arans
 */
@WebServlet(name = "BlogCrud", urlPatterns = {"/blogcrud"})
public class BlogCrud extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet BlogCrud</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet BlogCrud at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Account a = (Account) session.getAttribute("acc");
        //get old post
        String action = request.getParameter("action");
        PostDAO pd = new PostDAO();
        CourseDAO course = new CourseDAO();
        List<Course> courseList = course.getAllCourse();

        if (action.equalsIgnoreCase("add")) {
            request.setAttribute("action", action);
            request.setAttribute("courseList", courseList);
            request.getRequestDispatcher("blogCrud.jsp").forward(request, response);
            
        } else if (action.equalsIgnoreCase("update")) {
            int postId = Integer.parseInt(request.getParameter("postId"));
            Post p = pd.getPostById(postId);
            request.setAttribute("action", action);

            request.setAttribute("post", p);
            request.setAttribute("courseList", courseList);
            request.getRequestDispatcher("blogCrud.jsp").forward(request, response);
            //delete
        } else {
            int postId = Integer.parseInt(request.getParameter("postId"));
            pd.deletePost(postId);
            request.setAttribute("msg", "Delete Successfully!");
            request.getRequestDispatcher("blogmanagement").forward(request, response);
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        //update to new post
        PostDAO pd = new PostDAO();
        String action = request.getParameter("action");
        HttpSession session = request.getSession(false);
        String page_raw = request.getParameter("page");
        PostDAO pl = new PostDAO();
        List<Post> postlist = pl.getAllPost();
        int page = 1;
        if (page_raw != null) {
            page = Integer.parseInt(page_raw);
        }
        long millis = System.currentTimeMillis();
        // creating a new object of the class Date  
        PrintWriter out = response.getWriter();
        java.sql.Date date = new java.sql.Date(millis);
        String thumbnail = request.getParameter("thumbnail");
        String content = request.getParameter("content");
        String brifInfo = request.getParameter("brifInfo");
        String title = request.getParameter("title");
        String course = request.getParameter("course");
        out.println(course);

        Account a = (Account) session.getAttribute("account");
        if (action.equalsIgnoreCase("add")) {

            pd.addPost(thumbnail, content, brifInfo, title, Integer.parseInt(course), a.getAid(), date);
            request.setAttribute("postlist", postlist);
            request.setAttribute("page", page);
            request.setAttribute("msg", "Add Successfully!");
            request.getRequestDispatcher("blogmanagement").forward(request, response);

        } else if (action.equalsIgnoreCase("update")) {
            int postId = Integer.parseInt(request.getParameter("postId"));
            Post p = pd.getPostById(postId);
            
            out.println(postId+" "+Integer.parseInt(course));
            request.setAttribute("action", action);
            p = pd.updatePost(postId, thumbnail, content, brifInfo, title, Integer.parseInt(course));
            //get old post
            request.setAttribute("postlist", postlist);
            request.setAttribute("page", page);
            request.setAttribute("msg", "Update Successfully!");
            request.getRequestDispatcher("blogmanagement").forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package chapter;

import dal.ChapterDAO;
import dal.CourseDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.Course;

/**
 *
 * @author khiem
 */
@WebServlet(name = "CrudChapter", urlPatterns = {"/crudchapter"})
public class CrudChapter extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CrudChapter</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CrudChapter at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        ChapterDAO pd = new ChapterDAO();
        CourseDAO cd = new CourseDAO();
        List<Course> courseList = cd.getAllCourse();

        if (action.equalsIgnoreCase("add")) {
            request.setAttribute("action", action);
            request.setAttribute("courseList", courseList);
            request.getRequestDispatcher("crudChapter.jsp").forward(request, response);
        } else if (action.equalsIgnoreCase("update")) {
            int ChapterId = Integer.parseInt(request.getParameter("ChapterId"));
            model.Chapter p = pd.getChapterById(ChapterId);
            request.setAttribute("action", action);
            request.setAttribute("chapter", p);
            request.setAttribute("courseList", courseList);
            request.getRequestDispatcher("crudChapter.jsp").forward(request, response);
            //delete
        } else {
            int ChapterId = Integer.parseInt(request.getParameter("ChapterId"));
            if (!pd.getAllLessonByChapId(ChapterId).isEmpty()) {
                request.setAttribute("msg", "This Chapter still contain Lessons! Cannot Delete!");
                request.getRequestDispatcher("chaptermanagement").forward(request, response);
            } else {

                pd.deleteChapter(ChapterId);
                request.setAttribute("msg", "Delete Successfully!");
                request.getRequestDispatcher("chaptermanagement").forward(request, response);
            }
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        String action = request.getParameter("action");
        ChapterDAO cd = new ChapterDAO();
        CourseDAO co = new CourseDAO();
        String course_raw = request.getParameter("course");
        int course = 0;
        String name = request.getParameter("name");
        String description = request.getParameter("description");
        if (course_raw != null) {
            course = Integer.parseInt(course_raw);
        }
        if (action.equalsIgnoreCase("update")) {//update
            int chapterid = Integer.parseInt(request.getParameter("chapterid"));
            cd.updateChapter(chapterid, co.getCourseById(course), name, description);
            request.setAttribute("msg", "Update Successfully!");
            request.getRequestDispatcher("chaptermanagement").forward(request, response);
        } else if (action.equalsIgnoreCase("add")) {
            cd.addChapter(co.getCourseById(course), name, description);
            request.setAttribute("msg", "Add Successfully!");
            request.getRequestDispatcher("chaptermanagement").forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

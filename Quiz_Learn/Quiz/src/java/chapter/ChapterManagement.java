/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package chapter;

import dal.ChapterDAO;
import dal.CourseDAO;
import dal.SubjectDAO;
import model.Chapter;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.Course;
import model.Lesson;
import model.Subject;

/**
 *
 * @author khiem
 */
@WebServlet(name = "ChapterManagement", urlPatterns = {"/chaptermanagement"})
public class ChapterManagement extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ChapterManagement</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ChapterManagement at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        String course_raw = request.getParameter("course");
        String page_raw = request.getParameter("page");
        String search_raw = request.getParameter("search");
        ChapterDAO p = new ChapterDAO();
        CourseDAO cd = new CourseDAO();
        SubjectDAO sd = new SubjectDAO();
        String subject_raw = request.getParameter("subject");
        List<Chapter> chapterlist = p.getAllChapter();
        List<Course> cList = cd.getAllCourse();
        List<Subject> sList = sd.getAllSubject();
        int page = 1;
        if (page_raw != null) {
            page = Integer.parseInt(page_raw);
        }
        for (Chapter ch : chapterlist) {
            out.println(ch.getCourse().getCourseName());
        }
        int subject = 0;
        if (subject_raw != null) {

            if (subject_raw.equalsIgnoreCase("0")) {
                ;
            } else {
                subject = Integer.parseInt(subject_raw);
                if (course_raw != null) {
                    if (course_raw.equalsIgnoreCase("0"))//get all course and chapter 
                    {
                        chapterlist=cd.getChapterByCourseIdAndSubjectId(subject, 0);
                        cList=cd.getCourseBySubjectId(subject);
                    } else {
                        int course = Integer.parseInt(course_raw);
                        chapterlist = cd.getChapterByCourseIdAndSubjectId(subject, course);
                    }
                }
            }
        }
        if (search_raw != null) {
            if (request.getParameter("one") == null) {
                chapterlist = p.getChapterByName(search_raw);
            } else {
                chapterlist = p.getOneChapterByName(search_raw);
            }
        }
        request.setAttribute("chapterlist", chapterlist);
        request.setAttribute("page", page);
        request.setAttribute("courseList", cList);
        request.setAttribute("subjectList", sList);
        request.getRequestDispatcher("chapterManagement.jsp").forward(request, response);

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ChapterDAO p = new ChapterDAO();
        CourseDAO cd = new CourseDAO();
        List<Chapter> chapterlist = p.getAllChapter();
        List<Course> cList = cd.getAllCourse();
        request.setAttribute("chapterlist", chapterlist);
        request.setAttribute("page", 1);
        request.setAttribute("courseList", cList);
        request.getRequestDispatcher("chapterManagement.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

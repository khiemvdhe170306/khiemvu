/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import model.Answer_Quiz;
import model.Attempt;
import model.Question;

/**
 *
 * @author khiem
 */
public class AttemptDAO extends DBContext {
    QuestionDAO qd=new QuestionDAO();
    AnswerDAO ad=new AnswerDAO();
    //UserDAO ud = new UserDAO();
    public List<Attempt> getListOfAttemptByQuizId(int userId, int quizId) {
        String sql = "select * from Quiz_History_Detail where quizId=? and userId=?";
        List<Attempt> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, quizId);
            st.setInt(2, userId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Attempt a = new Attempt();
                a.setId(rs.getInt("historyId"));
                //a.setAccount(ud.getAccountByAid(userId));
                a.setQuizId(rs.getInt("quizId"));
                
                a.setMark(rs.getInt("Mark"));
                a.setGrade(rs.getDouble("grade"));
                a.setEnd(rs.getTimestamp("end"));
                a.setStart(rs.getTimestamp("start"));
                a.setAttempt(rs.getInt("attempt"));
                a.setTime(getTime(rs.getInt("time")));
                list.add(a);
            }
        } catch (Exception e) {
        }
        return list;
    }
    public Attempt getListOfAttemptByHistoryId(int historyId) {
        String sql = "select * from Quiz_History_Detail where historyId=?";
        List<Attempt> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, historyId);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Attempt a = new Attempt();
                a.setId(rs.getInt("historyId"));
                //a.setAccount(ud.getAccountByAid(userId));
                a.setQuizId(rs.getInt("quizId"));
                a.setMark(rs.getInt("Mark"));
                a.setGrade(rs.getDouble("grade"));
                a.setEnd(rs.getTimestamp("end"));
                a.setStart(rs.getTimestamp("start"));
                a.setAttempt(rs.getInt("attempt"));
                a.setTime(getTime(rs.getInt("time")));
                
                return a;
            }
        } catch (Exception e) {
        }
        return null;
    }
    public String getTime(int time)
    {
        time=time/1000;
        int second=0;
        int minute=0;
        minute=time/60;
        second=time%60;
        String t="";
        if(minute>0) t+=minute +"p ";
        t+=second+"s ";
        return t;
    }
    
    public List<Question> getListOfQuestionByHistoryId(int historyId) {
        String sql = "select * from Quiz_History where historyId=?";
        List<Question> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, historyId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {               
                int questionId=rs.getInt("quesId");
                Question q=qd.getQuestionById(questionId);
                list.add(q);
            }
        } catch (Exception e) {
        }
        return list;
    }
    public double getHighestAttempt(List<Attempt> list){
        double high=0;
        for(Attempt att:list)
        {
            if(att.getGrade()>high)
            {
                high=att.getGrade();
            }
        }
        return high;
    }
    public int getAttempt(int userId, int quizId) {
        String sql = "select max(attempt) as count  from Quiz_History_Detail where userId=? and quizId=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userId);
            st.setInt(2, quizId);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt("count");
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public int addQuizDetail(int historyId, int userId, int quizId, double grade, int attempt, int Mark, Timestamp start, Timestamp end, long time) {
        String sql = "INSERT INTO [dbo].[Quiz_History_Detail]\n"
                + "([historyId],[userId],[quizId],[grade],[Mark],[attempt],[start],[end],[time]) VALUES(?,?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, historyId);
            st.setInt(2, userId);
            st.setInt(3, quizId);
            st.setDouble(4, grade);
            st.setInt(5, Mark);
            st.setInt(6, attempt);
            st.setTimestamp(7, start);
            st.setTimestamp(8, end);
            st.setInt(9, (int) time);
            return st.executeUpdate();
            
        } catch (SQLException e) {
            System.out.println(e);
        }
        return 0;
    }

    public int getHistoryId() {
        String sql = "select max(historyId) as count  from Quiz_History";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt("count");
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public int deleteFromAnswer_Quiz(int historyId) {
        
        String sql = "delete from Answer_Quiz";
        
        try {
            List<Answer_Quiz> list = changeChoosenAnswer(historyId);
            for (Answer_Quiz aq : list) {
                addChosenAnswer(historyId, aq.getA(), aq.getB());
            }
            PreparedStatement st = connection.prepareStatement(sql);
            return st.executeUpdate();
            
        } catch (SQLException e) {
            System.out.println(e);
        }
        return 0;
    }
    
    public int addChosenAnswer(int historyId, int ans, int questionId) {
        String sql = "insert into Quiz_History values(?,?,?)";
        
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, historyId);
            st.setInt(2, ans);
            st.setInt(3, questionId);
            return st.executeUpdate();
            
        } catch (SQLException e) {
        }
        return 0;
    }

    public List<Answer_Quiz> changeChoosenAnswer(int historyId) {
        String sql = "select * from Answer_Quiz";
        List<Answer_Quiz> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int ans = rs.getInt("answerId");
                int ques = rs.getInt("quesId");
                list.add(new Answer_Quiz(ans, ques));
                
            }
            
        } catch (SQLException e) {
        }
        return list;
    }
    
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import java.util.List;
import model.Chapter;
import model.Question;
import model.Quiz;

/**
 *
 * @author Admin
 */
public class QuizDAO extends DBContext {

    ChapterDAO cd = new ChapterDAO();
    AnswerDAO ad = new AnswerDAO();
    QuestionDAO qd = new QuestionDAO();

    //LIST quiz by chapterId
    public List<Quiz> getAllQuizByChapter(int chapId) {
        String sql = "select * from Quiz where chapId=?";
        List<Quiz> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, chapId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Quiz a = new Quiz();
                int qId = rs.getInt("quizId");
                a.setQuizId(qId);
                a.setChapter(cd.getChapterById(chapId));
                a.setTitle(rs.getString("title"));
                a.setImage(rs.getString("image"));
                a.setDuration(rs.getInt("duration"));
                a.setNumberOfQuestion(rs.getInt("numberOfQuestion"));
                a.setQuestion(qd.getAllQuestionByQuiz(rs.getInt("quizId")));
                a.setStatus(status(a.getQuestion(), a.getNumberOfQuestion()));
                a.setAid(rs.getInt("creator"));
                
                
                list.add(a);
            }
        } catch (SQLException e) {
        }
        return list;
    }

    //List quiz by subject (course/chapter)
    public List<Quiz> getAllQuizByCourse(int subjectId, int courseId, int chapId,int aid) {
        String sql = "select q.quizId,q.title,q.numberOfQuestion,q.image,q.duration,q.creator from Quiz q  "
                + "inner join Chapter ch on q.chapId=ch.chapId "
                + "inner join Course c on ch.courseId=c.courseId "
                + "inner join Subject s on s.subjectId=c.subjectId where q.creator="+aid+" and s.subjectId=" + subjectId;
        if (courseId > 0) {
            sql += " and c.courseId=" + courseId;
        }
        if (chapId > 0) {
            sql += "and ch.chapId=" + chapId;
        }
        List<Quiz> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Quiz a = new Quiz();
                int qId = rs.getInt("quizId");
                a.setQuizId(qId);
                a.setChapter(cd.getChapterById(chapId));
                a.setTitle(rs.getString("title"));
                a.setImage(rs.getString("image"));
                a.setDuration(rs.getInt("duration"));
                a.setNumberOfQuestion(rs.getInt("numberOfQuestion"));
                a.setQuestion(qd.getAllQuestionByQuiz(rs.getInt("quizId")));
                a.setStatus(status(a.getQuestion(), a.getNumberOfQuestion()));
                a.setAid(rs.getInt("creator"));
                
                
                list.add(a);
            }
        } catch (SQLException e) {
        }
        return list;
    }

    //LIST quiz
    public List<Quiz> getAllQuiz() {
        String sql = "select * from Quiz";
        List<Quiz> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Quiz a = new Quiz();
                int qId = rs.getInt("quizId");
                a.setQuizId(qId);
                a.setChapter(cd.getChapterById(rs.getInt("chapId")));
                a.setTitle(rs.getString("title"));
                a.setImage(rs.getString("image"));
                a.setDuration(rs.getInt("duration"));
                a.setNumberOfQuestion(rs.getInt("numberOfQuestion"));
                a.setQuestion(qd.getAllQuestionByQuiz(rs.getInt("quizId")));
                a.setStatus(status(a.getQuestion(), a.getNumberOfQuestion()));
                a.setAid(rs.getInt("creator"));
                
                
                list.add(a);
            }
        } catch (SQLException e) {
        }
        return list;
    }
     public List<Quiz> getAllQuiz(int aid) {
        String sql = "select * from Quiz where creator="+aid;
        List<Quiz> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Quiz a = new Quiz();
                int qId = rs.getInt("quizId");
                a.setQuizId(qId);
                a.setChapter(cd.getChapterById(rs.getInt("chapId")));
                a.setTitle(rs.getString("title"));
                a.setImage(rs.getString("image"));
                a.setDuration(rs.getInt("duration"));
                a.setNumberOfQuestion(rs.getInt("numberOfQuestion"));
                a.setQuestion(qd.getAllQuestionByQuiz(rs.getInt("quizId")));
                a.setStatus(status(a.getQuestion(), a.getNumberOfQuestion()));
                a.setAid(rs.getInt("creator"));
                
                
                list.add(a);
            }
        } catch (SQLException e) {
        }
        return list;
    }

    public Quiz getQuizById(int quizId) {
        String sql = "select * from Quiz where quizId=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, quizId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Quiz a = new Quiz();
                int qId = rs.getInt("quizId");
                a.setQuizId(qId);
                a.setTitle(rs.getString("title"));
                a.setImage(rs.getString("image"));
                a.setDuration(rs.getInt("duration"));
                a.setNumberOfQuestion(rs.getInt("numberOfQuestion"));
                a.setQuestion(qd.getAllQuestionByQuiz(rs.getInt("quizId")));
                a.setStatus(status(a.getQuestion(), a.getNumberOfQuestion()));
                a.setAid(rs.getInt("creator"));
                
                
                return a;
            }
        } catch (Exception e) {
        }
        return null;
    }

    public int getQuizid() {
        String sql = "select max(quizId) as count from Quiz";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt("count");
            }
        } catch (Exception e) {
        }
        return 0;
    }

    //search quiz
    public List<Quiz> getQuizByName(String name) {
        String sql = "select * from Quiz "
                + "where title like '%" + name + "%'";
        List<Quiz> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Quiz a = new Quiz();
                int qId = rs.getInt("quizId");
                a.setQuizId(qId);
                a.setTitle(rs.getString("title"));
                a.setImage(rs.getString("image"));
                a.setDuration(rs.getInt("duration"));
                a.setNumberOfQuestion(rs.getInt("numberOfQuestion"));
                a.setQuestion(qd.getAllQuestionByQuiz(rs.getInt("quizId")));
                a.setStatus(status(a.getQuestion(), a.getNumberOfQuestion()));
                a.setAid(rs.getInt("creator"));
                
                
                list.add(a);
            }
        } catch (SQLException e) {
        }
        return list;
    }

    //delete quiz
    public int deleteQuiz(int quizId) {
        deleteAll(quizId);
        String sql = "delete from Quiz where quizId = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, quizId);
            return st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
        return 0;
    }

    public int deleteAll(int quizId) {
        String sql = "select * from Question where quizId=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, quizId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int id = rs.getInt("questionId");
                qd.deleteQuestion(id);
            }
            return 1;
        } catch (Exception e) {
            System.out.println(e);
        }
        return 0;
    }

    //add quiz
    public Quiz addQuiz(int chapId, String title, String image, int duration, int numberOfQuestion,int aid) {
        String sql = "INSERT INTO [dbo].[Quiz]\n"
                + "           ([quizId]\n"
                + "           ,[title]\n"
                + "           ,[chapId]\n"
                + "           ,[image]\n"
                + "           ,[duration]\n"
                + "           ,[numberOfQuestion]\n"
                + "           ,[creator])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, getQuizid() + 1);
            st.setString(2, title);
            st.setInt(3, chapId);
            st.setString(4, image);
            st.setInt(5, duration);
            st.setInt(6, numberOfQuestion);
            st.setInt(7, aid);
            st.executeUpdate();
        } catch (Exception e) {
        }
        return getQuizById(getQuizid());
    }

    //update quiz
    public Quiz updateQuiz(int quizId, String title, int chapId, String image, int duration, int numberOfQuestion) {
        String sql = "UPDATE [dbo].[Quiz]\n"
                + "   SET [title] = ?\n"
                + "      ,[chapId] = ?\n"
                + "      ,[image] = ?\n"
                + "      ,[duration] = ?\n"
                + "      ,[numberOfQuestion] = ?\n"
                + " WHERE [quizId]=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, title);
            st.setInt(2, chapId);
            st.setString(3, image);
            st.setInt(4, duration);
            st.setInt(5, numberOfQuestion);
            st.setInt(6, quizId);
            st.executeUpdate();
        } catch (Exception e) {
        }
        return getQuizById(quizId);
    }

    //check quiz status by question
    public boolean status(List<Question> q, int numberOfQuestion) {
        if (q.size() >= numberOfQuestion) {
            return true;
        } else {
            return false;
        }
    }

    public List<Question> getAllQuestionByQuizId(int quizId) {
        String sql = "select * from Question where quizId=?";
        List<Question> list = new ArrayList<>();
        QuestionDAO question = new QuestionDAO();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, quizId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Question a = new Question();

                int qId = rs.getInt("questionId");
                a.setqContent(rs.getString("qContent"));
                a.setQuestionId(qId);
                a.setListAnswer(question.getAllAnswerFromQuestion(qId));
                a.setNumberOfTrue(question.numberOfTrueAnswer(qId));
                a.setChoice(question.getTrueChoiceFromQuestion(rs.getInt("questionId")));
                a.setChosen(question.getChosenFromQuestion(rs.getInt("questionId")));
                a.setqExplainContent(question.getQuesionExplainByQuestionId(qId));
                list.add(a);
            }
        } catch (SQLException e) {
        }
        return list;
    }

}

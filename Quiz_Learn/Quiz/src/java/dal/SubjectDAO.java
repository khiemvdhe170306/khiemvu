/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Account;
import model.PricePackage;
import model.Subject;

/**
 *
 * @author Admin
 */
public class SubjectDAO extends DBContext{
    CourseDAO cd = new CourseDAO();
    public int getSubid() {
        String sql = "select max(subjectId)  as count from Subject";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt("count");
            }
        } catch (Exception e) {
        }
        return 0;
    }
    
    //LIST ALL subject
    public List<Subject> getAllSubject() {
        String sql = "select * from Subject ";
        List<Subject> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Subject a=new Subject();
                a.setSubjectId(rs.getInt("subjectId"));
                a.setDescription(rs.getString("description"));
                a.setTitle(rs.getString("title"));
                a.setCourse(cd.getCourseBySubjectId(rs.getInt("subjectId")));
                list.add(a);
            }
        } catch (SQLException e) {
        }
        return list;
    }
    
    public Subject getSubjectById(int id) {
        String sql = "select * from Subject "
                + "where subjectId = " + id;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Subject a=new Subject();
                a.setDescription(rs.getString("description"));
                a.setTitle(rs.getString("title"));
                a.setSubjectId(rs.getInt("subjectId"));
                a.setCourse(cd.getCourseBySubjectId(rs.getInt("subjectId")));
                return a;
            }
        } catch (SQLException e) {
        }
        return null;
    }  
    
    //delete subject
    public Subject deleteSubject(int subjectId){
        String sql = "delete from Subject where subjectId = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, subjectId);
            st.executeUpdate();
        } catch (Exception e) {
        }
        return getSubjectById(subjectId);
    }
    
    //add subject
    public Subject addSubject(String title, String description){
        String sql = "INSERT INTO [dbo].[Subject]\n" +
"           ([subjectId]\n" +
"           ,[title]\n" +
"           ,[description])\n" +
"     VALUES(?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, getSubid()+1);
            st.setString(2, title);
            st.setString(3, description);
            st.executeUpdate();
        } catch (Exception e) {
        }
        return getSubjectById(getSubid());
    }
    
    //update subject    
    public Subject updateSubject(int subjectId, String title, String description){
        String sql = "UPDATE [dbo].[Subject]\n"
                + "   SET [title] = ?\n"
                + "      ,[description] = ?\n"
                + " WHERE subjectId = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, title);
            st.setString(2, description);
            st.setInt(3, subjectId);
            st.executeUpdate();
        } catch (Exception e) {
        }
        return getSubjectById(subjectId);
    }
}

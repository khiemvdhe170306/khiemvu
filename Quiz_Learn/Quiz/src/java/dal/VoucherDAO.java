/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.Voucher;

/**
 *
 * @author khiem
 */
public class VoucherDAO extends DBContext{
    
    public int getMaxVoucherUser()
    {
        String sql = "select max(id) as count from Voucher";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt("count");
            }
        } catch (Exception e) {
        }
        return 0;
    }
    public int addVoucher(String name,int percentage){   
        String sql = "insert into Voucher values(?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, getMaxVoucherUser()+1);
            st.setString(2, name);
            st.setInt(3, percentage);
            return st.executeUpdate();
        } catch (Exception e) {
        }
        return 0;
    }
    
    public int deleteVoucher(int vid){   
        String sql = "delete from Voucher where id=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, vid);
            return st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
        return 0;
    }
    
    public int updateVoucher(int voucherId,String name,int percentage){   
        String sql = "update Voucher set name=?,percentage=? where id=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, name);
            st.setInt(2, percentage);
            st.setInt(3, voucherId);
            return st.executeUpdate();
        } catch (Exception e) {
        }
        return 0;
    }
    public Voucher getVoucherById(int id) {
        String sql = "select * from Voucher where id=?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Voucher a = new Voucher(rs.getInt("id"), rs.getString("name"), rs.getInt("percentage"));
                return a;
            }

        } catch (SQLException e) {
        }
        return null;
    }
}

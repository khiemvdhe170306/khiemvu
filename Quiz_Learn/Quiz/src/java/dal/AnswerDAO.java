/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.locks.StampedLock;
import model.Answer;
import model.Answer_Quiz;
import model.Question;

/**
 *
 * @author khiem
 */
public class AnswerDAO extends DBContext {

    
    //GET NUMBER OF Answer
    public int getAnswerId() {
        String sql = "select max(answerId) as count from Answer";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt("count");
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public int getRandomNumber(int min, int max) {
        return (int) ((Math.random() * (max - min)) + min);
    }

    //select * from Answer where quesId=10
    public List<Answer> getAllAnswerFromQuestion(int questionId) {
        String sql = "select * from Answer where quesId=?";
        List<Answer> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, questionId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Answer a = new Answer();
                a.setContent(rs.getString("content"));
                a.setAid(rs.getInt("answerId"));
                a.setCorrect(rs.getBoolean("correct"));
                a.setQuestionId(rs.getInt("quesId"));
                list.add(a);
            }

        } catch (SQLException e) {
        }
        return list;
    }

    //select * from Answer where quesId=10
    public Answer getAnswerById(int answerId) {
        String sql = "select * from Answer where answerId=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, answerId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Answer a = new Answer();
                a.setContent(rs.getString("content"));
                a.setAid(rs.getInt("answerId"));
                a.setCorrect(rs.getBoolean("correct"));
                a.setQuestionId(rs.getInt("quesId"));
                return a;
            }

        } catch (SQLException e) {
        }
        return null;
    }
    //select * from Answer where content=
    public int getAnswerByContent(String content,int questionId) {
        content=content.trim();
        String sql = "select * from Answer where content=? and quesId=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, content);
            st.setInt(2, questionId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return 1;
            }

        } catch (SQLException e) {
        }
        return 0;
    }
    

    public int addAnswer(String content, boolean correct, int questionId) {
        String sql = "insert into Answer values(?,?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, getAnswerId() + 1);
            st.setString(2, content);
            st.setBoolean(3, correct);
            st.setInt(4, questionId);
            return st.executeUpdate();
        } catch (Exception e) {
        }
        return 0;
    }

    public int deleteAnswer(int answerId) {
        String sql = "delete from Answer where answerId=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, answerId);
            return st.executeUpdate();
        } catch (Exception e) {
        }
        return 0;
    }

    public int updateAnswer(int answerId, String content, boolean correct) {
        String sql = "update Answer set content=?,correct=? where answerId=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, content);
            st.setBoolean(2, correct);
            st.setInt(3, answerId);
            return st.executeUpdate();
        } catch (Exception e) {
        }
        return 0;
    }

    public boolean checkAnswerCorrect(Answer a, Question q) {
        if (a.isCorrect() && q.getNumberOfTrue() == 1) {
            return false;
        } else {
            return true;
        }
    }

    public int updateAnswerTrue(int questionId, boolean plus) {
        String sql = "update Question_True set numberOfCorrect=? where questionId=?";
        int num = numberOfTrueAnswer(questionId);
        if (plus) {
            num += 1;
        } else {
            num -= 1;
        }
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, num);
            st.setInt(2, questionId);
            return st.executeUpdate();
        } catch (Exception e) {
        }
        return 0;
    }
    public int addAnswerTrue(int questionId) {
        String sql = "insert into Question_True values(?,?)";
       
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(2, 0);
            st.setInt(1, questionId);
            return st.executeUpdate();
        } catch (Exception e) {
        }
        return 0;
    }

    //get true answer
    public int numberOfTrueAnswer(int questionId) {
        String sql = "select * from Question_True where questionId=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, questionId);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt("numberOfCorrect");
            }
        } catch (Exception e) {
        }
        return 0;
    }

    //update answer to false
    public boolean updateAnswerToFalse(Answer a, boolean correct) {
        int numberOfTrue = numberOfTrueAnswer(a.getQuestionId());
        return !(numberOfTrue == 1 && !correct && a.isCorrect());
    }

    //get user chosen
    public List<Answer> getChosenFromQuestion(int questionId) {
        String sql = "select answerId from Answer_Quiz  where quesId=?";
        List<Answer> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, questionId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                if(rs.getInt("answerId")==0)return null;
                
                int aid=rs.getInt("answerId");
                Answer a=getAnswerById(aid);
                list.add(a);
            }

        } catch (SQLException e) {
        }
        return list;
    }
  

    //get true choice
    public List<Answer> getTrueChoiceFromQuestion(int questionId) {
        String sql = "select * from Answer a join Question q on a.quesId=q.questionId where a.quesId=? and a.correct=1";
        List<Answer> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, questionId);
            ResultSet rs = st.executeQuery();
            
            while (rs.next()) {
               
                Answer a = new Answer();
                a.setContent(rs.getString("content"));
                a.setAid(rs.getInt("answerId"));
                a.setCorrect(rs.getBoolean("correct"));
                list.add(a);
            }

        } catch (SQLException e) {
        }
        return list;
    }

    public int addChosenAnswer2(int aid) {
        String sql = "insert into Answer_Quiz values(?,?)";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, aid);
            st.setInt(2, getQuestionByAId(aid).getQuestionId());
            return st.executeUpdate();

        } catch (SQLException e) {
        }
        return 0;
    }
    public int addChosenAnswer3(int aid,int quesId)
    {
        String sql = "insert into Answer_Quiz values(?,?)";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, aid);
            st.setInt(2, quesId);
            return st.executeUpdate();

        } catch (SQLException e) {
        }
        return 0;
    }

    public int addChosenAnswer(int historyId, int ans, int questionId) {
        String sql = "insert into Quiz_History values(?,?,?)";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, historyId);
            st.setInt(2, ans);
            st.setInt(3, questionId);
            return st.executeUpdate();

        } catch (SQLException e) {
        }
        return 0;
    }

    public List<Answer_Quiz> changeChoosenAnswer(int historyId) {
        String sql = "select * from Answer_Quiz";
        List<Answer_Quiz> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int ans = rs.getInt("answerId");
                int ques = rs.getInt("quesId");
                list.add(new Answer_Quiz(ans, ques));

            }

        } catch (SQLException e) {
        }
        return list;
    }

    //delete from Answer_Quiz
    public int deleteFromAnswer_Quiz() {
        String sql = "delete from Answer_Quiz";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            return st.executeUpdate();

        } catch (SQLException e) {
        }
        return 0;
    }

    //select * from Question where quesId=10
    public Question getQuestionByAId(int aId) {
        String sql = "select * from Question q join Answer a on q.questionId=a.quesId where answerId=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, aId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Question a = new Question();
                int qId = rs.getInt("questionId");
                a.setqContent(rs.getString("qContent"));
                a.setQuestionId(qId);
                a.setListAnswer(getAllAnswerFromQuestion(qId));
                a.setNumberOfTrue(numberOfTrueAnswer(qId));
                a.setChoice(getTrueChoiceFromQuestion(rs.getInt("questionId")));
                a.setChosen(getChosenFromQuestion(rs.getInt("questionId")));
                return a;
            }

        } catch (SQLException e) {
        }
        return null;
    }

    public int deleteAll(int questionId) {
        String sql = "delete from Answer where  quesId=?\n"
                + "delete from Question_True where questionId=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, questionId);
            st.setInt(2, questionId);
            return st.executeUpdate();
        } catch (Exception e) {
        }
        return 0;
    }

    public boolean compareAnswer(List<Answer> aL, List<Answer> bL) {
        if(aL==null) return false;
        List<Integer> a = new ArrayList<>();
        List<Integer> b = new ArrayList<>();
        for (Answer ans : aL) {
            a.add(ans.getAid());
        }
        for (Answer ans : bL) {
            b.add(ans.getAid());
        }
        return a.containsAll(b) && b.containsAll(a);
    }

    public boolean getChoice(List<Answer> aL, Answer a) {
        List<Integer> list = new ArrayList<>();
        if(aL ==null) return false;
        for (Answer ans : aL) {
            list.add(ans.getAid());
        }
        return list.contains(a.getAid());
    }

    public Timestamp getDate() {
        Timestamp date = new Timestamp(System.currentTimeMillis());

        return date;
    }

    public List<Question> getQuizHandle(List<Question> list,int numberOfQuestion) {
        int rand = getRandomNumber(1, 6);
        list = getQuizHandleQuestion(list, rand);
        //list = getQuizHandleAnswer(list);
        return list.subList(0, numberOfQuestion);
    }

    //shuffle list
    public List<Question> getQuizHandleQuestion(List<Question> list, int rand) {
        
        //Collections.shuffle(list);
        switch (rand) {
            case 1:
                Collections.sort(list, (o1, o2) -> {
                    return o1.getqContent().compareTo(o2.getqContent()); 
                });
                break;
            case 2:
                Collections.sort(list, (o1, o2) -> {
                    return o1.getNumberOfTrue() - o2.getNumberOfTrue(); 
                });
                break;
            case 3:
                Collections.sort(list, (o1, o2) -> {
                    return o2.getqContent().compareTo(o1.getqContent()); 
                });
                break;
            case 4:
                Collections.sort(list, (o1, o2) -> {
                    return o2.getQuestionId() -  o1.getQuestionId(); 
                });
                break;
            default:

                break;
        }

        return list;
    }

    //shuffle answer
    public List<Question> getQuizHandleAnswer(List<Question> list) {
        

        for (Question q : list) {
            int rand = getRandomNumber(1, 6);
            switch (rand) {
                case 1:
                    Collections.sort(q.getListAnswer(), (o1, o2) -> {
                        return o1.getContent().compareTo(o2.getContent()); 
                    });
                    break;
                case 2:
                    Collections.sort(q.getListAnswer(), (o1, o2) -> {
                        return o1.getContent().compareTo(o2.getContent()); 
                    });
                    break;
                case 3:
                    Collections.sort(q.getListAnswer(), (o1, o2) -> {
                        return o1.getAid()-o2.getAid(); 
                    });
                    break;
                case 4:
                    Collections.sort(q.getListAnswer(), (o1, o2) -> {
                        return o2.getAid()-o1.getAid(); 
                    });
                    break;
                default:
                    break;
            }

        }
        return list;
    }

    //shuffle answer
    public void getChosen(List<Question> list) {

        for (Question q : list) {
            q.setChosen(getChosenFromQuestion(q.getQuestionId()));
            q.setChoice(getTrueChoiceFromQuestion(q.getQuestionId()));
        }

    }
     //get user chosen
    public List<Answer> getChosenFromHistoryId(int historyId,int questionId) {
        String sql = "select * from Quiz_History where historyId=? and quesId=?";
        List<Answer> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, historyId);
            st.setInt(2, questionId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                if(rs.getInt("answerId")==0)return null;
                
                int aid=rs.getInt("answerId");
                Answer a=getAnswerById(aid);
                list.add(a);
            }

        } catch (SQLException e) {
        }
        return list;
    }
    public void getHistoryChosen(List<Question> list,int historyId)
    {
        for (Question q : list) {
            q.setChosen(getChosenFromHistoryId(historyId,q.getQuestionId()));
        }
    }
    
    public List<Question> getAllQuestionByQuiz(int quizId) {
        String sql = "select * from Question where quizId=?";
        List<Question> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, quizId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Question a = new Question();
                int qId = rs.getInt("questionId");
                a.setqContent(rs.getString("qContent"));
                a.setQuestionId(qId);
                a.setListAnswer(getAllAnswerFromQuestion(qId));
                a.setNumberOfTrue(numberOfTrueAnswer(qId));
                a.setStatus(getStatus(a));
                a.setChoice(getTrueChoiceFromQuestion(rs.getInt("questionId")));
                a.setChosen(getChosenFromQuestion(rs.getInt("questionId")));
                a.setqExplainContent(getQuesionExplainByQuestionId(qId));
                if (a.isStatus()) {
                    list.add(a);
                }
            }

        } catch (SQLException e) {
        }
        return list;
    }

    public boolean getStatus(Question q) {
        return q.getListAnswer().size() >= 2 && q.getNumberOfTrue() > 0;
    }

    public String getQuesionExplainByQuestionId(int questionId) {
        String sql = "select * from Question_Explaination "
                + "where questionId=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, questionId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("qExplainContent");

            }
        } catch (Exception e) {
        }
        return null;
    }

}

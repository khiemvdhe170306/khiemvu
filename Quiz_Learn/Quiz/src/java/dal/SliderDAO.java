package dal;

import java.sql.Connection;
import model.Slider;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SliderDAO extends DBContext {

    // Lấy danh sách tất cả các slider
    public List<Slider> getSliderPagination(int offset, int fetch) {
        String sql = "SELECT * FROM Slider \n" +
                     "Order by sliderId\n" +
                     "OFFSET ? ROWS FETCH NEXT ? ROWS ONLY";
        List<Slider> sliders = new ArrayList<>();
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, offset);
            statement.setInt(2, fetch);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Slider slider = new Slider();
                slider.setSliderId(resultSet.getInt("sliderId"));
                slider.setStatus(resultSet.getBoolean("status"));
                slider.setTitle(resultSet.getString("title"));
                slider.setContent(resultSet.getString("content"));
                slider.setNotes(resultSet.getString("notes"));
                slider.setCourseId(resultSet.getInt("courseId"));
                slider.setImage(resultSet.getString("image"));
                sliders.add(slider);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return sliders;
    }
     public List<Slider> getSliders() {
        String sql = "SELECT * FROM Slider";
        List<Slider> sliders = new ArrayList<>();
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
             
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Slider slider = new Slider();
                slider.setSliderId(resultSet.getInt("sliderId"));
                slider.setStatus(resultSet.getBoolean("status"));
                slider.setTitle(resultSet.getString("title"));
                slider.setContent(resultSet.getString("content"));
                slider.setNotes(resultSet.getString("notes"));
                slider.setCourseId(resultSet.getInt("courseId"));
                slider.setImage(resultSet.getString("image"));
                sliders.add(slider);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return sliders;
    }

    // Lấy slider theo ID
    public Slider getSliderById(int sliderId) {
        String sql = "SELECT s.*,sj.courseId as courseId, sj.courseName as course_title, sj.description FROM Slider s JOIN [Course] sj ON s.courseId = sj.courseId  WHERE sliderId=?";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, sliderId);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                Slider slider = new Slider();
                slider.setSliderId(resultSet.getInt("sliderId"));
                slider.setStatus(resultSet.getBoolean("status"));
                slider.setTitle(resultSet.getString("title"));
                slider.setContent(resultSet.getString("content"));
                slider.setNotes(resultSet.getString("notes"));
                slider.setImage( resultSet.getString("image"));
                slider.setCourseId(resultSet.getInt("courseId"));
                slider.setCourse_title(resultSet.getString("course_title"));
                slider.setDescription(resultSet.getString("description"));
                return slider;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public int getTotalSlider(){
        int count = 0;
        String sql = "select count(sliderId) as total from slider";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                count = resultSet.getInt(1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }

    // Thêm slider mới
    public boolean addSlider(Slider slider) {
        String sql = "INSERT INTO Slider (status, title, content, notes, courseId, image) VALUES (?, ?, ?, ?, ?, ?)";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setBoolean(1, slider.isStatus());
            statement.setString(2, slider.getTitle());
            statement.setString(3, slider.getContent());
            statement.setString(4, slider.getNotes());
            statement.setInt(5, slider.getCourseId());
            statement.setString(6, slider.getImage());
            int rowsInserted = statement.executeUpdate();
            return rowsInserted > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    // Cập nhật thông tin slider
    public boolean updateSlider(Slider slider) {
        String sql = "UPDATE Slider SET status=?, title=?, content=?, notes=?, courseId=?, image = ? WHERE sliderId=?";
        try {
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setBoolean(1, slider.isStatus());
            statement.setString(2, slider.getTitle());
            statement.setString(3, slider.getContent());
            statement.setString(4, slider.getNotes());
            statement.setInt(5, slider.getCourseId());
            statement.setString(6, slider.getImage());
            statement.setInt(7, slider.getSliderId());
            int rowsUpdated = statement.executeUpdate();
            return rowsUpdated > 0;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    // Xóa slider theo ID
    public void deleteSlider(int sliderId) {
        PreparedStatement stm = null;
        try {
            String sql = "DELETE FROM Slider WHERE sliderId=?";
            stm = connection.prepareStatement(sql);
            stm.setInt(1, sliderId);
            stm.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(SliderDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                stm.close();
            } catch (SQLException ex) {
                Logger.getLogger(SliderDAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
     public boolean isCourseIdExists(int courseId) {
        try {
            String sql = "SELECT COUNT(*) FROM Course WHERE courseId = ?";
            PreparedStatement statement = connection.prepareStatement(sql);
            statement.setInt(1, courseId);

            ResultSet resultSet = statement.executeQuery();

            if (resultSet.next()) {
                int count = resultSet.getInt(1);
                return count > 0;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            // Xử lý lỗi kết nối hoặc truy vấn
        }
        return false;
    }
    public static void main(String[] args) {
        SliderDAO dao = new SliderDAO();
        Slider s = dao.getSliderById(20);
        System.out.println(s);
//        List<Slider> list = dao.getAllSliders();
//        for (Slider slider : list) {
//            System.out.println(slider);
//        }
    }
}

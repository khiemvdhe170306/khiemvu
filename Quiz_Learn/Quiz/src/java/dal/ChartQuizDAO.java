/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import model.Chapter;
import model.Question;
import model.Quiz;
import org.json.JSONObject;

/**
 *
 * @author admin
 */
public class ChartQuizDAO extends DBContext {
    
    
 public String getChartData() {
    Map<String, Object> chartData = new HashMap<>();
    try {
        String sql2 = "SELECT Course.courseName, COUNT(Lesson.lessonId) AS lessonCount " +
                      "FROM Course " +
                      "JOIN Chapter ON Course.courseId = Chapter.courseId " +
                      "JOIN Lesson ON Chapter.chapId = Lesson.chapId " +
                      "GROUP BY Course.courseName ";
        PreparedStatement st2 = connection.prepareStatement(sql2);
        ResultSet rs2 = st2.executeQuery();
        List<String> labels = new ArrayList<>();
        List<Integer> data = new ArrayList<>();
        while (rs2.next()) {
            labels.add(rs2.getString("courseName"));
            data.add(rs2.getInt("lessonCount"));
        }
        Map<String, Object> lessonByCourseData = new HashMap<>();
        lessonByCourseData.put("labels", labels);
        Map<String, Object> dataset = new HashMap<>();
        dataset.put("label", "Number of Lessons");
        dataset.put("data", data);
        lessonByCourseData.put("datasets", Collections.singletonList(dataset));
        chartData.put("lessonByCourseData", lessonByCourseData);
        
         String sql3 = "SELECT Subject.title, COUNT(Lesson.lessonId) AS lessonCount " +
                      "FROM Subject " +
                      "JOIN Course ON Subject.subjectId = Course.subjectId " +
                      "JOIN Chapter ON Course.courseId = Chapter.courseId " +
                      "JOIN Lesson ON Chapter.chapId = Lesson.chapId " +
                      "GROUP BY Subject.title";
        PreparedStatement st3 = connection.prepareStatement(sql3);
        ResultSet rs3 = st3.executeQuery();
        List<String> labels3 = new ArrayList<>();
        List<Integer> data3 = new ArrayList<>();
        while (rs3.next()) {
            labels3.add(rs3.getString("title"));
            data3.add(rs3.getInt("lessonCount"));
        }
        Map<String, Object> lessonBySubjectData = new HashMap<>();
        lessonBySubjectData.put("labels", labels3);
        Map<String, Object> dataset3 = new HashMap<>();
        dataset3.put("label", "Number of Lessons");
        dataset3.put("data", data3);
        lessonBySubjectData.put("datasets", Collections.singletonList(dataset3));
        chartData.put("lessonBySubjectData", lessonBySubjectData);

        // New code for Posts by Course
        String sql4 = "SELECT Course.courseName, COUNT(Post.postId) AS postCount " +
                      "FROM Course " +
                      "JOIN Post ON Course.courseId = Post.courseid " +
                      "GROUP BY Course.courseName";
        PreparedStatement st4 = connection.prepareStatement(sql4);
        ResultSet rs4 = st4.executeQuery();
        List<String> labels4 = new ArrayList<>();
        List<Integer> data4 = new ArrayList<>();
        while (rs4.next()) {
            labels4.add(rs4.getString("courseName"));
            data4.add(rs4.getInt("postCount"));
        }
        Map<String, Object> postByCourseData = new HashMap<>();
        postByCourseData.put("labels", labels4);
        Map<String, Object> dataset4 = new HashMap<>();
        dataset4.put("label", "Number of Posts");
        dataset4.put("data", data4);
        postByCourseData.put("datasets", Collections.singletonList(dataset4));
        chartData.put("postByCourseData", postByCourseData);
        // leson by chapter
         String sql5 = "SELECT Chapter.chapName, COUNT(Lesson.lessonId) AS lessonCount " +
                      "FROM Chapter " +
                      "JOIN Lesson ON Chapter.chapId = Lesson.chapid " +
                      "GROUP BY Chapter.chapName";
        PreparedStatement st5 = connection.prepareStatement(sql5);
        ResultSet rs5 = st5.executeQuery();
        List<String> labels5 = new ArrayList<>();
        List<Integer> data5 = new ArrayList<>();
        while (rs5.next()) {
            labels5.add(rs5.getString("chapName"));
            data5.add(rs5.getInt("lessonCount"));
        }
        Map<String, Object> LessonByChapterData = new HashMap<>();
        LessonByChapterData.put("labels", labels5);
        Map<String, Object> dataset5 = new HashMap<>();
        dataset5.put("label", "Number of Lesson");
        dataset5.put("data", data5);
        LessonByChapterData.put("datasets", Collections.singletonList(dataset5));
        chartData.put("LessonByChapterData", LessonByChapterData);
        
        // slider by Course
        
         String sql6 = "SELECT Course.courseName, COUNT(Slider.SliderId) AS SliderCount " +
                      "FROM Course " +
                      "JOIN Slider ON Course.courseId = Slider.courseid " +
                      "GROUP BY Course.courseName";
        PreparedStatement st6 = connection.prepareStatement(sql6);
        ResultSet rs6 = st6.executeQuery();
        List<String> labels6 = new ArrayList<>();
        List<Integer> data6 = new ArrayList<>();
        while (rs6.next()) {
            labels6.add(rs6.getString("courseName"));
            data6.add(rs6.getInt("SliderCount"));
        }
        Map<String, Object> SliderByCourseData = new HashMap<>();
        SliderByCourseData.put("labels", labels6);
        Map<String, Object> dataset6 = new HashMap<>();
        dataset6.put("label", "Number of Slider");
        dataset6.put("data", data6);
        SliderByCourseData.put("datasets", Collections.singletonList(dataset6));
        chartData.put("SliderByCourseData", SliderByCourseData);

     // Chapter by Course
        
         String sql7 = "SELECT Course.courseName, COUNT(Chapter.chapId) AS ChapterCount " +
                      "FROM Course " +
                      "JOIN Chapter ON Course.courseId = Chapter.courseid " +
                      "GROUP BY Course.courseName";
        PreparedStatement st7 = connection.prepareStatement(sql7);
        ResultSet rs7 = st7.executeQuery();
        List<String> labels7 = new ArrayList<>();
        List<Integer> data7 = new ArrayList<>();
        while (rs7.next()) {
            labels7.add(rs7.getString("courseName"));
            data7.add(rs7.getInt("ChapterCount"));
        }
        Map<String, Object> ChapterByCourseData = new HashMap<>();
        ChapterByCourseData.put("labels", labels7);
        Map<String, Object> dataset7 = new HashMap<>();
        dataset7.put("label", "Number of Chapter");
        dataset7.put("data", data7);
        ChapterByCourseData.put("datasets", Collections.singletonList(dataset7));
        chartData.put("ChapterByCourseData", ChapterByCourseData);
        
        // Course by Subject
         String sql8 = "SELECT Subject.title, COUNT(Course.courseId) AS CourseCount " +
                      "FROM Subject " +
                      "JOIN Course ON Subject.subjectId = Course.subjectId " +
                      "GROUP BY Subject.title";
        PreparedStatement st8 = connection.prepareStatement(sql8);
        ResultSet rs8 = st8.executeQuery();
        List<String> labels8 = new ArrayList<>();
        List<Integer> data8 = new ArrayList<>();
        while (rs8.next()) {
            labels8.add(rs8.getString("title"));
            data8.add(rs8.getInt("CourseCount"));
        }
        Map<String, Object> CourseBySubjectData = new HashMap<>();
        CourseBySubjectData.put("labels", labels8);
        Map<String, Object> dataset8 = new HashMap<>();
        dataset8.put("label", "Number of Course");
        dataset8.put("data", data8);
        CourseBySubjectData.put("datasets", Collections.singletonList(dataset8));
        chartData.put("CourseBySubjectData", CourseBySubjectData);
        
        // QUizByCHapter
         String sql9 = "SELECT Chapter.chapName, COUNT(Quiz.quizId) AS quizCount" +
"                      FROM Chapter" +
"                      JOIN Quiz ON Quiz.quizId = Quiz.chapId" +
"                      GROUP BY Chapter.chapName";
        PreparedStatement st9 = connection.prepareStatement(sql9);
        ResultSet rs9 = st9.executeQuery();
        List<String> labels9 = new ArrayList<>();
        List<Integer> data9 = new ArrayList<>();
        while (rs9.next()) {
            labels9.add(rs9.getString("chapName"));
            data9.add(rs9.getInt("quizCount"));
        }
        Map<String, Object> QuizByChapterData = new HashMap<>();
        QuizByChapterData.put("labels", labels9);
        Map<String, Object> dataset9 = new HashMap<>();
        dataset9.put("label", "Number of Quiz");
        dataset9.put("data", data9);
        QuizByChapterData.put("datasets", Collections.singletonList(dataset9));
        chartData.put("QuizByChapterData", QuizByChapterData);

    } catch (SQLException e) {
        e.printStackTrace();
    }
    JSONObject json = new JSONObject(chartData);
    return json.toString();
}

}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Date;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import model.Account;
import model.AccountInfo;
import model.Course;
import model.PricePackage;
import model.Quiz;
import model.Registration;
import model.Role;
import model.Voucher;

/**
 *
 * @author khiem
 */
public class UserDAO extends DBContext {
    LevelDAO ld=new LevelDAO();
    //GET NUMBER OF ACCOUNT
    public int getAid() {
        String sql = "select max(aid) as count from Account";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt("count");
            }
        } catch (Exception e) {
        }
        return 0;
    }

    //GET NUMBER OF ACCOUNT
    public int getRoleId() {
        String sql = "select max(role) as count from Account_Role";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt("count");
            }
        } catch (Exception e) {
        }
        return 0;
    }

    //GET Account by aid
    public Account getAccountByAid(int aid) {
        String sql = "select * from Account where aid=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, aid);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Account a = new Account(rs.getInt("aid"), rs.getString("email"), Decrypt(rs.getBlob("password")), rs.getInt("role"), rs.getBoolean("status"));
                a.setMoney(moneySpendByAccount(rs.getInt("aid")));
                a.setRoleName(getAccountRoleById(rs.getInt("role")));
                if (getAccountImageByID(rs.getInt("aid")) != null) {
                    a.setImage(base64image(getAccountImageByID(rs.getInt("aid"))));
                }
                a.setUrl(rs.getString("url"));
                a.setAccountinfo(getAccountInfoByAid(rs.getInt("aid")));
                a.setMember(getRegistrationByAccountId(rs.getInt("aid")));
                a.setVoucher(getListVoucherByAccountId(rs.getInt("aid")));
                a.setCourse(getUserRegisCourse(rs.getInt("aid")));
                a.setVip(vip(a));
                a.setLevel(ld.getLevel(rs.getInt("aid")));
                
                return a;
            }
        } catch (Exception e) {
        }
        return null;
    }

    //login
    public Account checkLogin(String email, String password) throws IOException {
        String sql = "select * from Account where email=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, email);

            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                if (!Decrypt(rs.getBlob("password")).equalsIgnoreCase(password)) {
                    return null;
                }
                Account a = new Account(rs.getInt("aid"), email, password, rs.getInt("role"), rs.getBoolean("status"));
                a.setMoney(moneySpendByAccount(rs.getInt("aid")));
                a.setRoleName(getAccountRoleById(rs.getInt("role")));
                if (getAccountImageByID(rs.getInt("aid")) != null) {
                    a.setImage(base64image(getAccountImageByID(rs.getInt("aid"))));
                }
                a.setUrl(rs.getString("url"));
                a.setAccountinfo(getAccountInfoByAid(rs.getInt("aid")));
                a.setMember(getRegistrationByAccountId(rs.getInt("aid")));
                a.setVoucher(getListVoucherByAccountId(rs.getInt("aid")));
                a.setCourse(getUserRegisCourse(rs.getInt("aid")));
                a.setVip(vip(a));
                a.setLevel(ld.getLevel(rs.getInt("aid")));

                return a;
            }

        } catch (SQLException e) {
        }
        return null;

    }

    //GET Account by aid
    public List<Account> getAccountByName(String Name) {
        String sql = "select * from Account where email like '%" + Name + "%'";
        List<Account> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Account a = new Account(rs.getInt("aid"), rs.getString("email"), Decrypt(rs.getBlob("password")), rs.getInt("role"), rs.getBoolean("status"));
                a.setMoney(moneySpendByAccount(rs.getInt("aid")));
                a.setRoleName(getAccountRoleById(rs.getInt("role")));
                if (getAccountImageByID(rs.getInt("aid")) != null) {
                    a.setImage(base64image(getAccountImageByID(rs.getInt("aid"))));
                }
                a.setUrl(rs.getString("url"));
                a.setAccountinfo(getAccountInfoByAid(rs.getInt("aid")));
                a.setMember(getRegistrationByAccountId(rs.getInt("aid")));
                a.setVoucher(getListVoucherByAccountId(rs.getInt("aid")));
                a.setCourse(getUserRegisCourse(rs.getInt("aid")));
                a.setVip(vip(a));
                a.setLevel(ld.getLevel(rs.getInt("aid")));

                list.add(a);
            }
        } catch (Exception e) {
        }
        return list;
    }

    //get Account by email
    public Account getAccountbyEmail(String email) throws IOException {
        String sql = "select * from Account where email=? ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, email);

            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Account a = new Account(rs.getInt("aid"), email, Decrypt(rs.getBlob("password")), rs.getInt("role"), rs.getBoolean("status"));
                a.setMoney(moneySpendByAccount(rs.getInt("aid")));
                a.setRoleName(getAccountRoleById(rs.getInt("role")));
                if (getAccountImageByID(rs.getInt("aid")) != null) {
                    a.setImage(base64image(getAccountImageByID(rs.getInt("aid"))));
                }
                a.setUrl(rs.getString("url"));
                a.setAccountinfo(getAccountInfoByAid(rs.getInt("aid")));
                a.setMember(getRegistrationByAccountId(rs.getInt("aid")));
                a.setVoucher(getListVoucherByAccountId(rs.getInt("aid")));
                a.setCourse(getUserRegisCourse(rs.getInt("aid")));
                a.setVip(vip(a));
                a.setLevel(ld.getLevel(rs.getInt("aid")));

                return a;
            }

        } catch (SQLException e) {
        }
        return null;
    }

    //update password
    public Account updateAccountPasswordByEmail(String email, String pass) throws IOException {
        String sql = "UPDATE [dbo].[Account]\n"
                + "   SET [password] = EncryptByPassPhrase('123','" + pass + "') "
                + " WHERE email=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, email);
            st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
        return getAccountbyEmail(email);
    }

    //LIST ALL ACCOUNT
    public List<Account> getAllAccount() throws IOException {
        String sql = "select * from Account ";
        List<Account> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Account a = new Account(rs.getInt("aid"), rs.getString("email"), Decrypt(rs.getBlob("password")), rs.getInt("role"), rs.getBoolean("status"));
                a.setMoney(moneySpendByAccount(rs.getInt("aid")));
                a.setRoleName(getAccountRoleById(rs.getInt("role")));
                if (getAccountImageByID(rs.getInt("aid")) != null) {
                    a.setImage(base64image(getAccountImageByID(rs.getInt("aid"))));
                }
                a.setUrl(rs.getString("url"));
                a.setAccountinfo(getAccountInfoByAid(rs.getInt("aid")));
                a.setMember(getRegistrationByAccountId(rs.getInt("aid")));
                a.setVoucher(getListVoucherByAccountId(rs.getInt("aid")));
                a.setCourse(getUserRegisCourse(rs.getInt("aid")));
                a.setVip(vip(a));
                a.setLevel(ld.getLevel(rs.getInt("aid")));

                list.add(a);
            }

        } catch (SQLException e) {
        }
        return list;
    }

    //List all role
    public List<Role> getAllAccountRole() {
        String sql = "select * from Account_Role ";
        List<Role> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Role a = new Role(rs.getInt("role"), rs.getString("name"), rs.getString("description"), rs.getString("access"));
                list.add(a);
            }

        } catch (SQLException e) {
        }
        return list;
    }

    //get role by id
    public Role getAccountRoleById(int id) {
        String sql = "select * from Account_Role where role=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Role a = new Role(rs.getInt("role"), rs.getString("name"), rs.getString("description"), rs.getString("access"));
                return a;
            }

        } catch (SQLException e) {
        }
        return null;
    }

    //delete role by id
    public int deleteAccountRole(int id) {
        String sql = "delete from Account_Role where role=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            return st.executeUpdate();

        } catch (SQLException e) {
        }
        return 0;
    }

    //add role by id
    public int addAccountRole(String name, String description, String access) {
        String sql = "INSERT INTO [dbo].[Account_Role] VALUES (?,?,?,?) ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, getRoleId() + 1);
            st.setString(2, name);
            st.setString(3, description);
            st.setString(4, access);
            return st.executeUpdate();

        } catch (SQLException e) {
        }
        return 0;
    }

    //update role by id
    public int updateAccountRole(int id, String name, String description, String access) {
        String sql = "UPDATE [dbo].[Account_Role] SET [name] = ?,[description] = ?,[access]=? WHERE [role] = ? ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(4, id);
            st.setString(1, name);
            st.setString(2, description);
            st.setString(3, access);
            return st.executeUpdate();

        } catch (SQLException e) {
        }
        return 0;
    }

    //get Image by id
    public Blob getAccountImageByID(int id) {
        String sql = "select * from AccountImage where aid=? ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getBlob("image");
            }

        } catch (SQLException e) {
        }
        return null;
    }

    //add image account
    public int addAccountImageByID(int id, InputStream input) {
        String sql = "insert into AccountImage values (?,?) ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            st.setBlob(2, input);
            return st.executeUpdate();

        } catch (SQLException e) {
        }
        return 0;
    }

    //UPDATE PROFILE ACCOUNT INFO
    public int updateAccountImage(int id, InputStream input) {
        String sql = "update AccountImage set  image=? where aid=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(2, id);
            st.setBlob(1, input);
            return st.executeUpdate();

        } catch (SQLException e) {
        }
        return 0;
    }

//CRUD ACCOUNT
    //create account
    public Account signUp(String email, String password, String url) throws IOException {
        String sql = "INSERT INTO [dbo].[Account]([aid],[email],[password],[role],[status],[url],[createDate])VALUES(?,?,?,1,?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, getAid() + 1);
            st.setString(2, email.trim());
            st.setBlob(3, Encrypt(password));
            st.setBoolean(4, true);
            st.setString(5, url);
            st.setDate(6, getDateNow());
            st.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e);
        }
        return getAccountByAid(getAid());
    }

    //insert into AccountInfo values(1,N'Vũ Đình Khiêm','0912763561','03 Tien Hai St',20,1)
    //ADD post
    public Account addAccountInfo(int aid, String fullname, String phone, String address, int age, boolean gender) {
        String sql = "insert into AccountInfo values(?,?,?,?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, aid);
            st.setString(2, fullname);
            st.setString(3, phone);
            st.setString(4, address);
            st.setInt(5, age);
            st.setBoolean(6, gender);
            st.executeUpdate();

        } catch (SQLException e) {
        }

        return getAccountByAid(aid);
    }

    //UPDATE[dbo].[AccountInfo] SET [fullname]='khiem',[phone]='1232132131',[address]='2',[age]=20,[gender]=1 WHERE aid=1
    //ADD post
    public Account updateAccountInfo(int aid, String fullname, String phone, String address, int age, boolean gender) {
        String sql = "UPDATE[dbo].[AccountInfo] SET [fullname]=?,[phone]=?,[address]=?,[age]=?,[gender]=? WHERE aid=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(6, aid);
            st.setString(1, fullname);
            st.setString(2, phone);
            st.setString(3, address);
            st.setInt(4, age);
            st.setBoolean(5, gender);
            st.executeUpdate();

        } catch (SQLException e) {
        }

        return getAccountByAid(aid);
    }

    //change role account
    public Account setAccountRole(int aid, int role) {
        String sql = "update Account set role=? where aid=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, role);
            st.setInt(2, aid);
            st.executeUpdate();

        } catch (SQLException e) {
        }

        return getAccountByAid(aid);
    }

    //change status
    public Account changeStatus(int aid) {
        String sql = "update Account set status=? where aid=?";
        Account a = getAccountByAid(aid);
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            if (a.isStatus()) {
                st.setBoolean(1, false);
            } else {
                st.setBoolean(1, true);
            }
            st.setInt(2, aid);
            st.executeUpdate();

        } catch (SQLException e) {
        }
        return a;
    }

    //sort UserList
    public List<Account> bubbleSortAccount(List<Account> arr, int n, int sort) {
        int i, j;
        boolean swapped;
        switch (sort) {
            case 1:
                for (i = 0; i < n - 1; i++) {
                    swapped = false;
                    for (j = 0; j < n - i - 1; j++) {
                        if (arr.get(j).getMoney() <= arr.get(j + 1).getMoney()) {
                            Collections.swap(arr, j, j + 1);
                            swapped = true;
                        }
                    }

                    // If no two elements were swapped by inner loop,
                    // then break
                    if (swapped == false) {
                        break;
                    }
                }
                break;
            case 2:
                for (i = 0; i < n - 1; i++) {
                    swapped = false;
                    for (j = 0; j < n - i - 1; j++) {
                        if (arr.get(j).getMoney() > arr.get(j + 1).getMoney()) {
                            Collections.swap(arr, j, j + 1);
                            swapped = true;
                        }
                    }

                    // If no two elements were swapped by inner loop,
                    // then break
                    if (swapped == false) {
                        break;
                    }
                }
                break;
            default:
                break;
        }
        return arr;
    }

    //Accouninfo
    //GET AccountInfo by aid
    public AccountInfo getAccountInfoByAid(int aid) {
        String sql = "select * from AccountInfo where aid=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, aid);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                AccountInfo a = new AccountInfo(rs.getInt("aid"), rs.getString("fullname"), rs.getString("phone"), rs.getString("address"), rs.getInt("age"), rs.getBoolean("gender"));
                return a;
            }
        } catch (Exception e) {
        }
        return null;
    }

    String base64image(Blob blob) throws IOException {
        InputStream inputStream = null;
        try {
            inputStream = blob.getBinaryStream();
        } catch (SQLException ex) {
            ;
        }
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        byte[] buffer = new byte[4096];
        int bytesRead = -1;

        while ((bytesRead = inputStream.read(buffer)) != -1) {
            outputStream.write(buffer, 0, bytesRead);
        }

        byte[] imageBytes = outputStream.toByteArray();
        String base64Image = Base64.getEncoder().encodeToString(imageBytes);
        return base64Image;
    }

    public String randomCode() {
        int min = 100000;
        int max = 999999;
        int random_int = (int) Math.floor(Math.random() * (max - min + 1) + min);
        return Integer.toString(random_int);
    }

    public String randomPassword() {
        Random r = new Random();
        char c = (char) (r.nextInt(26) + 'a');
        String a = c + randomCode();
        char d = (char) (r.nextInt(26) + 'A');
        a += d;
        return a;
    }

    //GET NUMBER OF ACCOUNT
    public int getRegisId() {
        String sql = "select max(regisId) as count from Registration_member";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt("count");
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public List<PricePackage> getAllPricePackage() {
        String sql = "select * from PricePackage";
        List<PricePackage> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                PricePackage a = new PricePackage();
                a.setAcessDuration(rs.getInt("acessduration"));
                a.setName(rs.getString("name"));
                a.setPrice(rs.getDouble(("price")));
                a.setPriceId(rs.getInt("priceId"));
                a.setDescription(rs.getString("description"));
                list.add(a);
            }
        } catch (Exception e) {
        }
        return list;
    }

    public PricePackage getPricePackageByPriceId(int priceId) {
        String sql = "select * from PricePackage where priceId = " + priceId;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                PricePackage a = new PricePackage();
                a.setAcessDuration(rs.getInt("acessDuration"));
                a.setName(rs.getString("name"));
                a.setPrice(rs.getDouble("price"));
                a.setDescription(rs.getString("description"));
                a.setPriceId(rs.getInt("priceId"));
                return a;
            }
        } catch (Exception e) {
        }
        return null;
    }

    public List<Registration> getRegistrationByAccountId(int accountId) {
        String sql = "select * from Registration_Member where userId=?";
        List<Registration> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, accountId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Registration a = new Registration();
                Date regis_date = rs.getDate("regis_Date");
                a.setRegis_date(regis_date);
                PricePackage pp = (PricePackage) getPricePackageByPriceId(rs.getInt("priceId"));
                a.setRegisId(rs.getInt("regisId"));
                a.setPricePackage(pp);
                a.setUserId(rs.getInt("userId"));
                int duration = pp.getAcessDuration();
                a.setDue_Date(getDueDate(regis_date, duration));
                a.setVoucher(getVoucherById(rs.getInt("voucher")));
                a.setPrice(getPriceBySale(rs.getInt("regisId"), a.getVoucher().getPercentage()));

                list.add(a);
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public List<Registration> getAllRegistration() {
        String sql = "select * from Registration_member";
        List<Registration> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Registration a = new Registration();
                Date regis_date = rs.getDate("regis_Date");
                a.setRegis_date(regis_date);
                PricePackage pp = (PricePackage) getPricePackageByPriceId(rs.getInt("priceId"));
                a.setRegisId(rs.getInt("regisId"));
                a.setPricePackage(pp);
                a.setUserId(rs.getInt("userId"));
                int duration = pp.getAcessDuration();
                a.setDue_Date(getDueDate(regis_date, duration));
                a.setVoucher(getVoucherById(rs.getInt("voucher")));
                a.setPrice(getPriceBySale(rs.getInt("regisId"), a.getVoucher().getPercentage()));

                list.add(a);
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public Registration getRegistrationById(int accountId) {
        String sql = "select * from Registration_member where userId=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, accountId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Registration a = new Registration();
                Date regis_date = rs.getDate("regis_Date");
                a.setRegis_date(regis_date);
                PricePackage pp = (PricePackage) getPricePackageByPriceId(rs.getInt("priceId"));
                a.setRegisId(rs.getInt("regisId"));
                a.setPricePackage(pp);
                a.setUserId(rs.getInt("userId"));
                int duration = pp.getAcessDuration();
                a.setDue_Date(getDueDate(regis_date, duration));
                a.setVoucher(getVoucherById(rs.getInt("voucher")));
                a.setPrice(getPriceBySale(rs.getInt("regisId"), a.getVoucher().getPercentage()));
                return a;
            }

        } catch (SQLException e) {
        }
        return null;
    }

    public Registration getRegistrationByRegisId(int regisId) {
        String sql = "select * from Registration_member where regisId=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, regisId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Registration a = new Registration();
                Date regis_date = rs.getDate("regis_Date");
                a.setRegis_date(regis_date);
                PricePackage pp = (PricePackage) getPricePackageByPriceId(rs.getInt("priceId"));
                a.setRegisId(rs.getInt("regisId"));
                a.setPricePackage(pp);
                a.setUserId(rs.getInt("userId"));
                int duration = pp.getAcessDuration();
                a.setDue_Date(getDueDate(regis_date, duration));
                a.setVoucher(getVoucherById(rs.getInt("voucher")));
                a.setPrice(getPriceBySale(rs.getInt("regisId"), a.getVoucher().getPercentage()));
                return a;
            }

        } catch (SQLException e) {
        }
        return null;
    }

    public int addRegistrationMember(int priceId, int userId, int voucher) {
        String sql = "INSERT [dbo].[Registration_Member] ([regisId], [regis_Date], [priceId], [userId],[voucher]) VALUES (?, ?, ?, ?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, getRegisId() + 1);
            st.setDate(2, getDateNow());
            st.setInt(3, priceId);
            st.setInt(4, userId);
            st.setInt(5, voucher);
            return st.executeUpdate();

        } catch (SQLException e) {
        }
        return 0;
    }

    public int updateRegistrationMember(int regisId, int priceId, int voucher) {
        String sql = "update Registration_Member set priceId=?,voucher=? where regisId=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            st.setInt(1, priceId);
            st.setInt(2, voucher);
            st.setInt(3, regisId);
            return st.executeUpdate();

        } catch (SQLException e) {
        }
        return 0;
    }

    public int deleteRegistrationMember(int regisId) {
        String sql = "delete from Registration_Member where regisId=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, regisId);
            return st.executeUpdate();

        } catch (SQLException e) {
        }
        return 0;
    }

    public Date getDueDate(Date a, int duration) {
        LocalDate ld = a.toLocalDate();
        LocalDate monthLater = ld.plusMonths(duration);
        java.sql.Date sqlDate = java.sql.Date.valueOf(monthLater);
        return sqlDate;
    }

    public Date getDateNow() {
        long millis = System.currentTimeMillis();
        // creating a new object of the class Date  
        java.sql.Date date = new java.sql.Date(millis);
        return date;
    }

    public Blob Encrypt(String password) {
        String sql = "DECLARE @pass varbinary(max)\n"
                + "set @pass= EncryptByPassPhrase('123','" + password + "')\n"
                + "select @pass as Encrypt";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getBlob("Encrypt");
            }

        } catch (SQLException e) {
            //System.out.println(e);
        }
        return null;
    }

    public String Decrypt(Blob password) {
        String sql = "select convert(varchar(100),DecryptByPassPhrase('123',?)) as giaima";
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            st.setBlob(1, password);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getString("giaima");
            }

        } catch (SQLException e) {
        }
        return null;
    }

    public boolean getAccountRoleByName(String role) {
        String sql = "select * from Account_Role where name ='" + role + "'";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return false;
            }

        } catch (SQLException e) {
        }
        return true;
    }

    //STATISTIC ACCOUNT
    //moneyEachAccount
    public double moneySpendByAccount(int aid) {
        List<Registration> rList = getRegistrationByAccountId(aid);
        int sum = 0;
        for (Registration r : rList) {
            sum += r.getPrice();
        }
        return sum;
    }

    public PricePackage getPricePackageByRegisId(int regisId) {
        String sql = "select p.price,p.acessDuration,p.priceId,p.name from Registration_Member rm inner join PricePackage p on p.priceId=rm.priceId where regisId=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, regisId);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                PricePackage a = new PricePackage();
                a.setPrice(rs.getDouble("price"));
                a.setPriceId(rs.getInt("priceId"));
                a.setName(rs.getString("name"));
                a.setAcessDuration(rs.getInt("acessDuration"));

                return a;
            }

        } catch (SQLException e) {
        }
        return null;
    }

    public List<Course> getUserRegisCourse(int aid) {
        String sql = "select c.courseId, c.courseName, c.subjectId, c.status, c.thumbnail, c.description, c.level, c.video from Course c join CourseRegistration cr on cr.courseId = c.courseId and cr.userId = " + aid;
        List<Course> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Course a = new Course();
                a.setCourseId(rs.getInt("courseId"));
                a.setDescription(rs.getString("description"));
                a.setStatus(rs.getBoolean("status"));
                a.setCourseName(rs.getString("courseName"));
                a.setSubjectId(rs.getInt("subjectId"));
                a.setThumbnail(rs.getString("thumbnail"));
                a.setLevel(rs.getBoolean("level"));
                a.setVideo(rs.getString("video"));
                list.add(a);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return list;
    }

    public double getPriceBySale(int regisId, int voucher) {

        return getPricePackageByRegisId(regisId).getPrice() * (100 - voucher) / 100;
    }

    public Voucher getVoucherById(int id) {
        String sql = "select * from Voucher where id=?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Voucher a = new Voucher(rs.getInt("id"), rs.getString("name"), rs.getInt("percentage"));
                
                return a;
            }

        } catch (SQLException e) {
        }
        return null;
    }

    public List<Voucher> getListVoucherByAccountId(int accountId) {
        String sql = "select * from Member_Voucher vm inner join Voucher v on v.id=vm.voucherId where aid=?";
        List<Voucher> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, accountId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Voucher a = new Voucher(rs.getInt("id"), rs.getString("name"), rs.getInt("percentage"));
                
                a.setMid(rs.getInt("mid"));
                list.add(a);
            }

        } catch (SQLException e) {
        }
        return list;
    }

    public List<Voucher> getAllVoucher() {
        String sql = "select * from  Voucher ";
        List<Voucher> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Voucher a = new Voucher(rs.getInt("id"), rs.getString("name"), rs.getInt("percentage"));
                a.setNumberOfDistribution(getVoucherDistribution(rs.getInt("id")));
                if(a.getPercentage()>0){
                list.add(a);}
            }

        } catch (SQLException e) {
        }
        return list;
    }
    public int getVoucherDistribution(int voucherId)
    {
        
         String sql = "select count(*) as count from Member_Voucher where voucherId=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, voucherId);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt("count");
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public int getMaxVoucherUser() {
        String sql = "select max(mid) as count from Member_Voucher";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt("count");
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public int addVoucherUser(int aid, int voucher) {
        String sql = "insert into Member_Voucher values (?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, getMaxVoucherUser() + 1);
            st.setInt(2, aid);
            st.setInt(3, voucher);

            return st.executeUpdate();

        } catch (SQLException e) {
        }
        return 0;
    }

    public int deleteVoucherUser(int mid) {
        String sql = "delete from Member_Voucher where mid=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, mid);

            return st.executeUpdate();

        } catch (SQLException e) {
        }
        return 0;
    }
    public int deleteVoucherUserByUser(int aid) {
        String sql = "delete from Member_Voucher where aid=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, aid);

            return st.executeUpdate();

        } catch (SQLException e) {
        }
        return 0;
    }

    public boolean vip(Account a) {
        List<Registration> list = a.getMember();

        if (a.getRole() > 1) {
            return true;
        }
        if (list == null || list.size() == 0) {
            return false;
        }
        if (list.get(list.size() - 1).getDue_Date().after(getDateNow())) {
            return true;
        }

        return false;
    }

    public boolean vip2(Account a) {
        List<Registration> list = a.getMember();

        if (list == null || list.size() == 0) {
            return false;
        }
        if (list.get(list.size() - 1).getDue_Date().after(getDateNow())) {
            return true;
        }

        return false;
    }

    public int getaccessUser() {
        String sql = "select max(accessId) as count from AccountAccess";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt("count");
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public int addAccessUser(int userId) {
        String sql = "INSERT INTO [dbo].[AccountAccess]\n"
                + "           ([accessId]\n"
                + "           ,[aid]\n"
                + "           ,[date])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, getaccessUser() + 1);
            st.setInt(2, userId);
            st.setDate(3, getDateNow());
            return st.executeUpdate();

        } catch (SQLException e) {
        }
        return 0;
    }

    public List<Account> sortVipAccount(List<Account> arr, int sort) {
        List<Account> list = new ArrayList<>();
        switch (sort) {
            case 0:
                return arr;
            case 1:
                for (Account a : arr) {
                    if (a.isVip()) {
                        list.add(a);
                    }
                }
                return list;
            case 2:
                for (Account a : arr) {
                        if (!a.isVip()) {
                                list.add(a);
                                }
                        }   break;
        }
        return list;
    }
    public List<Quiz> getAllQuizByCourse(int courseId) {
        String sql = "select q.quizId,q.title,q.numberOfQuestion,q.image,q.duration,q.creator from Quiz q  "
                + "inner join Chapter ch on q.chapId=ch.chapId "
                + "inner join Course c on ch.courseId=c.courseId "
                + "inner join Subject s on s.subjectId=c.subjectId where c.courseId=" + courseId;
        
        List<Quiz> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Quiz a = new Quiz();
                int qId = rs.getInt("quizId");
                a.setQuizId(qId);
                a.setTitle(rs.getString("title"));
                a.setImage(rs.getString("image"));
                a.setDuration(rs.getInt("duration"));
                a.setNumberOfQuestion(rs.getInt("numberOfQuestion"));
                a.setAid(courseId);
                
                list.add(a);
            }
        } catch (SQLException e) {
        }
        return list;
    }
    public Course getCourseByquiz(int quizId) {
        String sql = "select c.courseId from Quiz q  "
                + "inner join Chapter ch on q.chapId=ch.chapId "
                + "inner join Course c on ch.courseId=c.courseId "
                + "inner join Subject s on s.subjectId=c.subjectId where q.quizId=" + quizId;
        
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Course a =new Course();
                int qId = rs.getInt("courseId");
                a.setCourseId(qId);
                
                return a;
            }
        } catch (SQLException e) {
        }
        return null;
    }
    public boolean checkComplete(List<Quiz> qList,int userId)
    {
        for(Quiz q :qList)
        {
            if(ld.HighestGradeOfUser(userId,q.getQuizId())<7)
            {
                return false;
            }
        }
        return true;
    }


}

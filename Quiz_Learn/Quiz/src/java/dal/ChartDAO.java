/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Account;
import model.Chart;
import model.Course;
import model.PricePackage;
import model.Registration;

/**
 *
 * @author khiem
 */
public class ChartDAO extends DBContext {

    UserDAO ud = new UserDAO();

    //get sale chart of total
    public List<Chart> getSaleChart(int year, int month) {
        List<Chart> list = new ArrayList<>();
        if (year == 0) {
            for (int i = 2021; i <= 2023; i++) {
                Chart a = new Chart();
                a.setMonth("Year " + i);
                a.setY(getSaleYear(i, 0, 12));
                list.add(a);
            }
        } else if (month == 0 && year != 0) {
            Chart a = new Chart();
            a.setMonth("January");
            a.setY(0);
            list.add(a);
            for (int i = 4; i <= 12; i += 4) {
                list.add(new Chart(i, getSaleYear(year, i - 4, i)));
            }
            for (Chart c : list) {
                switch (c.getX()) {
                    case 12:
                        c.setMonth("December");
                        break;
                    case 4:
                        c.setMonth("April");
                        break;
                    case 8:
                        c.setMonth("August");
                        break;

                    default:
                        break;
                }
            }
        } else {
            for (int i = 1; i <= 4; i++) {
                Chart a = new Chart(i, getSaleMonth(year, month, i * 7 - 7, i * 7));
                a.setMonth("Week " + i);
                list.add(a);
            }
        }
        return list;
    }

    //get Sale Year
    public int getSaleYear(int year, int monthFrom, int monthTo) {
        String sql = "select * from Registration_Member where year(regis_Date)=? and month(regis_Date)>=? and month(regis_Date)<=?";
        List<Registration> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, year);
            st.setInt(2, monthFrom);
            st.setInt(3, monthTo);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                Registration a = new Registration();
                Date regis_date = rs.getDate("regis_Date");
                a.setRegis_date(regis_date);
                PricePackage pp = (PricePackage) ud.getPricePackageByPriceId(rs.getInt("priceId"));
                a.setRegisId(rs.getInt("regisId"));
                a.setPricePackage(pp);
                a.setUserId(rs.getInt("userId"));
                int duration = pp.getAcessDuration();
                a.setDue_Date(ud.getDueDate(regis_date, duration));
                a.setVoucher(ud.getVoucherById(rs.getInt("voucher")));
                a.setPrice(ud.getPriceBySale(rs.getInt("regisId"), a.getVoucher().getPercentage()));

                list.add(a);
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return getSale(list);
    }

    //get sale month
    public int getSaleMonth(int year, int month, int dayFrom, int dayTo) {
        String sql = "select * from Registration_Member where year(regis_Date)=? and month(regis_Date)=? and day(regis_Date)>=? and day(regis_Date)<=?";
        List<Registration> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, year);
            st.setInt(2, month);
            st.setInt(3, dayFrom);
            st.setInt(4, dayTo);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Registration a = new Registration();
                Date regis_date = rs.getDate("regis_Date");
                a.setRegis_date(regis_date);
                PricePackage pp = (PricePackage) ud.getPricePackageByPriceId(rs.getInt("priceId"));
                a.setRegisId(rs.getInt("regisId"));
                a.setPricePackage(pp);
                a.setUserId(rs.getInt("userId"));
                int duration = pp.getAcessDuration();
                a.setDue_Date(ud.getDueDate(regis_date, duration));
                a.setVoucher(ud.getVoucherById(rs.getInt("voucher")));
                a.setPrice(ud.getPriceBySale(rs.getInt("regisId"), a.getVoucher().getPercentage()));

                list.add(a);
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return getSale(list);
    }

    public List<Registration> getAllRegistration() {
        String sql = "select * from Registration_member";
        List<Registration> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Registration a = new Registration();
                Date regis_date = rs.getDate("regis_Date");
                a.setRegis_date(regis_date);
                PricePackage pp = (PricePackage) ud.getPricePackageByPriceId(rs.getInt("priceId"));
                a.setRegisId(rs.getInt("regisId"));
                a.setPricePackage(pp);
                a.setUserId(rs.getInt("userId"));
                int duration = pp.getAcessDuration();
                a.setDue_Date(ud.getDueDate(regis_date, duration));
                a.setVoucher(ud.getVoucherById(rs.getInt("voucher")));
                a.setPrice(ud.getPriceBySale(rs.getInt("regisId"), a.getVoucher().getPercentage()));

                list.add(a);
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return list;
    }

    public int getSale(List<Registration> list) {
        int result = 0;
        for (Registration r : list) {
            result += r.getPrice();
        }
        return result;
    }

    //get sale chart of each brand
    public List<Chart> getSaleChartEachCourse(int year, int month) {

        List<Course> cList = getAllCourse();
        List<Chart> list = new ArrayList<>();
        if (year != 0) {
            for (int i = 0; i < cList.size(); i++) {
                Chart a = new Chart();
                a.setCourse(cList.get(i));
                a.setY(getSaleEachCourse(year, month, cList.get(i).getCourseId()));
                list.add(a);
            }
        } else {
            for (int i = 0; i < cList.size(); i++) {
                Chart a = new Chart();
                a.setCourse(cList.get(i));
                a.setY(getSaleEachCourseAll(cList.get(i).getCourseId()));
                list.add(a);
            }
        }
        return list;
    }

    //LIST ALL sub
    public List<Course> getAllCourse() {
        String sql = "select * from Course ";
        List<Course> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Course a = new Course();
                a.setCourseId(rs.getInt("courseId"));
                a.setDescription(rs.getString("description"));
                a.setStatus(rs.getBoolean("status"));
                a.setCourseName(rs.getString("courseName"));
                a.setSubjectId(rs.getInt("subjectId"));
                a.setThumbnail(rs.getString("thumbnail"));
                a.setLevel(rs.getBoolean("level"));
                a.setVideo(rs.getString("video"));
                list.add(a);
            }

        } catch (SQLException e) {
        }
        return list;
    }

    //get Sale
    public int getSaleEachCourse(int year, int month, int cid) {
        String sql = " select count(*) as sale from CourseRegistration where year(regis_Date)=? and month(regis_Date)>=? and month(regis_Date)<=? and courseId=?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, year);
            st.setInt(4, cid);
            if (month == 0) {
                st.setInt(2, 0);
                st.setInt(3, 12);
            } else {
                st.setInt(2, month);
                st.setInt(3, month);
            }
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt("sale");
            }
        } catch (Exception e) {
        }
        return 0;
    }

    //get Sale each Course all
    public int getSaleEachCourseAll(int cid) {
        String sql = " select count(*) as sale from CourseRegistration where courseId=?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, cid);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt("sale");
            }
        } catch (Exception e) {
        }
        return 0;
    }

    //get sale chart of each brand
    public List<Chart> getSaleChartEachPricePackage(int year, int month) {

        List<PricePackage> cList = getAllPricePackage();
        List<Chart> list = new ArrayList<>();
        if (year != 0) {
            for (int i = 0; i < cList.size(); i++) {
                Chart a = new Chart();
                a.setPricePackage(cList.get(i));
                a.setY(getSaleEachPricePackage(year, month, cList.get(i).getPriceId()));
                list.add(a);
            }
        } else {
            for (int i = 0; i < cList.size(); i++) {
                Chart a = new Chart();
                a.setPricePackage(cList.get(i));
                a.setY(getSaleEachPricePackageAll(cList.get(i).getPriceId()));
                list.add(a);
            }
        }
        return list;
    }

    public List<PricePackage> getAllPricePackage() {
        String sql = "select * from PricePackage";
        List<PricePackage> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                PricePackage a = new PricePackage();
                a.setAcessDuration(rs.getInt("acessduration"));
                a.setName(rs.getString("name"));
                a.setPrice(rs.getDouble(("price")));
                a.setPriceId(rs.getInt("priceId"));
                a.setDescription(rs.getString("description"));
                list.add(a);
            }
        } catch (Exception e) {
        }
        return list;
    }

    //get Sale
    public int getSaleEachPricePackage(int year, int month, int pid) {
        String sql = "select count(*) as sale from Registration_Member rm inner join PricePackage pp on rm.priceId=pp.priceId  where year(regis_Date)=? and month(regis_Date)>=? and month(regis_Date)<=? and pp.priceId=?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, year);
            st.setInt(4, pid);
            if (month == 0) {
                st.setInt(2, 0);
                st.setInt(3, 12);
            } else {
                st.setInt(2, month);
                st.setInt(3, month);
            }
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt("sale");
            }
        } catch (Exception e) {
        }
        return 0;
    }

    //get Sale each Course all
    public int getSaleEachPricePackageAll(int pid) {
        String sql = "select count(*) as sale from Registration_Member rm inner join PricePackage pp on rm.priceId=pp.priceId where pp.priceId=?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, pid);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt("sale");
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public List<Chart> getTopCourse(int year, int month) {
        if (year > 0) {
            return getTopChartCourse(year, month);
        } else {
            return getTopChartCourse();
        }
    }

    public List<Chart> getTopChartCourse() {

        String sql = "select courseId,count(*) as sale from CourseRegistration group by courseId order by sale desc";
        List<Chart> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Chart a = new Chart();
                Course p = getCourseById((rs.getInt("courseId")));
                a.setCourse(p);
                a.setY(rs.getInt("sale"));
                list.add(a);
            }
        } catch (Exception e) {
        }
        return list;
    }

    public List<Chart> getTopChartCourse(int year, int month) {
        String sql = "select courseId,count(*) as sale from CourseRegistration where year(regis_Date)=? and month(regis_Date)>=? and month(regis_Date)<=?  group by courseId order by sale desc";
        List<Chart> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, year);
            if (month == 0) {
                st.setInt(2, 0);
                st.setInt(3, 12);
            } else {
                st.setInt(2, month);
                st.setInt(3, month);
            }
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Chart a = new Chart();
                Course p = getCourseById((rs.getInt("courseId")));
                a.setCourse(p);
                a.setY(rs.getInt("sale"));
                list.add(a);
            }
        } catch (Exception e) {
        }
        return list;
    }

    public Course getCourseById(int id) {
        String sql = "select * from Course "
                + "where courseId = " + id;
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Course a = new Course();
                a.setCourseId(rs.getInt("courseId"));
                a.setDescription(rs.getString("description"));
                a.setStatus(rs.getBoolean("status"));
                a.setCourseName(rs.getString("courseName"));
                a.setSubjectId(rs.getInt("subjectId"));
                a.setThumbnail(rs.getString("thumbnail"));
                a.setLevel(rs.getBoolean("level"));
                a.setVideo(rs.getString("video"));
                //a.setListPack((ArrayList<PricePackage>) getPricePackageBy(a.getSubjectId()));
                return a;
            }

        } catch (SQLException e) {
        }
        return null;
    }

    public List<Chart> getTopAccount(int year, int month) {
        if (year > 0) {
            return getTopChartAccount(year, month);
        } else {
            return getTopChartAccount();
        }
    }

    public List<Chart> getTopChartAccount() {

        String sql = "select a.aid,sum(pp.price*(100-v.percentage)/100) as sale from Account a inner join Registration_Member rm on a.aid=rm.userId inner join PricePackage pp on rm.priceId=pp.priceId inner join Voucher v on v.id=rm.voucher group by a.aid order by sale desc";
        List<Chart> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Chart a = new Chart();
                Account p = ud.getAccountByAid(rs.getInt("aid"));
                a.setAccount(p);
                a.setY(rs.getInt("sale"));
                list.add(a);
            }
        } catch (Exception e) {
        }
        return list;
    }

    public List<Chart> getTopChartAccount(int year, int month) {
        String sql = "select a.aid,sum(pp.price*(100-v.percentage)/100) as sale from Account a inner join Registration_Member rm on a.aid=rm.userId inner join PricePackage pp on rm.priceId=pp.priceId inner join Voucher v on v.id=rm.voucher where year(regis_Date)=? and month(regis_Date)>=? and month(regis_Date)<=? group by a.aid order by sale desc";
        List<Chart> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, year);
            if (month == 0) {
                st.setInt(2, 0);
                st.setInt(3, 12);
            } else {
                st.setInt(2, month);
                st.setInt(3, month);
            }
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Chart a = new Chart();
                Account p = ud.getAccountByAid(rs.getInt("aid"));
                a.setAccount(p);
                a.setY(rs.getInt("sale"));
                list.add(a);
            }
        } catch (Exception e) {
        }
        return list;
    }

    //get Age Circle
    public List<Chart> getAgeChartCircle(int year, int month) {
        if (year > 0) {
            return getAgeChart(year, month);
        } else {
            return getAgeChart();
        }
    }

    //get age
    public List<Chart> getAgeChart() {
        String sql = "select sum(sale) as number from AccountInfo ai inner join \n"
                + "(select userId,count(*) as sale from  Registration_Member group by userId) as rm on ai.aid=rm.userId where age>=? and age<?";
        List<Chart> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, 10);
            st.setInt(2, 25);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Chart a = new Chart();
                a.setMonth("10-25");
                a.setY(rs.getInt("number"));
                list.add(a);
            }
            st.setInt(1, 25);
            st.setInt(2, 30);
            rs = st.executeQuery();
            if (rs.next()) {
                Chart a = new Chart();
                a.setMonth("25-30");
                a.setY(rs.getInt("number"));
                list.add(a);
            }
            st.setInt(1, 30);
            st.setInt(2, 40);
            rs = st.executeQuery();
            if (rs.next()) {
                Chart a = new Chart();
                a.setMonth("30-40");
                a.setY(rs.getInt("number"));
                list.add(a);
            }
            st.setInt(1, 40);
            st.setInt(2, 60);
            rs = st.executeQuery();
            if (rs.next()) {
                Chart a = new Chart();
                a.setMonth(">40");
                a.setY(rs.getInt("number"));
                list.add(a);
            }
        } catch (Exception e) {
        }
        return list;
    }

    //get age
    public List<Chart> getAgeChart(int year, int month) {
        String sql = "select sum(sale) as number from AccountInfo ai inner join \n"
                + "(select userId,count(*) as sale from  Registration_Member where year(regis_Date)=? and month(regis_Date)>=? and month(regis_Date)<=? group by userId) as rm on ai.aid=rm.userId where age>=? and age<?";
        List<Chart> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, year);
            if (month == 0) {
                st.setInt(2, 0);
                st.setInt(3, 12);
            } else {
                st.setInt(2, month);
                st.setInt(3, month);
            }
            st.setInt(4, 10);
            st.setInt(5, 25);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Chart a = new Chart();
                a.setMonth("10-25");
                a.setY(rs.getInt("number"));
                list.add(a);
            }
            st.setInt(4, 25);
            st.setInt(5, 30);
            rs = st.executeQuery();
            if (rs.next()) {
                Chart a = new Chart();
                a.setMonth("25-30");
                a.setY(rs.getInt("number"));
                list.add(a);
            }
            st.setInt(4, 30);
            st.setInt(5, 40);
            rs = st.executeQuery();
            if (rs.next()) {
                Chart a = new Chart();
                a.setMonth("30-40");
                a.setY(rs.getInt("number"));
                list.add(a);
            }
            st.setInt(4, 40);
            st.setInt(5, 60);
            rs = st.executeQuery();
            if (rs.next()) {
                Chart a = new Chart();
                a.setMonth(">40");
                a.setY(rs.getInt("number"));
                list.add(a);
            }
        } catch (Exception e) {
        }
        return list;
    }

    //get gender Circle
    public List<Chart> getGenderChartCircle(int year, int month) {
        if (year > 0) {
            return getGenderChart(year, month);
        } else {
            return getGenderChart();
        }
    }

    //get gender
    public List<Chart> getGenderChart() {
        String sql = "select sum(sale) as number from AccountInfo ai inner join \n"
                + "(select userId,count(*) as sale from  Registration_Member  group by userId) as rm on ai.aid=rm.userId where gender=?";
        List<Chart> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setBoolean(1, true);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Chart a = new Chart();
                a.setMonth("Male");
                a.setY(rs.getInt("number"));
                list.add(a);
            }

            st.setBoolean(1, false);
            rs = st.executeQuery();
            if (rs.next()) {
                Chart a = new Chart();
                a.setMonth("FeMale");
                a.setY(rs.getInt("number"));
                list.add(a);
            }

        } catch (Exception e) {
        }
        return list;
    }

    //get gender
    public List<Chart> getGenderChart(int year, int month) {
        String sql = "select sum(sale) as number from AccountInfo ai inner join \n"
                + "(select userId,count(*) as sale from  Registration_Member where year(regis_Date)=? and month(regis_Date)>=? and month(regis_Date)<=? group by userId) as rm on ai.aid=rm.userId where gender=?";
        List<Chart> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, year);
            if (month == 0) {
                st.setInt(2, 0);
                st.setInt(3, 12);
            } else {
                st.setInt(2, month);
                st.setInt(3, month);
            }
            st.setBoolean(4, true);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Chart a = new Chart();
                a.setMonth("Male");
                a.setY(rs.getInt("number"));
                list.add(a);
            }

            st.setBoolean(4, false);
            rs = st.executeQuery();
            if (rs.next()) {
                Chart a = new Chart();
                a.setMonth("FeMale");
                a.setY(rs.getInt("number"));
                list.add(a);
            }

        } catch (Exception e) {
        }
        return list;
    }

    public List<Chart> getAccessAccount(int year, int month) {
        if (year > 0) {
            return getTopAccessAccount(year, month);
        } else {
            return getTopAccessAccount();
        }
    }

    public List<Chart> getTopAccessAccount() {

        String sql = "select a.aid,count(*) as sale from AccountAccess aa inner join Account a on aa.aid=a.aid group by a.aid order by sale desc";
        List<Chart> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Chart a = new Chart();
                Account p = ud.getAccountByAid(rs.getInt("aid"));
                a.setAccount(p);
                a.setY(rs.getInt("sale"));
                list.add(a);
            }
        } catch (Exception e) {
        }
        return list;
    }

    public List<Chart> getTopAccessAccount(int year, int month) {
        String sql = "select a.aid,count(*) as sale from AccountAccess aa inner join Account a on aa.aid=a.aid where year(aa.date)=? and month(aa.date)>=? and month(aa.date)<=? group by a.aid order by sale desc";
        List<Chart> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, year);
            if (month == 0) {
                st.setInt(2, 0);
                st.setInt(3, 12);
            } else {
                st.setInt(2, month);
                st.setInt(3, month);
            }
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Chart a = new Chart();
                Account p = ud.getAccountByAid(rs.getInt("aid"));
                a.setAccount(p);
                a.setY(rs.getInt("sale"));
                list.add(a);
            }
        } catch (Exception e) {
        }
        return list;
    }

    //get Age Circle
    public List<Chart> getAgeAccessCircle(int year, int month) {
        if (year > 0) {
            return getAgeAccess(year, month);
        } else {
            return getAgeAccess();
        }
    }

    //get age
    public List<Chart> getAgeAccess() {
        String sql = "select sum(sale) as number from AccountInfo ai inner join (select a.aid,count(*) as sale from AccountAccess aa inner join Account a on aa.aid=a.aid group by a.aid) as rm on ai.aid=rm.aid where age>=? and age<?";
        List<Chart> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, 10);
            st.setInt(2, 25);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Chart a = new Chart();
                a.setMonth("10-25");
                a.setY(rs.getInt("number"));
                list.add(a);
            }
            st.setInt(1, 25);
            st.setInt(2, 30);
            rs = st.executeQuery();
            if (rs.next()) {
                Chart a = new Chart();
                a.setMonth("25-30");
                a.setY(rs.getInt("number"));
                list.add(a);
            }
            st.setInt(1, 30);
            st.setInt(2, 40);
            rs = st.executeQuery();
            if (rs.next()) {
                Chart a = new Chart();
                a.setMonth("30-40");
                a.setY(rs.getInt("number"));
                list.add(a);
            }
            st.setInt(1, 40);
            st.setInt(2, 60);
            rs = st.executeQuery();
            if (rs.next()) {
                Chart a = new Chart();
                a.setMonth(">40");
                a.setY(rs.getInt("number"));
                list.add(a);
            }
        } catch (Exception e) {
        }
        return list;
    }

    //get age
    public List<Chart> getAgeAccess(int year, int month) {
        String sql = "select sum(sale) as number from AccountInfo ai inner join (select a.aid,count(*) as sale from AccountAccess aa inner join Account a on aa.aid=a.aid where year(aa.date)=? and month(aa.date)>=? and month(aa.date)<=? group by a.aid) as rm on ai.aid=rm.aid where age>=? and age<?";
        List<Chart> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, year);
            if (month == 0) {
                st.setInt(2, 0);
                st.setInt(3, 12);
            } else {
                st.setInt(2, month);
                st.setInt(3, month);
            }
            st.setInt(4, 10);
            st.setInt(5, 25);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Chart a = new Chart();
                a.setMonth("10-25");
                a.setY(rs.getInt("number"));
                list.add(a);
            }
            st.setInt(4, 25);
            st.setInt(5, 30);
            rs = st.executeQuery();
            if (rs.next()) {
                Chart a = new Chart();
                a.setMonth("25-30");
                a.setY(rs.getInt("number"));
                list.add(a);
            }
            st.setInt(4, 30);
            st.setInt(5, 40);
            rs = st.executeQuery();
            if (rs.next()) {
                Chart a = new Chart();
                a.setMonth("30-40");
                a.setY(rs.getInt("number"));
                list.add(a);
            }
            st.setInt(4, 40);
            st.setInt(5, 60);
            rs = st.executeQuery();
            if (rs.next()) {
                Chart a = new Chart();
                a.setMonth(">40");
                a.setY(rs.getInt("number"));
                list.add(a);
            }
        } catch (Exception e) {
        }
        return list;
    }

    //get gender Circle
    public List<Chart> getGenderAccessCircle(int year, int month) {
        if (year > 0) {
            return getGenderAccess(year, month);
        } else {
            return getGenderAccess();
        }
    }

    //get gender
    public List<Chart> getGenderAccess() {
        String sql = "select sum(sale) as number from AccountInfo ai inner join (select a.aid,count(*) as sale from AccountAccess aa inner join Account a on aa.aid=a.aid group by a.aid) as rm on ai.aid=rm.aid where gender=?";
        List<Chart> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setBoolean(1, true);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Chart a = new Chart();
                a.setMonth("Male");
                a.setY(rs.getInt("number"));
                list.add(a);
            }

            st.setBoolean(1, false);
            rs = st.executeQuery();
            if (rs.next()) {
                Chart a = new Chart();
                a.setMonth("FeMale");
                a.setY(rs.getInt("number"));
                list.add(a);
            }

        } catch (Exception e) {
        }
        return list;
    }

    //get gender
    public List<Chart> getGenderAccess(int year, int month) {
        String sql = "select sum(sale) as number from AccountInfo ai inner join (select a.aid,count(*) as sale from AccountAccess aa inner join Account a on aa.aid=a.aid where year(aa.date)=? and month(aa.date)>=? and month(aa.date)<=? group by a.aid) as rm on ai.aid=rm.aid where gender=?";
        List<Chart> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, year);
            if (month == 0) {
                st.setInt(2, 0);
                st.setInt(3, 12);
            } else {
                st.setInt(2, month);
                st.setInt(3, month);
            }
            st.setBoolean(4, true);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Chart a = new Chart();
                a.setMonth("Male");
                a.setY(rs.getInt("number"));
                list.add(a);
            }

            st.setBoolean(4, false);
            rs = st.executeQuery();
            if (rs.next()) {
                Chart a = new Chart();
                a.setMonth("FeMale");
                a.setY(rs.getInt("number"));
                list.add(a);
            }

        } catch (Exception e) {
        }
        return list;
    }

    //get gender Circle
    public List<Chart> getGenderSignUpChartCircle(int year, int month) {
        if (year > 0) {
            return getGenderSignUpChart(year, month);
        } else {
            return getGenderSignUpChart();
        }
    }

    //get gender
    public List<Chart> getGenderSignUpChart() {
        String sql = "select count(*) as number from Account a inner join AccountInfo ai on a.aid=ai.aid where gender=?";
        List<Chart> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setBoolean(1, true);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Chart a = new Chart();
                a.setMonth("Male");
                a.setY(rs.getInt("number"));
                list.add(a);
            }

            st.setBoolean(1, false);
            rs = st.executeQuery();
            if (rs.next()) {
                Chart a = new Chart();
                a.setMonth("FeMale");
                a.setY(rs.getInt("number"));
                list.add(a);
            }

        } catch (Exception e) {
        }
        return list;
    }

    //get gender
    public List<Chart> getGenderSignUpChart(int year, int month) {
        String sql = "select count(*) as number from Account a inner join AccountInfo ai on a.aid=ai.aid where year(createDate)=? and month(createDate)>=? and month(createDate)<=? and gender=?";
        List<Chart> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, year);
            if (month == 0) {
                st.setInt(2, 0);
                st.setInt(3, 12);
            } else {
                st.setInt(2, month);
                st.setInt(3, month);
            }
            st.setBoolean(4, true);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Chart a = new Chart();
                a.setMonth("Male");
                a.setY(rs.getInt("number"));
                list.add(a);
            }

            st.setBoolean(4, false);
            rs = st.executeQuery();
            if (rs.next()) {
                Chart a = new Chart();
                a.setMonth("FeMale");
                a.setY(rs.getInt("number"));
                list.add(a);
            }

        } catch (Exception e) {
        }
        return list;
    }

    //get gender Circle
    public List<Chart> getAgeSignUpChartCircle(int year, int month) {
        if (year > 0) {
            return getAgeSignUpChart(year, month);
        } else {
            return getAgeSignUpChart();
        }
    }

    //get gender
    public List<Chart> getAgeSignUpChart() {
        String sql = "select count(*) as number from Account a inner join AccountInfo ai on a.aid=ai.aid where age>=? and age<?";
        List<Chart> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, 10);
            st.setInt(2, 25);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Chart a = new Chart();
                a.setMonth("10-25");
                a.setY(rs.getInt("number"));
                list.add(a);
            }
            st.setInt(1, 25);
            st.setInt(2, 30);
            rs = st.executeQuery();
            if (rs.next()) {
                Chart a = new Chart();
                a.setMonth("25-30");
                a.setY(rs.getInt("number"));
                list.add(a);
            }
            st.setInt(1, 30);
            st.setInt(2, 40);
            rs = st.executeQuery();
            if (rs.next()) {
                Chart a = new Chart();
                a.setMonth("30-40");
                a.setY(rs.getInt("number"));
                list.add(a);
            }
            st.setInt(1, 40);
            st.setInt(2, 60);
            rs = st.executeQuery();
            if (rs.next()) {
                Chart a = new Chart();
                a.setMonth(">40");
                a.setY(rs.getInt("number"));
                list.add(a);
            }
        } catch (Exception e) {
        }
        return list;
    }

    //get gender
    public List<Chart> getAgeSignUpChart(int year, int month) {
        String sql = "select count(*) as number from Account a inner join AccountInfo ai on a.aid=ai.aid where year(createDate)=? and month(createDate)>=? and month(createDate)<=? and  age>=? and age<?";
        List<Chart> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, year);
            if (month == 0) {
                st.setInt(2, 0);
                st.setInt(3, 12);
            } else {
                st.setInt(2, month);
                st.setInt(3, month);
            }
            st.setInt(4, 10);
            st.setInt(5, 25);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Chart a = new Chart();
                a.setMonth("10-25");
                a.setY(rs.getInt("number"));
                list.add(a);
            }
            st.setInt(4, 25);
            st.setInt(5, 30);
            rs = st.executeQuery();
            if (rs.next()) {
                Chart a = new Chart();
                a.setMonth("25-30");
                a.setY(rs.getInt("number"));
                list.add(a);
            }
            st.setInt(4, 30);
            st.setInt(5, 40);
            rs = st.executeQuery();
            if (rs.next()) {
                Chart a = new Chart();
                a.setMonth("30-40");
                a.setY(rs.getInt("number"));
                list.add(a);
            }
            st.setInt(4, 40);
            st.setInt(5, 60);
            rs = st.executeQuery();
            if (rs.next()) {
                Chart a = new Chart();
                a.setMonth(">40");
                a.setY(rs.getInt("number"));
                list.add(a);
            }
        } catch (Exception e) {
        }
        return list;

    }

    public double getSaleMembershipDifference(int year, int month) {
        if (year>0) {
            if(month>1)
            {
                double change=getTotalSaleMembership(year, month)-getTotalSaleMembership(year, month-1);
                if(getTotalSaleMembership(year, month-1)==0)return 100;
                double percent=change/getTotalSaleMembership(year, month-1);
                
                return percent*100;
            }
            else if(month==1)
            {
                
                double change=getTotalSaleMembership(year, 1)-getTotalSaleMembership(year-1, 12);
                if(getTotalSaleMembership(year-1, 12)==0)return 100;
                double percent=change/getTotalSaleMembership(year-1, 12);
                return percent*100;
            }
            else{
                double change=getTotalSaleMembership(year, 0)-getTotalSaleMembership(year-1, 0);
                if(getTotalSaleMembership(year-1, 0)==0)return 100;
                double percent=change/getTotalSaleMembership(year-1, 0);
                return percent*100;
                
            }
        } 
        return 100;
    }

    public int getTotalSaleMembership(int year, int month) {
        if (year == 0) {
            String sql = "select count(*) as sale from Registration_Member rm inner join PricePackage pp on rm.priceId=pp.priceId";
            try {
                PreparedStatement st = connection.prepareStatement(sql);
                ResultSet rs = st.executeQuery();
                if (rs.next()) {
                    return rs.getInt("sale");
                }
            } catch (SQLException e) {
                System.out.println(e);

            }
        } else {
            String sql = "select count(*) as sale from Registration_Member rm inner join PricePackage pp on rm.priceId=pp.priceId where year(regis_Date)=? and month(regis_Date)>=? and month(regis_Date)<=?";

            try {
                PreparedStatement st = connection.prepareStatement(sql);
                st.setInt(1, year);
                if (month == 0) {
                    st.setInt(2, 0);
                    st.setInt(3, 12);
                } else {
                    st.setInt(2, month);
                    st.setInt(3, month);
                }
                ResultSet rs = st.executeQuery();
                if (rs.next()) {
                    return rs.getInt("sale");
                }
            } catch (SQLException e) {
                System.out.println(e);
            }

        }
        return 0;
    }
    public double getMoneyMembershipDifference(int year, int month) {
        if (year>0) {
            if(month>1)
            {
                double change=getTotalMoneyMembership(year, month)-getTotalMoneyMembership(year, month-1);
                if(getTotalMoneyMembership(year, month-1)==0)return 100;
                double percent=change/getTotalMoneyMembership(year, month-1);
                
                return percent*100;
            }
            else if(month==1)
            {
                
                double change=getTotalMoneyMembership(year, 1)-getTotalMoneyMembership(year-1, 12);
                if(getTotalMoneyMembership(year-1, 12)==0)return 100;
                double percent=change/getTotalMoneyMembership(year-1, 12);
                return percent*100;
            }
            else{
                double change=getTotalMoneyMembership(year, 0)-getTotalMoneyMembership(year-1, 0);
                if(getTotalMoneyMembership(year-1, 0)==0)return 100;
                double percent=change/getTotalMoneyMembership(year-1, 0);
                return percent*100;
                
            }
        } 
        return 100;
    }
    public int getTotalMoneyMembership(int year, int month) {
        if (year == 0) {
            String sql = "select sum(price*(100-v.percentage)/100) as number from Registration_Member rm inner join PricePackage pp on rm.priceId=pp.priceId  inner join Voucher v on rm.voucher=v.id";
            try {
                PreparedStatement st = connection.prepareStatement(sql);
                ResultSet rs = st.executeQuery();
                if (rs.next()) {
                    return rs.getInt("number");
                }
            } catch (SQLException e) {
                System.out.println(e);

            }
        } else {
            String sql = "select sum(price*(100-v.percentage)/100) as number from Registration_Member rm inner join PricePackage pp on rm.priceId=pp.priceId  inner join Voucher v on rm.voucher=v.id  where year(regis_Date)=? and month(regis_Date)>=? and month(regis_Date)<=?";

            try {
                PreparedStatement st = connection.prepareStatement(sql);
                st.setInt(1, year);
                if (month == 0) {
                    st.setInt(2, 0);
                    st.setInt(3, 12);
                } else {
                    st.setInt(2, month);
                    st.setInt(3, month);
                }
                ResultSet rs = st.executeQuery();
                if (rs.next()) {
                    return rs.getInt("number");
                }
            } catch (SQLException e) {
                System.out.println(e);
            }

        }
        return 0;
    }
    public double getCourseMembershipDifference(int year, int month) {
        if (year>0) {
            if(month>1)
            {
                double change=getTotalCourseMembership(year, month)-getTotalCourseMembership(year, month-1);
                if(getTotalCourseMembership(year, month-1)==0)return 100;
                double percent=change/getTotalCourseMembership(year, month-1);
                
                return percent*100;
            }
            else if(month==1)
            {
                
                double change=getTotalCourseMembership(year, 1)-getTotalCourseMembership(year-1, 12);
                if(getTotalCourseMembership(year-1, 12)==0)return 100;
                double percent=change/getTotalCourseMembership(year-1, 12);
                return percent*100;
            }
            else{
                double change=getTotalCourseMembership(year, 0)-getTotalCourseMembership(year-1, 0);
                if(getTotalCourseMembership(year-1, 0)==0)return 100;
                double percent=change/getTotalCourseMembership(year-1, 0);
                return percent*100;
                
            }
        } 
        return 100;
    }
    public int getTotalCourseMembership(int year, int month) {
        if (year == 0) {
            String sql = "select count(*) as number from CourseRegistration";
            try {
                PreparedStatement st = connection.prepareStatement(sql);
                ResultSet rs = st.executeQuery();
                if (rs.next()) {
                    return rs.getInt("number");
                }
            } catch (SQLException e) {
                System.out.println(e);

            }
        } else {
            String sql = "select count(*) as number from CourseRegistration where year(regis_Date)=? and month(regis_Date)>=? and month(regis_Date)<=?";

            try {
                PreparedStatement st = connection.prepareStatement(sql);
                st.setInt(1, year);
                if (month == 0) {
                    st.setInt(2, 0);
                    st.setInt(3, 12);
                } else {
                    st.setInt(2, month);
                    st.setInt(3, month);
                }
                ResultSet rs = st.executeQuery();
                if (rs.next()) {
                    return rs.getInt("number");
                }
            } catch (SQLException e) {
                System.out.println(e);
            }

        }
        return 0;
    }
    public double getSignUpDifference(int year, int month) {
        if (year>0) {
            if(month>1)
            {
                double change=getTotalSignUp(year, month)-getTotalSignUp(year, month-1);
                if(getTotalSignUp(year, month-1)==0)return 100;
                double percent=change/getTotalSignUp(year, month-1);
                
                return percent*100;
            }
            else if(month==1)
            {
                
                double change=getTotalSignUp(year, 1)-getTotalSignUp(year-1, 12);
                if(getTotalSignUp(year-1, 12)==0)return 100;
                double percent=change/getTotalSignUp(year-1, 12);
                return percent*100;
            }
            else{
                double change=getTotalSignUp(year, 0)-getTotalSignUp(year-1, 0);
                if(getTotalSignUp(year-1, 0)==0)return 100;
                double percent=change/getTotalSignUp(year-1, 0);
                return percent*100;
                
            }
        } 
        return 100;
    }
    public int getTotalSignUp(int year, int month) {
        if (year == 0) {
            String sql = "select count(*) as number from Account";
            try {
                PreparedStatement st = connection.prepareStatement(sql);
                ResultSet rs = st.executeQuery();
                if (rs.next()) {
                    return rs.getInt("number");
                }
            } catch (SQLException e) {
                System.out.println(e);

            }
        } else {
            String sql = "select count(*) as number from Account where year(createDate)=? and month(createDate)>=? and month(createDate)<=?";

            try {
                PreparedStatement st = connection.prepareStatement(sql);
                st.setInt(1, year);
                if (month == 0) {
                    st.setInt(2, 0);
                    st.setInt(3, 12);
                } else {
                    st.setInt(2, month);
                    st.setInt(3, month);
                }
                ResultSet rs = st.executeQuery();
                if (rs.next()) {
                    return rs.getInt("number");
                }
            } catch (SQLException e) {
                System.out.println(e);
            }

        }
        return 0;
    }
    public double getAccessDifference(int year, int month) {
        if (year>0) {
            if(month>1)
            {
                double change=getTotalAccess(year, month)-getTotalAccess(year, month-1);
                if(getTotalAccess(year, month-1)==0)return 100;
                double percent=change/getTotalAccess(year, month-1);
                
                return percent*100;
            }
            else if(month==1)
            {
                
                double change=getTotalAccess(year, 1)-getTotalAccess(year-1, 12);
                if(getTotalAccess(year-1, 12)==0)return 100;
                double percent=change/getTotalAccess(year-1, 12);
                return percent*100;
            }
            else{
                double change=getTotalAccess(year, 0)-getTotalAccess(year-1, 0);
                if(getTotalAccess(year-1, 0)==0)return 100;
                double percent=change/getTotalAccess(year-1, 0);
                return percent*100;
                
            }
        } 
        return 100;
    }
    public int getTotalAccess(int year, int month) {
        if (year == 0) {
            String sql = "select count(*) as number from Account a inner join AccountAccess aa on a.aid=aa.aid";
            try {
                PreparedStatement st = connection.prepareStatement(sql);
                ResultSet rs = st.executeQuery();
                if (rs.next()) {
                    return rs.getInt("number");
                }
            } catch (SQLException e) {
                System.out.println(e);

            }
        } else {
            String sql = "select count(*) as number from Account a inner join AccountAccess aa on a.aid=aa.aid where year(date)=? and month(date)>=? and month(date)<=?";

            try {
                PreparedStatement st = connection.prepareStatement(sql);
                st.setInt(1, year);
                if (month == 0) {
                    st.setInt(2, 0);
                    st.setInt(3, 12);
                } else {
                    st.setInt(2, month);
                    st.setInt(3, month);
                }
                ResultSet rs = st.executeQuery();
                if (rs.next()) {
                    return rs.getInt("number");
                }
            } catch (SQLException e) {
                System.out.println(e);
            }

        }
        return 0;
    }
    //get sale chart of total
    public List<Chart> getUserSignUpChart(int year, int month) {
        List<Chart> list = new ArrayList<>();
        if (year == 0) {
            for (int i = 2021; i <= 2023; i++) {
                Chart a = new Chart();
                a.setMonth("Year " + i);
                a.setY(getUserSignUpYear(i, 0, 12));
                list.add(a);
            }
        } else if (month == 0 && year != 0) {
            Chart a = new Chart();
            a.setMonth("January");
            a.setY(0);
            list.add(a);
            for (int i = 4; i <= 12; i += 4) {
                list.add(new Chart(i, getUserSignUpYear(year, i - 4, i)));
            }
            for (Chart c : list) {
                switch (c.getX()) {
                    case 12:
                        c.setMonth("December");
                        break;
                    case 4:
                        c.setMonth("April");
                        break;
                    case 8:
                        c.setMonth("August");
                        break;

                    default:
                        break;
                }
            }
        } else {
            for (int i = 1; i <= 4; i++) {
                Chart a = new Chart(i, getUserSignUpMonth(year, month, i * 7 - 7, i * 7));
                a.setMonth("Week " + i);
                list.add(a);
            }
        }
        return list;
    }

    //get Sale Year
    public int getUserSignUpYear(int year, int monthFrom, int monthTo) {
        String sql = "select count(*) as number from Account where year(createDate)=? and month(createDate)>=? and month(createDate)<=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, year);
            st.setInt(2, monthFrom);
            st.setInt(3, monthTo);
            ResultSet rs = st.executeQuery();

            while (rs.next()) {
                return rs.getInt("number");
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return 0;
    }

    //get sale month
    public int getUserSignUpMonth(int year, int month, int dayFrom, int dayTo) {
        String sql = "select count(*) as number from Account where year(createDate)=? and month(createDate)=? and day(createDate)>=? and day(createDate)<=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, year);
            st.setInt(2, month);
            st.setInt(3, dayFrom);
            st.setInt(4, dayTo);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getInt("number");
            }

        } catch (SQLException e) {
            System.out.println(e);
        }
        return 0;
    }




}

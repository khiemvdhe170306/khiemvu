/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import model.Account;
import model.AccountInfo;
import model.Attempt;
import model.Course;
import model.CourseRegistration;
import model.Level;
import model.Question;
import model.Quiz;
import model.Rank;
import model.Voucher;

/**
 *
 * @author khiem
 */
public class LevelDAO extends DBContext {

    public List<Attempt> getListOfAttemptByQuizId(int userId, int quizId) {
        String sql = "select * from Quiz_History_Detail where quizId=? and userId=?";
        List<Attempt> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, quizId);
            st.setInt(2, userId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Attempt a = new Attempt();
                a.setId(rs.getInt("historyId"));
                //a.setAccount(ud.getAccountByAid(userId));
                a.setQuizId(rs.getInt("quizId"));

                a.setMark(rs.getInt("Mark"));
                a.setGrade(rs.getDouble("grade"));
                a.setEnd(rs.getTimestamp("end"));
                a.setStart(rs.getTimestamp("start"));
                a.setAttempt(rs.getInt("attempt"));
                a.setTime(getTime(rs.getInt("time")));
                list.add(a);
            }
        } catch (Exception e) {
        }
        return list;
    }

    public double getHighestAttempt(List<Attempt> list) {
        double high = 0;
        for (Attempt att : list) {
            if (att.getGrade() > high) {
                high = att.getGrade();
            }
        }
        return high;
    }

    public String getTime(int time) {
        time = time / 1000;
        int second = 0;
        int minute = 0;
        minute = time / 60;
        second = time % 60;
        String t = "";
        if (minute > 0) {
            t += minute + "p ";
        }
        t += second + "s ";
        return t;
    }

    //totel highest of quiz
    public int TotalGradeOfUser(int userId) {
        int result = 0;
        for (Quiz q : getAllQuiz()) {
            List<Attempt> list = getListOfAttemptByQuizId(userId, q.getQuizId());
            //plus highest attempt of quiz
            result += getHighestAttempt(list);
        }
        return result;
    }

    //highest of a quiz
    public int HighestGradeOfUser(int userId, int quizId) {
        int result = 0;

        List<Attempt> list = getListOfAttemptByQuizId(userId, quizId);
        //plus highest attempt of quiz
        result += getHighestAttempt(list);
        return result;
    }

    public Level getLevel(int userId) {
        int totalXP = TotalGradeOfUser(userId);
        int requiredXPPerLevel = 3; // Required XP to reach the next level, change this as needed
        int currentLevel = 0;
        int requiredXP = 0;
        int currentXP = totalXP;

        Level level = new Level();

        while (currentXP >= requiredXPPerLevel) {
            currentLevel++;
            requiredXP += requiredXPPerLevel;
            currentXP -= requiredXPPerLevel;
        }
        level.setCurrentLevel(currentLevel);
        level.setCurrentXP(currentXP);
        level.setRequireXP(requiredXP);
        return level;
    }

    //LIST quiz
    public List<Quiz> getAllQuiz() {
        String sql = "select * from Quiz";
        List<Quiz> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Quiz a = new Quiz();
                int qId = rs.getInt("quizId");
                a.setQuizId(qId);
                a.setTitle(rs.getString("title"));
                a.setImage(rs.getString("image"));
                a.setDuration(rs.getInt("duration"));
                list.add(a);
            }
        } catch (SQLException e) {
        }
        return list;
    }

    public Level NumberOfQuizDone(int userId) {
        String sql = "SELECT userId, COUNT(*) as count\n"
                + "FROM [Quiz_History_Detail]\n"
                + "WHERE userId = ?\n"
                + "GROUP BY userId;";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userId);
            ResultSet rs = st.executeQuery();

            if (rs.next()) {
                int count = rs.getInt("count");
                Level level = new Level();
                level.setNumberOfQuizDone(count);

                return level;
            }
        } catch (SQLException e) {
            System.out.println(e);
        }
        return null;
    }

    public CourseRegistration NumberOfCourseRegistration(int userId) {
        CourseRegistration courseRegistration = new CourseRegistration();
        String sql = "    SELECT userId, SUM(courseCount) AS totalCourseCount\n"
                + "FROM (\n"
                + "    SELECT userId, courseId, COUNT(courseId) AS courseCount\n"
                + "    FROM CourseRegistration\n"
                + "	where userId = ?\n"
                + "    GROUP BY userId, courseId\n"
                + ") AS counts\n"
                + "GROUP BY userId";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userId);
            ResultSet rs = st.executeQuery();

            if (rs.next()) {
                int totalCourseCount = rs.getInt("totalCourseCount");
                courseRegistration.setUserId(userId);
                courseRegistration.setTotalCourseCount(totalCourseCount);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return courseRegistration;
    }

    public int getTotalCourse() {
        int totalNumbeOfCourseCount = 0;
        String sql = "SELECT COUNT(DISTINCT courseId) AS totalNumbeOfCourseCount FROM Course";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();

            if (rs.next()) {
                totalNumbeOfCourseCount = rs.getInt("totalNumbeOfCourseCount");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return totalNumbeOfCourseCount;
    }

    public List<Voucher> getListVoucherByAccountId(int accountId) {
        String sql = "select * from Member_Voucher vm inner join Voucher v on v.id=vm.voucherId where aid=?";
        List<Voucher> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, accountId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Voucher a = new Voucher(rs.getInt("id"), rs.getString("name"), rs.getInt("percentage"));

                a.setMid(rs.getInt("mid"));
                list.add(a);
            }

        } catch (SQLException e) {
        }
        return list;
    }

    public List<Quiz> getAllQuizByCourse(int courseId) {
        String sql = "select q.quizId,q.title,q.numberOfQuestion,q.image,q.duration from Quiz q  "
                + "inner join Chapter ch on q.chapId=ch.chapId "
                + "inner join Course c on ch.courseId=c.courseId where c.courseId= ?";
        List<Quiz> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, courseId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Quiz a = new Quiz();
                QuestionDAO qd = new QuestionDAO();
                int qId = rs.getInt("quizId");
                a.setQuizId(qId);
                a.setTitle(rs.getString("title"));
                a.setImage(rs.getString("image"));
                a.setDuration(rs.getInt("duration"));
                a.setNumberOfQuestion(rs.getInt("numberOfQuestion"));
                a.setQuestion(qd.getAllQuestionByQuiz(rs.getInt("quizId")));
                a.setStatus(status(a.getQuestion(), a.getNumberOfQuestion()));
                list.add(a);
            }
        } catch (SQLException e) {
        }
        return list;
    }

    public boolean status(List<Question> q, int numberOfQuestion) {
        if (q.size() >= numberOfQuestion) {
            return true;
        } else {
            return false;
        }
    }

    public int getHighestGradeOfEachQuiz(int quizId, int userId) {
        String sql = "SELECT MAX(Mark) AS score\n"
                + "                FROM Quiz_History_Detail\n"
                + "                WHERE quizId = ? AND userId = ?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, quizId);
            st.setInt(2, userId);
            ResultSet rs = st.executeQuery();

            if (rs.next()) {
                return rs.getInt("score");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return 0;
    }

    public int getTotalAttemptsForUserAndQuiz(int quizId, int userId) {
        String sql = "SELECT COUNT(*) AS TotalAttempts\n"
                + "FROM Quiz_History_Detail\n"
                + "WHERE userId = ? AND quizId = ?\n"
                + "GROUP BY userId, quizId";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, quizId);
            st.setInt(2, userId);
            ResultSet rs = st.executeQuery();

            if (rs.next()) {
                return rs.getInt("TotalAttempts");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return 0;
    }

    public List<Rank> rank(List<Rank> rankList, int sort) {
        switch (sort) {
            case 1:
                Comparator<Rank> levelDecreaseComparator = new Comparator<Rank>() {
                    @Override
                    public int compare(Rank a, Rank b) {
                        // Your comparison logic here
                        //final int scoreCmp = -Double.compare(getHighestAttempt((List<Attempt>) a.getAttempt()),getHighestAttempt((List<Attempt>) b.getAttempt()));
                        return b.getLevel().getCurrentLevel() - a.getLevel().getCurrentLevel();
                        // Your comparison logic here
                        //final int scoreCmp = -Double.compare(getHighestAttempt((List<Attempt>) a.getAttempt()),getHighestAttempt((List<Attempt>) b.getAttempt()));

                    }
                };
                Collections.sort(rankList, levelDecreaseComparator);
                break;
            case 2:
                Comparator<Rank> levelIncreaseComparator = new Comparator<Rank>() {
                    @Override
                    public int compare(Rank a, Rank b) {
                        // Your comparison logic here
                        //final int scoreCmp = -Double.compare(getHighestAttempt((List<Attempt>) a.getAttempt()),getHighestAttempt((List<Attempt>) b.getAttempt()));
                        return a.getLevel().getCurrentLevel() - b.getLevel().getCurrentLevel();
                        // Your comparison logic here
                        //final int scoreCmp = -Double.compare(getHighestAttempt((List<Attempt>) a.getAttempt()),getHighestAttempt((List<Attempt>) b.getAttempt()));

                    }
                };
                Collections.sort(rankList, levelIncreaseComparator);
                break;
            default:
                throw new AssertionError();
        }

        // Sorting the rankList using the custom comparator
        return rankList;
    }

    public List<Rank> getListRank() {
        List<Rank> rList = new ArrayList<>();
        for (Account u : getAllAccount()) {
            rList.add(getRank(u.getAid()));
        }
        return rList;
    }

    public List<Account> getAllAccount() {
        String sql = "select * from Account";
        List<Account> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Account a = new Account();
                a.setAid(rs.getInt("aid"));
                list.add(a);
            }

        } catch (SQLException e) {
        }
        return list;
    }

    public Rank getRank(int userId) {

        Rank r = new Rank();

        r.setAttempt(getListOfAttemptByUserId(userId));
        r.setLevel(getLevel(userId));
        r.setUser(getAccountByAid(userId));

        return r;
    }

    public List<Attempt> getListOfAttemptByUserId(int userId) {
        String sql = "select * from Quiz_History_Detail where userId=?";
        List<Attempt> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Attempt a = new Attempt();
                a.setId(rs.getInt("historyId"));
                //a.setAccount(ud.getAccountByAid(userId));
                a.setQuizId(rs.getInt("quizId"));

                a.setMark(rs.getInt("Mark"));
                a.setGrade(rs.getDouble("grade"));
                a.setEnd(rs.getTimestamp("end"));
                a.setStart(rs.getTimestamp("start"));
                a.setAttempt(rs.getInt("attempt"));
                a.setTime(getTime(rs.getInt("time")));
                list.add(a);
            }
        } catch (Exception e) {
        }
        return list;
    }

    public Account getAccountByAid(int aid) {
        String sql = "select * from Account where aid=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, aid);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Account a = new Account();
                a.setAid(rs.getInt("aid"));
                a.setEmail(rs.getString("email"));
                if (getAccountImageByID(rs.getInt("aid")) != null) {
                    a.setImage(base64image(getAccountImageByID(rs.getInt("aid"))));
                }
                a.setUrl(rs.getString("url"));
                a.setAccountinfo(getAccountInfoByAid(rs.getInt("aid")));
                return a;
            }
        } catch (Exception e) {
        }
        return null;
    }

    public Blob getAccountImageByID(int id) {
        String sql = "select * from AccountImage where aid=? ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getBlob("image");
            }

        } catch (SQLException e) {
        }
        return null;
    }

    String base64image(Blob blob) throws IOException {
        InputStream inputStream = null;
        try {
            inputStream = blob.getBinaryStream();
        } catch (SQLException ex) {
            ;
        }
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        byte[] buffer = new byte[4096];
        int bytesRead = -1;

        while ((bytesRead = inputStream.read(buffer)) != -1) {
            outputStream.write(buffer, 0, bytesRead);
        }

        byte[] imageBytes = outputStream.toByteArray();
        String base64Image = Base64.getEncoder().encodeToString(imageBytes);
        return base64Image;
    }

    public AccountInfo getAccountInfoByAid(int aid) {
        String sql = "select * from AccountInfo where aid=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, aid);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                AccountInfo a = new AccountInfo(rs.getInt("aid"), rs.getString("fullname"), rs.getString("phone"), rs.getString("address"), rs.getInt("age"), rs.getBoolean("gender"));
                return a;
            }
        } catch (Exception e) {
        }
        return null;
    }

    public List<Course> getUserRegisCourse(int aid) {
        String sql = "select c.courseId, c.courseName, c.subjectId, c.status, c.thumbnail, c.description, c.level, c.video from Course c join CourseRegistration cr on cr.courseId = c.courseId and cr.userId = " + aid;
        List<Course> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Course a = new Course();
                a.setCourseId(rs.getInt("courseId"));
                a.setDescription(rs.getString("description"));
                a.setStatus(rs.getBoolean("status"));
                a.setCourseName(rs.getString("courseName"));
                a.setSubjectId(rs.getInt("subjectId"));
                a.setThumbnail(rs.getString("thumbnail"));
                a.setLevel(rs.getBoolean("level"));
                a.setVideo(rs.getString("video"));
                list.add(a);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return list;
    }
}

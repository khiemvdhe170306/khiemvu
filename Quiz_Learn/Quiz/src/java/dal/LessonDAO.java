/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Lesson;

/**
 *
 * @author Admin
 */
public class LessonDAO extends DBContext {

    ChapterDAO w = new ChapterDAO();

    public int getLesid() {
        String sql = "select max(lessonId) as count from Lesson";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt("count");
            }
        } catch (Exception e) {
        }
        return 0;
    }

    //List lesson
    public List<Lesson> getAllLesson() {
        String sql = "select * from Lesson ";
        List<Lesson> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Lesson a = new Lesson();
                a.setLessonId(rs.getInt("lessonId"));
                int chapid = rs.getInt("chapId");
                a.setChapter(w.getChapterById(chapid));
                a.setLessonName(rs.getString("lessonName"));
                a.setContent(rs.getString("content"));
                a.setImage(rs.getString("image"));
                a.setStatus(rs.getBoolean("status"));
                list.add(a);
            }
        } catch (SQLException e) {
        }
        return list;
    }
    
    //List lesson by subject (course/chapter)
    public List<Lesson> getAllLessonByCourse(int subjectId,int courseId,int chapId) {
        String sql = "select l.chapId,l.content,l.image,l.lessonId,l.lessonName from Lesson l  "
                + "inner join Chapter ch on l.chapId=ch.chapId "
                + "inner join Course c on ch.courseId=c.courseId "
                + "inner join Subject s on s.subjectId=c.subjectId where s.subjectId="+subjectId;
        if(courseId>0)
        {
            sql+=" and c.courseId="+courseId;
        }
        if(chapId>0)
            {
                sql+=" and ch.chapId="+chapId;
            }
        List<Lesson> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Lesson a = new Lesson();
                a.setLessonId(rs.getInt("lessonId"));
                int chapid = rs.getInt("chapId");
                a.setChapter(w.getChapterById(chapid));
                a.setLessonName(rs.getString("lessonName"));
                a.setContent(rs.getString("content"));
                a.setImage(rs.getString("image"));
                list.add(a);
            }
        } catch (SQLException e) {
        }
        return list;
    }

    
    //search lesson
    public List<Lesson> getLessonByName(String name) {
        String sql = "select * from Lesson "
                + "where lessonName like '%" + name + "%'";
        List<Lesson> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Lesson a = new Lesson();
                a.setLessonId(rs.getInt("lessonId"));
                int chapid = rs.getInt("chapId");
                a.setChapter(w.getChapterById(chapid));
                a.setLessonName(rs.getString("lessonName"));
                a.setContent(rs.getString("content"));
                a.setImage(rs.getString("image"));
                a.setStatus(rs.getBoolean("status"));
                list.add(a);
            }
        } catch (SQLException e) {
        }
        return list;
    }

    
    public Lesson getLessonById(int id) {
        String sql = "select * from Lesson "
                + "where lessonId = " + id;
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Lesson a = new Lesson();
                a.setLessonId(rs.getInt("lessonId"));
                int chapid = rs.getInt("chapId");
                a.setChapter(w.getChapterById(chapid));
                a.setLessonName(rs.getString("lessonName"));
                a.setContent(rs.getString("content"));
                a.setImage(rs.getString("image"));
                a.setStatus(rs.getBoolean("status"));

                return a;
            }

        } catch (SQLException e) {
        }
        return null;
    }

    public Lesson deleteLesson(int lessonId) {
        String sql = "delete from Lesson where lessonId = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, lessonId);
            st.executeUpdate();
        } catch (Exception e) {
        }
        return getLessonById(lessonId);
    }

    public Lesson addLesson(int chapId, String lessonName, String content, String image) {
        String sql = "INSERT INTO [dbo].[Lesson]\n"
                + "           ([lessonId]\n"
                + "           ,[chapId]\n"
                + "           ,[lessonName]\n"
                + "           ,[content]\n"
                + "           ,[image])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, getLesid() + 1);
            st.setInt(2, chapId);
            st.setString(3, lessonName);
            st.setString(4, content);
            st.setString(5, image);
            st.executeUpdate();
        } catch (Exception e) {
        }

        return getLessonById(getLesid());

    }

    public Lesson updateLesson(int lessonId, String lessonName, String content, String image, int chapId) {
        String sql = "UPDATE [dbo].[Lesson]\n"
                + "   SET [chapId] = ?\n"
                + "      ,[lessonName] = ?\n"
                + "      ,[content] = ?\n"
                + "      ,[image] = ?\n"
                + " WHERE [lessonId] =?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            st.setInt(1, chapId);
            st.setString(2, lessonName);
            st.setString(3, content);
            st.setString(4, image);
            st.setInt(5, lessonId);

            st.executeUpdate();
        } catch (Exception e) {
        }
        return getLessonById(lessonId);
    }

    public static void main(String[] args) {
        LessonDAO d = new LessonDAO();
        Lesson s = d.getLessonById(3);
        System.out.println(s);
    }


}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import model.PricePackage;
import model.Registration;

/**
 *
 * @author Acer
 */
public class PricePackageDAO extends DBContext {

    public int getLastPriceId() {
        String sql = "select max(priceId) as count from PricePackage";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt("count");
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public int getLastRegisMemId() {
        String sql = "select max(regisId) as count from Registration_Member";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt("count");
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public List<PricePackage> getAllPack() {
        String sql = "select * from PricePackage";
        List<PricePackage> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                PricePackage a = new PricePackage();
                a.setAcessDuration(rs.getInt("acessduration"));
                a.setName(rs.getString("name"));
                a.setPrice(rs.getDouble(("price")));
                a.setPriceId(rs.getInt("priceId"));
                a.setDescription(rs.getString("description"));
                list.add(a);
            }
        } catch (Exception e) {
        }
        return list;
    }

    public PricePackage getPricePackageById(int priceId) {
        String sql = "select * from PricePackage where priceId = " + priceId;
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                PricePackage a = new PricePackage();
                a.setAcessDuration(rs.getInt("acessDuration"));
                a.setName(rs.getString("name"));
                a.setPrice(rs.getDouble("price"));
                a.setDescription(rs.getString("description"));
                a.setPriceId(rs.getInt("priceId"));
                return a;
            }
        } catch (Exception e) {
        }
        return null;
    }

    public int addPricePackage(String name, int accessDuration, double price, String description ) {
        String sql = "INSERT INTO [dbo].[PricePackage]\n"
                + "           ([priceId]\n"
                + "           ,[name]\n"
                + "           ,[acessDuration]\n"
                + "           ,[price]\n"
                + "           ,[description])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?)";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, getLastPriceId() + 1);
            st.setString(2, name);
            st.setInt(3, accessDuration);
            st.setDouble(4, price);
            st.setString(5, description);
            return st.executeUpdate();
        } catch (Exception e) {
        }
        return 0;
    }

    public int updatePricePackage(String name, int accessDuration, double price, String description, int priceId) {
        String sql = "UPDATE [dbo].[PricePackage]\n"
                + "   SET [name] = ?\n"
                + "      ,[acessDuration] = ?\n"
                + "      ,[price] = ?\n"
                + "      ,[description] = ?\n"
                + " WHERE [priceId] = ?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, name);
            st.setInt(2, accessDuration);
            st.setDouble(3, price);
            st.setString(4, description);
            st.setInt(5, priceId);
            return st.executeUpdate();
        } catch (Exception e) {
        }
        return 0;
    }

    public int deletePricePackage(int priceId) {
        String sql = "DELETE FROM [dbo].[PricePackage]\n"
                + "      WHERE priceId=?";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, priceId);
            return st.executeUpdate();
        } catch (Exception e) {
        }
        return 0;
    }

    public int regisMember(Date regisDate, int priceId, int userId) {
        String sql = "INSERT INTO [dbo].[Registration_Member]\n"
                + "           ([regisId]\n"
                + "           ,[regis_Date]\n"
                + "           ,[priceId]\n"
                + "           ,[userId])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?)";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, getLastRegisMemId() + 1);
            st.setDate(2, regisDate);
            st.setInt(3, priceId);
            st.setInt(4, userId);
            return st.executeUpdate();
        } catch (Exception e) {
        }
        return 0;
    }

    public static void main(String[] args) {
        PricePackageDAO d = new PricePackageDAO();
        System.out.println(d.getPricePackageById(1));
    }
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.sql.Timestamp;
import java.util.Comparator;
import static jdk.nashorn.internal.objects.NativeDate.getTime;
import java.sql.SQLException;
import java.util.Base64;
import java.util.Collections;
import model.Account;
import model.AccountInfo;
import model.Answer_Quiz;
import model.Attempt;
import model.Course;
import model.CourseRegistration;
import model.Level;
import model.Quiz;
import model.Rank;

/**
 *
 * @author Admin
 */
public class RankDAO extends DBContext {

    UserDAO user = new UserDAO();

    public List<Rank> rank(List<Rank> rankList, int sort) {
        switch (sort) {
            case 1:
                Comparator<Rank> levelDecreaseComparator = new Comparator<Rank>() {
                    @Override
                    public int compare(Rank a, Rank b) {
                        return b.getLevel().getCurrentLevel() - a.getLevel().getCurrentLevel();

                    }
                };
                Collections.sort(rankList, levelDecreaseComparator);
                break;
            case 2:
                Comparator<Rank> levelIncreaseComparator = new Comparator<Rank>() {
                    @Override
                    public int compare(Rank a, Rank b) {
                        return a.getLevel().getCurrentLevel() - b.getLevel().getCurrentLevel();

                    }
                };
                Collections.sort(rankList, levelIncreaseComparator);
                break;
          
            default:
                throw new AssertionError();

        }

        // Sorting the rankList using the custom comparator
        return rankList;
    }

    public List<Attempt> rankAttempt(List<Attempt> rankList, int sort) {
        
                Collections.sort(rankList, (o1, o2) -> {
                    return o2.getMark()-o1.getMark(); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/LambdaBody
                });
        return rankList;
    }

    public List<Attempt> getListOfAttemptByQuizId(int userId, int quizId) {
        String sql = "select * from Quiz_History_Detail where quizId=? and userId=?";
        List<Attempt> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, quizId);
            st.setInt(2, userId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Attempt a = new Attempt();
                a.setId(rs.getInt("historyId"));
                //a.setAccount(ud.getAccountByAid(userId));
                a.setQuizId(rs.getInt("quizId"));

                a.setMark(rs.getInt("Mark"));
                a.setGrade(rs.getDouble("grade"));
                a.setEnd(rs.getTimestamp("end"));
                a.setStart(rs.getTimestamp("start"));
                a.setAttempt(rs.getInt("attempt"));
                a.setTime(getTime(rs.getInt("time")));
                list.add(a);
            }
        } catch (Exception e) {
        }
        return list;
    }

    public List<Attempt> getListOfAttemptByUserId(int userId) {
        String sql = "select * from Quiz_History_Detail where userId=?";
        List<Attempt> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Attempt a = new Attempt();
                a.setId(rs.getInt("historyId"));
                a.setQuizId(rs.getInt("quizId"));
                a.setMark(rs.getInt("Mark"));
                a.setGrade(rs.getDouble("grade"));
                a.setEnd(rs.getTimestamp("end"));
                a.setStart(rs.getTimestamp("start"));
                a.setAttempt(rs.getInt("attempt"));
                a.setTime(getTime(rs.getInt("time")));
                list.add(a);
            }
        } catch (Exception e) {
        }
        return list;
    }

    public int getHighestScore(int userId) {
        int high = 0;
        List<Attempt> list = getListOfAttemptByUserId(userId);
        for (Attempt att : list) {
            if (att.getMark() > high) {
                high = att.getMark();
            }
        }
        return high;
    }

    public int getHighestScore(int userId, int quizId) {
        int high = 0;
        List<Attempt> list = getListOfAttemptByQuizId(userId, quizId);
        for (Attempt att : list) {
            if (att.getMark() > high) {
                high = att.getMark();
            }
        }
        return high;
    }
    public Answer_Quiz getHighestScore2(int userId, int quizId) {
        int high = 0;
        
        List<Attempt> list = getListOfAttemptByQuizId(userId, quizId);
        if(list.size()>0){
            Answer_Quiz gradeAttempt=new Answer_Quiz(high, high);
        for (Attempt att : list) {
            if (att.getMark() > high) {
                high = att.getMark();
                gradeAttempt.setA(att.getId());
                gradeAttempt.setB(high);
            }
        };return gradeAttempt;
        }
        
        return null;
    }
    public List<Attempt> getHighestScoreAttempt(){
        List<Attempt> a=new ArrayList<>();
        for(Account u:getAllAccount())
        {
            for(Quiz q:getAllQuiz()){
                Answer_Quiz gradeAttempt=getHighestScore2(u.getAid(),q.getQuizId());
                if(gradeAttempt!=null)
                a.add(getAttemptByHistoryId(gradeAttempt.getA()));
            }
        }
        return a;
    }
     public Attempt getAttemptByHistoryId(int historyId) {
        String sql = "select * from Quiz_History_Detail where historyId=?";
        List<Attempt> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, historyId);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Attempt a = new Attempt();
                a.setId(rs.getInt("historyId"));
                a.setAccount(getAccountByAid(rs.getInt("userId")));
                a.setQuizId(rs.getInt("quizId"));
                a.setMark(rs.getInt("Mark"));
                a.setGrade(rs.getDouble("grade"));
                a.setEnd(rs.getTimestamp("end"));
                a.setStart(rs.getTimestamp("start"));
                a.setAttempt(rs.getInt("attempt"));
                a.setTime(getTime(rs.getInt("time")));
                
                return a;
            }
        } catch (Exception e) {
        }
        return null;
    }
    public String getTime(int time) {
        time = time / 1000;
        int second = 0;
        int minute = 0;
        minute = time / 60;
        second = time % 60;
        String t = "";
        if (minute > 0) {
            t += minute + "p ";
        }
        t += second + "s ";
        return t;
    }

    public double getHighestAttempt(List<Attempt> list) {
        double high = 0;
        for (Attempt att : list) {
            if (att.getGrade() > high) {
                high = att.getGrade();
            }
        }
        return high;
    }

    public List<Rank> getListRank() {
        List<Rank> rList = new ArrayList<>();
        for (Account u : getAllAccount()) {
            rList.add(getRank(u.getAid()));
        }
        return rList;
    }

    //day nha loz khanh()
    public Rank getRank(int userId) {
        Rank r = new Rank();
        r.setAttempt(getListOfAttemptByUserId(userId));
        r.setLevel(getLevel(userId));
        r.setUser(getAccountByAid(userId));
        r.setHighetScore(getHighestScore(userId));
        return r;
    }

    public Level getLevel(int userId) {
        int totalXP = TotalGradeOfUser(userId);
        int requiredXPPerLevel = 3; // Required XP to reach the next level, change this as needed
        int currentLevel = 0;
        int requiredXP = 0;
        int currentXP = totalXP;

        Level level = new Level();

        while (currentXP >= requiredXPPerLevel) {
            currentLevel++;
            requiredXP += requiredXPPerLevel;
            currentXP -= requiredXPPerLevel;
        }
        level.setCurrentLevel(currentLevel);
        level.setCurrentXP(currentXP);
        level.setRequireXP(requiredXP);
        return level;
    }

    //totel highest of quiz
    public int TotalGradeOfUser(int userId) {
        int result = 0;
        for (Quiz q : getAllQuiz()) {
            List<Attempt> list = getListOfAttemptByQuizId(userId, q.getQuizId());
            //plus highest attempt of quiz
            result += getHighestAttempt(list);
        }
        return result;
    }

    //highest of a quiz
    public int HighestGradeOfUser(int userId, int quizId) {
        int result = 0;

        List<Attempt> list = getListOfAttemptByQuizId(userId, quizId);
        //plus highest attempt of quiz
        result += getHighestAttempt(list);
        return result;
    }

    //LIST quiz
    public List<Quiz> getAllQuiz() {
        String sql = "select * from Quiz";
        List<Quiz> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Quiz a = new Quiz();
                int qId = rs.getInt("quizId");
                a.setQuizId(qId);
                a.setTitle(rs.getString("title"));
                a.setImage(rs.getString("image"));
                a.setDuration(rs.getInt("duration"));
                a.setNumberOfQuestion(rs.getInt("numberOfQuestion"));
                list.add(a);
            }
        } catch (SQLException e) {
        }
        return list;
    }

    public List<Account> getAllAccount() {
        String sql = "select * from Account";
        List<Account> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Account a = new Account();
                a.setAid(rs.getInt("aid"));
                //a.setCourse(course);
                list.add(a);
            }

        } catch (SQLException e) {
        }
        return list;
    }

    public Account getAccountByAid(int aid) {
        String sql = "select * from Account where aid=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, aid);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Account a = new Account();
                a.setAid(rs.getInt("aid"));
                a.setEmail(rs.getString("email"));
                if (getAccountImageByID(rs.getInt("aid")) != null) {
                    a.setImage(base64image(getAccountImageByID(rs.getInt("aid"))));
                }
                a.setUrl(rs.getString("url"));
                a.setAccountinfo(getAccountInfoByAid(rs.getInt("aid")));
                return a;
            }
        } catch (Exception e) {
        }
        return null;
    }
    //get Image by id

    public Blob getAccountImageByID(int id) {
        String sql = "select * from AccountImage where aid=? ";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, id);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getBlob("image");
            }

        } catch (SQLException e) {
        }
        return null;
    }

    String base64image(Blob blob) throws IOException {
        InputStream inputStream = null;
        try {
            inputStream = blob.getBinaryStream();
        } catch (SQLException ex) {
            ;
        }
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        byte[] buffer = new byte[4096];
        int bytesRead = -1;

        while ((bytesRead = inputStream.read(buffer)) != -1) {
            outputStream.write(buffer, 0, bytesRead);
        }

        byte[] imageBytes = outputStream.toByteArray();
        String base64Image = Base64.getEncoder().encodeToString(imageBytes);
        return base64Image;
    }

    public AccountInfo getAccountInfoByAid(int aid) {
        String sql = "select * from AccountInfo where aid=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, aid);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                AccountInfo a = new AccountInfo(rs.getInt("aid"), rs.getString("fullname"), rs.getString("phone"), rs.getString("address"), rs.getInt("age"), rs.getBoolean("gender"));
                return a;
            }
        } catch (Exception e) {
        }
        return null;
    }
}

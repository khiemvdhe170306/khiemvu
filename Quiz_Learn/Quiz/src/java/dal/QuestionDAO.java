/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Answer;
import model.Question;
import model.Quiz;

/**
 *
 * @author khiem
 */
public class QuestionDAO extends DBContext {

    //LIST ALL ACCOUNT
    public int getQuestionId() {
        String sql = "select max(questionId) as count from Question";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt("count");
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public int getQuestionExplainId() {
        String sql = "select max(qExplainId) as count from Question_Explaination";

        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt("count");
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public List<Answer> getAllAnswerFromQuestion(int questionId) {
        String sql = "select * from Answer where quesId=?";
        List<Answer> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, questionId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Answer a = new Answer();
                a.setContent(rs.getString("content"));
                a.setAid(rs.getInt("answerId"));
                a.setCorrect(rs.getBoolean("correct"));
                a.setQuestionId(rs.getInt("quesId"));
                list.add(a);
            }

        } catch (SQLException e) {
        }
        return list;
    }

    public List<Question> getAllQuestionByQuiz(int quizId) {
        String sql = "select * from Question where quizId=?";
        List<Question> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, quizId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Question a = new Question();
                int qId = rs.getInt("questionId");
                a.setqContent(rs.getString("qContent"));
                a.setQuestionId(qId);
                a.setListAnswer(getAllAnswerFromQuestion(qId));
                a.setNumberOfTrue(numberOfTrueAnswer(qId));
                a.setStatus(getStatus(a));
                a.setChoice(getTrueChoiceFromQuestion(rs.getInt("questionId")));
                a.setChosen(getChosenFromQuestion(rs.getInt("questionId")));
                a.setQuiz(getQuizById(rs.getInt("quizId")));
                a.setqExplainContent(getQuesionExplainByQuestionId(qId));
                list.add(a);
            }

        } catch (SQLException e) {
        }
        return list;
    }
        public List<Question> getAllQuestionByQuizAndContent(int quizId, String qContent) {
        String sql = "select * from Question where quizId="+quizId+" AND qContent like '%"+qContent +"%'";
        List<Question> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Question a = new Question();
                int qId = rs.getInt("questionId");
                a.setqContent(rs.getString("qContent"));
                a.setQuestionId(qId);
                a.setListAnswer(getAllAnswerFromQuestion(qId));
                a.setNumberOfTrue(numberOfTrueAnswer(qId));
                a.setStatus(getStatus(a));
                a.setChoice(getTrueChoiceFromQuestion(rs.getInt("questionId")));
                a.setChosen(getChosenFromQuestion(rs.getInt("questionId")));
                a.setQuiz(getQuizById(rs.getInt("quizId")));
                a.setqExplainContent(getQuesionExplainByQuestionId(qId));
                list.add(a);
            }

        } catch (SQLException e) {
        }
        return list;
    }
    

    public Quiz getQuizById(int quizId) {
        String sql = "select * from Quiz where quizId=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, quizId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Quiz a = new Quiz();
                int qId = rs.getInt("quizId");
                a.setQuizId(qId);
                a.setTitle(rs.getString("title"));
                a.setDuration(rs.getInt("duration"));
                return a;
            }
        } catch (Exception e) {
        }
        return null;
    }

    //select * from Question where quesId=10
    public Question getQuestionById(int questionId) {
        String sql = "select * from Question where questionId=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, questionId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Question a = new Question();
                int qId = rs.getInt("questionId");
                a.setqContent(rs.getString("qContent"));
                a.setQuestionId(qId);
                a.setListAnswer(getAllAnswerFromQuestion(qId));
                a.setNumberOfTrue(numberOfTrueAnswer(qId));
                a.setStatus(getStatus(a));
                a.setChoice(getTrueChoiceFromQuestion(rs.getInt("questionId")));
                a.setChosen(getChosenFromQuestion(rs.getInt("questionId")));
                a.setQuiz(getQuizById(rs.getInt("quizId")));
                a.setqExplainContent(getQuesionExplainByQuestionId(qId));
                return a;
            }

        } catch (SQLException e) {
        }
        return null;
    }

    public Question getQuestionExplainById(int questionId) {
        String sql = "select * from Question_Explaination where questionId=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, questionId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Question a = new Question();
                int qId = rs.getInt("questionId");
                a.setqExplainId(qId);
                a.setQuestionId(qId);
                a.setqExplainContent(getQuesionExplainByQuestionId(qId));
                return a;
            }

        } catch (SQLException e) {
        }
        return null;
    }

    public List<Question> getAllQuestion() {
        String sql = "select * from Question";
        List<Question> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Question a = new Question();
                int qId = rs.getInt("questionId");
                a.setqContent(rs.getString("qContent"));
                a.setQuestionId(qId);
                a.setListAnswer(getAllAnswerFromQuestion(qId));
                a.setNumberOfTrue(numberOfTrueAnswer(qId));
                a.setStatus(getStatus(a));
                a.setChoice(getTrueChoiceFromQuestion(rs.getInt("questionId")));
                a.setChosen(getChosenFromQuestion(rs.getInt("questionId")));
                a.setQuiz(getQuizById(rs.getInt("quizId")));
                a.setqExplainId(qId);
                a.setqExplainContent(getQuesionExplainByQuestionId(qId));
                list.add(a);
            }

        } catch (SQLException e) {
        }
        return list;
    }
    public Answer getAnswerById(int answerId) {
        String sql = "select * from Answer where answerId=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, answerId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Answer a = new Answer();
                a.setContent(rs.getString("content"));
                a.setAid(rs.getInt("answerId"));
                a.setCorrect(rs.getBoolean("correct"));
                a.setQuestionId(rs.getInt("quesId"));
                return a;
            }

        } catch (SQLException e) {
        }
        return null;
    }
    public List<Question> searchQuestionByContent(String searchq) {
        String sql = "select * from Question where qContent like '%" + searchq + "%'";
        List<Question> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Question a = new Question();
                int qId = rs.getInt("questionId");
                a.setqContent(rs.getString("qContent"));
                a.setQuestionId(qId);
                a.setListAnswer(getAllAnswerFromQuestion(qId));
                a.setNumberOfTrue(numberOfTrueAnswer(qId));
                a.setStatus(getStatus(a));
                a.setChoice(getTrueChoiceFromQuestion(rs.getInt("questionId")));
                a.setChosen(getChosenFromQuestion(rs.getInt("questionId")));
                a.setQuiz(getQuizById(rs.getInt("quizId")));
                a.setqExplainContent(getQuesionExplainByQuestionId(qId));
                list.add(a);
            }

        } catch (SQLException e) {
        }
        return list;
    }

    public int addQuestion(String qContent, int quizId) {
        String sql = "INSERT INTO Question VALUES (?, ?, ?)";
        try {

            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, getQuestionId() + 1);
            st.setString(2, qContent);
            st.setInt(3, quizId);
            return st.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e);
        }
        return 0;

    }

    public Question updateQuestion(int questionId, String qContent, int quizId) {
        String sql = "UPDATE [dbo].[Question]\n"
                + "   SET\n"
                + "      [qContent] = ?\n"
                + "      ,[quizId] = ?\n"
                + " WHERE [questionId] = ?";
        try {

            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, qContent);
            st.setInt(2, quizId);
            st.setInt(3, questionId);
            st.executeUpdate();

        } catch (SQLException e) {

        }
        return getQuestionById(questionId);

    }

    public int deleteQuestion(int questionId) {
        deleteQuestionDiscuss(questionId);
        deleteQuestionExplainQuestionId(questionId);
        deleteQuestionTrue(questionId);
        deleteAll(questionId);
        try {

            String sql = "delete from Question where questionId=?";
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, questionId);
            return st.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println(e);
        }
        
        return 0;
    }
    public int deleteAll(int questionId) {
        String sql = "delete from Answer where  quesId=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, questionId);
            return st.executeUpdate();
        } catch (Exception e) {
            System.out.println(e);
        }
        return 0;
    }
    public int deleteQuestionTrue(int questionId) {
        String sql = "delete from Question_True where questionId=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, questionId);
            return st.executeUpdate();

        } catch (SQLException e) {
            System.out.println();
        }

        return 0;
    }
    public int deleteQuestionDiscuss(int questionId) {
        String sql = "delete from QuestionDiscussion where qdQuestionId=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, questionId);
            return st.executeUpdate();

        } catch (SQLException e) {
            System.out.println();
        }

        return 0;
    }
    public int deleteQuestionExplainQuestionId(int questionId) {
        String sql = "delete from Question_Explaination where questionId=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, questionId);
            return st.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e);
        }

        return 0;
    }

    //get true answer
    public int numberOfTrueAnswer(int questionId) {
        String sql = "select * from Question_True where questionId=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, questionId);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt("numberOfCorrect");
            }
        } catch (Exception e) {
        }
        return 0;
    }

    //get user chosen
    public List<Answer> getChosenFromQuestion(int questionId) {
        String sql = "select answerId from Answer_Quiz  where quesId=?";
        List<Answer> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, questionId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                if(rs.getInt("answerId")==0)return null;
                
                int aid=rs.getInt("answerId");
                Answer a=getAnswerById(aid);
                list.add(a);
                list.add(a);
            }

        } catch (SQLException e) {
        }
        return list;
    }

    //get true choice
    public List<Answer> getTrueChoiceFromQuestion(int questionId) {
        String sql = "select * from Answer a join Question q on a.quesId=q.questionId where a.quesId=? and a.correct=1";
        List<Answer> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, questionId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Answer a = new Answer();
                a.setContent(rs.getString("content"));
                a.setAid(rs.getInt("answerId"));
                a.setCorrect(rs.getBoolean("correct"));
                list.add(a);
            }

        } catch (SQLException e) {
        }
        return list;
    }

    public String getQuesionExplainByQuestionId(int questionId) {
        String sql = "select * from Question_Explaination "
                + "where questionId=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, questionId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                return rs.getString("qExplainContent");

            }
        } catch (Exception e) {
        }
        return null;
    }

    public Question deleteQuestionExplain(int questionId) {
        String sql = "DELETE FROM [dbo].[Question_Explaination] "
                + "WHERE questionId=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, questionId);
            st.executeUpdate();

        } catch (SQLException e) {
        }

        return getQuestionById(getQuestionId());
    }

    public int addQuestionExplain(int questionId, String qExplainContent) {
        String sql = "INSERT INTO [dbo].[Question_Explaination]\n"
                + "           ([qExplainId]\n"
                + "           ,[questionId]\n"
                + "           ,[qExplainContent])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?)";
        try {

            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, getQuestionExplainId() + 1);
            st.setInt(2, questionId);
            st.setString(3, qExplainContent);
            return st.executeUpdate();

        } catch (SQLException e) {
            System.out.println(e);
        }
        return 0;

    }

    public Question updateQuestionExplain(int questionId, String qExplainContent) {
        String sql = "UPDATE [dbo].[Question_Explaination]\n"
                + "   SET\n"
                + "      [qExplainContent] = ?\n"
                + " WHERE [questionId] = ?";
        try {

            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, qExplainContent);
            st.setInt(2, questionId);
            st.executeUpdate();

        } catch (SQLException e) {

        }
        return getQuestionExplainById(questionId);

    }

    public boolean getStatus(Question q) {
        return q.getListAnswer().size() >= 2 && q.getNumberOfTrue() > 0;
    }

}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Post;
import model.Subject;

/**
 *
 * @author hieu
 */
public class PostDAO extends DBContext {

    CourseDAO s = new CourseDAO();

    //GET NUMBER OF Post
    public int getPostid() {
        String sql = "select max(postId) as count from Post";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt("count");
            }
        } catch (Exception e) {
        }
        return 0;
    }

    //LIST ALL Post/
    public List<Post> getAllPost() {
        String sql = "select * from Post ";
        List<Post> list = new ArrayList<>();
        UserDAO d=new UserDAO();
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Post a = new Post();
                a.setContent(rs.getString("content"));
                a.setPostId(rs.getInt("postId"));
                a.setUser(d.getAccountByAid(rs.getInt("userId")));
                a.setBrifInfor(rs.getString("brifInfor"));
                a.setTitle(rs.getString("title"));
                int id = rs.getInt("courseid");
                 a.setCourse(s.getCourseById(id));
                a.setThumbnail(rs.getString("thumbnail"));
                a.setPostDate(rs.getDate("date"));
                list.add(a);
            }

        } catch (SQLException e) {
        }
        return list;
    }
    //LIST ALL Post/
    public List<Post> getTopPost() {
        String sql = "select top 5 * from Post order by postId desc";
        List<Post> list = new ArrayList<>();
        UserDAO d=new UserDAO();
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Post a = new Post();
                a.setContent(rs.getString("content"));
                a.setPostId(rs.getInt("postId"));
                a.setUser(d.getAccountByAid(rs.getInt("userId")));
                a.setBrifInfor(rs.getString("brifInfor"));
                a.setTitle(rs.getString("title"));
                int id = rs.getInt("courseid");
                 a.setCourse(s.getCourseById(id));
                a.setThumbnail(rs.getString("thumbnail"));
                a.setPostDate(rs.getDate("date"));
                list.add(a);
            }

        } catch (SQLException e) {
        }
        return list;
    }

    public List<Post> getLastPostById() {
        List<Post> list = new ArrayList<>();
        UserDAO d=new UserDAO();
        try {
            String sql = "select * from Post order by postId desc ";

            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Post a = new Post();
                a.setContent(rs.getString("content"));
                a.setPostId(rs.getInt("postId"));
                a.setUser(d.getAccountByAid(rs.getInt("userId")));
                a.setBrifInfor(rs.getString("brifInfor"));
                a.setTitle(rs.getString("title"));
                int id = rs.getInt("courseid");
                 a.setCourse(s.getCourseById(id));
                a.setThumbnail(rs.getString("thumbnail"));
                a.setPostDate(rs.getDate("date"));
                list.add(a);
            }

        } catch (SQLException e) {
        }
        return list;
    }

    //INSERT [dbo].[Post] ([thumbnail] , [userId],  [content], [brifInfor], [title],[courseid],[date] ) VALUES(?,?,?,?,?,?,?,?)
    //ADD post
    public Post addPost(String thumbnail, String content, String brifInfor, String title, int course, int userId, Date date) {
        String sql = "INSERT [dbo].[Post] ([thumbnail] , [userId],  [content],  [brifInfor], [title],[courseid],[date]) VALUES(?,?,?,?,?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, thumbnail);
            st.setInt(2, userId);
            st.setString(3, content);
            st.setString(4, brifInfor);
            st.setString(5, title);
            st.setInt(6, course);
            st.setDate(7, date);
            st.executeUpdate();

        } catch (SQLException e) {
        }

        return getPostById(getPostid());
    }
//update
    //UPDATE category

    public Post updatePost(int postId, String thumbnail, String content, String brifInfor, String title, int course) {
        String sql = "UPDATE [dbo].[Post] SET [thumbnail] =?,[content] =?,[brifInfor] = ? ,[title] = ?,[courseid] =? WHERE postId=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, thumbnail);
            st.setString(2, content);
            st.setString(3, brifInfor);
            st.setString(4, title);
            st.setInt(5, course);
            st.setInt(6, postId);
            st.executeUpdate();

        } catch (SQLException e) {
        }

        return getPostById(postId);
    }
//Delete 
    //delete from Post where postId=5

    public Post deletePost(int postId) {
        String sql = "delete from Post where postId=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, postId);
            st.executeUpdate();

        } catch (SQLException e) {
        }

        return getPostById(getPostid());
    }

    public Post getPostById(int postid) {
        UserDAO d=new UserDAO();
        try {
            String sql = "select * from Post where postId=? ";

            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, postid);
            ResultSet rs = st.executeQuery();
            
            while (rs.next()) {
                Post a = new Post();
                a.setContent(rs.getString("content"));
                a.setPostId(rs.getInt("postId"));
                a.setUser(d.getAccountByAid(rs.getInt("userId")));
                a.setBrifInfor(rs.getString("brifInfor"));
                a.setTitle(rs.getString("title"));
                int id = rs.getInt("courseid");
                 a.setCourse(s.getCourseById(id));
                a.setThumbnail(rs.getString("thumbnail"));
                a.setPostDate(rs.getDate("date"));
                return a;
            }

        } catch (SQLException e) {
        }
        return null;
    }

    //Search Post by title
    public List<Post> searchPostByTitle(String title) {
        List<Post> list = new ArrayList<>();
        UserDAO d=new UserDAO();
        try {
            String sql = "select * from Post where title like '%"+title+"%' ";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Post a = new Post();
                a.setContent(rs.getString("content"));
                a.setPostId(rs.getInt("postId"));
                a.setUser(d.getAccountByAid(rs.getInt("userId")));
                a.setBrifInfor(rs.getString("brifInfor"));
                a.setTitle(rs.getString("title"));
                 int id=rs.getInt("courseid");
                a.setCourse(s.getCourseById(id));
                a.setThumbnail(rs.getString("thumbnail"));
                a.setPostDate(rs.getDate("date"));
                list.add(a);
            }

        } catch (SQLException e) {
        }
        return list;
    }
    
        public List<Post> getLastPostByCourseId(int courseid) {
        List<Post> list = new ArrayList<>();
        UserDAO d=new UserDAO();
        
        try {
            String sql = "select * from Post where courseid=? order by postId desc ";
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            st.setInt(1, courseid);
            while (rs.next()) {
                Post a = new Post();
                a.setContent(rs.getString("content"));
                a.setPostId(rs.getInt("postId"));
                a.setUser(d.getAccountByAid(rs.getInt("userId")));
                a.setBrifInfor(rs.getString("brifInfor"));
                a.setTitle(rs.getString("title"));
                 int id=rs.getInt("courseid");
                a.setCourse(s.getCourseById(id));
                a.setThumbnail(rs.getString("thumbnail"));
                a.setPostDate(rs.getDate("date"));
                list.add(a);
            }

        } catch (SQLException e) {
        }
        return list;
    }
         
        

}

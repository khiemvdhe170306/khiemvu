/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import model.Account;

import model.Chapter;
import model.Course;
import model.Lesson;
import model.Question;
import model.Quiz;


/**
 *
 * @author khiem
 */
public class ChapterDAO extends DBContext{
    QuestionDAO qd=new QuestionDAO();
    public int getChapterid() {
        String sql = "select max(chapId) as count from Chapter";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt("count");
            }
        } catch (Exception e) {
        }
        return 0;
    }
    //LessonDAO ld=new LessonDAO();
    public List<Chapter>  getAllChapter(){
        
        String sql = "select * from Chapter ";
        List<Chapter> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Chapter a=new Chapter();
                a.setDescription(rs.getString("description"));
                a.setChapId(rs.getInt("chapId"));
                a.setChapName(rs.getString("chapName"));
                a.setCourse(getCourseByChapId(rs.getInt("chapId")));
                a.setLesson(getAllLessonByChapId(rs.getInt("chapId")));
                a.setQuiz(getAllQuizByChapter(rs.getInt("chapId")));
                
                list.add(a);
            }

        } catch (SQLException e) {
        }
        return list;
    }  
    //List lesson
    public List<Lesson> getAllLessonByChapId(int chapId) {
        String sql = "select * from Lesson where chapId=" + chapId;
        List<Lesson> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Lesson a = new Lesson();
                a.setLessonId(rs.getInt("lessonId"));
                int chapid = rs.getInt("chapId");
                //a.setChapter(getChapterById(chapid));
                a.setLessonName(rs.getString("lessonName"));
                a.setContent(rs.getString("content"));
                a.setImage(rs.getString("image"));
                a.setStatus(rs.getBoolean("status"));

                list.add(a);
            }
        } catch (SQLException e) {
        }
        return list;
    }
    public Chapter  getChapterById(int chapId){
        
        String sql = "select * from Chapter where chapId=? ";
       
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, chapId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Chapter a=new Chapter();
                a.setDescription(rs.getString("description"));
                a.setChapId(rs.getInt("chapId"));
                a.setChapName(rs.getString("chapName"));
                a.setCourse(getCourseByChapId(rs.getInt("chapId")));
                a.setLesson(getAllLessonByChapId(rs.getInt("chapId")));
                a.setQuiz(getAllQuizByChapter(rs.getInt("chapId")));
                
                return a;
            }

        } catch (SQLException e) {
        }
        return null;
    }  
    public Course getCourseByChapId(int id) {
        String sql = "select c.courseId,c.courseName,c.description,c.status,c.subjectId,c.thumbnail from Course c inner join Chapter ch on c.courseId=ch.courseId where ch.chapId="+id;
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Course a = new Course();
                a.setCourseId(rs.getInt("courseId"));
                a.setDescription(rs.getString("description"));
                a.setStatus(rs.getBoolean("status"));
                a.setCourseName(rs.getString("courseName"));
                a.setSubjectId(rs.getInt("subjectId"));
                a.setThumbnail(rs.getString("thumbnail"));
                //a.setListPack((ArrayList<PricePackage>) getPricePackageBy(a.getSubjectId()));
                return a;
            }

        } catch (SQLException e) {
        }
        return null;
    }
    public int deleteChapter(int chapId){
        
        String sql = "delete from Chapter where chapId=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, chapId);
            return st.executeUpdate();

        } catch (SQLException e) {
        }

        return 0;
    }
    public int addChapter(Course course,String chapName,String description ){
        String sql = "insert into Chapter values(?,?,?,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, getChapterid()+1);
            st.setInt(2, course.getCourseId());
            st.setString(3, chapName);
            st.setString(4, description);
            return st.executeUpdate();

        } catch (SQLException e) {
        }

        return 0;
    }
    public int updateChapter(int chapId,Course course,String chapName,String description ){
        String sql = "UPDATE [dbo].[Chapter]\n" +
"   SET [courseId] = ?,[chapName] =? ,[description] =? \n" +
" WHERE chapId=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(4, chapId);
            st.setInt(1, course.getCourseId());
            st.setString(2, chapName);
            st.setString(3, description);
            return st.executeUpdate();

        } catch (SQLException e) {
        }

        return 0;
    }
    
   public List<Chapter> getChapterByCourseId(int courseId){
       String sql = "select * from Chapter where courseId = " + courseId;
       List<Chapter> list = new ArrayList<>();
       try {
           PreparedStatement st = connection.prepareStatement(sql);
           ResultSet rs = st.executeQuery();
           while (rs.next()) {
                Chapter a=new Chapter();
                a.setDescription(rs.getString("description"));
                a.setChapId(rs.getInt("chapId"));
                a.setChapName(rs.getString("chapName"));
                a.setCourse(getCourseByChapId(rs.getInt("chapId")));
                a.setLesson(getAllLessonByChapId(rs.getInt("chapId")));
                a.setQuiz(getAllQuizByChapter(rs.getInt("chapId")));
                list.add(a);
            }
       } catch (Exception e) {
       }
        return list;
   } 
   //GET Account by aid
    public List<Chapter> getChapterByName(String Name) {
        String sql = "select * from Chapter where chapName like '%"+Name+"%'";
        List<Chapter> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Chapter a=new Chapter();
                a.setDescription(rs.getString("description"));
                a.setChapId(rs.getInt("chapId"));
                a.setChapName(rs.getString("chapName"));
                a.setCourse(getCourseByChapId(rs.getInt("chapId")));
                a.setLesson(getAllLessonByChapId(rs.getInt("chapId")));
                a.setQuiz(getAllQuizByChapter(rs.getInt("chapId")));
                list.add(a);
            }

        } catch (SQLException e) {
        }
        return list;
    }
    public List<Quiz> getAllQuizByChapter(int chapId) {
        String sql = "select * from Quiz where chapId=?";
        List<Quiz> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, chapId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Quiz a = new Quiz();
                int qId = rs.getInt("quizId");
                a.setQuizId(qId);
                a.setTitle(rs.getString("title"));
                a.setImage(rs.getString("image"));
                a.setDuration(rs.getInt("duration"));
                a.setQuestion(qd.getAllQuestionByQuiz(rs.getInt("quizId")));
                a.setStatus(status(a.getQuestion()));
                if(a.isStatus()){
                    list.add(a);
                }
                
            }

        } catch (SQLException e) {
        }
        return list;
    }
    public boolean status(List<Question> q)
    {
        if(q.size()>=10)
        {
            return true;
        }
        else
            return false;
    }
    public List<Chapter> getOneChapterByName(String Name) {
        String sql = "select * from Chapter where chapName like '"+Name+"'";
        List<Chapter> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Chapter a=new Chapter();
                a.setDescription(rs.getString("description"));
                a.setChapId(rs.getInt("chapId"));
                a.setChapName(rs.getString("chapName"));
                a.setCourse(getCourseByChapId(rs.getInt("chapId")));
                a.setLesson(getAllLessonByChapId(rs.getInt("chapId")));
                a.setQuiz(getAllQuizByChapter(rs.getInt("chapId")));
                list.add(a);
            }

        } catch (SQLException e) {
        }
        return list;
    }  
}

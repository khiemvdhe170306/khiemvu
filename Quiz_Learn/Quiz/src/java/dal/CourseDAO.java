/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dal;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import model.Account;
import model.Chapter;
import model.Course;
import model.CourseRegistration;
import model.Post;
import model.PricePackage;
import model.Rating;

/**
 *
 * @author Acer
 */
public class CourseDAO extends DBContext {

    //GET NUMBER OF Subject
    public int getCourseid() {
        String sql = "select count(*)  as count from Course";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt("count");
            }
        } catch (Exception e) {
        }
        return 0;
    }

    //GET NUMBER OF PricePackage
    public int getPackid() {
        String sql = "select count(*)  as count from PricePackage";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt("count");
            }
        } catch (Exception e) {
        }
        return 0;
    }

    //LIST ALL sub
    public List<Course> getAllCourse() {
        String sql = "select * from Course ";
        List<Course> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Course a = new Course();
                a.setCourseId(rs.getInt("courseId"));
                a.setDescription(rs.getString("description"));
                a.setStatus(rs.getBoolean("status"));
                a.setCourseName(rs.getString("courseName"));
                a.setSubjectId(rs.getInt("subjectId"));
                a.setThumbnail(rs.getString("thumbnail"));
                a.setLevel(rs.getBoolean("level"));
                a.setVideo(rs.getString("video"));
                a.setChapter(getChapterByCourseId(rs.getInt("courseId")));
                list.add(a);
            }

        } catch (SQLException e) {
        }
        return list;
    }

    //LIST ALL sub
//    public List<Subject> getTopSubject() {
//        String sql = "select top 5 * from Subject order by subjectId desc";
//        List<Subject> list = new ArrayList<>();
//        try {
//            PreparedStatement st = connection.prepareStatement(sql);
//
//            ResultSet rs = st.executeQuery();
//            while (rs.next()) {
//                Subject a = new Subject();
//                a.setCategoryId(rs.getInt("categoryId"));
//                a.setDescription(rs.getString("description"));
//                a.setStatus(rs.getBoolean("status"));
//                a.setTitle(rs.getString("title"));
//                a.setSubjectId(rs.getInt("subjectId"));
//                a.setThumbnail(rs.getString("thumbnail"));
//                a.setListPack((ArrayList<PricePackage>) getPricePackageBy(a.getSubjectId()));
//                list.add(a);
//            }
//
//        } catch (SQLException e) {
//        }
//        return list;
//    }
    //List all pricePacket
    public List<PricePackage> getAllPricePackage() {
        String sql = "select * from PricePackage ";
        List<PricePackage> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                PricePackage a = new PricePackage();
                a.setPrice(rs.getDouble("price"));
                a.setPriceId(rs.getInt("priceId"));
                a.setName(rs.getString("name"));
                a.setAcessDuration(rs.getInt("acessDuration"));
                a.setDescription(rs.getString("description"));
                list.add(a);
            }

        } catch (SQLException e) {
        }
        return list;
    }

    //list all pricepacket
    public List<PricePackage> getPricePackageBy(int subId) {
        String sql = "select priceId, acessDuration, name, price, salePrice, status from PricePackage where subjectId=? ";
        List<PricePackage> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, subId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                PricePackage a = new PricePackage();
                a.setPrice(rs.getDouble("price"));
                a.setPriceId(rs.getInt("priceId"));
                a.setName(rs.getString("name"));
                a.setAcessDuration(rs.getInt("acessDuration"));
                a.setDescription(rs.getString("description"));
                list.add(a);
            }

        } catch (SQLException e) {
        }
        return list;
    }

    public List<Course> getCourseByName(String name) {
        String sql = "select * from Course "
                + "where courseName like '%" + name + "%'";
        List<Course> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Course a = new Course();
                a.setCourseId(rs.getInt("courseId"));
                a.setDescription(rs.getString("description"));
                a.setStatus(rs.getBoolean("status"));
                a.setCourseName(rs.getString("courseName"));
                a.setSubjectId(rs.getInt("subjectId"));
                a.setThumbnail(rs.getString("thumbnail"));
                a.setLevel(rs.getBoolean("level"));
                a.setVideo(rs.getString("video"));
                a.setChapter(getChapterByCourseId(rs.getInt("courseId")));
                list.add(a);
            }

        } catch (SQLException e) {
        }
        return list;
    }

    public Course getCourseById(int id) {
        String sql = "select * from Course "
                + "where courseId = " + id;
        try {
            PreparedStatement st = connection.prepareStatement(sql);

            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Course a = new Course();
                a.setCourseId(rs.getInt("courseId"));
                a.setDescription(rs.getString("description"));
                a.setStatus(rs.getBoolean("status"));
                a.setCourseName(rs.getString("courseName"));
                a.setSubjectId(rs.getInt("subjectId"));
                a.setThumbnail(rs.getString("thumbnail"));
                a.setLevel(rs.getBoolean("level"));
                a.setVideo(rs.getString("video"));
                //a.setListPack((ArrayList<PricePackage>) getPricePackageBy(a.getSubjectId()));
                return a;
            }

        } catch (SQLException e) {
        }
        return null;
    }

    public Course deleteCourse(int courseId) {
        String sql = "delete from Course where courseId = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, courseId);
            st.executeUpdate();
        } catch (Exception e) {
        }
        return getCourseById(courseId);
    }

    public Course addCourse(int courseId, String courseName, int subjectId, int status, String thumbnail, String description, int level, String video) {
        String sql = "INSERT INTO [dbo].[Course]\n"
                + "           ([courseId]\n"
                + "           ,[courseName]\n"
                + "           ,[subjectId]\n"
                + "           ,[status]\n"
                + "           ,[thumbnail]\n"
                + "           ,[description]"
                + "           ,[level]\n"
                + "           ,[video])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, getLastCourse().getCourseId() + 1);
            st.setString(2, courseName);
            st.setInt(3, subjectId);
            st.setInt(4, status);
            st.setString(5, thumbnail);
            st.setString(6, description);
            st.setInt(7, level);
            st.setString(8, video);
            st.executeUpdate();
        } catch (Exception e) {
        }

        return getCourseById(courseId);
    }

    public Course updateCourse(int courseId, String courseName, int subjectId, String thumbnail, String description, int level, String video) {
        String sql = "UPDATE [dbo].[Course]\n"
                + "   SET [courseName] = ?\n"
                + "      ,[subjectId] = ?\n"
                + "      ,[thumbnail] = ?\n"
                + "      ,[description] = ?\n"
                + "      ,[level] = ?\n"
                + "      ,[video] = ?\n"
                + " WHERE [courseId] = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, courseName);
            st.setInt(2, subjectId);
            st.setString(3, thumbnail);
            st.setString(4, description);
            st.setInt(5, level);
            st.setString(6, video);
            st.setInt(7, courseId);
            st.executeUpdate();
        } catch (Exception e) {
        }
        return getCourseById(courseId);
    }

    public List<Chapter> getChapterByCourseId(int courseId) {
        String sql = "select * from Chapter where courseId = " + courseId;
        List<Chapter> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Chapter a = new Chapter();
                a.setChapId(rs.getInt("chapId"));
                a.setChapName(rs.getString("chapName"));
                a.setCourse(getCourseById(rs.getInt("courseId")));
                a.setDescription(rs.getString("description"));
                list.add(a);
            }
        } catch (Exception e) {
        }
        return list;
    }

    public List<Chapter> getChapterByCourseIdAndSubjectId(int subjectId, int courseId) {
        String sql = "select ch.chapId,ch.chapName,ch.courseId,ch.description from Chapter ch inner join Course c on ch.courseId=c.courseId  where subjectId = " + subjectId;
        if (courseId > 0) {
            sql += " and c.courseId=" + courseId;
        }
        List<Chapter> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Chapter a = new Chapter();
                a.setChapId(rs.getInt("chapId"));
                a.setChapName(rs.getString("chapName"));
                a.setCourse(getCourseById(rs.getInt("courseId")));
                a.setDescription(rs.getString("description"));
                list.add(a);
            }
        } catch (Exception e) {
        }
        return list;
    }

    public List<Post> getPostByCourseId(int courseId) {
        String sql = "select * from Post where courseId = " + courseId;
        List<Post> list = new ArrayList<>();
        UserDAO d = new UserDAO();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Post a = new Post();
                a.setContent(rs.getString("content"));
                a.setPostId(rs.getInt("postId"));
                a.setUser(d.getAccountByAid(rs.getInt("userId")));
                a.setBrifInfor(rs.getString("brifInfor"));
                a.setTitle(rs.getString("title"));
                int id = rs.getInt("courseid");
                a.setCourse(getCourseById(id));
                a.setThumbnail(rs.getString("thumbnail"));
                a.setPostDate(rs.getDate("date"));
                list.add(a);
            }
        } catch (Exception e) {
        }
        return list;
    }

    public Course getLastCourse() {
        String sql = "select top 1 * from Course order by courseId desc";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Course a = new Course();
                a.setCourseId(rs.getInt("courseId"));
                a.setDescription(rs.getString("description"));
                a.setStatus(rs.getBoolean("status"));
                a.setCourseName(rs.getString("courseName"));
                a.setSubjectId(rs.getInt("subjectId"));
                a.setThumbnail(rs.getString("thumbnail"));
                a.setLevel(rs.getBoolean("level"));
                a.setVideo(rs.getString("video"));
                //a.setListPack((ArrayList<PricePackage>) getPricePackageBy(a.getSubjectId()));
                return a;
            }
        } catch (Exception e) {
        }
        return null;
    }

    public List<Course> getCourseBySubjectId(int subjectId) {
        String sql = "select * from Course where subjectId = " + subjectId;
        List<Course> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Course a = new Course();
                a.setCourseId(rs.getInt("courseId"));
                a.setDescription(rs.getString("description"));
                a.setStatus(rs.getBoolean("status"));
                a.setCourseName(rs.getString("courseName"));
                a.setSubjectId(rs.getInt("subjectId"));
                a.setThumbnail(rs.getString("thumbnail"));
                a.setLevel(rs.getBoolean("level"));
                a.setVideo(rs.getString("video"));
                list.add(a);
            }
        } catch (Exception e) {
        }
        return list;
    }

    public CourseRegistration regisCourse(Date date, int courseId, int aid) {
        String sql = "INSERT INTO [dbo].[CourseRegistration]\n"
                + "           ([regis_Date]\n"
                + "           ,[courseId]\n"
                + "           ,[userId])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setDate(1, date);
            st.setInt(2, courseId);
            st.setInt(3, aid);
            st.executeUpdate();
        } catch (Exception e) {
        }
        return null;
    }

    public CourseRegistration getRegisCourseByUserId(int userId, int courseId) {
        String sql = "select * from CourseRegistration where userId = ? and courseId = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, userId);
            st.setInt(2, courseId);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                CourseRegistration a = new CourseRegistration();
                a.setCourseId(rs.getInt("courseId"));
                a.setRegisDate(rs.getDate("regis_Date"));
                a.setRegisId(rs.getInt("regisId"));
                a.setUserId(rs.getInt("userId"));
                return a;
            }
        } catch (Exception e) {
        }
        return null;
    }

    public CourseRegistration unEnroll(int userId, int courseId) {
        String sql = "DELETE FROM [dbo].[CourseRegistration]\n"
                + "      WHERE userId = ? and courseId = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(2, courseId);
            st.setInt(1, userId);
            st.executeUpdate();
        } catch (Exception e) {
        }
        return null;
    }

    public List<Course> getUserRegisCourse(int aid) {
        String sql = "select c.courseId, c.courseName, c.subjectId, c.status, c.thumbnail, c.description, c.level, c.video from Course c join CourseRegistration cr on cr.courseId = c.courseId and cr.userId = " + aid;
        List<Course> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Course a = new Course();
                a.setCourseId(rs.getInt("courseId"));
                a.setDescription(rs.getString("description"));
                a.setStatus(rs.getBoolean("status"));
                a.setCourseName(rs.getString("courseName"));
                a.setSubjectId(rs.getInt("subjectId"));
                a.setThumbnail(rs.getString("thumbnail"));
                a.setLevel(rs.getBoolean("level"));
                a.setVideo(rs.getString("video"));
                list.add(a);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return list;
    }

    public Course checkDupCourseName(String courseName) {
        String sql = "select * from Course where courseName = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setString(1, courseName);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                Course a = new Course();
                a.setCourseId(rs.getInt("courseId"));
                a.setDescription(rs.getString("description"));
                a.setStatus(rs.getBoolean("status"));
                a.setCourseName(rs.getString("courseName"));
                a.setSubjectId(rs.getInt("subjectId"));
                a.setThumbnail(rs.getString("thumbnail"));
                a.setLevel(rs.getBoolean("level"));
                a.setVideo(rs.getString("video"));
                return a;
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return null;
    }

    public int changeStatus(int courseId, int status) {
        String sql = "UPDATE [dbo].[Course]\n"
                + "   SET [status] = ?\n"
                + " WHERE [courseId] = ?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, status);
            st.setInt(2, courseId);
            return st.executeUpdate();
        } catch (Exception e) {
        }
        return 0;
    }

    public List<Rating> getAllRating(int courseId) {
        String sql = "select r.aid,r.date,r.comment,r.rating,r.rid from Ratings r where courseId=? order by r.rid desc";
        UserDAO d = new UserDAO();
        List<Rating> list = new ArrayList<>();
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, courseId);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                Rating p = new Rating();
                Account a = d.getAccountByAid(rs.getInt("aid"));
                p.setDate(rs.getDate("date"));
                p.setA(a);
                p.setComment(rs.getString("comment"));
                p.setRating(rs.getInt("rating"));
                list.add(p);
            }
        } catch (SQLException e) {
        }
        return list;
    }

    //get rating of a pid
    public int getRating(int courseId) {
        List<Rating> list = new ArrayList<>();
        list = getAllRating(courseId);
        if (list == null || list.size() <= 0) {
            return 0;
        } else {
            int rate = 0;
            for (Rating c : list) {
                rate += c.getRating();
            }
            rate = rate / list.size();
            return rate;
        }
    }

    //Add Comment to Ratings table
    public int addRating(int aid, int courseId, int rating, String comment, Date date) {
        String sql = "INSERT INTO [dbo].[Ratings]\n"
                + "           ([courseId]\n"
                + "           ,[aid]\n"
                + "           ,[rating]\n"
                + "           ,[date]\n"
                + "           ,[comment])\n"
                + "     VALUES\n"
                + "           (?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?\n"
                + "           ,?)";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, courseId);
            st.setInt(2, aid);
            st.setInt(3, rating);
            st.setDate(4, date);
            st.setString(5, comment);
            return st.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e);
        }
        return 0;

    }

    //get Ratings ID to add Comments table
    public int getRatingsId() {
        String sql = "select top 1(rid) from Ratings order by rid desc";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            ResultSet rs = st.executeQuery();
            if (rs.next()) {
                return rs.getInt("rid");
            }

        } catch (Exception e) {
        }
        return 0;
    }

    public int deleteUserVoucher(int aid, int voucherId) {
        String sql = "delete from Member_Voucher where aid=? and voucherId=?";
        try {
            PreparedStatement st = connection.prepareStatement(sql);
            st.setInt(1, aid);
            st.setInt(2, voucherId);
            return st.executeUpdate();

        } catch (SQLException e) {
        }
        return 0;
    }
}

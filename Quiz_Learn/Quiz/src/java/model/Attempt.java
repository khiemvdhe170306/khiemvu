/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Timestamp;

/**
 *
 * @author khiem
 */
public class Attempt {
    private int id;
    private Account account;
    private int quizId;
    private int Mark;
    private double grade;
    private int attempt;
    private Timestamp start;
    private Timestamp end;
    private String time;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public int getQuizId() {
        return quizId;
    }

    public void setQuizId(int quizId) {
        this.quizId = quizId;
    }

    public int getMark() {
        return Mark;
    }

    public void setMark(int Mark) {
        this.Mark = Mark;
    }

    public int getAttempt() {
        return attempt;
    }

    public void setAttempt(int attempt) {
        this.attempt = attempt;
    }

    public Timestamp getStart() {
        return start;
    }

    public void setStart(Timestamp start) {
        this.start = start;
    }

    public Timestamp getEnd() {
        return end;
    }

    public void setEnd(Timestamp end) {
        this.end = end;
    }

    
    public Attempt() {
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public double getGrade() {
        return grade;
    }

    public void setGrade(double grade) {
        this.grade = grade;
    }

    public Attempt(int id, Account account, int quizId, int Mark, int attempt, Timestamp start, Timestamp end, String time) {
        this.id = id;
        this.account = account;
        this.quizId = quizId;
        this.Mark = Mark;
        this.attempt = attempt;
        this.start = start;
        this.end = end;
        this.time = time;
    }

    @Override
    public String toString() {
        return "Attempt{" + "id=" + id + ", account=" + account + ", quizId=" + quizId + ", Mark=" + Mark + ", grade=" + grade + ", attempt=" + attempt + ", start=" + start + ", end=" + end + ", time=" + time + '}';
    }

    
    
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.List;

/**
 *
 * @author Acer
 */
public class Course {
    private int courseId;
    private String courseName;
    private int subjectId;
    private boolean status;
    private String thumbnail;
    private String description;
    private String video;
    private boolean level;
    private List<Chapter> chapter;
    private int totalNumbeOfCourseCount;

    public Course() {
    }

    public Course(int courseId, String courseName, int subjectId, boolean status, String thumbnail, String description, String video, boolean level, List<Chapter> chapter) {
        this.courseId = courseId;
        this.courseName = courseName;
        this.subjectId = subjectId;
        this.status = status;
        this.thumbnail = thumbnail;
        this.description = description;
        this.video = video;
        this.level = level;
        this.chapter = chapter;
    }

    public boolean isLevel() {
        return level;
    }

    public void setLevel(boolean level) {
        this.level = level;
    }

    

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }
    
    

    public List<Chapter> getChapter() {
        return chapter;
    }

    public void setChapter(List<Chapter> chapter) {
        this.chapter = chapter;
    }

    
    

    

    
    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    public int getSubjectId() {
        return subjectId;
    }

    public void setSubjectId(int subjectId) {
        this.subjectId = subjectId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getTotalNumbeOfCourseCount() {
        return totalNumbeOfCourseCount;
    }

    public void setTotalNumbeOfCourseCount(int totalNumbeOfCourseCount) {
        this.totalNumbeOfCourseCount = totalNumbeOfCourseCount;
    }
    

    @Override
    public String toString() {
        return "Course{" + "courseId=" + courseId + ", courseName=" + courseName + ", subjectId=" + subjectId + ", status=" + status + ", thumbnail=" + thumbnail + ", description=" + description + '}';
    }
    
    
}

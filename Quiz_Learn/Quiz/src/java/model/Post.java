    /*
     * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
     * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
     */
    package model;

    import java.sql.Date;

    /**
     *
     * @author khiem
     */
    public class Post {
        private int postId;
        private Account user;
        private String thumbnail;
        private String content;
        private String brifInfor;
        private String title;
        private Course course;
        private Date postDate;


        public Date getPostDate() {
            return postDate;
        }

        public void setPostDate(Date postDate) {
            this.postDate = postDate;
        }

        public int getPostId() {
            return postId;
        }

        public void setPostId(int postId) {
            this.postId = postId;
        }

    public Account getUser() {
        return user;
    }

    public void setUser(Account user) {
        this.user = user;
    }

        

        public String getThumbnail() {
            return thumbnail;
        }

        public void setThumbnail(String thumbnail) {
            this.thumbnail = thumbnail;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public String getBrifInfor() {
            return brifInfor;
        }

        public void setBrifInfor(String brifInfor) {
            this.brifInfor = brifInfor;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public Post(int postId, String thumbnail, String content, String brifInfor, String title, Date postDate) {
            this.postId = postId;

            this.thumbnail = thumbnail;
            this.content = content;
            this.brifInfor = brifInfor;
            this.title = title;
            this.postDate = postDate;
        }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

      






        public Post() {
        }


    }

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author khiem
 */
public class PricePackage {
    private int priceId;
    private String name;
    private int acessDuration;
    private double price;
    private String description;

    public PricePackage() {
    }

    public PricePackage(int priceId, String name, int acessDuration, double price, String description) {
        this.priceId = priceId;
        this.name = name;
        this.acessDuration = acessDuration;
        this.price = price;
        this.description = description;
    }

    

   

    

    public int getPriceId() {
        return priceId;
    }

    public void setPriceId(int priceId) {
        this.priceId = priceId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAcessDuration() {
        return acessDuration;
    }

    public void setAcessDuration(int acessDuration) {
        this.acessDuration = acessDuration;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "PricePackage{" + "priceId=" + priceId + ", name=" + name + ", acessDuration=" + acessDuration + ", price=" + price + ", description=" + description + '}';
    }

    

    
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author admin
 */
public class Slider {
    
    private int sliderId;
    private boolean status;
    private String title;
    private String content;
    private String notes;
    private int courseId;
    private String image;
    private String course_title;
    private String description;

    public int getSliderId() {
        return sliderId;
    }

    public void setSliderId(int sliderId) {
        this.sliderId = sliderId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }



    public Slider() {
    }
    


    public Slider(int sliderId, boolean status, String title, String content, String notes, int courseId, String image) {
        this.sliderId = sliderId;
        this.status = status;
        this.title = title;
        this.content = content;
        this.notes = notes;
        this.courseId = courseId;
        this.image = image;
    }
    // contrutor for add 
    public Slider( boolean status, String title, String content, String notes, int courseId, String image) {
        this.status = status;
        this.title = title;
        this.content = content;
        this.notes = notes;
        this.courseId = courseId;
        this.image = image;
    }

    @Override
    public String toString() {
        return "Slider{" + "sliderId=" + sliderId + ", status=" + status + ", title=" + title + ", content=" + ", notes=" +   ", courseId=" + courseId + ", image="  + ", subject_title=" + course_title + ", description=" + description + '}';
    }

   
    
    
    // for subject details

    public String getCourse_title() {
        return course_title;
    }

    public void setCourse_title(String course_title) {
        this.course_title = course_title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Slider(int sliderId, boolean status, String title, String content, String notes, int courseId, String image, String course_title, String description) {
        this.sliderId = sliderId;
        this.status = status;
        this.title = title;
        this.content = content;
        this.notes = notes;
        this.courseId = courseId;
        this.image = image;
        this.course_title = course_title;
        this.description = description;
    }
    
    
}

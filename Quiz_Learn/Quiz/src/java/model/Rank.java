/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Timestamp;
import java.util.List;

/**
 *
 * @author Admin
 */
public class Rank{
     private int rankId;
     private int averageScore;
     private int highetScore;
     private Account user;
     private List<Attempt> attempt;
     private Course course;
     private Level level;
     
    public Rank() {
    }

    public int getRankId() {
        return rankId;
    }

    public void setRankId(int rankId) {
        this.rankId = rankId;
    }


    public int getAverageScore() {
        return averageScore;
    }

    public void setAverageScore(int averageScore) {
        this.averageScore = averageScore;
    }

    public Account getUser() {
        return user;
    }

    public void setUser(Account user) {
        this.user = user;
    }

    public int getHighetScore() {
        return highetScore;
    }

    public void setHighetScore(int highetScore) {
        this.highetScore = highetScore;
    }

    public List<Attempt> getAttempt() {
        return attempt;
    }

    public void setAttempt(List<Attempt> attempt) {
        this.attempt = attempt;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    



    
     
}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author arans
 */
public class Level {
    private int requireXP;
    private int currentLevel;
    private int currentXP;
    private String rank;
    private int NumberOfQuizDone;

    public Level() {
    }

    public Level(int requireXP, int currentLevel, int currentXP, String rank) {
        this.requireXP = requireXP;
        this.currentLevel = currentLevel;
        this.currentXP = currentXP;
        this.rank = rank;
    }

    public int getRequireXP() {
        return requireXP;
    }

    public void setRequireXP(int requireXP) {
        this.requireXP = requireXP;
    }

    public int getCurrentLevel() {
        return currentLevel;
    }

    public void setCurrentLevel(int currentLevel) {
        this.currentLevel = currentLevel;
    }

    public int getCurrentXP() {
        return currentXP;
    }

    public void setCurrentXP(int currentXP) {
        this.currentXP = currentXP;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public int getNumberOfQuizDone() {
        return NumberOfQuizDone;
    }

    public void setNumberOfQuizDone(int NumberOfQuizDone) {
        this.NumberOfQuizDone = NumberOfQuizDone;
    }
    
    
    
    
}

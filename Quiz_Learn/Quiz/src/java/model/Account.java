/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Blob;
import java.util.List;

/**
 *
 * @author khiem
 */
public class Account {
    private int aid;
    private String email;
    private String password;
    private int role;
    private boolean status;
    private double money;
    private AccountInfo accountinfo;
    private Role roleName;
    private String image;
    private String url;
    private List<Registration> member;
    private List<Voucher> voucher;
    private List<Course> course;
    private boolean vip;
    private Level level;
    public Account() {
    }
    
    public Account(int aid, String email, String password, int role, boolean status) {
        this.aid = aid;
        this.email = email;
        this.password = password;
        this.role = role;
        this.status = status;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
    
    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    
    
    public AccountInfo getAccountinfo() {
        return accountinfo;
    }

    public void setAccountinfo(AccountInfo accountinfo) {
        this.accountinfo = accountinfo;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }
    
    
    
    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    

    

    public int getAid() {
        return aid;
    }

    public void setAid(int aid) {
        this.aid = aid;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRoleName() {
        return roleName;
    }

    public void setRoleName(Role roleName) {
        this.roleName = roleName;
    }

    public List<Registration> getMember() {
        return member;
    }

    public void setMember(List<Registration> member) {
        this.member = member;
    }

    public List<Voucher> getVoucher() {
        return voucher;
    }

    public void setVoucher(List<Voucher> voucher) {
        this.voucher = voucher;
    }

    public List<Course> getCourse() {
        return course;
    }

    public void setCourse(List<Course> course) {
        this.course = course;
    }

    public boolean isVip() {
        return vip;
    }

    public void setVip(boolean vip) {
        this.vip = vip;
    }

    public Level getLevel() {
        return level;
    }

    public void setLevel(Level level) {
        this.level = level;
    }

    
    
    
}

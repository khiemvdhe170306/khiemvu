/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;


import java.util.List;

/**
 *
 * @author khiem
 */
public class Question {
    private int questionId;
    private List<Answer> listAnswer;
    private List<Answer> choice;
    private  List<Answer> chosen;
    private String qContent;
    private boolean qValue;
    private String qLevel;
    private Quiz quiz;
    private int numberOfTrue;
    private int qExplainId;
    private String qExplainContent;
    private boolean status;
    public int getQuestionId() {
        return questionId;
    }

    public Question(int questionId, String qContent, String qLevel, Quiz quiz) {
        this.questionId = questionId;
        this.qContent = qContent;
        this.qLevel = qLevel;
        this.quiz = quiz;
    }

    public int getNumberOfTrue() {
        return numberOfTrue;
    }

    public void setNumberOfTrue(int numberOfTrue) {
        this.numberOfTrue = numberOfTrue;
    }

   
    
    public void setQuestionId(int questionId) {
        this.questionId = questionId;
    }

    public List<Answer> getListAnswer() {
        return listAnswer;
    }

    public void setListAnswer(List<Answer> listAnswer) {
        this.listAnswer = listAnswer;
    }

    public Quiz getQuiz() {
        return quiz;
    }

    public void setQuiz(Quiz quiz) {
        this.quiz = quiz;
    }

    

    

    public Question() {
    }

    public List<Answer> getChoice() {
        return choice;
    }

    public void setChoice(List<Answer> choice) {
        this.choice = choice;
    }

    public List<Answer> getChosen() {
        return chosen;
    }

    public void setChosen(List<Answer> chosen) {
        this.chosen = chosen;
    }

    

    

    public boolean isqValue() {
        return qValue;
    }

    public void setqValue(boolean qValue) {
        this.qValue = qValue;
    }

    public String getqContent() {
        return qContent;
    }

    public void setqContent(String qContent) {
        this.qContent = qContent;
    }

    public String getqLevel() {
        return qLevel;
    }

    public void setqLevel(String qLevel) {
        this.qLevel = qLevel;
    }


    public String getqExplainContent() {
        return qExplainContent;
    }

    public void setqExplainContent(String qExplainContent) {
        this.qExplainContent = qExplainContent;
    }

    public int getqExplainId() {
        return qExplainId;
    }

    public void setqExplainId(int qExplainId) {
        this.qExplainId = qExplainId;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
    

   
    
    
    
}

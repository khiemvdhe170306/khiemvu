/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Date;

/**
 *
 * @author Acer
 */
public class Registration {
//    [regisId] [int] primary key,
//	[regis_Date] [date] NULL ,
//	[due_Date] date ,
//	[subId] [int] references [Subject](subjectId),
//	[priceId] [int] references PricePackage(priceId),
//	[userId] [int] references Account(aid))
    
    private int regisId;
    private Date regis_date;
    private Date due_Date;
    private PricePackage pricePackage;
    private int userId;
    private Voucher voucher;
    private double price;

    public Registration() {
    }

    public PricePackage getPricePackage() {
        return pricePackage;
    }

    public void setPricePackage(PricePackage pricePackage) {
        this.pricePackage = pricePackage;
    }

    public Registration(int regisId, Date regis_date, Date due_Date,  PricePackage pricePackage, int userId) {
        this.regisId = regisId;
        this.regis_date = regis_date;
        this.due_Date = due_Date;
        this.pricePackage = pricePackage;
        this.userId = userId;
    }

   

    public int getRegisId() {
        return regisId;
    }

    public void setRegisId(int regisId) {
        this.regisId = regisId;
    }

    public Date getRegis_date() {
        return regis_date;
    }

    public void setRegis_date(Date regis_date) {
        this.regis_date = regis_date;
    }

    public Date getDue_Date() {
        return due_Date;
    }

    public void setDue_Date(Date due_Date) {
        this.due_Date = due_Date;
    }
    

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public Voucher getVoucher() {
        return voucher;
    }

    public void setVoucher(Voucher voucher) {
        this.voucher = voucher;
    }

    

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
    
    
}

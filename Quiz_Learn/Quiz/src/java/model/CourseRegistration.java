/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Date;

/**
 *
 * @author Acer
 */
public class CourseRegistration {
    private int regisId;
    private Date regisDate;
    private int courseId;
    private int userId;
    private int TotalCourseCount;

    public CourseRegistration() {
    }

    public CourseRegistration(int regisId, Date regisDate, int courseId, int userId) {
        this.regisId = regisId;
        this.regisDate = regisDate;
        this.courseId = courseId;
        this.userId = userId;
    }

    public int getCourseId() {
        return courseId;
    }

    public void setCourseId(int courseId) {
        this.courseId = courseId;
    }

    

    public int getRegisId() {
        return regisId;
    }

    public void setRegisId(int regisId) {
        this.regisId = regisId;
    }

    public Date getRegisDate() {
        return regisDate;
    }

    public void setRegisDate(Date regisDate) {
        this.regisDate = regisDate;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getTotalCourseCount() {
        return TotalCourseCount;
    }

    public void setTotalCourseCount(int TotalCourseCount) {
        this.TotalCourseCount = TotalCourseCount;
    }
    
    
}

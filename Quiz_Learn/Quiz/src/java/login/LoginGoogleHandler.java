/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package login;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.Account;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;

/**
 *
 * @author khiem
 */
@WebServlet(name = "LoginGoogleHandler", urlPatterns = {"/LoginGoogleHandler"})
public class LoginGoogleHandler extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        UserDAO u=new UserDAO();
        PrintWriter out = response.getWriter();
        String code_raw = request.getParameter("code");
        String accessToken = getToken(code_raw);
        UserGoogleDto user = getUserInfo(accessToken);
        Account a = u.getAccountbyEmail(user.getEmail());
        HttpSession session = request.getSession(false);

        if (a == null) {
            String pass=u.randomPassword();
            session.setAttribute("userName", user.getEmail());
            session.setAttribute("ImageUrl", user.getPicture());
            String code=u.randomCode();
            mailSend.Send(user.getEmail(), "Your default password is"+ pass+" ."
                    + "/nEnter code to kickstart account: "+code);
            
            session.setAttribute("code", code);
            request.getRequestDispatcher("check.jsp").forward(request, response);
        } else {
            if (a.isStatus()) {
                request.setAttribute("ms", "Login Succesfull");
            } else {
                request.setAttribute("ms", "Account is Suspended!!!");
                request.getRequestDispatcher("login.jsp").forward(request, response);
            }

        }
        session.setAttribute("account", a);
        request.setAttribute("status", "success");
        request.getRequestDispatcher("home").forward(request, response);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    public static String getToken(String code) throws ClientProtocolException, IOException {
        // call api to get token
        String response = Request.Post(Constants.GOOGLE_LINK_GET_TOKEN)
                .bodyForm(Form.form().add("client_id", Constants.GOOGLE_CLIENT_ID)
                        .add("client_secret", Constants.GOOGLE_CLIENT_SECRET)
                        .add("redirect_uri", Constants.GOOGLE_REDIRECT_URI).add("code", code)
                        .add("grant_type", Constants.GOOGLE_GRANT_TYPE).build())
                .execute().returnContent().asString();

        JsonObject jobj = new Gson().fromJson(response, JsonObject.class);
        String accessToken = jobj.get("access_token").toString().replaceAll("\"", "");
        return accessToken;
    }

    public static UserGoogleDto getUserInfo(final String accessToken) throws ClientProtocolException, IOException {
        String link = Constants.GOOGLE_LINK_GET_USER_INFO + accessToken;
        String response = Request.Get(link).execute().returnContent().asString();

        UserGoogleDto googlePojo = new Gson().fromJson(response, UserGoogleDto.class);

        return googlePojo;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the +
    // sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        String code_raw = request.getParameter("code");
        HttpSession session = request.getSession();
        String code = (String) session.getAttribute("code");
        UserDAO d = new UserDAO();
        
        if (code.equalsIgnoreCase(code_raw)) {
            String pass = (String) session.getAttribute("pass");
            String acc = (String) session.getAttribute("userName");
            String url="https://cmshn.fpt.edu.vn/pluginfile.php/267785/user/icon/trema/f2?rev=2091306";
            if( session.getAttribute("ImageUrl")!=null){
                 url=   (String) session.getAttribute("ImageUrl");}
           
            session.setAttribute("account",d.signUp(acc, "111",url) );
            session.removeAttribute("pass");
            session.removeAttribute("userName");
            request.setAttribute("status", "sinup");
            request.getRequestDispatcher("home").forward(request, response);

        } else {
            request.setAttribute("status", "code is not match");
            request.getRequestDispatcher("check.jsp").forward(request, response);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

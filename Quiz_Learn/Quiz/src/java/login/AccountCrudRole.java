/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package login;

import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Account;
import model.Role;

/**
 *
 * @author khiem
 */
@WebServlet(name = "AccountCrudRole", urlPatterns = {"/crudrole"})
public class AccountCrudRole extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AccountCrudRole</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AccountCrudRole at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        UserDAO u = new UserDAO();

        String action = request.getParameter("action");
        if (action.equalsIgnoreCase("update")) {//update
            int roleid = Integer.parseInt(request.getParameter("roleid"));
            Role a = u.getAccountRoleById(roleid);
            request.setAttribute("role", a);
            request.setAttribute("action", "update");
            request.getRequestDispatcher("accountRoleCrud.jsp").forward(request, response);
        } else if (action.equalsIgnoreCase("add")) {
            request.setAttribute("action", "add");
            request.getRequestDispatcher("accountRoleCrud.jsp").forward(request, response);
        } else {//delete
            int roleid = Integer.parseInt(request.getParameter("roleid"));
            if(u.deleteAccountRole(roleid)==0)
            {
                request.setAttribute("status", "Unable to delete there is account with this role");
            }
            request.getRequestDispatcher("rolemanagement").forward(request, response);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        UserDAO u = new UserDAO();
        String action = request.getParameter("action");
        String role = request.getParameter("name").trim();
        String description = request.getParameter("description");
        String authority = "";
        String quiz = request.getParameter("quiz");
        if (quiz != null) {
            authority += quiz;
            authority += ",";
        }
        String course = request.getParameter("course");
        if (course != null) {
            authority += course;
            authority += ",";
        }
        String account = request.getParameter("account");
        if (account != null) {
            authority += account;
            authority += ",";
        }
        String blog = request.getParameter("blog");
        if (blog != null) {
            authority += blog;
            authority += ",";
        }
        String subject = request.getParameter("subject");
        if (subject != null) {
            authority += subject;
            authority += ",";
        }
        String slider = request.getParameter("slider");
        if (slider != null) {
            authority += slider;
            authority += ",";
        }
        if (action.equalsIgnoreCase("update")) {//update
            int roleid = Integer.parseInt(request.getParameter("roleid"));
            

                u.updateAccountRole(roleid, role, description, authority);
                request.getRequestDispatcher("rolemanagement").forward(request, response);
            
        } else if (action.equalsIgnoreCase("add")) {
            if (!u.getAccountRoleByName(role)) {
                request.setAttribute("action", "add");
                request.setAttribute("name",role );
                request.setAttribute("description", description);
                request.setAttribute("authority", authority);
                request.setAttribute("ms", "Role Name duplicate");
                request.getRequestDispatcher("accountRoleCrud.jsp").forward(request, response);

            } else {
                u.addAccountRole(role, description, authority);
                request.getRequestDispatcher("rolemanagement").forward(request, response);
            }
        }

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

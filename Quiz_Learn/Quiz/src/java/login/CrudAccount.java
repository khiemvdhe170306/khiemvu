/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package login;

import com.oracle.wls.shaded.org.apache.bcel.generic.AALOAD;
import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Account;

/**
 *
 * @author khiem
 */
@WebServlet(name="CrudAccount", urlPatterns={"/crudaccount"})
public class CrudAccount extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CrudAccount</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CrudAccount at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        UserDAO u =new UserDAO();
        int aid=Integer.parseInt(request.getParameter("aid"));
        String action=request.getParameter("action");
        out.println(aid+" "+action);
       request.setAttribute("role", u.getAllAccountRole());
        if(action.equalsIgnoreCase("update")){//update
            Account a=u.getAccountByAid(aid);
            request.setAttribute("account", a);
            request.setAttribute("action", "update");
            request.getRequestDispatcher("crudAccount.jsp").forward(request, response);
        }
        else if(action.equalsIgnoreCase("add")){
            
            request.setAttribute("action", "add");
            request.getRequestDispatcher("crudAccount.jsp").forward(request, response);
        }
        else{//change status
            u.changeStatus(aid);
            Account a=u.getAccountByAid(aid);
            String web="accountmanagement";
            String r=request.getParameter("page");
            if(r!=null)web+="?page="+r;
            if(!a.isStatus()){
            mailSend.SendWarning(a.getEmail(), "Your account has been Blocked by QuiKK leanrning website");}
            request.getRequestDispatcher(web).forward(request, response);
        }
        
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        UserDAO u=new UserDAO();
        String action=request.getParameter("action");
        String fullname=request.getParameter("fullname");
        String phone=request.getParameter("phone");
        String age_raw=request.getParameter("age");
        String aid_raw=request.getParameter("aid");
        String address=request.getParameter("address");
        String gender_raw=request.getParameter("gender");
        int age=0,aid=0;boolean gender=false;
        String email=request.getParameter("email");
        String password=request.getParameter("password");
        //parse
        if(age_raw!=null) age=Integer.parseInt(age_raw);
        if(aid_raw!=null) aid=Integer.parseInt(aid_raw);
        if(gender_raw!=null&&gender_raw.equalsIgnoreCase("1")) gender=true;
        request.setAttribute("role", u.getAllAccountRole());
        
//DAO 
        //add ai
        if(action.equalsIgnoreCase("add")){
            if(u.getAccountbyEmail(email)!=null){
                request.setAttribute("password", password);
                request.setAttribute("email", email);
                request.setAttribute("action", "add");
                request.setAttribute("status", "email duplicate in database");
                request.getRequestDispatcher("crudAccount.jsp").forward(request, response);
            }else{
            out.println(u.signUp(email.trim(), password,"https://cmshn.fpt.edu.vn/pluginfile.php/267785/user/icon/trema/f2?rev=2091306").getAid());
            request.setAttribute("status", "Add account"+ email +"successfully");
            }
            
            
        //change ai 
        }else if(action.equalsIgnoreCase("update")){
            if(u.getAccountInfoByAid(aid)==null){
               
                out.println(u.addAccountInfo(aid, fullname.trim(), phone, address.trim(), age, gender).getAccountinfo().getAge());
            }else{
            out.println(u.updateAccountInfo(aid, fullname.trim(), phone, address.trim(), age, gender).getAccountinfo().getAddress());           
            }  
            request.setAttribute("ms", "Update account"+ u.getAccountByAid(aid).getEmail() +"successfully");
        //change password 
        }else if(action.equalsIgnoreCase("changepassword")){
            out.println(email+" "+password);
            out.println(u.updateAccountPasswordByEmail(email,password).getPassword());
            request.setAttribute("ms", "Update account"+ email +"successfully");
        //change role
        }else if(action.equalsIgnoreCase("changeRole")){
            String role_raw=request.getParameter("role");
            int role=1;
            if(role_raw!=null) role=Integer.parseInt(role_raw);
            out.println(u.setAccountRole(aid, role).getRole());
            if(role>1)
            {
                u.deleteVoucherUserByUser(aid);
            }
            request.setAttribute("ms", "Update account "+ u.getAccountByAid(aid).getEmail() +" successfully");
        }
        
        request.getRequestDispatcher("accountmanagement").forward(request, response);
            
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package login;

import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.Account;

/**
 *
 * @author khiem
 */
@WebServlet(name = "CrudUser", urlPatterns = {"/cruduser"})
public class CrudUser extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CrudUser</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CrudUser at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        UserDAO u = new UserDAO();
        String action = request.getParameter("action");
        request.setAttribute("action", action);
        request.getRequestDispatcher("crudUser.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        UserDAO u = new UserDAO();
        String action = request.getParameter("action");
        if (action.equalsIgnoreCase("update")) {
            String fullname = request.getParameter("fullname");
            String phone = request.getParameter("phone");
            String age_raw = request.getParameter("age");
            String aid_raw = request.getParameter("aid");
            String address = request.getParameter("address");
            String gender_raw = request.getParameter("gender");
            int age = 0, aid = 0;
            boolean gender = false;
            if (age_raw != null) {
                age = Integer.parseInt(age_raw);
            }
            if (aid_raw != null) {
                aid = Integer.parseInt(aid_raw);
            }
            if (gender_raw != null && gender_raw.equalsIgnoreCase("1")) {
                gender = true;
            }
            Account a = new Account();
            if (u.getAccountInfoByAid(aid) == null) {

                a = u.addAccountInfo(aid, fullname, phone, address, age, gender);
            } else {
                a = u.updateAccountInfo(aid, fullname, phone, address, age, gender);
            }
            HttpSession session = request.getSession(false);
            session.setAttribute("account", a);
        } else if (action.equalsIgnoreCase("updatePassword")) {
            String email = request.getParameter("email");
            String oldpassword = request.getParameter("oldpassword");
            String password = request.getParameter("password");
            String newpassword = request.getParameter("newpassword");
            if(newpassword.equalsIgnoreCase(oldpassword)){
            Account a = u.updateAccountPasswordByEmail(email, password);
            HttpSession session = request.getSession(false);
            session.setAttribute("account", a);}
            else{
                request.setAttribute("action", "updatePassword");
                request.setAttribute("ms", "not match password");
                request.getRequestDispatcher("crudUser.jsp").forward(request, response);
            }
        }
        request.setAttribute("action", "view");
        request.getRequestDispatcher("crudUser.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

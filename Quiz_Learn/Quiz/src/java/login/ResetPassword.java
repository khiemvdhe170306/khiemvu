/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package login;

import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import model.Account;

/**
 *
 * @author khiem
 */
@WebServlet(name="ResetPassword", urlPatterns={"/resetpassword"})
public class ResetPassword extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ResetPassword</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ResetPassword at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
         PrintWriter out = response.getWriter(); 
         UserDAO u=new UserDAO();
         String email=request.getParameter("email");
          Account a=u.getAccountbyEmail(email);
          if(a==null){
              request.setAttribute("ms", email+" is not registered!!! Check your email");
              out.println("null "+email);
              request.getRequestDispatcher("forgetpassword").forward(request, response);
          }else if(!u.getAccountbyEmail(email).isStatus())  
          {
              request.setAttribute("ms", email+" is blocked!!! Enter another gmail");
              out.println("null "+email);
              request.getRequestDispatcher("forgetpassword").forward(request, response);
          }
          String code=u.randomCode();
          HttpSession session = request.getSession(false);
          session.setAttribute("codeMail",code);
          session.setAttribute("userName",email );
          session.setAttribute("resetpass",1 );
          mailSend.Send(request.getParameter("email"),"Enter code to reset password: "+code); 
          request.getRequestDispatcher("resetPassword.jsp").forward(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        String code=request.getParameter("code");
        String pass=request.getParameter("password");
        HttpSession session = request.getSession(false);
        
        String code_raw=(String) session.getAttribute("codeMail");
        String email=(String) session.getAttribute("userName");
        UserDAO u=new UserDAO();
        out.println(email+" "+pass);
        if(code.equalsIgnoreCase(code_raw)){
            out.println(u.updateAccountPasswordByEmail(email,pass).toString());
            //set email after update
            Account a=u.getAccountbyEmail(email);
            //remove session not used
            session.removeAttribute("resetpass");session.removeAttribute("userName");session.removeAttribute("codeMail");
            //set login account
            session.setAttribute("account", a);
            request.getRequestDispatcher("home").forward(request, response);
        }else{
            request.setAttribute("status", "Code does not match!!!Enter again");
            request.getRequestDispatcher("resetPassword.jsp").forward(request, response);
        }
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package login;

import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.*;
import model.Account;

/**
 *
 * @author khiem
 */
@WebServlet(name = "AccountManagement", urlPatterns = {"/accountmanagement"})
public class AccountManagement extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AccountManagement</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AccountManagement at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        String search=request.getParameter("search");
        String page_raw = request.getParameter("page");
        String sort_raw = request.getParameter("sort");
        String vip_raw=request.getParameter("vip");
        UserDAO u = new UserDAO();
        List<Account> account = u.getAllAccount();
        String filter_raw=request.getParameter("filter");
        int page = 1;
        if (page_raw != null) {
            page = Integer.parseInt(page_raw);
        }
        int vip=0;
        if (vip_raw != null) {
            vip = Integer.parseInt(vip_raw);
            account=u.sortVipAccount(account, vip);
        }
        if (sort_raw != null&&!sort_raw.equalsIgnoreCase("")) {
            int sort;
            sort = Integer.parseInt(sort_raw);
            account = u.bubbleSortAccount(account, account.size(), sort);
        }
        if(search!=null)
            account=u.getAccountByName(search);
        request.setAttribute("role", u.getAllAccountRole());
        request.setAttribute("account", account);
        request.setAttribute("page", page);
        request.getRequestDispatcher("accountManagement.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package quiz;

import dal.AnswerDAO;
import dal.AttemptDAO;
import dal.QuestionDAO;
import dal.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import login.mailSend;
import model.Account;
import model.Answer;
import model.Answer_Quiz;
import model.Course;
import model.Question;
import model.Quiz;

/**
 *
 * @author khiem
 */
@WebServlet(name = "QuizReview", urlPatterns = {"/quizreview"})
public class QuizReview extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet QuizReview</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet QuizReview at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        AnswerDAO ad = new AnswerDAO();
        QuestionDAO qd = new QuestionDAO();
        AttemptDAO atd = new AttemptDAO();
        UserDAO ud=new UserDAO();
        HttpSession session = request.getSession();
        Quiz quiz = (Quiz) session.getAttribute("quiz");
        Account account=(Account) session.getAttribute("account");
        Course c=ud.getCourseByquiz(quiz.getQuizId());
        boolean check=ud.checkComplete(ud.getAllQuizByCourse(c.getCourseId()), account.getAid());
        
        PrintWriter out = response.getWriter();
        String number_raw = request.getParameter("lengthAnswer");
        int number = 2;
        if (number_raw != null) {
            number = Integer.parseInt(number_raw);
        }
        int HistoryId = atd.getHistoryId() + 1;
        //get list
        List<Question> quizList=new ArrayList<>();
        for(int i=1;i<=10;i++)
        {
            int quest=0;
            String quest_raw=request.getParameter("question"+i);
            out.println("add"+quest_raw);
            if(quest_raw!=null)
            {
                quest=Integer.parseInt(quest_raw);
                quizList.add(qd.getQuestionById(quest));
            }
        }
        //List<Question> quizList=(List<Question>) session.getAttribute("list");
        for (Question q : quizList ) {
            int count=0;
            
            for(Answer a:q.getListAnswer())
            {
                String answer = request.getParameter("Answer" + a.getAid());

                if (answer != null) {
                out.println("ans: "+a.getAid());
                ad.addChosenAnswer3(a.getAid(),q.getQuestionId());
                count++;
                }
            }
            out.println(q.getQuestionId()+" count: "+count);
            if(count==0) ad.addChosenAnswer3(0,q.getQuestionId());
            
        }
//        for (int i = 1; i < number; i++) {
//            String answer = request.getParameter("Answer" + i);
//
//            if (answer != null) {
//                int ans = Integer.parseInt(answer);
//                //out.println("ans: "+ans);
//                ad.addChosenAnswer2(ans);
//            }
//        }

        //get from Answer_Quiz to add into quiz history detail
        ad.getChosen(quizList);
        int result = 0;
        for (Question q : quizList) {
            for (Answer a : q.getListAnswer()) {
                if (ad.getChoice(q.getChosen(), a)) {
                    a.setChoice(true);
                }
            }
            if (ad.compareAnswer( q.getChosen(),q.getChoice())) {
                q.setqValue(true);
                result++;
            } else {
                q.setqValue(false);
            }
        }

        Timestamp a = (Timestamp) session.getAttribute("time");
        Timestamp b = ad.getDate();
        Long time = b.getTime() - a.getTime();
        double grade = (double) result / 10 * 10;
        int attempt = atd.getAttempt(account.getAid(), quiz.getQuizId())+1;
        //add into quiz history detail
        out.println("add Quiz Detail:"+atd.addQuizDetail(HistoryId, account.getAid(), quiz.getQuizId(), grade, attempt, result, a, b, time));
        //delete from answer_quiz and add into quiz history
        out.println("add quiz delete"+atd.deleteFromAnswer_Quiz(HistoryId));
        boolean check2=ud.checkComplete(ud.getAllQuizByCourse(c.getCourseId()), account.getAid());
        if(!check&&check2)
        {
            mailSend.Send(account.getEmail(), "your account has been complete course and receive voucher");
            ud.addVoucherUser(account.getAid(), 2);
            request.setAttribute("message", "Congratulation!!! you have completed this Course and receive new Voucher 50%");
        }
        
        String time_taken=""+time/1000+"s";
        request.setAttribute("start", a);
        request.setAttribute("end", b);
        request.setAttribute("time_taken", time_taken);
        request.setAttribute("marks", result);
        request.setAttribute("question", quizList);
        request.setAttribute("grade", grade);
        
        request.getRequestDispatcher("QuizReview.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

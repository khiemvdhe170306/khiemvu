/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package quiz;

import dal.AnswerDAO;
import dal.AttemptDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.List;
import model.Answer;
import model.Attempt;
import model.Question;

/**
 *
 * @author khiem
 */
@WebServlet(name="HistoryReview", urlPatterns={"/historyreview"})
public class HistoryReview extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet HistoryReview</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet HistoryReview at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        AttemptDAO atd=new AttemptDAO();
        AnswerDAO ad=new AnswerDAO();
        PrintWriter out = response.getWriter();
        String history_raw=request.getParameter("historyId");
        int historyId=0;
       
        try {
            historyId=Integer.parseInt(history_raw);
        } catch (Exception e) {
        }
        List<Question> qList=atd.getListOfQuestionByHistoryId(historyId);
        ad.getHistoryChosen(qList, historyId);
        
        
        for (Question q : qList) {
            for (Answer a : q.getListAnswer()) {
                if (ad.getChoice(q.getChosen(), a)) {
                    a.setChoice(true);
                }
            }
            if (ad.compareAnswer( q.getChosen(),q.getChoice())) {
                q.setqValue(true);
            } else {
                q.setqValue(false);
            }
        }
        
        Attempt attempt=atd.getListOfAttemptByHistoryId(historyId);
        
        request.setAttribute("start",attempt.getStart() );
        request.setAttribute("end", attempt.getEnd());
        request.setAttribute("time_taken", attempt.getTime());
        request.setAttribute("marks", attempt.getMark());
        request.setAttribute("question", qList);
        request.setAttribute("grade",attempt.getGrade());
        
        request.getRequestDispatcher("QuizReview.jsp").forward(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

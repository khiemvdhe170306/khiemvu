/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package quiz;

import dal.ChapterDAO;
import dal.QuizDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.Account;
import model.Chapter;
import model.Quiz;

/**
 *
 * @author Admin
 */
@WebServlet(name="CrudQuizServlet", urlPatterns={"/crudquiz"})
public class CrudQuizServlet extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CrudQuizServlet</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CrudQuizServlet at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        HttpSession session = request.getSession();
        String action = request.getParameter("action");
        QuizDAO qd = new QuizDAO();
        ChapterDAO chd = new ChapterDAO();
        List<Chapter> chapterlist = chd.getAllChapter();
        String msg="";
        if(action.equalsIgnoreCase("add")){
            request.setAttribute("action", action);
            request.setAttribute("chapterlist", chapterlist);
            request.getRequestDispatcher("crudQuiz.jsp").forward(request, response);
        } else if(action.equalsIgnoreCase("update")) {
            int quizId = Integer.parseInt(request.getParameter("quizId"));
            Quiz q = qd.getQuizById(quizId);
            request.setAttribute("action", action);
            request.setAttribute("chapterlist", chapterlist);
            request.setAttribute("quiz", q);
            request.getRequestDispatcher("crudQuiz.jsp").forward(request, response);
        } else {//delete
            int quizId = Integer.parseInt(request.getParameter("quizId"));
            
            int deleteStatus = qd.deleteQuiz(quizId);
            if(deleteStatus==1){
                msg = "Delete successfull";
            }else{
                msg = "Delete fail";
            }
            request.setAttribute("msg",msg);
            request.getRequestDispatcher("quizmanagement").forward(request, response);
        }
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        QuizDAO qd = new QuizDAO();
        String action = request.getParameter("action");
        HttpSession session = request.getSession(false);
        String page_raw = request.getParameter("page");
        QuizDAO q = new QuizDAO();
        List<Quiz> quizlist = q.getAllQuiz();
        int page = 1;
        if(page_raw!=null){
            page = Integer.parseInt(page_raw);
        }
        PrintWriter out = response.getWriter();
        String title = request.getParameter("title");
        String image = request.getParameter("image");
        String duration = request.getParameter("duration");     
        String chapId = request.getParameter("chapId");
        String numberOfQuestion = request.getParameter("numberOfQuestion");
        int aid=1;
        Account a=(Account) session.getAttribute("account");
        if(a!=null)
        {
            aid=a.getAid();
        }
        if(action.equalsIgnoreCase("add")){
            qd.addQuiz(Integer.parseInt(chapId), title,image, Integer.parseInt(duration),Integer.parseInt(numberOfQuestion),aid);
            request.setAttribute("quizlist", quizlist);
            request.setAttribute("page", page);
            out.println(chapId);
            out.println(title);
            out.println(numberOfQuestion);
            out.println(image);
            out.println(duration);
            request.getRequestDispatcher("quizmanagement").forward(request, response);
        } else if(action.equalsIgnoreCase("update")) {
            int quizId = Integer.parseInt(request.getParameter("quizId"));
            Quiz qu = qd.getQuizById(quizId);
            request.setAttribute("action", action);
            qu = qd.updateQuiz(quizId, title, Integer.parseInt(chapId),image, Integer.parseInt(duration),Integer.parseInt(numberOfQuestion));
            request.setAttribute("quizlist", quizlist);
            request.setAttribute("page", page);
            request.getRequestDispatcher("quizmanagement").forward(request, response);
        }
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

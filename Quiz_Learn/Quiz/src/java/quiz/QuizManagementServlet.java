/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package quiz;

import dal.ChapterDAO;
import dal.CourseDAO;
import dal.QuizDAO;
import dal.SubjectDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;
import model.Account;
import model.Chapter;
import model.Course;
import model.Quiz;
import model.Subject;

/**
 *
 * @author Admin
 */
@WebServlet(name="QuizManagementServlet", urlPatterns={"/quizmanagement"})
public class QuizManagementServlet extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet QuizManagementServlet</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet QuizManagementServlet at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        String course_raw = (String) request.getParameter("course");
        String chapter_raw = request.getParameter("chapter");
        String page_raw = request.getParameter("page");
        String subject_raw = request.getParameter("subject");
        HttpSession session = request.getSession(false);
        QuizDAO q = new QuizDAO();
        CourseDAO cd = new CourseDAO();
        ChapterDAO chd = new ChapterDAO();
        SubjectDAO sd = new SubjectDAO();
        List<Course> cList = cd.getAllCourse();
        List<Chapter> chList = chd.getAllChapter();
        
        List<Subject> sList = sd.getAllSubject();
        
        int aid=1;
        Account a=(Account) session.getAttribute("account");
        if(a!=null)
        {
            aid=a.getAid();
        }
        List<Quiz> quizlist = q.getAllQuiz(aid);
        int page = 1;
        if(page_raw != null){
            page = Integer.parseInt(page_raw);
        }
        
        int subject;
        if (subject_raw != null) {
            if (subject_raw.equalsIgnoreCase("0")) {
                ;
            } else {
                subject=Integer.parseInt(subject_raw);
                if (course_raw != null) {
                    if (course_raw.equalsIgnoreCase("0"))//get all course and chapter 
                    {
                        quizlist = q.getAllQuizByCourse(subject, 0, 0,aid);
                        cList=cd.getCourseBySubjectId(subject);
                        chList=cd.getChapterByCourseIdAndSubjectId(subject, 0);
                    } else {
                        int course = Integer.parseInt(course_raw);
                        chList = cd.getChapterByCourseId(course);
                        int chapter;
                        if(chapter_raw == null) //filter with course where chapter is null
                        {
                            quizlist = q.getAllQuizByCourse(subject, course, 0,aid);
                        } else { 
                            chapter = Integer.parseInt(chapter_raw);
                            quizlist = q.getAllQuizByCourse(subject, course, chapter,aid);
                        }
                    }
                }
            }
        }
        
        request.setAttribute("subjectList", sList);
        request.setAttribute("chapterlist", chList);
        request.setAttribute("courselist", cList);
        request.setAttribute("page", page);
        request.setAttribute("quizlist", quizlist);
        request.getRequestDispatcher("quizManagement.jsp").forward(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        QuizDAO qd = new QuizDAO();
        CourseDAO cd = new CourseDAO();
        ChapterDAO chd = new ChapterDAO();
        List<Quiz> qList = qd.getAllQuiz();
        List<Course> cList = cd.getAllCourse();
        List<Chapter> chList = chd.getAllChapter();
        
        request.setAttribute("quizlist", qList);
        request.setAttribute("page", 1);
        request.setAttribute("courselist", cList);
        request.setAttribute("chapterlist", chList);
        request.getRequestDispatcher("quizManagement.jsp").forward(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

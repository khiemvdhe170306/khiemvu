/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package payment;

import dal.UserDAO;
import dal.VoucherDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import model.Voucher;

/**
 *
 * @author khiem
 */
@WebServlet(name = "CrudVoucher", urlPatterns = {"/crudvoucher"})
public class CrudVoucher extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CrudVoucher</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CrudVoucher at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        UserDAO ud = new UserDAO();
        String voucher_raw = request.getParameter("vid");
        String action = request.getParameter("action");
        VoucherDAO vd = new VoucherDAO();
        int voucher = 1;
        try {
            voucher = Integer.parseInt(voucher_raw);
        } catch (Exception e) {
            System.out.println(e);
        }
        if (action.equalsIgnoreCase("update")) {//update
            Voucher v = ud.getVoucherById(voucher);
            request.setAttribute("voucher", v);
            request.setAttribute("action", "update");
            request.getRequestDispatcher("crudVoucher.jsp").forward(request, response);
        } else if (action.equalsIgnoreCase("add")) {
            request.setAttribute("action", "add");
            request.getRequestDispatcher("crudVoucher.jsp").forward(request, response);
        } else if (action.equalsIgnoreCase("delete")) {
            if(vd.deleteVoucher(voucher)==1)
            {
            request.setAttribute("status", "Delete successfully");
            request.getRequestDispatcher("vouchermanagement").forward(request, response);
            }
            else{
                request.setAttribute("status", "Fail to delete!!! You need to delete all voucher distribution for member first");
                request.getRequestDispatcher("vouchermanagement").forward(request, response);
            }
        }

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        VoucherDAO vd=new VoucherDAO();
        String action = request.getParameter("action");
        //HttpSession session = request.getSession(false);
        String page_raw = request.getParameter("page");
        String name=request.getParameter("name").trim();
        String percentage_raw=request.getParameter("percentage");
        String voucher_raw=request.getParameter("voucherid");
        int percent=10;
        if(percentage_raw!=null)
            percent=Integer.parseInt(percentage_raw);
        int page = 1;
        if (page_raw != null) {
            page = Integer.parseInt(page_raw);
        };
        int voucher=1;
        if(voucher_raw!=null)
        {
            voucher=Integer.parseInt(voucher_raw);
        };
        if (action.equalsIgnoreCase("add")) {
            vd.addVoucher(name, percent);
            request.setAttribute("status", "Add successfully");
            request.getRequestDispatcher("vouchermanagement").forward(request, response);

        } else if (action.equalsIgnoreCase("update")) {
            vd.updateVoucher(voucher, name, percent);
            request.setAttribute("status", "Update successfully");
            request.getRequestDispatcher("vouchermanagement").forward(request, response);            
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}

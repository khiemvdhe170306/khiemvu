﻿
	Create table [dbo].[Quiz](
	[quizId] [int] PRIMARY KEY NOT NULL,
	[title] [varchar](max) NULL,
	[chapId] [int] REFERENCES Chapter(chapId),
	[image] [varchar](max) Null,
	[duration] [int] Null)
	CREATE TABLE [dbo].[Question](
	[questionId] [int] primary key,
	[qContent] [nvarchar](max) NULL,
	[quizId] [int] NULL references Quiz(quizId),
	)
	Create table [dbo].[Question_True](
	[questionId] [int] primary key references Question(questionId),
	[numberOfCorrect] int
	)

	CREATE TABLE [dbo].[Answer](
	[answerId] [int] primary key,
	[content] [varchar](255) NULL,
	[correct] [bit] NULL,
	[quesId] [int] NULL references Question(questionId))
	CREATE TABLE Answer_Quiz(
	[answerId] [int],
	[quesId] [int]  references Question(questionId),
	)
	
	Create table [dbo].[Question_Explaination](
	[qExplainId][int] primary key not null,
	[questionId] [int] references Question(questionId),
	[qExplainContent] [nvarchar] (max)
	)



	/*Course 1*/
Insert Into Quiz Values 
(1, 'The Roles of Statistics in Engineering', 1,'https://slideplayer.com/8994582/27/images/slide_1.jpg', 15), 
(2, 'Probability', 2,'http://3.bp.blogspot.com/-nXEymBe0Mu4/Tnoqi3SyhdI/AAAAAAAAEc0/TYORfg3LtLY/s1600/probability_1.png', 15), 
(3, 'Discrete Random Variables and Probability Distribution', 3,'https://image1.slideserve.com/2995428/slide1-l.jpg', 15)

Insert Into Question Values
(1, 'Data that describe a characteristic about a population is known as a _____', 1),
(2, 'Transportation officials tell us that 70% of drivers wear seat belts while driving. Find the probability that more than 579 drivers in a sample of 800 drivers wear seat belts.

Let P(Z < 1.5) = 0.0668; P(Z<0.25) = 0.6', 1),
(3, 'A basketball player is asked to shot free throws in sets of four. The player shoots 100 sets of 4 free throws. The probability distribution for making a particular number of free throws is given below. Determine the standard deviation for this discrete probability distribution.

X P(X)
0 0.02
1 0.07
2 0.22
3 0.27
4 0.42', 1),
(4, 'A method of gathering data when subjects are exposed to certain treatments and the data of interest is recorded is known as', 1),
(5, 'Medicare would like to test the hypothesis that the average monthly rate for one-bedroom assisted-living facility is equal to $3,300. A random sample of 12 assisted living facilities had an average rate of $3,690 per month and a standard of $530. It is believed that the monthly rate for onebedroom assisted-living facility is normally distributed. Use the signinicance level of 0.05 for this hypothesis test, what is the value of the test statistic?', 1),
(6, 'The residuals represent', 1),
(7, 'Construct a 95% confidence interval for the population standard deviation of a random sample of 15 men who have a mean weight of 165.2 pounds with a standard deviation of 10.3 pounds. Assume the population is normally distributed.', 1),
(8, 'Suppose X is a uniform random variable over the interval [40, 50). Find the probability that a randomly selected observation exceeds 43.', 1),
(9, 'Trang Tien is a producer of ice cream and would like to test the hypothesis that the average consumes more than 17 ounces of ice cream per month. A random sample of 15 Vietnamese people was found to consume an average of 18.2 ounces of ice cream last month. The standard deviation for this sample was 3.9 ounces. What is the test statistic for this hypothesis test?', 1),
(10, 'A card is drawn from a standard deck of 52 playing cards. Find the probability that the card is an ace or a king.', 1),

(11, 'A pair of dice is thrown twice. What is the probability of getting totals of 7 and 11?', 2),
(12, 'The probability density function of the length of a hinge for fastening a door is f(x) = 1 for 74 <x< 75. Find P(X < 74.3).', 2),
(13, 'The probability of a successful optical alignment in the assembly of an optical data storage product is 0.8. Assume the trials are independent. What is the probability that the first successful alignment requires exactly four trials?', 2),
(14, 'An airline knows from experience that the distribution of the number of suitcases that get lost each week on a certain route is approximately normal with u = 15.5 and o = 3.6. What is the probability that during a given week the airline will lose less than 20 suitcases?
Let P(Z < -1.25) =0.1056, P(Z < -0.37) =0.3944, P(Z < -0.24) =0.4040,P(Z < 1.25) =0.8944.', 2),
(15, 'Suppose X has an exponential distribution with λ = 2. Which in the following statements is TRUE?', 2),
(16, 'At a computer manufacturing company, the actual size of computer chips is normally distributed with a mean of 1 centimeter and a standard deviation of 0.1 centimeter. A random sample of 12 computer chips is taken. What is the probability that the sample mean will be between 0.99 and 1.01 centimeters?

Let P(Z < 0.1) = 0.54, P(Z < 0.346) = 0.64, P(Z < -0.346) = 0.37.', 2),
(17, 'Suppose that X has a discrete uniform distribution on the integers 10 to 99. Which of the followings are true?
(i) P(X> 49) = 5/9
(ii) E(2X) = 109.', 2),
(18, 'A laboratory tested 70 chicken eggs and found that the mean amount of cholesterol was 190 milligrams. Assume that the standard deviation is 15.1 milligrams. Construct a 95% lower confidence bound for the true mean cholesterol content of all such eggs.', 2),
(19, 'A group of 10 individuals are used for a biological case study. The group contains 3 people with blood type 0, 7 with blood type  What is the probability that a random sample of 5 will contain 1 person with blood type 0, 4 people with blood type ', 2),
(20, 'Construct a 98% confidence interval for the population mean. Assume the population has a normal distribution. A study of 14 bowlers showed that their average score was 192 with a standard deviation of 8', 2),

(21, 'A group of students were asked if they carry a credit card. The responses are listed in the table.
If a student is selected at random, find the probability that he or she owns a credit card given that the student is a freshman.
Round your answer to three decimal places.', 3),
(22, 'A common rule of thumb for determining how many classes to use when developing a frequency distribution with classes is:', 3),
(23, 'Given two events A and B with P(A) = 0.4, P(B) = 0.5. Find P(AUB).', 3),
(24, 'Find the mean for the binomial distribution which has the stated values of n=20 and p=0.6. Round answer to the nearest tenth.', 3),
(25, 'Suppose a 95% confidence interval for population mean turns out to be (1000, 2100). To make more useful inferences from the data, it is desired to reduce the width of the confidence interval. Which of the following will result in a reduced interval width?', 3),
(26, 'The random variable X represents the number of credit cards that adults have along with the corresponding probabilities. Find the mean and standard deviation.

X P(X)
0 0.49
1 0.05
2 0.32
3 0.07
4 0.07', 3),
(27, 'The probability that a tennis set will go to a tie-breaker is 17%. What is the probability that two of three sets will go to tie-breakers?', 3),
(28, 'Of 1000 randomly selected cases of lung cancer, 731 resulted in death within 10 years. Calculate a 95% confidence interval on the death rate from lung cancer.', 3),
(29, 'A group of 40 bowlers showed that their average score was 192 with a population standard deviation of 8. Assume that bowlers scores are normally distributed. Find the 95% confidence interval of the mean score of all bowlers.', 3),
(30, 'Consider the following sample data:
25 11 6 4 2 17 9 6
For these data the sample mean is:', 3)

Insert Into Answer Values
(1, ' statistic', 0 ,1),
(2, ' survey', 0 ,1),
(3, ' parameter', 1 ,1),
(4, ' sample', 0 ,1),

(5, ' 0.0668', 0 ,2),
(6, ' 0.9332', 1 ,2),
(7, ' 0.6', 0 ,2),
(8, ' 0.4', 0 ,2),

(9, ' 1.21', 0 ,3),
(10, ' 1.05', 1 ,3),
(11, ' 0.28', 0 ,3),
(12, ' 1.10', 0 ,3),

(13, ' None of the other choices is correct', 0 ,4),
(14, ' observation.', 0 ,4),
(15, ' experiments.', 1 ,4),
(16, ' retrospective', 0 ,4),

(17, ' -1.37', 0 ,5),
(18, ' -2.16', 0 ,5),
(19, ' 2.21', 0 ,5),
(20, ' 2.55', 1 ,5),

(21, ' the predicted value of Y for the average X value.', 0 ,6),
(22, ' the difference between the actual Y values and the predicted Y values.', 1 ,6),
(23, ' the difference between the actual y values and the mean of Y.', 0 ,6),
(24, ' the square root of the slope.', 0 ,6),

(25, ' (7.9, 15.0)', 0 ,7),
(26, ' (56.9, 263.9)', 0 ,7),
(27, ' (7.5, 16.2)', 1 ,7),
(28, ' (2.3, 5.1)', 0 ,7),

(29, ' 0.3', 0 ,8),
(30, ' 0.1', 0 ,8),
(31, ' 0.9', 0 ,8),
(32, ' 0.7', 1 ,8),

(33, ' 2.49', 0 ,9),
(34, ' 1.19', 1 ,9),
(35, ' 3.01', 0 ,9),
(36, ' 1.83', 0 ,9),

(37, ' 1/13', 0 ,10),
(38, ' 2/13', 1 ,10),
(39, ' 4/13', 0 ,10),
(40, ' 8/13', 0 ,10),

(41, ' 1/54',1,11),
(42, ' 1/24',0,11),
(43, ' 1/14',0,11),
(44, ' 1/18',0,11),

(45, ' 0.81',0,12),
(46, ' 0.30',1,12),
(47, ' 0.36',0,12),
(48, ' 0.74',0,12),

(49, ' 0.6756',0,13),
(50, ' 0.1262',0,13),
(51, ' 0.4332',0,13),
(52, ' 0.0064',1,13),

(53, ' 0.4040',0,14),
(54, ' 0.8944',1,14),
(55, ' 0.3944',0,14),
(56, ' 0.1056',0,14),

(57, ' P(X ≥ 2) = 0.0183',0,15),
(58, ' P(X ≤ 1) = 0.8647',0,15),
(59, ' P(X ≤ 0) = 0',0,15),
(60, ' All of the others',1,15),

(61, ' 0.73',0,16),
(62, ' 0.63',0,16),
(63, ' 0.37',0,16),
(64, ' 0.27',1,16),

(65, ' (i) only',0,17),
(66, ' None of the other choices is correct',0,17),
(67, ' Both (i) and (ii)',0,17),
(68, ' (ii) only',1,17),

(69, ' 185.28',0,18),
(70, ' 187.03',0,18),
(71, ' 186.46',1,18),
(72, ' 184.79',0,18),

(73, ' 0.48',0,19),
(74, ' 0.45',0,19),
(75, ' 0.51',0,19),
(76, ' 0.42',1,19),

(77, ' (222.3, 256.1)',0,20),
(78, ' (186.3, 197.7)',1,20),
(79, ' (115.4, 158.8)',0,20),
(80, ' (328.3, 386.9)',0,20),

(81, ' 0.600',0,21),
(82, ' 0.240',0,21),
(83, ' 0.400',1,21),
(84, ' 0.393',0,21),

(85, ' no fewer than 6 classes.',0,22),
(86, ' None of the other choices is correct',0,22),
(87, ' equal to 0.25 times the number of data values',0,22),
(88, ' between 5 and 20 classes.',1,22),

(89, ' 0.9, if A and B are independent.',0,23),
(90, ' 0.7, if A and B are disjoint.',0,23),
(91, ' 0.7, if A and B are independent.',1,23),
(92, ' 0.2, if A and B are independent',0,23),

(93, ' 12.0',1,24),
(94, ' 11.5',0,24),
(95, ' 12.7',0,24),
(96, ' 12.3',0,24),

(97, ' Both increase the sample size and decrease the confidence level.',1,25),
(98, ' Decrease the confidence level.',0,25),
(99, ' Increase the sample size.',0,25),
(100, ' Both increase the confidence level and decrease the sample size.',0,25),

(101, ' mean: 1.39; standard deviation: 0.64',0,26),
(102, ' mean: 1.18; standard deviation: 0.64',0,26),
(103, ' mean: 1.18; standard deviation: 1.30',1,26),
(104, ' mean: 1.39; standard deviation: 0.80',0,26),

(105, ' 0.072',1,27),
(106, ' 0.170',0,27),
(107, ' 0.028',0,27),
(108, ' 0.351',0,27),

(109, ' (0.703; 0.758)',1,28),
(110, ' (0.307; 0.751)',0,28),
(111, ' (0.707; 0.754)',0,28),
(112, ' None of the others',0,28),

(113, ' (186.5, 197.5)',0,29),
(114, ' (189.5, 194.5)',1,29),
(115, ' (188.5, 195.6)',0,29),
(116, ' (187.3, 196.1)',0,29),

(117, ' 3',0,30),
(118, ' 7',0,30),
(119, ' 10',1,30),
(120, ' 8',0,30)

Insert Into Question Values
(151,'Suppose that the probability of event A is 0.2 and the probability of event B is 0.4. Also, suppose that
the two events are independent. Then P(A|B) is',1),
(152,'Which of the following is an example of a relative frequency probability based on measuring a
representative sample and observing relative frequencies of possible outcomes? ',1),
(153,'Which of the following statements best describes the relationship between a parameter and a statistic? ',1),
(154,'Which of the following is the most common example of a situation for which the main parameter of
interest is a population proportion? ',1),
(155,'Which statement is not true about confidence intervals?',1),
(156,'Which statement is not true about the 95% confidence level? ',1),
(157,'In a random sample of 50 men, 40% said they preferred to walk up stairs rather than take the elevator.
In a random sample of 40 women, 50% said they preferred the stairs. The difference between the two
sample proportions (men – women) is to be calculated. Which of the following choices correctly
denotes the difference between the two sample proportions that is desired? ',1),
(158,'Which of the following statements is correct about a parameter and a statistic associated with repeated
random samples of the same size from the same population? ',1),
(159,'Five hundred (500) random samples of size n=900 are taken from a large population in which 10% are
left-handed. The proportion of the sample that is left-handed is found for each sample and a histogram
of these 500 proportions is drawn. Which interval covers the range into which about 68% of the
values in the histogram will fall? ',1),
(160,'A randomly selected sample of 400 students at a university with 15-week semesters was asked
whether or not they think the semester should be shortened to 14 weeks (with longer classes). Fortysix percent (46%) of the 400 students surveyed answered "yes." Which one of the following
statements about the number 46% is correct? ',1),

(161,'Which of the following examples involves paired data?',2),
(162,'A poll is done to estimate the proportion of adult Americans who like their jobs. The poll is based on a
random sample of 400 individuals. What is the “conservative” margin of error of this poll? ',2),
(163,'The expected value of a random variable is the ',2),
(164,'The payoff (X) for a lottery game has the following probability distribution.
X = payoff $0 $5
probability 0.8 0.2
What is the expected value of X= payoff? ',2),
(165,'Which one of these variables is a continuous random variable?',2),
(166,'Heights of college women have a distribution that can be approximated by a normal curve with a
mean of 65 inches and a standard deviation equal to 3 inches. About what proportion of college
women are between 65 and 67 inches tall? ',2),
(167,'Which one of these variables is a binomial random variable? ',2),
(168,'. Suppose that vehicle speeds at an interstate location have a normal distribution with a mean equal to
70 mph and standard deviation equal to 8 mph. What is the z-score for a speed of 64 mph? ',2),
(169,'Pulse rates of adult men are approximately normal with a mean of 70 and a standard deviation of 8.
Which choice correctly describes how to find the proportion of men that have a pulse rate greater than
78?',2),
(170,'The probability is p = 0.80 that a patient with a certain disease will be successfully treated with a new
medical treatment. Suppose that the treatment is used on 40 patients. What is the "expected value" of
the number of patients who are successfully treated? ',2),

(171,'Suppose that a quiz consists of 20 True-False questions. A student hasnt studied for the exam and will
just randomly guesses at all answers (with True and False equally likely). How would you find the
probability that the student will get 8 or fewer answers correct?',3),
(172,'The normal approximation to the binomial distribution is most useful for finding which of the
following? ',3),
(173,'A lottery ticket displays the probabilities of winning various prizes on the back of the ticket. These
probabilities are examples of: ',3),
(174,'A climate expert was asked to assess the probability that global warming will make some cities
uninhabitable in the next 100 years. The answer to this question for the expert is an example of:',3),
(175,'Imagine a test for a certain disease. Suppose the probability of a positive test result is .95 if someone
has the disease, but the probability is only .08 that someone has the disease if his or her test result was
positive. A patient receives a positive test, and the doctor tells him that he is very likely to have the
disease. The doctors response is:',3),
(176,'Which one of the following probabilities is a "cumulative" probability?',3),
(177,'A medical treatment has a success rate of .8. Two patients will be treated with this treatment.
Assuming the results are independent for the two patients, what is the probability that neither one of
them will be successfully cured? ',3),
(178,' If you roll a pair of dice, what is the probability that (at least) one of the dice is a 4 or the sum of the dice is 7?',3),
(179,'If the occurrence of one event means that another cannot happen, then the events are',3),
(180,' If a card is chosen from a standard deck of cards, what is the probability of getting a diamond (♦) or a club(♣)?',3)

Insert Into Answer Values
(601, ' P(A) = 0.2', 1 ,151),
(602, ' P(A)/P(B) = 0.2/0.4 = ½ ', 0 ,151),
(603, ' P(A) × P(B) = (0.2)(0.4) = 0.08', 0 ,151),
(604, ' None of the above.', 0 ,151),

(605, ' According to the late Carl Sagan, the probability that the earth will be hit by a civilizationthreatening asteroid in the next century is about 0.001. ', 0 ,152),
(606, ' If you flip a fair coin, the probability that it lands with heads up is ½', 0 ,152),
(607, ' Based on a recent Newsweek poll, the probability that a randomly selected adult in the US
would say they oppose federal funding for stem cell research is about 0.37.', 1 ,152),
(608, ' A new airline boasts that the probability that its flights will be on time is 0.92, because 92% of all
flights it has ever flown did arrive on time. ', 0 ,152),

(609, ' A sample', 0 ,153),
(610, ' A sample statistic', 1 ,153),
(611, ' A population8', 0 ,153),
(612, ' A population parameter', 0 ,153),

(613, ' A binomial experiment ', 1 ,154),
(614, ' A normal experiment ', 0 ,154),
(615, ' A A randomized experiment', 0 ,154),
(616, ' An observational study', 0 ,154),

(617, ' A confidence interval is an interval of values computed from sample data that is likely to include
the true population value. 
', 0 ,155),
(618, ' An approximate formula for a 95% confidence interval is sample estimate ± margin of error. ', 0 ,155),
(619, ' A confidence interval between 20% and 40% means that the population proportion lies between
20% and 40%. t', 1 ,155),
(620, ' A 99% confidence interval procedure has a higher probability of producing intervals that will
include the population parameter than a 95% confidence interval procedure. 
', 0 ,155),

(621, ' Confidence intervals computed by using the same procedure will include the true population value
for 95% of all possible random samples taken from the population. ', 0 ,156),
(622, ' The procedure that is used to determine the confidence interval will provide an interval that
includes the population parameter with probability of 0.95. ', 0 ,156),
(623, ' The probability that the true value of the population parameter falls between the bounds of an
already computed confidence interval is roughly 95%. ', 1 ,156),
(624, ' If we consider all possible randomly selected samples of the same size from a population, the 95%
is the percentage of those samples for which the confidence interval includes the population
parameter. ', 0 ,156),

(625, ' p1 − p2 = 0.10', 0 ,157),
(626, ' pˆ1 − pˆ2 = 0.10', 0 ,157),
(627, ' p1 − p2 = −0.10', 0 ,157),
(628, ' pˆ1 − pˆ 2 = −0.10', 1 ,157),

(629, ' Values of a parameter will vary from sample to sample but values of a statistic will not. ', 0 ,158),
(630, ' Values of both a parameter and a statistic may vary from sample to sample. ', 0 ,158),
(631, ' Values of a parameter will vary according to the sampling distribution for that parameter. ', 0 ,158),
(632, ' Values of a statistic will vary according to the sampling distribution for that statistic', 1 ,158),

(633, ' .1 ± .010', 1 ,159),
(634, ' .1 ± .0134', 0 ,159),
(635, ' .1 ± .0167', 0 ,159),
(636, ' .1 ± .020 ', 0 ,159),

(637, ' It is a sample statistic. ', 1 ,160),
(638, ' It is a population parameter', 0 ,160),
(639, ' It is a margin of error', 0 ,160),
(640, ' It is a standard error', 0 ,160),

(641, ' A study compared the average number of courses taken by a random sample of 100 freshmen at a
university with the average number of courses taken by a separate random sample of 100
freshmen at a community college. ', 0 ,161),
(642, ' A group of 100 students were randomly assigned to receive vitamin C (50 students) or a placebo
(50 students). The groups were followed for 2 weeks and the proportions with colds were
compared. ', 0 ,161),
(643, ' A group of 50 students had their blood pressures measured before and after watching a movie
containing violence. The mean blood pressure before the movie was compared with the mean
pressure after the movie. ', 1 ,161),
(644, ' None of the above.', 0 ,161),

(645, ' 0.10 ', 0 ,162),
(646, ' 0.05', 1 ,162),
(647, ' 0.04', 0 ,162),
(648, ' . 0.025', 0 ,162),

(649, ' value that has the highest probability of occurring.', 0 ,163),
(650, ' mean value over an infinite number of observations of the variable. ', 1 ,163),
(651, ' largest value that will ever occur', 0 ,163),
(652, ' most common value over an infinite number of observations of the variable.', 0 ,163),

(653, ' $0  ', 0 ,164),
(654, ' $0.50 ', 0 ,164),
(655, ' $1.00 ', 1 ,164),
(656, ' $2.50 ', 0 ,164),

(657, ' The time it takes a randomly selected student to complete an exam. ', 1 ,165),
(658, ' The number of tattoos a randomly selected person has. ', 0 ,165),
(659, ' The number of women taller than 68 inches in a random sample of 5 women', 0 ,165),
(660, ' The number of correct guesses on a multiple choice test', 0 ,165),

(661, ' 0.75 ', 0 ,166),
(662, ' 0.50 ', 0 ,166),
(663, ' 0.25 ', 1 ,166),
(664, ' 0.17', 0 ,166),

(665, ' time it takes a randomly selected student to complete a multiple choice exam', 0 ,167),
(666, ' number of women taller a randomly selected person owns ', 0 ,167),
(667, ' number of women taller than 68 inches in a random sample of 5 women  ', 1 ,167),
(668, ' number of CDs a randomly selected person owns', 0 ,167),

(669, ' -0.75 ', 1 ,168),
(670, ' +0.75  ', 0 ,168),
(671, ' −6 ', 0 ,168),
(672, ' +6 ', 0 ,168),

(673, ' Find the area to the left of z = 1 under a standard normal curve.', 0 ,169),
(674, ' Find the area between z = −1 and z = 1 under a standard normal curve. ', 0 ,169),
(675, ' Find the area to the right of z =1 under a standard normal curve. ', 1 ,169),
(676, ' Find the area to the right of z = −1 under a standard normal curve.', 0 ,169),

(677, ' 40 ', 0 ,170),
(678, ' 20', 0 ,170),
(679, ' 8', 0 ,170),
(680, ' 32', 1 ,170),

(681, ' Find the probability that X=8 in a binomial distribution with n = 20 and p=0.5. ', 0 ,171),
(682, ' Find the area between 0 and 8 in a uniform distribution that goes from 0 to 20.', 0 ,171),
(683, ' Find the probability that X=8 for a normal distribution with mean of 10 and standard deviation of
5 . ', 0 ,171),
(684, 'Find the cumulative probability for 8 in a binomial distribution with n = 20 and p = 0.5. ', 1 ,171),

(685, ' The probability P(X = k) when X is a binomial random variable with large n. ', 0 ,172),
(686, ' The probability P(X ≤ k) when X is a binomial random variable with large n. ', 1 ,172),
(687, ' The probability P(X = k) when X is a normal random variable with small n', 0 ,172),
(688, ' The probability P(X ≤ k) when X is a normal random variable with small n.', 0 ,172),

(689, ' Relative frequency probabilities based on long-run observation.  ', 0 ,173),
(690, ' Relative frequency probabilities based on physical assumptions. ', 1 ,173),
(691, ' Personal probabilities. ', 0 ,173),
(692, ' Random probabilities.', 0 ,173),

(693, ' A relative frequency probability based on long-run observation.  ', 0 ,174),
(694, ' A relative frequency probability based on physical assumptions. ', 0 ,174),
(695, ' A random probability. ', 0 ,174),
(696, ' A personal probability. ', 1 ,174),

(697, ' An example of "Confusion of the inverse." ', 1 ,175),
(698, ' An example of the "Law of small numbers." ', 0 ,175),
(699, ' An example of "The gamblers fallacy."', 0 ,175),
(700, ' Correct, because the test is 95% accurate when someone has the disease. ', 0 ,175),

(701, ' The probability that there are exactly 4 people with Type O+ blood in a sample of 10 people. ', 0 ,176),
(702, ' The probability of exactly 3 heads in 6 flips of a coin. ', 0 ,176),
(703, ' The probability that the accumulated annual rainfall in a certain city next year, rounded to the
nearest inch, will be 18 inches. ', 0 ,176),
(704, ' The probability that a randomly selected womans height is 67 inches or less. ', 1 ,176),

(705, ' 5 ', 0 ,177),
(706, ' 36', 0 ,177),
(707, ' 8', 0 ,177),
(708, ' 4', 1 ,177),

(709, ' 4/36 ', 0 ,178),
(710, ' 13/36', 0 ,178),
(711, ' 21/36', 0 ,178),
(712, ' 15/36', 1 ,178),

(713, ' Independent ', 0 ,179),
(714, ' Mutually Exclusive', 1	,179),
(715, ' Bayesian', 0 ,179),
(716, ' Empirical', 0 ,179),

(717, ' 1/2 ', 1 ,180),
(718, ' 13/52', 0 ,180),
(719, ' 20/52', 0 ,180),
(720, ' 12/52', 0 ,180)
/*Course 2 */
insert into Quiz values(4,'Limits',4,'https://tse1.mm.bing.net/th?id=OIP.J9aA_klz6MrlhaynARRvAwHaFj&pid=Api&P=0&h=180',15)
insert into Quiz values(5,'Derivatives',5,'https://tse1.mm.bing.net/th?id=OIP.DwXfawKdtxQbL2Ju5OSWHgHaDt&pid=Api&P=0&h=180',15)
insert into Quiz values(6,'Applications of derivatives',6,'https://tse1.mm.bing.net/th?id=OIP.MFVIm1CBlRZgWk98s9jHpAHaFq&pid=Api&P=0&h=180',15)
--select * from Post
insert into Question values(31,N'Which of the following type of numbers are closed under only multiplication?',4)
insert into Answer values(121,N'Rational number',0,31)
insert into Answer values(122,N'Integer',0,31)
insert into Answer values(123,N'Whole number',1,31)
insert into Answer values(124,N'Natural number',0,31)
--delete  from Answer
--delete from Question
--delete from QUiz
insert into Question values(32,N'<p><img src="https://quizizz.com/media/resource/gs/quizizz-media/quizzes/005d9537-d7cc-402d-8f66-9e4a42dc2835?w=900&amp;h=900" alt="Quiz image"></p>',4)
insert into Answer values(125,N'1',0,32)
insert into Answer values(126,N'2',0,32)
insert into Answer values(127,N'3',1,32)
insert into Answer values(128,N'Not exist',0,32)
insert into Question values(33,N'<p>&nbsp;</p><p>What is the limits of the function when <i>x</i> approching <i>a</i>?<img src="https://quizizz.com/media/resource/gs/quizizz-media/quizzes/820a1945-6384-4927-a442-8038d198104f?w=200&amp;h=200" alt="Quiz image"></p><p>&nbsp;</p>',4)
insert into Answer values(132,N'1',0,33)
insert into Answer values(129,N'2',1,33)
insert into Answer values(130,N'Not exist',0,33)
insert into Answer values(131,N'Undefined',0,33)
insert into Question values(34,N'<p>What is the limits of the graph above when <i>x</i> approaching 0?<img src="https://quizizz.com/media/resource/gs/quizizz-media/quizzes/b1cbcd8b-1d09-44cb-ae49-1521c8556efc?w=200&amp;h=200" alt="Image"></p>',4)
insert into Answer values(136,N'Undefined',0,34)
insert into Answer values(133,N'1',0,34)
insert into Answer values(134,N'0',0,34)
insert into Answer values(135,N'Not exist',1,34)

insert into Question values(35,N'<p>Which of the following is true?<img src="https://quizizz.com/media/resource/gs/quizizz-media/quizzes/41158ecb-fff7-4a31-acf0-02c3670c05f0?w=200&amp;h=200" alt="Image"></p>',4)
insert into Answer values(140,N'limit of f(x)=2',1,35)
insert into Answer values(137,N'limit of f(x)=-2',0,35)
insert into Answer values(138,N'limit of f(x)=0',0,35)
insert into Answer values(139,N'Not exist',0,35)
insert into Question values(36,N'<figure class="image"><img src="https://quizizz.com/media/resource/gs/quizizz-media/quizzes/3b9fdacd-0e03-4d4b-b7bd-6cd10a5e1129?w=200&amp;h=200" alt="Image"></figure>',4)
insert into Answer values(144,N'-81',0,36)
insert into Answer values(141,N'9',0,36)
insert into Answer values(142,N'-9',0,36)
insert into Answer values(143,N'81',1,36)

insert into Question values(37,N'<figure class="image"><img src="https://quizizz.com/media/resource/gs/quizizz-media/quizzes/68aef35e-b410-48a6-b637-d6d1cb91d0e1?w=200&amp;h=200" alt="Image"></figure>',4)
insert into Answer values(148,N'undefined',0,37)
insert into Answer values(145,N'0',0,37)
insert into Answer values(146,N'1',0,37)
insert into Answer values(147,N'Not exist',1,37)

insert into Question values(38,N'<p>Find the limits above<img src="https://quizizz.com/media/resource/gs/quizizz-media/quizzes/8fbc1da5-73a4-4d2a-b428-31d61a2fa88c?w=200&amp;h=200" alt="Image"></p>',4)
insert into Answer values(152,N'-1',1,38)
insert into Answer values(149,N'0',0,38)
insert into Answer values(150,N'1',0,38)
insert into Answer values(151,N'Not exist',0,38)

insert into Question values(39,N'<figure class="image"><img src="https://quizizz.com/media/resource/gs/quizizz-media/quizzes/005d9537-d7cc-402d-8f66-9e4a42dc2835?w=200&amp;h=200" alt="Quiz image"></figure>',4)
insert into Answer values(156,N'1',1,39)
insert into Answer values(153,N'Not exist',0,39)
insert into Answer values(154,N'2',0,39)
insert into Answer values(155,N'3',0,39)

insert into Question values(40,N'<figure class="image"><img src="https://quizizz.com/media/resource/gs/quizizz-media/quizzes/5da9efa5-4ebb-4a49-bf8f-e093e05c89c2?w=200&amp;h=200" alt="Quiz image"></figure>',4)
insert into Answer values(160,N'1',1,40)
insert into Answer values(157,N'Not exist',0,40)
insert into Answer values(158,N'2',0,40)
insert into Answer values(159,N'3',0,40)
insert into Question values(41,N'Let f(x) = (x - 1)(x + 2). Find f(0)',5)
insert into Answer values(161,N'1',1,41)
insert into Answer values(162,N'0',0,41)
insert into Answer values(163,N'2',0,41)
insert into Answer values(164,N'3',0,41)

insert into Question values(42,N'Let y = (x - 1)(x + 2). Find dy/dx.',5)
insert into Answer values(165,N'2x+1',1,42)
insert into Answer values(166,N'2x-1',0,42)
insert into Answer values(167,N'4x-1',0,42)
insert into Answer values(168,N'4x+1',0,42)

insert into Question values(43,N'Which function has the derivative of g''(x) = 4x3 + 2x - 1',5)
insert into Answer values(169,N'g(x) = 2x^4 + 2x^2 - 1',1,43)
insert into Answer values(170,N'g(x) = x^4 + x^2 - 1',0,43)
insert into Answer values(171,N'g(x) = 2x^4 + 2x^2 ',0,43)
insert into Answer values(172,N'g(x) = 2x^4 ',0,43)


insert into Question values(44,N'What is the derivative of f(x) = 2x3 - 4x + 5?',5)
insert into Answer values(173,N'f''(x) = 6x^2 - 4x',0,44)
insert into Answer values(174,N'f''(x) = 3x^2 - 4x',0,44)
insert into Answer values(175,N'f''(x) = x^2 - 2x',0,44)
insert into Answer values(176,N'f''(x) = 6x^2 - 4',1,44)

insert into Question values(45,N'Find the derivative of the given equation f(x) = 1 - x^2',5)
insert into Answer values(177,N'1 - 2x',0,45)
insert into Answer values(178,N'2x',0,45)
insert into Answer values(179,N'-2x',1,45)
insert into Answer values(180,N'-2',0,45)


insert into Question values(46,N'Find the derivative of the given equation f''(x) = -4x',5)
insert into Answer values(181,N'-4',1,45)
insert into Answer values(182,N'-2',0,45)
insert into Answer values(183,N'2',0,45)
insert into Answer values(184,N'4',0,45)

insert into Question values(47,N'Find the derivative of the given equation f(x) = 7',5)
insert into Answer values(185,N'7',0,46)
insert into Answer values(186,N'7x',0,46)
insert into Answer values(187,N'14',0,46)
insert into Answer values(188,N'0',1,46)

insert into Question values(48,N'<p>Find the limits above<img src="https://quizizz.com/media/resource/gs/quizizz-media/quizzes/8fbc1da5-73a4-4d2a-b428-31d61a2fa88c?w=200&amp;h=200" alt="Image"></p>',5)
insert into Answer values(192,N'-1',1,48)
insert into Answer values(189,N'0',0,48)
insert into Answer values(190,N'1',0,48)
insert into Answer values(191,N'Not exist',0,48)

insert into Question values(49,N'<figure class="image"><img src="https://quizizz.com/media/resource/gs/quizizz-media/quizzes/005d9537-d7cc-402d-8f66-9e4a42dc2835?w=200&amp;h=200" alt="Quiz image"></figure>',5)
insert into Answer values(196,N'1',1,49)
insert into Answer values(193,N'Not exist',0,49)
insert into Answer values(194,N'2',0,49)
insert into Answer values(195,N'3',0,49)

insert into Question values(50,N'<figure class="image"><img src="https://quizizz.com/media/resource/gs/quizizz-media/quizzes/5da9efa5-4ebb-4a49-bf8f-e093e05c89c2?w=200&amp;h=200" alt="Quiz image"></figure>',5)
insert into Answer values(200,N'1',1,50)
insert into Answer values(197,N'Not exist',0,50)
insert into Answer values(198,N'2',0,50)
insert into Answer values(199,N'3',0,50)

insert into Question values(51,N'Chris is sitting on the edge of a dock tossing rocks into the water. As each rock hits the water, small circles appear traveling outward from the point of impact. The radius of the circle is changing at a rate of 5 in/sec.  How fast is the area of the outer circle changing when the diameter is 8 in?',6)
insert into Answer values(201,N'80pi in/sec',0,51)
insert into Answer values(202,N'20pi in/sec',1,51)
insert into Answer values(203,N'60pi in/sec',0,51)
insert into Answer values(204,N'40pi in/sec',0,51)

insert into Question values(52,N'Brandon is starting to clean up after a birthday party. He begins deflating each spherical balloon by puncturing a hole in each. The air leaves the balloon at a constant rate of 2 cm3/sec.  How fast is the diameter changing when the diameter is 8 cm?',6)
insert into Answer values(205,N'-1/(16pi) cm/sec',1,52)
insert into Answer values(206,N'-1/16 cm/sec',0,52)
insert into Answer values(207,N'-1/(4pi) cm/sec',0,52)
insert into Answer values(208,N'-1/(8pi) cm/sec',0,52)


insert into Question values(53,N'A spherical snowball melts so that its radius decreases at a rate of 4 in/sec. At what rate is the volume of the snowball changing when the radius is 4 in?',6)
insert into Answer values(209,N'-262π in3/sec',0,53)
insert into Answer values(210,N'-247π in3/sec',0,53)
insert into Answer values(211,N'-256π in3/sec',1,53)
insert into Answer values(212,N'-263π in3/sec',0,53)


insert into Question values(54,N'If a function has a second derivative that is negative, what does that tell you?',6)
insert into Answer values(213,N'The function is increasing',0,54)
insert into Answer values(214,N'The function is decreasing',0,54)
insert into Answer values(215,N'The function is concave up',0,54)
insert into Answer values(216,N'The function is concave down',1,54)


insert into Question values(55,N'If f''(x) >0, what is true about f(x)?',6)
insert into Answer values(217,N'it is concave up there',0,55)
insert into Answer values(218,N'it is concave down there',0,55)
insert into Answer values(219,N'it has an inflection point',0,55)
insert into Answer values(220,N'it is increasing',1,55)


insert into Question values(56,N'How many points of inflection does a parabola have?',6)
insert into Answer values(221,N' 0 because the concavity of a parabola never changes',1,56)
insert into Answer values(222,N'1 because there is only one critical point',0,56)
insert into Answer values(223,N'2 because parabolas increase then decrease (or vice vers',0,56)
insert into Answer values(224,N'Depends on the parabola',0,56)

insert into Question values(57,N'A rectangular solid that has a square base has a surface area of 150 square inches.  Find the maximum volume of the solid.  (use Calculus)',6)
insert into Answer values(225,N'5',0,57)
insert into Answer values(226,N'125',0,57)
insert into Answer values(227,N'150',1,57)
insert into Answer values(228,N'250',0,57)

insert into Question values(58,N'A farmer wants to construct a rectangular pigpen using 400 ft of fencing. The pen will be built next to an existing stone wall, so only three sides of fencing need to be constructed to enclose the pen. What dimensions should the farmer use to construct the pen with the largest possible area?',6)
insert into Answer values(229,N'100ft x 200ft',1,58)
insert into Answer values(230,N'102ft x 96ft',0,58)
insert into Answer values(231,N'52ft x 196ft',0,58)
insert into Answer values(232,N'50ft x 300ft',0,58)

insert into Question values(59,N'Find the differential for the function y = cos(x)',6)
insert into Answer values(233,N'dy = -sin(x) dx',0,59)
insert into Answer values(234,N'dx = -sin(x) dy',0,59)
insert into Answer values(235,N'dy = cos (x) dx',1,59)
insert into Answer values(236,N'dx = cos(x) dy',0,59)

insert into Question values(60,N'Let y = x3 Find the differential when x =2 and dx = 0.1.',6)
insert into Answer values(237,N'0.1',0,60)
insert into Answer values(238,N'0.12',0,60)
insert into Answer values(239,N'1.1',1,60)
insert into Answer values(240,N'1.2',0,60)
insert into Question values(181,N'Let f(x) = (x - 1)(x + 2). Find f(0)',4)
insert into Answer values(721,N'1',1,181)
insert into Answer values(722,N'0',0,181)
insert into Answer values(723,N'2',0,181)
insert into Answer values(724,N'3',0,181)

insert into Question values(182,N'Let y = (x - 1)(x + 2). Find dy/dx.',4)
insert into Answer values(725,N'2x+1',1,182)
insert into Answer values(726,N'2x-1',0,182)
insert into Answer values(727,N'4x-1',0,182)
insert into Answer values(728,N'4x+1',0,182)

insert into Question values(183,N'Which function has the derivative of g''(x) = 4x3 + 2x - 1',4)
insert into Answer values(729,N'g(x) = 2x^4 + 2x^2 - 1',1,183)
insert into Answer values(730,N'g(x) = x^4 + x^2 - 1',0,183)
insert into Answer values(731,N'g(x) = 2x^4 + 2x^2 ',0,183)
insert into Answer values(732,N'g(x) = 2x^4 ',0,183)


insert into Question values(184,N'What is the derivative of f(x) = 2x3 - 4x + 4?',4)
insert into Answer values(733,N'f''(x) = 6x^2 - 4x',0,184)
insert into Answer values(734,N'f''(x) = 3x^2 - 4x',0,184)
insert into Answer values(735,N'f''(x) = x^2 - 2x',0,184)
insert into Answer values(736,N'f''(x) = 6x^2 - 4',1,184)

insert into Question values(185,N'Find the derivative of the given equation f(x) = 1 - x^2',4)
insert into Answer values(737,N'1 - 2x',0,185)
insert into Answer values(738,N'2x',0,185)
insert into Answer values(739,N'-2x',1,185)
insert into Answer values(740,N'-2',0,185)


insert into Question values(186,N'Find the derivative of the given equation f''(x) = -4x',4)
insert into Answer values(741,N'-4',1,186)
insert into Answer values(742,N'-2',0,186)
insert into Answer values(743,N'2',0,186)
insert into Answer values(744,N'4',0,186)

insert into Question values(187,N'Find the derivative of the given equation f(x) = 7',4)
insert into Answer values(745,N'7',0,187)
insert into Answer values(746,N'7x',0,187)
insert into Answer values(747,N'14',0,187)
insert into Answer values(748,N'0',1,187)

insert into Question values(188,N'<p>Find the limits above<img src="https://quizizz.com/media/resource/gs/quizizz-media/quizzes/8fbc1da4-73a4-4d2a-b428-31d61a2fa88c?w=200&amp;h=200" alt="Image"></p>',4)
insert into Answer values(752,N'-1',1,188)
insert into Answer values(749,N'0',0,188)
insert into Answer values(750,N'1',0,188)
insert into Answer values(751,N'Not exist',0,188)

insert into Question values(189,N'<figure class="image"><img src="https://quizizz.com/media/resource/gs/quizizz-media/quizzes/004d9437-d7cc-1902d-8f66-9e4a42dc2834?w=200&amp;h=200" alt="Quiz image"></figure>',4)
insert into Answer values(756,N'1',1,189)
insert into Answer values(753,N'Not exist',0,189)
insert into Answer values(754,N'2',0,189)
insert into Answer values(755,N'3',0,189)

insert into Question values(190,N'<figure class="image"><img src="https://quizizz.com/media/resource/gs/quizizz-media/quizzes/4da9efa4-4ebb-4a189-bf8f-e093e04c89c2?w=200&amp;h=200" alt="Quiz image"></figure>',4)
insert into Answer values(760,N'1',1,190)
insert into Answer values(757,N'Not exist',0,190)
insert into Answer values(758,N'2',0,190)
insert into Answer values(759,N'3',0,190)
--5


insert into Question values(191,N'Chris is sitting on the edge of a dock tossing rocks into the water. As each rock hits the water, small circles appear traveling outward from the point of impact. The radius of the circle is changing at a rate of 5 in/sec.  How fast is the area of the outer circle changing when the diameter is 8 in?',5)
insert into Answer values(761,N'80pi in/sec',0,191)
insert into Answer values(762,N'76pi in/sec',1,191)
insert into Answer values(763,N'60pi in/sec',0,191)
insert into Answer values(764,N'40pi in/sec',0,191)

insert into Question values(192,N'Brandon is starting to clean up after a birthday party. He begins deflating each spherical balloon by puncturing a hole in each. The air leaves the balloon at a constant rate of 2 cm3/sec.  How fast is the diameter changing when the diameter is 8 cm?',5)
insert into Answer values(765,N'-1/(16pi) cm/sec',1,192)
insert into Answer values(766,N'-1/16 cm/sec',0,192)
insert into Answer values(767,N'-1/(4pi) cm/sec',0,192)
insert into Answer values(768,N'-1/(8pi) cm/sec',0,192)


insert into Question values(193,N'A spherical snowball melts so that its radius decreases at a rate of 4 in/sec. At what rate is the volume of the snowball changing when the radius is 4 in?',5)
insert into Answer values(769,N'-262π in3/sec',0,193)
insert into Answer values(770,N'-247π in3/sec',0,193)
insert into Answer values(771,N'-256π in3/sec',1,193)
insert into Answer values(772,N'-263π in3/sec',0,193)


insert into Question values(194,N'If a function has a second derivative that is negative, what does that tell you?',5)
insert into Answer values(773,N'The function is increasing',0,194)
insert into Answer values(774,N'The function is decreasing',0,194)
insert into Answer values(775,N'The function is concave up',0,194)
insert into Answer values(776,N'The function is concave down',1,194)


insert into Question values(195,N'If f''(x) >0, what is true about f(x)?',5)
insert into Answer values(777,N'it is concave up there',0,195)
insert into Answer values(778,N'it is concave down there',0,195)
insert into Answer values(779,N'it has an inflection point',0,195)
insert into Answer values(780,N'it is increasing',1,195)


insert into Question values(196,N'How many points of inflection does a parabola have?',5)
insert into Answer values(781,N' 0 because the concavity of a parabola never changes',1,196)
insert into Answer values(782,N'1 because there is only one critical point',0,196)
insert into Answer values(783,N'2 because parabolas increase then decrease (or vice vers',0,196)
insert into Answer values(784,N'Depends on the parabola',0,196)

insert into Question values(197,N'A rectangular solid that has a square base has a surface area of 150 square inches.  Find the maximum volume of the solid.  (use Calculus)',5)
insert into Answer values(785,N'5',0,197)
insert into Answer values(786,N'125',0,197)
insert into Answer values(787,N'150',1,197)
insert into Answer values(788,N'250',0,197)

insert into Question values(198,N'A farmer wants to construct a rectangular pigpen using 400 ft of fencing. The pen will be built next to an existing stone wall, so only three sides of fencing need to be constructed to enclose the pen. What dimensions should the farmer use to construct the pen with the largest possible area?',5)
insert into Answer values(789,N'100ft x 200ft',1,198)
insert into Answer values(790,N'102ft x 96ft',0,198)
insert into Answer values(791,N'52ft x 196ft',0,198)
insert into Answer values(792,N'50ft x 300ft',0,198)

insert into Question values(199,N'Find the differential for the function y = cos(x)',5)
insert into Answer values(793,N'dy = -sin(x) dx',0,199)
insert into Answer values(794,N'dx = -sin(x) dy',0,199)
insert into Answer values(795,N'dy = cos (x) dx',1,199)
insert into Answer values(796,N'dx = cos(x) dy',0,199)

insert into Question values(200,N'Let y = x3 Find the differential when x =2 and dx = 0.1.',5)
insert into Answer values(797,N'0.1',0,200)
insert into Answer values(798,N'0.12',0,200)
insert into Answer values(799,N'1.1',1,200)
insert into Answer values(800,N'1.2',0,200)



insert into Question values(201,N'Which of the following type of numbers are closed under only multiplication?',6)
insert into Answer values(801,N'Rational number',0,201)
insert into Answer values(802,N'Integer',0,201)
insert into Answer values(803,N'Whole number',1,201)
insert into Answer values(804,N'Natural number',0,201)

insert into Question values(202,N'<p><img src="https://quizizz.com/media/resource/gs/quizizz-media/quizzes/005d9537-d7cc-402d-8f66-9e4a42dc2835?w=900&amp;h=900" alt="Quiz image"></p>',6)
insert into Answer values(805,N'1',0,202)
insert into Answer values(806,N'2',0,202)
insert into Answer values(807,N'3',1,202)
insert into Answer values(808,N'Not exist',0,202)
insert into Question values(203,N'<p>&nbsp;</p><p>What is the limits of the function when <i>x</i> approching <i>a</i>?<img src="https://quizizz.com/media/resource/gs/quizizz-media/quizzes/820a1945-6384-4927-a442-8038d198104f?w=200&amp;h=200" alt="Quiz image"></p><p>&nbsp;</p>',6)
insert into Answer values(812,N'1',0,203)
insert into Answer values(809,N'2',1,203)
insert into Answer values(810,N'Not exist',0,203)
insert into Answer values(811,N'Undefined',0,203)
insert into Question values(204,N'<p>What is the limits of the graph above when <i>x</i> approaching 0?<img src="https://quizizz.com/media/resource/gs/quizizz-media/quizzes/b1cbcd8b-1d09-44cb-ae49-1521c8556efc?w=200&amp;h=200" alt="Image"></p>',6)
insert into Answer values(816,N'Undefined',0,204)
insert into Answer values(813,N'1',0,204)
insert into Answer values(814,N'0',0,204)
insert into Answer values(815,N'Not exist',1,204)

insert into Question values(205,N'<p>Which of the following is true?<img src="https://quizizz.com/media/resource/gs/quizizz-media/quizzes/41158ecb-fff7-4a31-acf0-02c20670c05f0?w=200&amp;h=200" alt="Image"></p>',6)
insert into Answer values(820,N'limit of f(x)=2',1,205)
insert into Answer values(817,N'limit of f(x)=-2',0,205)
insert into Answer values(818,N'limit of f(x)=0',0,205)
insert into Answer values(819,N'Not exist',0,205)
insert into Question values(206,N'<figure class="image"><img src="https://quizizz.com/media/resource/gs/quizizz-media/quizzes/3b9fdacd-0e03-4d4b-b7bd-6cd10a5e1129?w=200&amp;h=200" alt="Image"></figure>',6)
insert into Answer values(824,N'-81',0,206)
insert into Answer values(821,N'9',0,206)
insert into Answer values(822,N'-9',0,206)
insert into Answer values(823,N'81',1,206)

insert into Question values(207,N'<figure class="image"><img src="https://quizizz.com/media/resource/gs/quizizz-media/quizzes/68aef35e-b410-48a6-b637-d6d1cb91d0e1?w=200&amp;h=200" alt="Image"></figure>',6)
insert into Answer values(828,N'undefined',0,207)
insert into Answer values(825,N'0',0,207)
insert into Answer values(826,N'1',0,207)
insert into Answer values(827,N'Not exist',1,207)

insert into Question values(208,N'<p>Find the limits above<img src="https://quizizz.com/media/resource/gs/quizizz-media/quizzes/8fbc1da5-73a4-4d2a-b428-31d61a2fa88c?w=200&amp;h=200" alt="Image"></p>',6)
insert into Answer values(832,N'-1',1,208)
insert into Answer values(829,N'0',0,208)
insert into Answer values(830,N'1',0,208)
insert into Answer values(831,N'Not exist',0,208)

insert into Question values(209,N'<figure class="image"><img src="https://quizizz.com/media/resource/gs/quizizz-media/quizzes/005d9537-d7cc-402d-8f66-9e4a42dc2835?w=200&amp;h=200" alt="Quiz image"></figure>',6)
insert into Answer values(836,N'1',1,209)
insert into Answer values(833,N'Not exist',0,209)
insert into Answer values(834,N'2',0,209)
insert into Answer values(835,N'3',0,209)

insert into Question values(210,N'<figure class="image"><img src="https://quizizz.com/media/resource/gs/quizizz-media/quizzes/5da9efa5-4ebb-4a49-bf8f-e093e05c89c2?w=200&amp;h=200" alt="Quiz image"></figure>',6)
insert into Answer values(840,N'1',1,210)
insert into Answer values(837,N'Not exist',0,210)
insert into Answer values(838,N'2',0,210)
insert into Answer values(839,N'3',0,210)

/*Course 3*/
INSERT INTO [dbo].[Quiz] VALUES (7 , 'The Foundations: Logic and Proofs', 7,'https://image.slidesharecdn.com/probability-160319173307/95/probability-42-638.jpg?cb=1458409236', 15)
INSERT INTO [dbo].[Quiz] VALUES (8 , 'Basic Structures: Sets, Functions, Sequences, and Sums', 8,'https://image3.slideserve.com/5535455/basic-structures-sets-functions-sequences-and-sums-l.jpg', 15)
INSERT INTO [dbo].[Quiz] VALUES (9 , 'The Fundamentals: Algorithms, the Integers', 9,'https://sp-uploads.s3.amazonaws.com/uploads/services/5780580/20221206221247_638fbe5fa1857_03_the_fundamentals__algorithms__integerspage0.jpg', 15)

INSERT INTO [dbo].[Question]  VALUES (61 , 'What is the value of x after this statement, assuming the initial value of x is 5? ‘If x equals to one then x=x+2 else x=0’.', 7)
INSERT INTO [dbo].[Question]  VALUES (62 , 'Which of the following statements is a proposition?', 7)
INSERT INTO [dbo].[Question]  VALUES (63 , 'P: “If Sahil bowls, Saurabh hits a century.”
											Q: “If Raju bowls, Sahil gets out on the first ball.”
											If P is true and Q is false, which of the following statements is true?', 7)
INSERT INTO [dbo].[Question]  VALUES (64 , 'Which of the following statements is true?', 7)
INSERT INTO [dbo].[Question]  VALUES (65 , 'P: “We should be honest.”
											Q: “We should be dedicated.”
											R: “We should be overconfident.”
											Which logical expression best represents the statement, “We should be honest or dedicated but not overconfident”?', 7)
INSERT INTO [dbo].[Question]  VALUES (66 , 'P: “This is a great website.”
											Q: “You should not come back here.”
											Which logical expression best represents the statement, “This is a great website, and you should come back here”?', 7)
INSERT INTO [dbo].[Question]  VALUES (67 , 'P: “I am in Delhi.”
											Q: “Delhi is clean.”
											Which of the following statements corresponds to q ^ p (q and p)?', 7)
INSERT INTO [dbo].[Question]  VALUES (68 , 'P: “I am in Bangalore.”
											Q: “I love cricket.”
											Which of the following statements corresponds to q -> p (q implies p)?', 7)
INSERT INTO [dbo].[Question]  VALUES (69 , 'Which of the following bits is the negation of the bits 010110?', 7)
INSERT INTO [dbo].[Question]  VALUES (70 , 'Which of the following option is suitable, if A is 10110110, B is 11100000, and C is 10100000?', 7)

INSERT INTO [dbo].[Question]  VALUES (71 , '{x: x is a real number between 1 and 2} is a/an', 8)
INSERT INTO [dbo].[Question]  VALUES (72 , '{x: x is an integer that is neither positive nor negative} is a/an', 8)
INSERT INTO [dbo].[Question]  VALUES (73 , 'Which of the following expresses the set {1, 5, 15, 25,…} in set-builder form.', 8)
INSERT INTO [dbo].[Question]  VALUES (74 , 'Which of the following expresses {x: x= n/ (n+1), n is a natural number less than 7} in roster form?', 8)
INSERT INTO [dbo].[Question]  VALUES (75 , 'Which of the following is a subset of the set {1, 2, 3, 4}?', 8)
INSERT INTO [dbo].[Question]  VALUES (76 , 'What is the total number of elements in the power set of {a, b}, where a and b are distinct elements?', 8)
INSERT INTO [dbo].[Question]  VALUES (77 , 'Which of the following is true for A = {∅, {∅}, 2, {2, ∅}, 3}?', 8)
INSERT INTO [dbo].[Question]  VALUES (78 , 'Which of the following is a subset of the set A = { }?', 8)
INSERT INTO [dbo].[Question]  VALUES (79 , 'Which of the following expresses {x: x is a positive prime number which divides 72} in roster form?', 8)
INSERT INTO [dbo].[Question]  VALUES (80 , 'The union of the sets {1, 2, 5} and {1, 2, 6} is the set', 8)

INSERT INTO [dbo].[Question]  VALUES (81 , 'What is the worst-case time complexity of the linear search algorithm?', 9)
INSERT INTO [dbo].[Question]  VALUES (82 , 'What is the difference between a recursive algorithm and an iterative algorithm?', 9)
INSERT INTO [dbo].[Question]  VALUES (83 , 'Which of the following is NOT a sorting algorithm?', 9)
INSERT INTO [dbo].[Question]  VALUES (84 , 'What is the difference between a greedy algorithm and a dynamic programming algorithm?', 9)
INSERT INTO [dbo].[Question]  VALUES (85 , 'What is the greatest common divisor (GCD) of 12 and 18?', 9)
INSERT INTO [dbo].[Question]  VALUES (86 , 'What is the least common multiple (LCM) of 12 and 18?', 9)
INSERT INTO [dbo].[Question]  VALUES (87 , 'Which of the following is NOT a prime number?', 9)
INSERT INTO [dbo].[Question]  VALUES (88 , 'What is the fundamental theorem of arithmetic?', 9)
INSERT INTO [dbo].[Question]  VALUES (89 , 'What is Fermats Little Theorem?', 9)
INSERT INTO [dbo].[Question]  VALUES (90 , 'What is the Chinese Remainder Theorem?', 9)

INSERT INTO [dbo].[Question]  VALUES (211 , 'Which of the following is a logical connective?', 7)
INSERT INTO [dbo].[Question]  VALUES (212 , 'Which of the following is a tautology?', 7)
INSERT INTO [dbo].[Question]  VALUES (213 , 'Which of the following is a contradiction?', 7)
INSERT INTO [dbo].[Question]  VALUES (214 , 'Which of the following is equivalent to ¬(p ∧ q)?', 7)
INSERT INTO [dbo].[Question]  VALUES (215 , 'Which of the following is equivalent to ¬(p ∨ q)?', 7)
INSERT INTO [dbo].[Question]  VALUES (216 , 'Which of the following is equivalent to p → q?', 7)
INSERT INTO [dbo].[Question]  VALUES (217 , 'Which of the following is equivalent to p ↔ q?', 7)
INSERT INTO [dbo].[Question]  VALUES (218 , 'Which of the following is a valid inference rule?', 7)
INSERT INTO [dbo].[Question]  VALUES (219 , 'Which of the following is a valid proof technique?', 7)
INSERT INTO [dbo].[Question]  VALUES (220 , 'Which of the following is a valid proof technique for proving an existential statement?', 7)

INSERT INTO [dbo].[Question]  VALUES (221 , 'Which of the following is a set?', 8)
INSERT INTO [dbo].[Question]  VALUES (222 , 'Which of the following is not an element of the set {1, 2, 3}?', 8)
INSERT INTO [dbo].[Question]  VALUES (223 , 'Which of the following is a subset of the set {1, 2, 3}?', 8)
INSERT INTO [dbo].[Question]  VALUES (224 , 'Which of the following is a subset of the set {1, 2, 3}?', 8)
INSERT INTO [dbo].[Question]  VALUES (225 , 'What is the union of the sets {1, 2, 3} and {2, 3, 4}?', 8)
INSERT INTO [dbo].[Question]  VALUES (226 , 'What is the intersection of the sets {1, 2, 3} and {2, 3, 4}?', 8)
INSERT INTO [dbo].[Question]  VALUES (227 , 'What is the intersection of the sets {1, 2, 3} and {2, 3, 4}?', 8)
INSERT INTO [dbo].[Question]  VALUES (228 , 'Which of the following is a function?', 8)
INSERT INTO [dbo].[Question]  VALUES (229 , 'What is the domain of the function f(x) = x^2?', 8)
INSERT INTO [dbo].[Question]  VALUES (230 , 'What is the domain of the function f(x) = x^2?', 8)

INSERT INTO [dbo].[Question]  VALUES (231 , 'Which of the following is an algorithm?', 9)
INSERT INTO [dbo].[Question]  VALUES (232 , 'Which of the following is not a property of an algorithm?', 9)
INSERT INTO [dbo].[Question]  VALUES (233 , 'Which of the following is not a basic operation in computer algorithms?', 9)
INSERT INTO [dbo].[Question]  VALUES (234 , 'What is the greatest common divisor of 24 and 36?', 9)
INSERT INTO [dbo].[Question]  VALUES (235 , 'What is the least common multiple of 24 and 36?', 9)
INSERT INTO [dbo].[Question]  VALUES (236 , 'Which of the following is not a prime number?', 9)
INSERT INTO [dbo].[Question]  VALUES (237 , 'What is the prime factorization of 60?', 9)
INSERT INTO [dbo].[Question]  VALUES (238 , 'What is the binary representation of the decimal number 13?', 9)
INSERT INTO [dbo].[Question]  VALUES (239 , 'What is the hexadecimal representation of the decimal number 255?', 9)
INSERT INTO [dbo].[Question]  VALUES (240 , 'What is the octal representation of the decimal number 64?', 9)


INSERT INTO [dbo].[Answer] VALUES (241 ,'0' ,1 ,61)
INSERT INTO [dbo].[Answer] VALUES (242 ,'1' ,0 ,61)
INSERT INTO [dbo].[Answer] VALUES (243 ,'2' ,0 ,61)
INSERT INTO [dbo].[Answer] VALUES (244 ,'3' ,0 ,61)

INSERT INTO [dbo].[Answer] VALUES (245 ,'The only odd prime number is 2.' ,1 ,62)
INSERT INTO [dbo].[Answer] VALUES (246 ,'Get me a glass of milkshake.' ,0 ,62)
INSERT INTO [dbo].[Answer] VALUES (247 ,'God bless you!' ,0 ,62)
INSERT INTO [dbo].[Answer] VALUES (248 ,'What is the time now?' ,0 ,62)

INSERT INTO [dbo].[Answer] VALUES (249 ,'“Sahil bowled and Saurabh hits a century.”' ,1 ,63)
INSERT INTO [dbo].[Answer] VALUES (250 ,'“Raju bowled and Sahil got out on first ball.”' ,0 ,63)
INSERT INTO [dbo].[Answer] VALUES (251 ,'“Raju did not bowl.”' ,0 ,63)
INSERT INTO [dbo].[Answer] VALUES (252 ,'“Sahil bowled and Saurabh got out.”' ,0 ,63)

INSERT INTO [dbo].[Answer] VALUES (253 ,'If the Sun is a planet, elephants will fly' ,1 ,64)
INSERT INTO [dbo].[Answer] VALUES (254 ,'3 + 2 = 8 if 5 - 2 = 7' ,0 ,64)
INSERT INTO [dbo].[Answer] VALUES (255 ,'1 > 3 and 3 is a positive integer' ,0 ,64)
INSERT INTO [dbo].[Answer] VALUES (256 ,'-2 > 3 or 3 is a negative integer' ,0 ,64)

INSERT INTO [dbo].[Answer] VALUES (257 ,'P V Q ∧ ~R' ,1 ,65)
INSERT INTO [dbo].[Answer] VALUES (258 ,'~P V ~Q V R' ,0 ,65)
INSERT INTO [dbo].[Answer] VALUES (259 ,'P ∧ ~Q ∧ R' ,0 ,65)
INSERT INTO [dbo].[Answer] VALUES (260 ,'P V Q ∧ R' ,0 ,65)

INSERT INTO [dbo].[Answer] VALUES (261 ,'P ∧ ~Q' ,1 ,66)
INSERT INTO [dbo].[Answer] VALUES (262 ,'~P V ~Q' ,0 ,66)
INSERT INTO [dbo].[Answer] VALUES (263 ,'P V Q' ,0 ,66)
INSERT INTO [dbo].[Answer] VALUES (264 ,'P ∧ Q' ,0 ,66)

INSERT INTO [dbo].[Answer] VALUES (265 ,'“Delhi is clean and I am in Delhi.”' ,1 ,67)
INSERT INTO [dbo].[Answer] VALUES (266 ,'“Delhi is not clean or I am in Delhi.”' ,0 ,67)
INSERT INTO [dbo].[Answer] VALUES (267 ,'“I am in Delhi and Delhi is not clean.”' ,0 ,67)
INSERT INTO [dbo].[Answer] VALUES (268 ,'“Delhi is clean but I am in Mumbai.”' ,0 ,67)

INSERT INTO [dbo].[Answer] VALUES (269 ,'“If I love cricket, then I am in Bangalore.”' ,1 ,68)
INSERT INTO [dbo].[Answer] VALUES (270 ,'“If I am in Bangalore, then I love cricket.”' ,0 ,68)
INSERT INTO [dbo].[Answer] VALUES (271 ,'“I am not in Bangalore.”' ,0 ,68)
INSERT INTO [dbo].[Answer] VALUES (272 ,'“I love cricket.”' ,0 ,68)

INSERT INTO [dbo].[Answer] VALUES (273 ,'101001' ,1 ,69)
INSERT INTO [dbo].[Answer] VALUES (274 ,'111001' ,0 ,69)
INSERT INTO [dbo].[Answer] VALUES (275 ,'001001' ,0 ,69)
INSERT INTO [dbo].[Answer] VALUES (276 ,'111111' ,0 ,69)

INSERT INTO [dbo].[Answer] VALUES (277 ,'C = A and B' ,1 ,70)
INSERT INTO [dbo].[Answer] VALUES (278 ,'C = ~B' ,0 ,70)
INSERT INTO [dbo].[Answer] VALUES (279 ,'C = ~A' ,0 ,70)
INSERT INTO [dbo].[Answer] VALUES (280 ,'C = A or B' ,0 ,70)

INSERT INTO [dbo].[Answer] VALUES (281 ,'Infinite set' ,1 ,71)
INSERT INTO [dbo].[Answer] VALUES (282 ,'Finite set' ,0 ,71)
INSERT INTO [dbo].[Answer] VALUES (283 ,'Empty set' ,0 ,71)
INSERT INTO [dbo].[Answer] VALUES (284 ,'None of the above' ,0 ,71)

INSERT INTO [dbo].[Answer] VALUES (285 ,'Non-empty and finite set' ,1 ,72)
INSERT INTO [dbo].[Answer] VALUES (286 ,'Non-empty set' ,0 ,72)
INSERT INTO [dbo].[Answer] VALUES (287 ,'Empty set' ,0 ,72)
INSERT INTO [dbo].[Answer] VALUES (288 ,'Finite set' ,0 ,72)

INSERT INTO [dbo].[Answer] VALUES (289 ,'{x: either x=1 or x=5n, where n is an odd natural number}' ,1 ,73)
INSERT INTO [dbo].[Answer] VALUES (290 ,'{x: either x=1 or x=5n, where n is a real number}' ,0 ,73)
INSERT INTO [dbo].[Answer] VALUES (291 ,'{x: either x=1 or x=5n, where n is a integer}' ,0 ,73)
INSERT INTO [dbo].[Answer] VALUES (292 ,'{x: x=5n, where n is a natural number}' ,0 ,73)

INSERT INTO [dbo].[Answer] VALUES (293 ,'{1⁄2, 2⁄3, 3⁄4, 4⁄5, 5⁄6, 6⁄7}' ,1 ,74)
INSERT INTO [dbo].[Answer] VALUES (294 ,'{1⁄2, 2⁄3, 4⁄5, 6⁄7}' ,0 ,74)
INSERT INTO [dbo].[Answer] VALUES (295 ,'{1⁄2, 2⁄3, 3⁄4, 4⁄5, 5⁄6, 6⁄7, 7⁄8}' ,0 ,74)
INSERT INTO [dbo].[Answer] VALUES (296 ,'Infinite set' ,0 ,74)

INSERT INTO [dbo].[Answer] VALUES (297 ,'All of the above' ,1 ,75)
INSERT INTO [dbo].[Answer] VALUES (298 ,'{1, 2}' ,0 ,75)
INSERT INTO [dbo].[Answer] VALUES (299 ,'{1, 2, 3}' ,0 ,75)
INSERT INTO [dbo].[Answer] VALUES (300 ,'{1}' ,0 ,75)

INSERT INTO [dbo].[Answer] VALUES (301 ,'4' ,1 ,76)
INSERT INTO [dbo].[Answer] VALUES (302 ,'2' ,0 ,76)
INSERT INTO [dbo].[Answer] VALUES (303 ,'3' ,0 ,76)
INSERT INTO [dbo].[Answer] VALUES (304 ,'5' ,0 ,76)

INSERT INTO [dbo].[Answer] VALUES (305 ,'∅ ⊂ A' ,1 ,77)
INSERT INTO [dbo].[Answer] VALUES (306 ,'{{∅,{∅}} ∈ A' ,0 ,77)
INSERT INTO [dbo].[Answer] VALUES (307 ,'{2} ∈ A' ,0 ,77)
INSERT INTO [dbo].[Answer] VALUES (308 ,'3 ⊂ A' ,0 ,77)

INSERT INTO [dbo].[Answer] VALUES (309 ,'All of the above' ,1 ,78)
INSERT INTO [dbo].[Answer] VALUES (310 ,'A' ,0 ,78)
INSERT INTO [dbo].[Answer] VALUES (311 ,'{}' ,0 ,78)
INSERT INTO [dbo].[Answer] VALUES (312 ,'∅' ,0 ,78)

INSERT INTO [dbo].[Answer] VALUES (313 ,'{2, 3}' ,1 ,79)
INSERT INTO [dbo].[Answer] VALUES (314 ,'{2, 3, 5}' ,0 ,79)
INSERT INTO [dbo].[Answer] VALUES (315 ,'{2, 3, 6}' ,0 ,79)
INSERT INTO [dbo].[Answer] VALUES (316 ,'{∅}' ,0 ,79)

INSERT INTO [dbo].[Answer] VALUES (317 ,'{1, 2, 5, 6}' ,1 ,80)
INSERT INTO [dbo].[Answer] VALUES (318 ,'{1, 2, 6, 1}' ,0 ,80)
INSERT INTO [dbo].[Answer] VALUES (319 ,'{1, 2, 1, 2}' ,0 ,80)
INSERT INTO [dbo].[Answer] VALUES (320 ,'{1, 5, 6, 3}' ,0 ,80)

INSERT INTO [dbo].[Answer] VALUES (321 ,'O(n)' ,1 ,81)
INSERT INTO [dbo].[Answer] VALUES (322 ,'O(1)' ,0 ,81)
INSERT INTO [dbo].[Answer] VALUES (323 ,'O(log n)' ,0 ,81)
INSERT INTO [dbo].[Answer] VALUES (324 ,'O(n^2)' ,0 ,81)

INSERT INTO [dbo].[Answer] VALUES (325 ,'A recursive algorithm calls itself, while an iterative algorithm does not.' ,1 ,82)
INSERT INTO [dbo].[Answer] VALUES (326 ,'A recursive algorithm uses a stack, while an iterative algorithm uses a queue.' ,0 ,82)
INSERT INTO [dbo].[Answer] VALUES (327 ,'A recursive algorithm is always more efficient than an iterative algorithm.' ,0 ,82)
INSERT INTO [dbo].[Answer] VALUES (328 ,'None of the above.' ,0 ,82)

INSERT INTO [dbo].[Answer] VALUES (329 ,'Hash table' ,1 ,83)
INSERT INTO [dbo].[Answer] VALUES (330 ,'Merge sort' ,0 ,83)
INSERT INTO [dbo].[Answer] VALUES (331 ,'Quicksort' ,0 ,83)
INSERT INTO [dbo].[Answer] VALUES (332 ,'Selection sort' ,0 ,83)

INSERT INTO [dbo].[Answer] VALUES (333 ,'A greedy algorithm makes the best choice at each step, while a dynamic programming algorithm considers all possible choices.' ,1 ,84)
INSERT INTO [dbo].[Answer] VALUES (334 ,'A greedy algorithm is always more efficient than a dynamic programming algorithm.' ,0 ,84)
INSERT INTO [dbo].[Answer] VALUES (335 ,'A greedy algorithm is only suitable for problems with optimal substructure, while a dynamic programming algorithm is suitable for any problem.' ,0 ,84)
INSERT INTO [dbo].[Answer] VALUES (336 ,'None of the above.' ,0 ,84)

INSERT INTO [dbo].[Answer] VALUES (337 ,'2' ,1 ,85)
INSERT INTO [dbo].[Answer] VALUES (338 ,'6' ,0 ,85)
INSERT INTO [dbo].[Answer] VALUES (339 ,'3' ,0 ,85)
INSERT INTO [dbo].[Answer] VALUES (340 ,'1' ,0 ,85)

INSERT INTO [dbo].[Answer] VALUES (341 ,'72' ,1 ,86)
INSERT INTO [dbo].[Answer] VALUES (342 ,'36' ,0 ,86)
INSERT INTO [dbo].[Answer] VALUES (343 ,'218' ,0 ,86)
INSERT INTO [dbo].[Answer] VALUES (344 ,'648' ,0 ,86)

INSERT INTO [dbo].[Answer] VALUES (345 ,'20' ,1 ,87)
INSERT INTO [dbo].[Answer] VALUES (346 ,'11' ,0 ,87)
INSERT INTO [dbo].[Answer] VALUES (347 ,'13' ,0 ,87)
INSERT INTO [dbo].[Answer] VALUES (348 ,'19' ,0 ,87)

INSERT INTO [dbo].[Answer] VALUES (349 ,'Every integer greater than 1 can be expressed as a product of prime numbers.' ,1 ,88)
INSERT INTO [dbo].[Answer] VALUES (350 ,'Every prime number can be expressed as a product of two smaller prime numbers.' ,0 ,88)
INSERT INTO [dbo].[Answer] VALUES (351 ,'The sum of two prime numbers is always prime.' ,0 ,88)
INSERT INTO [dbo].[Answer] VALUES (352 ,'The difference of two prime numbers is always even.' ,0 ,88)

INSERT INTO [dbo].[Answer] VALUES (353 ,'If p is a prime number and a is not divisible by p, then a^p - 1 is divisible by p.' ,1 ,89)
INSERT INTO [dbo].[Answer] VALUES (354 ,'If p is a prime number and a is divisible by p, then a^p - 1 is not divisible by p.' ,0 ,89)
INSERT INTO [dbo].[Answer] VALUES (355 ,'If p is a prime number and a is not divisible by p, then a^p - 1 is not divisible by p.' ,0 ,89)
INSERT INTO [dbo].[Answer] VALUES (356 ,'If p is a prime number and a is divisible by p, then a^p - 1 is divisible by p.' ,0 ,89)

INSERT INTO [dbo].[Answer] VALUES (357 ,'A theorem that states that there is a unique solution to the system of congruences x ≡ a_1 (mod m_1), x ≡ a_2 (mod m_2), ..., x ≡ a_n (mod m_n), where the m_i s are pairwise relatively prime.' ,1 ,90)
INSERT INTO [dbo].[Answer] VALUES (358 ,'A theorem that states that the sum of two prime numbers is always prime.' ,0 ,90)
INSERT INTO [dbo].[Answer] VALUES (359 ,'A theorem that states that the difference of two prime numbers is always even.' ,0 ,90)
INSERT INTO [dbo].[Answer] VALUES (360 ,' None of the above.' ,0 ,90)

INSERT INTO [dbo].[Answer] VALUES (841 ,'and' ,1 ,211)
INSERT INTO [dbo].[Answer] VALUES (842 ,'x' ,0 ,211)
INSERT INTO [dbo].[Answer] VALUES (843 ,'2' ,0 ,211)
INSERT INTO [dbo].[Answer] VALUES (844 ,'3' ,0 ,211)

INSERT INTO [dbo].[Answer] VALUES (845 ,'p ∨ ¬p' ,1 ,212)
INSERT INTO [dbo].[Answer] VALUES (846 ,'p ∧ ¬p' ,0 ,212)
INSERT INTO [dbo].[Answer] VALUES (847 ,'p ↔ ¬p' ,0 ,212)
INSERT INTO [dbo].[Answer] VALUES (848 ,'p → ¬p' ,0 ,212)

INSERT INTO [dbo].[Answer] VALUES (849 ,'p ∧ ¬p' ,1 ,213)
INSERT INTO [dbo].[Answer] VALUES (850 ,'p ∨ ¬p' ,0 ,213)
INSERT INTO [dbo].[Answer] VALUES (851 ,'p ↔ ¬p' ,0 ,213)
INSERT INTO [dbo].[Answer] VALUES (852 ,'p → ¬p' ,0 ,213)

INSERT INTO [dbo].[Answer] VALUES (853 ,'¬p ∨ ¬q' ,1 ,214)
INSERT INTO [dbo].[Answer] VALUES (854 ,'¬p ∧ ¬q' ,0 ,214)
INSERT INTO [dbo].[Answer] VALUES (855 ,'p ∧ ¬q' ,0 ,214)
INSERT INTO [dbo].[Answer] VALUES (856 ,'p ∨ ¬q' ,0 ,214)

INSERT INTO [dbo].[Answer] VALUES (857 ,'¬p ∧ ¬q' ,1 ,215)
INSERT INTO [dbo].[Answer] VALUES (858 ,'¬p ∨ ¬q' ,0 ,215)
INSERT INTO [dbo].[Answer] VALUES (859 ,'p ∧ ¬q' ,0 ,215)
INSERT INTO [dbo].[Answer] VALUES (860 ,'p ∨ ¬q' ,0 ,215)

INSERT INTO [dbo].[Answer] VALUES (861 ,'¬p ∨ q' ,1 ,216)
INSERT INTO [dbo].[Answer] VALUES (862 ,'p ∧ q' ,0 ,216)
INSERT INTO [dbo].[Answer] VALUES (863 ,'¬p ∧ ¬q' ,0 ,216)
INSERT INTO [dbo].[Answer] VALUES (864 ,'p ∨ ¬q' ,0 ,216)

INSERT INTO [dbo].[Answer] VALUES (865 ,'(p → q) ∧ (q → p)' ,1 ,217)
INSERT INTO [dbo].[Answer] VALUES (866 ,'(p ∧ q) ∨ (¬p ∧ ¬q)' ,0 ,217)
INSERT INTO [dbo].[Answer] VALUES (867 ,'(p ∧ ¬q) ∨ (¬p ∧ q)' ,0 ,217)
INSERT INTO [dbo].[Answer] VALUES (868 ,'(p ∨ q) ∧ (¬p ∨ ¬q)' ,0 ,217)

INSERT INTO [dbo].[Answer] VALUES (869 ,'All of the above' ,1 ,218)
INSERT INTO [dbo].[Answer] VALUES (870 ,'Modus Ponens' ,0 ,218)
INSERT INTO [dbo].[Answer] VALUES (871 ,'Modus Tollens' ,0 ,218)
INSERT INTO [dbo].[Answer] VALUES (872 ,'Disjunctive Syllogism' ,0 ,218)

INSERT INTO [dbo].[Answer] VALUES (873 ,'All of the above' ,1 ,219)
INSERT INTO [dbo].[Answer] VALUES (874 ,'Direct Proof' ,0 ,219)
INSERT INTO [dbo].[Answer] VALUES (875 ,'Proof by Contradiction' ,0 ,219)
INSERT INTO [dbo].[Answer] VALUES (876 ,'Proof by Contrapositive' ,0 ,219)

INSERT INTO [dbo].[Answer] VALUES (877 ,'Proof by Construction' ,1 ,220)
INSERT INTO [dbo].[Answer] VALUES (878 ,'Direct Proof' ,0 ,220)
INSERT INTO [dbo].[Answer] VALUES (879 ,'Proof by Contradiction' ,0 ,220)
INSERT INTO [dbo].[Answer] VALUES (880 ,'Proof by Contrapositive' ,0 ,220)

INSERT INTO [dbo].[Answer] VALUES (881 ,'{2}' ,1 ,221)
INSERT INTO [dbo].[Answer] VALUES (882 ,'2' ,0 ,221)
INSERT INTO [dbo].[Answer] VALUES (883 ,'(2)' ,0 ,221)
INSERT INTO [dbo].[Answer] VALUES (884 ,'[2]' ,0 ,221)

INSERT INTO [dbo].[Answer] VALUES (885 ,'4' ,1 ,222)
INSERT INTO [dbo].[Answer] VALUES (886 ,'1' ,0 ,222)
INSERT INTO [dbo].[Answer] VALUES (887 ,'2' ,0 ,222)
INSERT INTO [dbo].[Answer] VALUES (888 ,'3' ,0 ,222)

INSERT INTO [dbo].[Answer] VALUES (889 ,'{2, 3}' ,1 ,223)
INSERT INTO [dbo].[Answer] VALUES (890 ,'{1, 2, 4}' ,0 ,223)
INSERT INTO [dbo].[Answer] VALUES (891 ,'{1, 2, 3, 4}' ,0 ,223)
INSERT INTO [dbo].[Answer] VALUES (892 ,'{4}' ,0 ,223)

INSERT INTO [dbo].[Answer] VALUES (893 ,'{1, 2, 3, 4}' ,1 ,224)
INSERT INTO [dbo].[Answer] VALUES (894 ,'{2, 3}' ,0 ,224)
INSERT INTO [dbo].[Answer] VALUES (895 ,'{1, 2, 3}' ,0 ,224)
INSERT INTO [dbo].[Answer] VALUES (896 ,'{2, 3, 2, 3, 4}' ,0 ,224)

INSERT INTO [dbo].[Answer] VALUES (897 ,'{2, 3}' ,1 ,225)
INSERT INTO [dbo].[Answer] VALUES (898 ,'{1, 2, 3, 4}' ,0 ,225)
INSERT INTO [dbo].[Answer] VALUES (899 ,' {1, 2, 3}' ,0 ,225)
INSERT INTO [dbo].[Answer] VALUES (900 ,'{}' ,0 ,225)

INSERT INTO [dbo].[Answer] VALUES (901 ,'{(1, 2), (2, 3), (3, 4)}' ,1 ,226)
INSERT INTO [dbo].[Answer] VALUES (902 ,'{(1, 2), (2, 3), (2, 4)}' ,0 ,226)
INSERT INTO [dbo].[Answer] VALUES (903 ,'{(1, 2), (2, 3), (3, 2)}' ,0 ,226)
INSERT INTO [dbo].[Answer] VALUES (904 ,'{(1, 2), (2, 3), (4, 5)}' ,0 ,226)

INSERT INTO [dbo].[Answer] VALUES (905 ,'All real numbers' ,1 ,227)
INSERT INTO [dbo].[Answer] VALUES (906 ,'All positive real numbers' ,0 ,227)
INSERT INTO [dbo].[Answer] VALUES (907 ,'All negative real numbers' ,0 ,227)
INSERT INTO [dbo].[Answer] VALUES (908 ,'All negative real numbers' ,0 ,227)

INSERT INTO [dbo].[Answer] VALUES (909 ,'All non-negative real numbers' ,1 ,228)
INSERT INTO [dbo].[Answer] VALUES (910 ,'All real numbers' ,0 ,228)
INSERT INTO [dbo].[Answer] VALUES (911 ,'All positive real numbers' ,0 ,228)
INSERT INTO [dbo].[Answer] VALUES (912 ,'All negative real numbers' ,0 ,228)

INSERT INTO [dbo].[Answer] VALUES (913 ,'3n - 1' ,1 ,229)
INSERT INTO [dbo].[Answer] VALUES (914 ,'2n' ,0 ,229)
INSERT INTO [dbo].[Answer] VALUES (915 ,'2n' ,0 ,229)
INSERT INTO [dbo].[Answer] VALUES (916 ,'2n + 1' ,0 ,229)

INSERT INTO [dbo].[Answer] VALUES (917 ,'4094' ,1 ,230)
INSERT INTO [dbo].[Answer] VALUES (918 ,'1022' ,0 ,230)
INSERT INTO [dbo].[Answer] VALUES (919 ,'2046' ,0 ,230)
INSERT INTO [dbo].[Answer] VALUES (920 ,'8190' ,0 ,230)

INSERT INTO [dbo].[Answer] VALUES (921 ,'Sorting a list of numbers in ascending order' ,1 ,231)
INSERT INTO [dbo].[Answer] VALUES (922 ,' Finding the square root of a number' ,0 ,231)
INSERT INTO [dbo].[Answer] VALUES (923 ,'Solving a system of linear equations' ,0 ,231)
INSERT INTO [dbo].[Answer] VALUES (924 ,'All of the above' ,0 ,231)

INSERT INTO [dbo].[Answer] VALUES (925 ,'Ambiguity' ,1 ,232)
INSERT INTO [dbo].[Answer] VALUES (926 ,'Finiteness' ,0 ,232)
INSERT INTO [dbo].[Answer] VALUES (927 ,'Definiteness' ,0 ,232)
INSERT INTO [dbo].[Answer] VALUES (928 ,'Effectiveness' ,0 ,232)

INSERT INTO [dbo].[Answer] VALUES (929 ,'Differentiation' ,1 ,233)
INSERT INTO [dbo].[Answer] VALUES (930 ,'Assignment' ,0 ,233)
INSERT INTO [dbo].[Answer] VALUES (931 ,'Comparison' ,0 ,233)
INSERT INTO [dbo].[Answer] VALUES (932 ,' Looping' ,0 ,233)

INSERT INTO [dbo].[Answer] VALUES (933 ,'12' ,1 ,234)
INSERT INTO [dbo].[Answer] VALUES (934 ,'1' ,0 ,234)
INSERT INTO [dbo].[Answer] VALUES (935 ,'2' ,0 ,234)
INSERT INTO [dbo].[Answer] VALUES (936 ,'3' ,0 ,234)

INSERT INTO [dbo].[Answer] VALUES (937 ,'144' ,1 ,235)
INSERT INTO [dbo].[Answer] VALUES (938 ,'24' ,0 ,235)
INSERT INTO [dbo].[Answer] VALUES (939 ,'36' ,0 ,235)
INSERT INTO [dbo].[Answer] VALUES (940 ,'72' ,0 ,235)

INSERT INTO [dbo].[Answer] VALUES (941 ,'4' ,1 ,236)
INSERT INTO [dbo].[Answer] VALUES (942 ,'2' ,0 ,236)
INSERT INTO [dbo].[Answer] VALUES (943 ,'3' ,0 ,236)
INSERT INTO [dbo].[Answer] VALUES (944 ,'5' ,0 ,236)

INSERT INTO [dbo].[Answer] VALUES (945 ,'2^2 x 3^1 x 5^1' ,1 ,237)
INSERT INTO [dbo].[Answer] VALUES (946 ,'2^1 x 3^1 x 5^1' ,0 ,237)
INSERT INTO [dbo].[Answer] VALUES (947 ,'2^2 x 3^1 x 5^2' ,0 ,237)
INSERT INTO [dbo].[Answer] VALUES (948 ,'2^3 x 3^1 x 5^1' ,0 ,237)

INSERT INTO [dbo].[Answer] VALUES (949 ,'1101' ,1 ,238)
INSERT INTO [dbo].[Answer] VALUES (950 ,'1010' ,0 ,238)
INSERT INTO [dbo].[Answer] VALUES (951 ,'1110' ,0 ,238)
INSERT INTO [dbo].[Answer] VALUES (952 ,'10011' ,0 ,238)

INSERT INTO [dbo].[Answer] VALUES (953 ,'FF' ,1 ,239)
INSERT INTO [dbo].[Answer] VALUES (954 ,'EE' ,0 ,239)
INSERT INTO [dbo].[Answer] VALUES (955 ,'DD' ,0 ,239)
INSERT INTO [dbo].[Answer] VALUES (956 ,'CC' ,0 ,239)

INSERT INTO [dbo].[Answer] VALUES (957 ,'100' ,1 ,240)
INSERT INTO [dbo].[Answer] VALUES (958 ,'110' ,0 ,240)
INSERT INTO [dbo].[Answer] VALUES (959 ,'120' ,0 ,240)
INSERT INTO [dbo].[Answer] VALUES (960 ,'130' ,0 ,240)

/*Course 4 */

/*CHAPTER */
INSERT INTO Quiz VALUES (10, N'Basics web app', 10,'https://kissflow.com/hubfs/How%20to%20Build%20a%20Web%20App.png', 15)
INSERT INTO Quiz VALUES (11, N'MVC model and servlet', 11,'https://aptech.fpt.edu.vn/wp-content/uploads/2022/12/giai-ma-mvc-la-gi.jpg', 15)
INSERT INTO Quiz VALUES (12, N'JSTL dictionary', 12,'https://images.velog.io/images/neulring/post/a7511ced-bdee-4e7c-adb4-453bba4c26be/JSTL.png', 15)
/*quiz id 10 */
INSERT INTO Question VALUES (91, N'_____ provides a way to identify a user across more than one page request or visit to a Web site and to store information about that user.', 10)
INSERT INTO Question VALUES (92, N'Which of the following directories are legal locations for the deployment descriptor file? Note that all paths are shown as from the root of the machine or drive.
Select one:', 10)
INSERT INTO Question VALUES (93, N'Name the default value of the scope attribute of <jsp:useBean>', 10)
INSERT INTO Question VALUES (94, N'The HttpServletRequest has methods by which you can find out about incoming information such as:', 10)
INSERT INTO Question VALUES (95, N'Which of the following statement is correct?', 10)
INSERT INTO Question VALUES (96, N'... is the well-known host name that refers to your own computer.', 10)
INSERT INTO Question VALUES (97, N'Which of the following elements defines the properties of an attribute that a tag needs?', 10)
INSERT INTO Question VALUES (98, N'Which interface and method should be used to retrieve a servlet initialization parameter value?', 10)
INSERT INTO Question VALUES (99, N'Which of the given jsp statement is equivalent to: =userbean.getAge(). Assume that userbean.getAge() returns an integer.', 10)
INSERT INTO Question VALUES (100, N'Which of the following is NOT a valid attribute for a useBean tag?', 10)
/*quiz id 11 */
INSERT INTO Question VALUES (101, N'You need to make sure that the response stream of your web application is secure. Which factor will you look at?', 11)
INSERT INTO Question VALUES (102, N'Which is NOT a standard technique for a session be definitely invalidated?', 11)
INSERT INTO Question VALUES (103, N'Which statements are BEST describe include directive of JSP file?', 11)
INSERT INTO Question VALUES (104, N'A JSP page uses the java.util.ArrayList class many times. Instead of referring the class by its complete package name each time, we want to just use ArrayList. Which attribute of page directive must be specified to achieve this.', 11)
INSERT INTO Question VALUES (105, N'Browsers typically cache the servers response to a POST request.', 11)
INSERT INTO Question VALUES (106, N'Which security mechanism limits access to the availability of resources to permitted groups of users or programs?', 11)
INSERT INTO Question VALUES (107, N'Identify correct statement about a WAR file:', 11)
INSERT INTO Question VALUES (108, N'Your web application logs a user in when she supplies username/password. At that time a session is created for the user. Your want to let the user to be logged in only for 20 minutes. The application should redirect the user to the login page upon any request after 20 minutes of activity. Which of the following HttpSession methods would be helpful to you for implementing this functionality?', 11)
INSERT INTO Question VALUES (109, N'Which statements are BEST describe page attribute of <jsp:include page = "..."> Action?', 11)
INSERT INTO Question VALUES (110, N'Which of the following statements are correct about HTTP Basic authentication mechanism?', 11)
/*quiz id 12 */
INSERT INTO Question VALUES (111, N'Which of the following statement is correct?', 12)
INSERT INTO Question VALUES (112, N'In which of the following cases will the method doEndTag() of a tag handler be invoked?', 12)
INSERT INTO Question VALUES (113, N'Which of the following classes define the methods setStatus(...) and sendError(...) used to send an HTTP error back to the browser?.', 12)
INSERT INTO Question VALUES (114, N'Which of the following file is the correct name and location of deployment descriptor of a web application? Assume that the web application is rooted at \doc-root.', 12)
INSERT INTO Question VALUES (115, N'Which directory is legal location for the deployment descriptor file? Note that all paths are shown as from the root of the web application directory', 12)
INSERT INTO Question VALUES (116, N'Your jsp page uses java.util.TreeMap. Adding which of the following statement will ensure that this class is available to the page?', 12)
INSERT INTO Question VALUES (117, N'A bean present in the page and identified as "mybean" has a property named "name". Which of the following is a correct way to print the value of this property?', 12)
INSERT INTO Question VALUES (118, N'Which of these is true about include directive?', 12)
INSERT INTO Question VALUES (119, N'Identify the method used to get an object available in a session.', 12)
INSERT INTO Question VALUES (120, N'A JSP file uses a tag as <myTaglib:myTag>. The myTag element here should be defined in the which element of the taglib element in the tag library descriptor file?', 12)

/*answer*/
INSERT INTO Answer VALUES (361, N' Cookie ',0, 91)
INSERT INTO Answer VALUES (362, N' Hidden Field ',0, 91)
INSERT INTO Answer VALUES (363, N' Session management ',1, 91)
INSERT INTO Answer VALUES (364, N' URL Rewrite ',0, 91)

INSERT INTO Answer VALUES (365, N' /WEB-INF ',1, 92)
INSERT INTO Answer VALUES (366, N' /WEB-INF/xml ',0, 92)
INSERT INTO Answer VALUES (367, N' /WEB-INF/classes ',0, 92)
INSERT INTO Answer VALUES (368, N' None of others ',0, 92)

INSERT INTO Answer VALUES (369, N' request ',0, 93)
INSERT INTO Answer VALUES (370, N' page ',1, 93)
INSERT INTO Answer VALUES (371, N' application ',0, 93)
INSERT INTO Answer VALUES (372, N' session ',0, 93)

INSERT INTO Answer VALUES (373, N' form data ',0, 94)
INSERT INTO Answer VALUES (374, N' cookies ',0, 94)
INSERT INTO Answer VALUES (375, N' HTTP request headers ',0, 94)
INSERT INTO Answer VALUES (376, N' All of the others ',1, 94)

INSERT INTO Answer VALUES (377, N' Authentication means determining whether one has access to a particular resource or not. ',0, 95)
INSERT INTO Answer VALUES (378, N' Data Integrity means that the data cannot be viewed by anybody other than its intended recepient. ',0, 95)
INSERT INTO Answer VALUES (379, N' Data Integrity means that the data is not modified in transit between the sender and the receiver. ',1, 95)
INSERT INTO Answer VALUES (380, N' All of the others ',0, 95)

INSERT INTO Answer VALUES (381, N' localhost ',1, 96)
INSERT INTO Answer VALUES (382, N' ip ',0, 96)
INSERT INTO Answer VALUES (383, N' DNS ',0, 96)
INSERT INTO Answer VALUES (384, N' computer name ',0, 96)

INSERT INTO Answer VALUES (385, N' attribute ',1, 97)
INSERT INTO Answer VALUES (386, N' tag-attribute-type ',0, 97)
INSERT INTO Answer VALUES (387, N' attribute-type ',0, 97)
INSERT INTO Answer VALUES (388, N' tag-attribute ',0, 97)

INSERT INTO Answer VALUES (389, N' ServletConfig: getInitParameter(String name) ',1, 98)
INSERT INTO Answer VALUES (390, N' ServletConfig: getParameter(String name) ',0, 98)
INSERT INTO Answer VALUES (391, N' ServletConfig: getInitParameterNames(String name) ',0, 98)
INSERT INTO Answer VALUES (392, N' SevletContext: getInitParameter(String name) ',0, 98)

INSERT INTO Answer VALUES (393, N' jsp:getProperty name="userbean" method="getAge" ',0, 99)
INSERT INTO Answer VALUES (394, N' jsp:getProperty name="userbean" property="Age" ',0, 99)
INSERT INTO Answer VALUES (395, N' jsp getProperty name="userbean" property="getAge" ',0, 99)
INSERT INTO Answer VALUES (396, N' jsp:getProperty name="userbean" property="age" ',1, 99)

INSERT INTO Answer VALUES (397, N' scope ',0, 100)
INSERT INTO Answer VALUES (398, N' beanName ',0, 100)
INSERT INTO Answer VALUES (399, N' claasName ',1, 100)
INSERT INTO Answer VALUES (400, N' Non of the other ',0, 100)

INSERT INTO Answer VALUES (401, N' authorization ',0, 101)
INSERT INTO Answer VALUES (402, N' authentication ',0, 101)
INSERT INTO Answer VALUES (403, N' data integrity ',1, 101)
INSERT INTO Answer VALUES (404, N' All of above ',0, 101)

INSERT INTO Answer VALUES (405, N' The container is shutdown and brought up again. ',0, 102)
INSERT INTO Answer VALUES (406, N' If the session time out is set to 0 using setMaxInactiveInterval() method. ',1, 102)
INSERT INTO Answer VALUES (407, N' No request comes from the client for more than "session timeout" period. ',0, 102)
INSERT INTO Answer VALUES (408, N' A servlet explicitly calls invalidate() on a session object. ',0, 102)

INSERT INTO Answer VALUES (409, N' Allows programmers to include their own new tags in the form of tag libraries. These libraries can be used to encapsulate functionality and simplify the coding of a JSP. ',0, 103)
INSERT INTO Answer VALUES (410, N' The scripting language used in the JSP. Currently, the only valid for this attribute is java. ',0, 103)
INSERT INTO Answer VALUES (411, N' Causes the JSP container to perform a translation-time insertion of another resources content. JSP is translated into a servlet and compiled, the referenced file replaces the include directive and is translated as if it were original part of the JSP. ',1, 103)
INSERT INTO Answer VALUES (412, N' Defines page setting for the JSP container to process. ',0, 103)

INSERT INTO Answer VALUES (413, N' extends ',0, 104)
INSERT INTO Answer VALUES (414, N' import ',1, 104)
INSERT INTO Answer VALUES (415, N' include ',0, 104)
INSERT INTO Answer VALUES (416, N' package ',0, 104)

INSERT INTO Answer VALUES (417, N' True ',0, 105)
INSERT INTO Answer VALUES (418, N' False ',1, 105)
INSERT INTO Answer VALUES (419, N' Both A and B are correct',0, 105)
INSERT INTO Answer VALUES (420, N' Both A and B are wrong ',0, 105)

INSERT INTO Answer VALUES (421, N' MD5 encryption ',0, 106)
INSERT INTO Answer VALUES (422, N' Authentication ',0, 106)
INSERT INTO Answer VALUES (423, N' Data integrity ',0, 106)
INSERT INTO Answer VALUES (424, N' Authorization ',1, 106)

INSERT INTO Answer VALUES (425, N' It is used by web application developer to deliver the web application in a single unit ',0, 107)
INSERT INTO Answer VALUES (426, N' It cannot be unpackaged by the container ',0, 107)
INSERT INTO Answer VALUES (427, N' It is an XML document ',0, 107)
INSERT INTO Answer VALUES (428, N' It contains web components such as servlets as well as EJBs ',1, 107)

INSERT INTO Answer VALUES (429, N' getCreationTime() ',0, 108)
INSERT INTO Answer VALUES (430, N' getLastAccessedTime() ',1, 108)
INSERT INTO Answer VALUES (431, N' getMaxInactiveInterval() ',0, 108)
INSERT INTO Answer VALUES (432, N' getLastAccessTime() ',0, 108)

INSERT INTO Answer VALUES (433, N' Dynamically includes another resource in a JSP. As the JSP executes, the referenced resource is included and processed ',1, 109)
INSERT INTO Answer VALUES (434, N' Forwards request processing to another JSP, servlet or static page. This action terminates the current JSPs execution ',0, 109)
INSERT INTO Answer VALUES (435, N' Specifies the relative URI path of the resource to include. The resource must be part of the same Web application ',0, 109)
INSERT INTO Answer VALUES (436, N' Specifies that the JSP uses a JavaBean instance. This action specifies the scope of the bean and assigns it an ID that scripting components can use to manipulate the bean ',0, 109)

INSERT INTO Answer VALUES (437, N' HTML FORM is used to capture username and password ',0, 110)
INSERT INTO Answer VALUES (438, N' Password is transmitted either in encrypted text or in plain text depending on the browser ',0, 110)
INSERT INTO Answer VALUES (439, N' Password is transmitted in an encrypted form ',0, 110)
INSERT INTO Answer VALUES (440, N' Password is transmitted as text ',1, 110)

INSERT INTO Answer VALUES (441, N' Authentication means determining whether one has access to a particular resource or not ',0, 111)
INSERT INTO Answer VALUES (442, N' Confidentiality and Authorization are one and the same thing ',0, 111)
INSERT INTO Answer VALUES (443, N' Authentication means proving whether one is what one claims to be ',1, 111)
INSERT INTO Answer VALUES (444, N' All of above ',0, 111)

INSERT INTO Answer VALUES (445, N' It will be invoked only if doStartTag() or doAfterBody() return Tag.DO_END_TAG ',1, 112)
INSERT INTO Answer VALUES (446, N' It will be invoked only if doStartTag() and doAfterBody() complete successfully ',0, 112)
INSERT INTO Answer VALUES (447, N' This method is invoked if doStartTag() method returns true ',0, 112)
INSERT INTO Answer VALUES (448, N' It will be invoked in all case even if doStartTag() or doAfterBody() throw an exception ',0, 112)

INSERT INTO Answer VALUES (449, N' Both are defined in ServletRequest ',0, 113)
INSERT INTO Answer VALUES (450, N' Both are defined in HttpServletResponse ',1, 113)
INSERT INTO Answer VALUES (451, N' Both are defined in ServletResponse ',0, 113)
INSERT INTO Answer VALUES (452, N' Both are defined in HttpServletRequest ',0, 113)

INSERT INTO Answer VALUES (453, N' \doc-root\dd.xml ',0, 114)
INSERT INTO Answer VALUES (454, N' \doc-root\web.xml ',0, 114)
INSERT INTO Answer VALUES (455, N' \doc-root\WEB-INF\web.xml ',1, 114)
INSERT INTO Answer VALUES (456, N' \doc-root\WEB_INF\dd.xml ',0, 114)

INSERT INTO Answer VALUES (457, N' \WEB-INF ',1, 115)
INSERT INTO Answer VALUES (458, N' \WEB-INF\xml ',0, 115)
INSERT INTO Answer VALUES (459, N' \WEB-INF\classes ',0, 115)
INSERT INTO Answer VALUES (460, N' All of above ',0, 115)

INSERT INTO Answer VALUES (461, N' ! page import="java.util.TreeMap" ',0, 116)
INSERT INTO Answer VALUES (462, N' ! import="java.util.TreeMap" ',0, 116)
INSERT INTO Answer VALUES (463, N' @ import="java.util.TreeMap" ',0, 116)
INSERT INTO Answer VALUES (464, N' @ page import="java.util.TreeMap" ',1, 116)

INSERT INTO Answer VALUES (465, N' =jsp:getProperty name="mybean" property="name" ',0, 117)
INSERT INTO Answer VALUES (466, N' out.println(mybean.getName()) ',0, 117)
INSERT INTO Answer VALUES (467, N' jsp:getProperty name="mybean" property="name" ',0, 117)
INSERT INTO Answer VALUES (468, N' =out.println(mybean.getName()) ',0, 117)

INSERT INTO Answer VALUES (469, N' The included file must have jsp extension. ',0, 118)
INSERT INTO Answer VALUES (470, N' The XML syntax of include directive in <jsp:include file="fileName"/> ',0, 118)
INSERT INTO Answer VALUES (471, N' The content of file included using include directive, cannot refer to variables local to the original page. ',0, 118)
INSERT INTO Answer VALUES (472, N' When using the include directive, the JSP container treats the file to be included as if it was part of the original file. ',1, 118)

INSERT INTO Answer VALUES (473, N' getValue of Session ',0, 119)
INSERT INTO Answer VALUES (474, N' getValue of HttpSession ',0, 119)
INSERT INTO Answer VALUES (475, N' getAttribute of Session ',0, 119)
INSERT INTO Answer VALUES (476, N' getAttribute of HttpSession ',1, 119)

INSERT INTO Answer VALUES (477, N' tagname ',0, 120)
INSERT INTO Answer VALUES (478, N' name ',1, 120)
INSERT INTO Answer VALUES (479, N' tag ',0, 120)
INSERT INTO Answer VALUES (480, N' prefix ',0, 120)


INSERT INTO [dbo].[Quiz] ([quizId], [title], [chapId],[image], [duration])
VALUES (13,'Basic Java language',13,'https://fsoft-academy.edu.vn/wp-content/uploads/2021/08/FJB-1024x540.png',15),
(14,'OOP concepts',14,'https://aptech.fpt.edu.vn/wp-content/uploads/2022/11/dinh-nghia-ve-lap-trinh-huong-doi-tuong-la-gi.jpg',15),
(15,'Array of Objects',15,'https://sp-uploads.s3.amazonaws.com/uploads/services/4557510/20220802113659_62e90c5b402af_arrayofobjectspage0.jpg',15)
INSERT INTO [dbo].[Question]([questionId],[qContent],[quizId])
VALUES (121,'Which statement is used to exit from a loop in Java?',13),
(122,'Which keyword is used to declare a variable whose value cannot be changed in Java? ',13),
(123,'In Java, arrays are indexed starting from: ',13),
(124,'Which method is automatically called when an object is created from a class? ',13),
(125,'What is the way to define a constant in Java? ',13),
(126,'Which data type is used for storing single characters in Java?',13),
(127,'What is the output of the following code snippet? int x = 5; int y = 2; System.out.println(x / y);',13),
(128,'Which loop is used when you want to execute a block of code at least once in Java? ',13),
(129,'What is the output of the following code snippet? String[] names = {"John", "Jane", "Jim"}; System.out.println(names[1]); ',13),
(130,'Which keyword is used to define a subclass of a class in Java? ',13),
(131,'What is an object in object-oriented programming?',14),
(132,'What is encapsulation in Java?',14),
(133,'What is inheritance in Java?',14),
(134,'Which OOP concept allows a class to have multiple methods with the same name but different parameters?',14),
(135,'What is a superclass in Java?',14),
(136,'Which keyword is used to prevent a class from being instantiated directly?',14),
(137,'What is method overloading in Java?',14),
(138,'What is a constructor in Java?',14),
(139,'What is composition in Java?',14),
(140,'Which OOP concept allows a class to have more than one relationship with other classes?',14),
(141,'In Java, what is an array of objects?',15),
(142,'How do you declare an array of objects in Java?',15),
(143,'What is the purpose of using an array of objects?',15),
(144,'How can you access an element in an array of objects?',15),
(145,'Which of the following statements initializes an array of Student objects with 5 elements?',15),
(146,'How do you assign an object to an element in an array of objects?',15),
(147,'What is the default value of an element in an uninitialized array of objects in Java?',15),
(148,'Which loop is commonly used to iterate through elements in an array of objects?',15),
(149,'What is the purpose of resizing an array of objects in Java?',15),
(150,'How do you get the length of an array of objects in Java?',15),
(271,'What is the default value of a char in Java?',13),
(272,'What is the output of the following code? public class Main {public static void main(String[] args) {int x = 5;System.out.println(x++);}',13),
(273,'Which keyword is used to define a constant in Java?',13),
(274,'What is the result of 8 % 3 in Java?',13),
(275,'Which data type is used to store decimal numbers in Java?',13),
(276,'Which keyword is used to create a subclass in Java?',13),
(277,'What is the purpose of the break statement in Java?',13),
(278,'Which of the following is NOT a valid identifier in Java?',13),
(279,' What is the difference between == and .equals() in Java?',13),
(280,'Which Java keyword is used to implement multiple inheritance through interfaces?',13),
(281,'What is encapsulation in object-oriented programming?',14),
(282,'What is inheritance in object-oriented programming?',14),
(283,'What is polymorphism in object-oriented programming?',14),
(284,'What is a class in object-oriented programming?',14),
(285,'What is a constructor in object-oriented programming?',14),
(286,'What is method overloading in object-oriented programming?',14),
(287,'What is composition in object-oriented programming?',14),
(288,'What is a superclass in object-oriented programming?',14),
(289,'What is a static method in object-oriented programming?',14),
(290,'What is an interface in object-oriented programming?',14),
(291,'What is an array of objects in programming?',15),
(292,'How is an array of objects declared in Java?',15),
(293,'In an array of objects, how do you access a specific object?',15),
(294,'What is the advantage of using an array of objects?',15),
(295,'Can an array of objects contain objects of different classes?',15),
(296,'How do you initialize an array of objects in Java?',15),
(297,'What is the default value of an element in an array of objects if it is not explicitly initialized?',15),
(298,'How do you access a specific attribute of an object in an array of objects?',15),
(299,'Can you change the size of an array of objects after it has been initialized?',15),
(300,'How do you create an array of objects with different initial values?',15)
INSERT INTO [dbo].[Answer]([answerId],[content],[correct],[quesId])
VALUES (481,' break',1,121),
(482,' stop',0,121),
(483,' exit',0,121),
(484,' end',0,121),
(485,' const',0,122),
(486,' final',1,122),
(487,' static',0,122),
(488,' var',0,122),
(489,' 0',1,123),
(490,' 1',0,123),
(491,' -1',0,123),
(492,' 10',0,123),
(493,' initialize()',0,124),
(494,' create()',0,124),
(495,' constructor()',1,124),
(496,' instantiate()',0,124),
(497,' finalVar',0,125),
(498,' const',0,125),
(499,'  final',1,125),
(500,' static',0,125),
(501,'  char',1,126),
(502,'  character',0,126),
(503,'  string',0,126),
(504,' int',0,126),
(505,' 2.5',0,127),
(506,' 2',1,127),
(507,' 2.0',0,127),
(508,' 2.50',0,127),
(509,' for loop',0,128),
(510,' while loop',0,128),
(511,' do-while loop',1,128),
(512,' if-else loop',0,128),
(513,' John',0,129),
(514,' Jane',1,129),
(515,' Jim',0,129),
(516,' Error',0,129),
(517,'  extend',0,130),
(518,' extends',1,130),
(519,' subclass',0,130),
(520,' inherits',0,130),
(521,' A blueprint for creating instances of a class',0,131),
(522,' A specific instance of a class',1,131),
(523,' A collection of methods',0,131),
(524,' A reserved keyword in Java',0,131),
(525,' The process of combining data and functions into a single unit',1,132),
(526,' The process of defining multiple constructors in a class',0,132),
(527,' The process of converting objects into primitive data types',0,132),
(528,'The process of creating multiple instances of a class',0,132),
(529,' The process of creating multiple instances of a class',0,133),
(530,' The process of defining multiple constructors in a class',0,133),
(531,' The process of one class acquiring the properties and behaviors of another class',1,133),
(532,' The process of converting objects into primitive data types',0,133),
(533,' Polymorphism',1,134),
(534,' Inheritance',0,134),
(535,' Encapsulation',0,134),
(536,' Abstraction',0,134),
(537,' A class that inherits properties and behaviors from another class',1,135),
(538,' A class that is inherited by another class',0,135),
(539,' A class that cannot be inherited',0,135),
(540,' A class with no properties or methods',0,135),
(541,' final',0,136),
(542,' private',0,136),
(543,' abstract',1,136),
(544,' static',0,136),
(545,' The process of creating multiple methods with the same name but different implementations',1,137),
(546,' The process of creating multiple methods with the same name and same parameters',0,137),
(547,' The process of defining multiple constructors in a class',0,137),
(548,' The process of combining data and functions into a single unit',0,137),
(549,' A method that is used to destroy objects',0,138),
(550,' A method used to create objects',0,138),
(551,' A special method that is automatically called when an object is created',1,138),
(552,' A method that is used to print output to the console',0,138),
(553,' The process of combining two or more classes to create a new class',1,139),
(554,' The process of creating multiple instances of a class',0,139),
(555,' The process of defining multiple constructors in a class',0,139),
(556,' The process of creating multiple methods with the same name but different implementations',0,139),
(557,' Association',1,140),
(558,' Inheritance',0,140),
(559,' Encapsulation',0,140),
(560,' Abstraction',0,140),
(561,' An array that can only store primitive data types',0,141),
(562,' An array that can store instances of classes',1,141),
(563,' An array that can store only Strings',0,141),
(564,' An array that can store integers',0,141),
(565,' array[] myArray = new array[];',0,142),
(566,' ObjectArray[] myArray = new ObjectArray[];',0,142),
(567,' Object[] myArray = new Object[10];',1,142),
(568,' ObjectArray myArray = new ObjectArray();',0,142),
(569,' To store only integers',0,143),
(570,' To store a collection of different data types',0,143),
(571,' To store a collection of objects of the same class',1,143),
(572,' To store only Strings',0,143),
(573,' By using the index of the element within square brackets',1,144),
(574,' By using a loop',0,144),
(575,' By converting the array to a String',0,144),
(576,' By using the name of element',0,144),
(577,' Student[] students = new Student[5];',1,145),
(578,' StudentArray students = new StudentArray(5);',0,145),
(579,' StudentArray[] students = new StudentArray[5];',0,145),
(580,' students = new Student[5];',0,145),
(581,' myArray.add(object);',0,146),
(582,' myArray.set(index, object);',0,146),
(583,' myArray[index] = object;',1,146),
(584,' myArray.insert(object, index);',0,146),
(585,' 0',0,147),
(586,' null',1,147),
(587,' "undefined"',0,147),
(588,' 1',0,147),
(589,' for-each loop',1,148),
(590,' while loop',0,148),
(591,' do-while loop',0,148),
(592,' for loop',0,148),
(593,' To increase the size of the array',0,149),
(594,' To decrease the size of the array',0,149),
(595,' To add or remove elements dynamically',1,149),
(596,' To sort the array',0,149),
(597,' myArray.size();',0,150),
(598,' myArray.length;',1,150),
(599,' myArray.getSize();',0,150),
(600,' myArray.length();',0,150),
(1081,' "0"',0,271),
(1082,' 0',1,271),
(1083,' null',0,271),
(1084,' ''',0,271),
(1085,' 5',1,272),
(1086,' 6',0,272),
(1087,' 4',0,272),
(1088,' 0',0,272),
(1089,' final',1,273),
(1090,' static',0,273),
(1091,' const',0,273),
(1092,' let',0,273),
(1093,' 2',1,274),
(1094,' 3',0,274),
(1095,' 4',0,274),
(1096,' 1',0,274),
(1097,' int',0,275),
(1098,' float',0,275),
(1099,' double',1,275),
(1100,' decimal',0,275),
(1101,' extends',1,276),
(1102,' subclass',0,276),
(1103,' inherits',0,276),
(1104,' superclass',0,276),
(1105,' To exit a loop or switch statement',1,277),
(1106,' To skip the next iteration of a loop',0,277),
(1107,' To return a value from a method',0,277),
(1108,' To terminate the program',0,277),
(1109,' myVariable',0,278),
(1110,' 123variable',1,278),
(1111,' _myVariable',0,278),
(1112,' $myVariable',0,278),
(1113,' == compares the memory addresses, .equals() compares the values',1,279),
(1114,' == compares the values, .equals() compares the memory addresses',0,279),
(1115,' They are functionally equivalent',0,279),
(1116,' == is used for primitive types, .equals() is used for objects',0,279),
(1117,' implements',1,280),
(1118,' extends',0,280),
(1119,' inherits',0,280),
(1120,' extends and implements',0,280),
(1121,' A process of combining data and functions into a single unit',0,281),
(1122,' A process of hiding the implementation details of an object',1,281),
(1123,' A process of creating multiple instances of a class',0,281),
(1124,' A process of allowing a class to inherit from multiple classes',0,281),
(1125,' The ability of a class to have multiple instances',0,282),
(1126,' The process of creating a new class from an existing class',1,282),
(1127,' The process of creating multiple objects from a class',0,282),
(1128,' The process of combining data and functions into a single unit',0,282),
(1129,' The process of creating multiple instances of a class',0,283),
(1130,' The process of hiding the implementation details of an object',0,283),
(1131,' The ability of a class to have multiple instances',0,283),
(1132,' The ability of a class to take on multiple forms or behaviors',1,283),
(1133,' A blueprint for creating objects',1,284),
(1134,' An instance of an object',0,284),
(1135,' A variable that holds the value of an object',0,284),
(1136,' A method that belongs to an object',0,284),
(1137,' A method that destroys an object',0,285),
(1138,' A method that initializes an object',1,285),
(1139,' A method that copies an object',0,285),
(1140,' A method that inherits from another class',0,285),
(1141,' The process of creating multiple instances of a method',0,286),
(1142,' The ability of a class to have multiple methods',0,286),
(1143,' The ability to define multiple versions of a method with different parameter lists',1,286),
(1144,' The process of combining data and functions into a single unit',0,286),
(1145,' The process of combining data and functions into a single unit',0,287),
(1146,' The process of creating multiple instances of a class',0,287),
(1147,' The process of creating an object within another object',1,287),
(1148,' The process of hiding the implementation details of an object',0,287),
(1149,' A class that has no subclasses',0,288),
(1150,' A class from which other classes inherit',1,288),
(1151,' A class that cannot have any methods',0,288),
(1152,' A class that cannot have any attributes',0,288),
(1153,' A method that belongs to a class rather than an instance of the class',1,289),
(1154,' A method that is defined without any parameters',0,289),
(1155,' A method that cannot be overridden by subclasses',0,289),
(1156,' A method that cannot be called from other classes',0,289),
(1157,' A class that cannot be instantiated',0,290),
(1158,' A contract that defines a set of methods that a class must implement',1,290),
(1159,' A method that is defined without any parameters',0,290),
(1160,' A method that is defined without a return type',0,290),
(1161,' A collection of objects with the same data type',1,291),
(1162,' An array that can only store objects',0,291),
(1163,' A special type of object',0,291),
(1164,' An object that contains an array',0,291),
(1165,'objectName[] objectArray;',0,292),
(1166,'Object[] objectArray;',0,292),
(1167,'Array objectArray[];',0,292),
(1168,'ObjectName[] objectArray;',1,292),
(1169,' By calling the getObject() method',0,293),
(1170,' Using a loop',0,293),
(1171,' Using the index of the object in the array',1,293),
(1172,' By using the object s name directly',0,293),
(1173,' It allows you to store different types of objects in one array',0,294),
(1174,' It provides a way to group related objects together',1,294),
(1175,' It eliminates the need for loops',0,294),
(1176,' It allows for dynamic resizing of objects',0,294),
(1177,' Yes',1,295),
(1178,' No',0,295),
(1179,' Only if the classes have a common superclass',0,295),
(1180,' Only if the classes are in the same package',0,295),
(1181,' objectArray = new ObjectName[];',0,296),
(1182,' objectArray = new ObjectName[size];',0,296),
(1183,' ObjectName[] objectArray = new ObjectName[size];',1,296),
(1184,' ObjectName[size] objectArray;',0,296),
(1185,' null',1,297),
(1186,' 0',0,297),
(1187,' false',0,297),
(1188,' empty string',0,297),
(1189,' attributeName(objectArray[index]);',0,298),
(1190,' objectArray.attributeName(index);',0,298),
(1191,' objectArray.attributeName[index];',0,298),
(1192,' objectArray[index].attributeName;',1,298),
(1193,' Yes',0,299),
(1194,' No',1,299),
(1195,' Only if the array is empty',0,299),
(1196,' Only if the objects in the array are of the same class',0,299),
(1197,' By using a loop to assign values to each element',0,300),
(1198,' By specifying the values in curly braces during initialization',1,300),
(1199,' By using the set() method after initialization',0,300),
(1200,'  By calling the constructor for each element individually',0,300);





--course 4 hieu
INSERT INTO Question VALUES (241, N'Which is the correct sequence?', 10)
INSERT INTO Question VALUES (242, N'Which statement is true about the EJB 3.0 stateful session beans?', 10)
INSERT INTO Question VALUES (243, N'Which statement about an entity instance lifecycle is correct?', 10)
INSERT INTO Question VALUES (244, N'Which is NOT Enterprise Beans?', 10)
INSERT INTO Question VALUES (245, N'Which Java technology provides a unified interface to multiple naming and directory services?', 10)
INSERT INTO Question VALUES (246, N'What sub element of <attribute> tag defines the name of the attribute that might be passed to the tag handler?', 10)
INSERT INTO Question VALUES (247, N'Which statement is NOT true about JMS?', 10)
INSERT INTO Question VALUES (248, N'Your servlet may receive a request, which the servlet cannot handle. In such cases, you want to redirect the request to another resource which may or may not be a part of the same web application. Which of the following options can be used to achieve this objective?', 10)
INSERT INTO Question VALUES (249, N'Which of the following elements are used for error handling and are child elements of<web-app> of a deployment descriptor?', 10)
INSERT INTO Question VALUES (250, N'A JSP file uses a tag as <myTaglib:myTag>. The myTag element here should be defined in the which element of the taglib element in the tag library descriptor file ?', 10)
/*quiz id 11 */
INSERT INTO Question VALUES (251, N'What will the following line of code return if present in the init() method of TestServlet?getlnitParameter(1);', 11)
INSERT INTO Question VALUES (252, N'Which of the following statement is true for <jsp:useBean> ?', 11)
INSERT INTO Question VALUES (253, N'Which statement is correct about the Java Persistence API support for the SQL queries?', 11)
INSERT INTO Question VALUES (254, N'Which of the following is NOT a standard technique for providing a sense of "state" to HTTP?', 11)
INSERT INTO Question VALUES (255, N'A(n)_enables a web application to obtain a Connection to a database.', 11)
INSERT INTO Question VALUES (256, N'Which is a valid PostConstruct method in a message-driven bean class?', 11)
INSERT INTO Question VALUES (257, N'The sendRedirect method defined in the HttpServlet class is equivalent to invoking the setStatus method with the following parameter and a Location header in the URL. Select one correct answer.', 11)
INSERT INTO Question VALUES (258, N'Which is NOT provided by the EJB tier in a multitier JEE (J2EE) application?', 11)
INSERT INTO Question VALUES (259, N'Which type of JEE (or J2EE) component is used to store business data persistently?', 11)
INSERT INTO Question VALUES (260, N'What is the purpose of JNDI?', 11)
/*quiz id 12 */
INSERT INTO Question VALUES (261, N'Which of the following is true about Data Integrity?', 12)
INSERT INTO Question VALUES (262, N'Which of the following statement is correct? (choose one)', 12)
INSERT INTO Question VALUES (263, N'Which is true about JDBC?', 12)
INSERT INTO Question VALUES (264, N'In which of the following cases will the method doEndTagO of a tag handler be invoked?', 12)
INSERT INTO Question VALUES (265, N'Data Integrity is the biggest issue for your web application. What will you do? (Choose one)', 12)
INSERT INTO Question VALUES (266, N'Which jsp tag is needed to ensure that implicit variable exception is available in the page that is meant to be an error page? (Choose one)', 12)
INSERT INTO Question VALUES (267, N'Where the bean is declared using the following useBean tag accessible? <jsp:useBean id="jbean" class="com.jclass.JBean" />', 12)
INSERT INTO Question VALUES (268, N'Which statements are BEST describe contentType attribute of @ page contentType=_. directive?', 12)
INSERT INTO Question VALUES (269, N'Which is NOT the main type of JSP constructs that you embed in a page?', 12)
INSERT INTO Question VALUES (270, N'Servlet Container calls the init method on a servlet instance_', 12)

/*answer*/
/*answer*/
INSERT INTO Answer VALUES (961, N' jsp page is complied-->Jsp page is translated ~>jsplnit is called ->JspService is called ->jspDestroy is called',0, 241)
INSERT INTO Answer VALUES (962, N' Jsp page is translated -->jsp page is complied-->jspService is called ->jsplnit is called ->JspDestroy is called',0, 241)
INSERT INTO Answer VALUES (963, N' Jsp page is translated -->jsp page is complied-->jsplnit is called ->JspService is called -->jspDestroy is called',1, 241)
INSERT INTO Answer VALUES (964, N' jsp page is complied-->Jsp page is translated --> JspService is called -->jsplnit is called -->jspDestroy is called',0, 241)

INSERT INTO Answer VALUES (965, N' Its conversational state is retained across method invocations and transactions',1, 242)
INSERT INTO Answer VALUES (966, N' Its conversational state is lost after passivation unless the bean class saves it to a database ',0, 242)
INSERT INTO Answer VALUES (967, N' Its conversational state is retained across method invocations and transactions',0, 242)
INSERT INTO Answer VALUES (968, N' None of others ',0, 242)

INSERT INTO Answer VALUES (969, N' A removed entity instance is NOT associated with a persistence context',0, 243)
INSERT INTO Answer VALUES (970, N' A managed entity instance is the instance associated with a persistence context',1, 243)
INSERT INTO Answer VALUES (971, N' A new entity instance is an instance with a fully populated state.',0, 243)
INSERT INTO Answer VALUES (972, N' A detached entity instance is an instance with no persistent identity.',0, 243)

INSERT INTO Answer VALUES (973, N' Message-Driven Beans',0, 244)
INSERT INTO Answer VALUES (974, N' Session Beans ',0, 244)
INSERT INTO Answer VALUES (975, N' Entity Beans ',0, 244)
INSERT INTO Answer VALUES (976, N' Business Beans',1, 244)

INSERT INTO Answer VALUES (977, N' JNI',0, 245)
INSERT INTO Answer VALUES (978, N' JDBC',0, 245)
INSERT INTO Answer VALUES (979, N' JNDI',1, 245)
INSERT INTO Answer VALUES (980, N' EJB',0, 245)

INSERT INTO Answer VALUES (981, N' name',1, 246)
INSERT INTO Answer VALUES (982, N' attribute-name',0, 246)
INSERT INTO Answer VALUES (983, N' attributename',0, 246)
INSERT INTO Answer VALUES (984, N' param-name',0, 246)

INSERT INTO Answer VALUES (985, N' JMS does NOT depend on MOM (Messaging-Oriented Middleware) products',1, 247)
INSERT INTO Answer VALUES (986, N' JSM support an event oriented approach to message reception.',0, 247)
INSERT INTO Answer VALUES (987, N' JMS support both synchronous and asynchronous message passing',0, 247)
INSERT INTO Answer VALUES (988, N' JMS provides common way for Java programs to access an enterprise messaging systems message',0, 247)

INSERT INTO Answer VALUES (989, N' response.sendRedirect("some url");',1, 248)
INSERT INTO Answer VALUES (990, N' RequestDispatcher rd = request.getRequestDispatcher("some url");rd.forward(request, response);',0, 248)
INSERT INTO Answer VALUES (991, N' request.sendRedirect("some url");',0, 248)
INSERT INTO Answer VALUES (992, N' RequestDispatcher rd = this.getServletContext().getRequestDispatcher("some url");rd.forward(request, response);',0, 248)

INSERT INTO Answer VALUES (993, N' error_page ',0, 249)
INSERT INTO Answer VALUES (994, N' error-location',0, 249)
INSERT INTO Answer VALUES (995, N' error',0, 249)
INSERT INTO Answer VALUES (996, N' error-page',1, 249)

INSERT INTO Answer VALUES (997, N' tagname',0, 250)
INSERT INTO Answer VALUES (998, N' tag',0, 250)
INSERT INTO Answer VALUES (999, N' name',1, 250)
INSERT INTO Answer VALUES (1000, N' prefix ',0, 250)

INSERT INTO Answer VALUES (1001, N' It will return "US". ',0, 251)
INSERT INTO Answer VALUES (1002, N' It will return null.',0, 251)
INSERT INTO Answer VALUES (1003, N' Compilation error. ',1, 251)
INSERT INTO Answer VALUES (1004, N' It will return "eastern".',0, 251)

INSERT INTO Answer VALUES (1005, N' The class attribute must be defined for jsp:useBean. ',0, 252)
INSERT INTO Answer VALUES (1006, N' The id attribute must be defined for jsp:useBean.',1, 252)
INSERT INTO Answer VALUES (1007, N' The scope attribute must be defined for jsp:useBean.',0, 252)
INSERT INTO Answer VALUES (1008, N' None of these others',0, 252)

INSERT INTO Answer VALUES (1009, N' SQL queries are expected to be portable across database ',0, 253)
INSERT INTO Answer VALUES (1010, N' SQL queries are NOT allowed to use parameters. ',0, 253)
INSERT INTO Answer VALUES (1011, N' The result of a SQL query is not limited to entities ',1, 253)
INSERT INTO Answer VALUES (1012, N' Only SELECT SQL queries are required to be supported. ',0, 253)

INSERT INTO Answer VALUES (1013, N' SSL Sessions ',0, 254)
INSERT INTO Answer VALUES (1014, N' HTTP is already a stateful protocol. ',1, 254)
INSERT INTO Answer VALUES (1015, N' Cookies ',0, 254)
INSERT INTO Answer VALUES (1016, N' URL rewriting ',0, 254)

INSERT INTO Answer VALUES (1017, N' Netbean ',0, 255)
INSERT INTO Answer VALUES (1018, N' DataSource ',1, 255)
INSERT INTO Answer VALUES (1019, N' Eclipse',0, 255)
INSERT INTO Answer VALUES (1020, N' Web server ',0, 255)

INSERT INTO Answer VALUES (1021, N' @PostConstruct
public boolean init() {return true;} ',0, 256)
INSERT INTO Answer VALUES (1022, N' @PostConstruct
private static void init() {} ',0, 256)
INSERT INTO Answer VALUES (1023, N' @PostConstruct
public static void init() {} ',0, 256)
INSERT INTO Answer VALUES (1024, N' @PostConstruct
private void init() {}; ',1, 256)

INSERT INTO Answer VALUES (1025, N' SC_N0T_F0UND ',0, 257)
INSERT INTO Answer VALUES (1026, N' SCJNTERNAL_SERVER_ERROR ',0, 257)
INSERT INTO Answer VALUES (1027, N' ESC_BAD_REQUEST ',0, 257)
INSERT INTO Answer VALUES (1028, N' SC_MOVED_TEMPORARILY ',1, 257)

INSERT INTO Answer VALUES (1029, N' Concurrency control ',0, 258)
INSERT INTO Answer VALUES (1030, N' XML Parsing ',1, 258)
INSERT INTO Answer VALUES (1031, N' Transaction management ',0, 258)
INSERT INTO Answer VALUES (1032, N' Security ',0, 258)

INSERT INTO Answer VALUES (1033, N' Entity Bean ',1, 259)
INSERT INTO Answer VALUES (1034, N' Stateful session beans ',0, 259)
INSERT INTO Answer VALUES (1035, N' Stateles session beans ',0, 259)
INSERT INTO Answer VALUES (1036, N' Java Beans ',0, 259)

INSERT INTO Answer VALUES (1037, N' To register Java Web Start applications with a web server ',0, 260)
INSERT INTO Answer VALUES (1038, N' To register Java Web Start applications with a web server ',0, 260)
INSERT INTO Answer VALUES (1039, N' To parse XML documents ',0, 260)
INSERT INTO Answer VALUES (1040, N' To access various derictory services using a single interface ',1, 260)

INSERT INTO Answer VALUES (1041, N' The information/data is not read by parties other than its intended recipients. ',0, 261)
INSERT INTO Answer VALUES (1042, N' The information/data is never modified. ',0, 261)
INSERT INTO Answer VALUES (1043, N' The information/data is not tampered with, in transit from host to client. ',1, 261)
INSERT INTO Answer VALUES (1044, N' The information/data is accessible only to users who are authenticated. ',0, 261)

INSERT INTO Answer VALUES (1045, N' Authorization means determining whether one has access to a particular resource or not. ',1, 262)
INSERT INTO Answer VALUES (1046, N' Confidentiality and Authorization are one and the same thing.',0, 262)
INSERT INTO Answer VALUES (1047, N' Authentication means determining whether one has access to a particular resource or not. ',0, 262)
INSERT INTO Answer VALUES (1048, N' All of these others ',0, 262)

INSERT INTO Answer VALUES (1049, N' The JDBC API is an extension of the ODBC API ',0, 263)
INSERT INTO Answer VALUES (1050, N' The JDBC API is included in J2SE ',1, 263)
INSERT INTO Answer VALUES (1051, N' All JDBC drivers are pure Java. ',0, 263)
INSERT INTO Answer VALUES (1052, N' JDBC is used to connect to MOM (Message-Oriented Middleware Product) ',0, 263)

INSERT INTO Answer VALUES (1053, N' This method is invoked if doStartTagO method returns true.',0, 264)
INSERT INTO Answer VALUES (1054, N' It will be invoked only if doStartTagO and doAfterBodyO complete successfully.',0, 264)
INSERT INTO Answer VALUES (1055, N' It will be invoked in all case even if doStartTagO or doAfterBodyO throw an exception.',1, 264)
INSERT INTO Answer VALUES (1056, N' All of these ',0, 264)

INSERT INTO Answer VALUES (1057, N' Use HTTPS instead of HTTP. ',1, 265)
INSERT INTO Answer VALUES (1058, N' Use HTTPS instead of HTTP. ',0, 265)
INSERT INTO Answer VALUES (1059, N' Use LDAP to store user credentials. ',0, 265)
INSERT INTO Answer VALUES (1060, N' Use HTTP digest authentication. ',0, 265)

INSERT INTO Answer VALUES (1061, N' @isErrorPage="true" ',0, 266)
INSERT INTO Answer VALUES (1062, N' @ page errorPage="this" ',0, 266)
INSERT INTO Answer VALUES (1063, N' @errorPage="true" ',0, 266)
INSERT INTO Answer VALUES (1064, N' @ page isErrorPage="true" ',1, 266)

INSERT INTO Answer VALUES (1065, N' within other servlets and JSP pages of the same web applications. ',0, 267)
INSERT INTO Answer VALUES (1066, N' throughout all future invocations of the JSP as long as the servlet engine is running. ',0, 267)
INSERT INTO Answer VALUES (1067, N' Throughout the remainder of the page. ',1, 267)
INSERT INTO Answer VALUES (1068, N' throughout all future invocations of the JSP as long as the session is not expired ',0, 267)

INSERT INTO Answer VALUES (1069, N' Specifies if the current page is an error page that will be invoked in response to an error on another page. If the attribute value is true, the implicit object exception is created and references the original excep ',0, 268)
INSERT INTO Answer VALUES (1070, N' Any exceptions in the current page that are not caught are sent to the error page for processing. The error page implicit object exception references the original exception. ',0, 268)
INSERT INTO Answer VALUES (1071, N' Specifies the class from which the translated JSP will be inherited. This attribute must be a fully qualified package and class name. ',0, 268)
INSERT INTO Answer VALUES (1072, N' Specifies the MIME type of the data in the response to the client The default type is text/html. ',1, 268)

INSERT INTO Answer VALUES (1073, N' directives ',0, 269)
INSERT INTO Answer VALUES (1074, N' scripting elements ',0, 269)
INSERT INTO Answer VALUES (1075, N' actions ',0, 269)
INSERT INTO Answer VALUES (1076, N' HTML code ',1, 269)

INSERT INTO Answer VALUES (1077, N' For each request to the servlet ',0, 270)
INSERT INTO Answer VALUES (1078, N' Only once in the life time of the servlet instance. ',1, 270)
INSERT INTO Answer VALUES (1079, N' For each request to the servlet that causes a new thread to be created. ',0, 270)
INSERT INTO Answer VALUES (1080, N' If the request is from the user whose session has expired. ',0, 270)

---add numberofquestion
ALTER TABLE Quiz
ADD numberOfQuestion int
update Quiz set numberOfQuestion=10
DECLARE @QuestionId INT = 1
DECLARE @NumberOfCorrect INT = 1

WHILE @QuestionId <= 300
BEGIN
    INSERT INTO [dbo].[Question_True] ([questionId], [numberOfCorrect])
    VALUES (@QuestionId, @NumberOfCorrect)

    SET @QuestionId = @QuestionId + 1
END
---question explain
INSERT INTO [dbo].[Question_Explaination] ([qExplainId], [questionId], [qExplainContent])
VALUES
    (1, 1, 'Data that describe a characteristic about a population is known as a parameter. So the correct answer is:  parameter'),
    (2, 2, 'Calculate the z-score, which tells you how many standard deviations 579 drivers are from the mean:

Z = (579 - 0.70 * 800) / sqrt[(0.70 * 0.30) / 800] ≈ 1.1728

You are given P(Z < 1.5) = 0.0668, which is the probability that the number of drivers is less than 1.5 standard deviations above the mean.

To find the probability of having more than 579 drivers (which is 1.1728 standard deviations above the mean), subtract the probability from the total probability of 1:

P(Z > 1.1728) ≈ 1 - 0.0668 ≈ 0.9332.'),
    (3, 3, 'Calculate the mean (μ):
μ = Σ (X * P(X))
μ = (0 * 0.02) + (1 * 0.07) + (2 * 0.22) + (3 * 0.27) + (4 * 0.42)
μ = 2.01

Calculate the variance (σ²):
σ² = Σ [(X - μ)² * P(X)]
σ² = (0 - 2.01)² * 0.02 + (1 - 2.01)² * 0.07 + (2 - 2.01)² * 0.22 + (3 - 2.01)² * 0.27 + (4 - 2.01)² * 0.42
σ² ≈ 1.05

Finally, calculate the standard deviation (σ):
σ = √σ²
σ ≈ √1.05
σ ≈ 1.025 (rounded to two decimal places)

So, the standard deviation for this discrete probability distribution is approximately 1.05. Therefore, the correct answer is:

 1.05'),
	(4, 4, '
The method of gathering data when subjects are exposed to certain treatments, and the data of interest is recorded is known as experiments (option C). In experiments, researchers deliberately manipulate one or more variables to study their effects on the subjects and record the outcomes. This method allows researchers to establish cause-and-effect relationships and is commonly used in scientific research to test hypotheses and make inferences.'),
    (5, 5, 'The critical z-value for a two-tailed test at a 0.05 significance level is approximately ±1.96 (for a 95% confidence level).

Since our calculated z-value (2.537) is greater than 1.96, you would reject the null hypothesis at the 0.05 significance level.

So, the value of the test statistic is approximately 2.55, which corresponds to option '),
    (6, 6, 'Residuals in the context of regression analysis are the discrepancies or errors between the actual observed values of the dependent variable (Y) and the values predicted by the regression model. These differences help assess how well the model fits the data and whether it captures the relationship between the independent variable(s) and the dependent variable.'),
	(7, 7, 'So, the 95% confidence interval for the population standard deviation is approximately (7.5, 16.2).

The correct answer is  (7.5, 16.2).'),
    (8, 8, 'The length of the entire interval [40, 50) is 50 - 40 = 10 units.

The length of the subinterval [43, 50) is 50 - 43 = 7 units. Therefore, the correct answer is:

 0.7'),
    (9, 9, 'So, the test statistic for this hypothesis test is approximately 1.19.

The correct answer is: B'),
	(10, 10, 'Simplify the fraction:

8/52 = 2/13

So, the probability of drawing an ace or a king is 2/13, which corresponds to option '),
	(11, 11, 'There are 2 ways to get a total of 11 with two dice: (5,6) and (6,5). The probability of each of these outcomes is 1/36, and there are 2 such combinations.

So, the probability of getting a total of 11 on one throw is (2 * 1/36) = 2/36 = 1/18.

Now, to find the probability of getting both a 7 and 11 on two throws, you multiply the probabilities:

(1/6) * (1/18) = 1/108
So, the correct answer is  1/54, not  1/24,  1/14, or  1/18.'),
    (12, 12, 'content'),
    (13, 13, '
To find the probability that the first successful alignment requires exactly four trials, you can use the geometric probability distribution. In this case, the probability of success (successful alignment) is 0.8, and you want to find the probability of requiring exactly four trials. So, the probability that the first successful alignment requires exactly four trials is 0.0064, which corresponds to option '),
	(14, 14, 'To find the probability that the airline will lose less than 20 suitcases during a given week, you can standardize the value of 20 using the given mean (μ = 15.5) and standard deviation (σ = 3.6) and then find the probability using the standard normal distribution. So, the probability that the airline will lose less than 20 suitcases during a given week is 0.1056, which corresponds to option '),
    (15, 15, 'None of the provided options are correct. The statement "P(X = 0) = 0" is not true for an exponential distribution, and options A and B do not match the correct values for those probabilities.

So, the correct answer is D'),
    (16, 16, 'To find the probability that the sample mean of 12 computer chips falls between 0.99 and 1.01 centimeters, you can use the z-score formula: Therefore, the correct answer is:

 0.27 (true).'),
	(17, 17, '(i) The statement P(X > 49) = 5/9 is not true. The probability P(X > 49) is actually 17/30.

(ii) The statement E(2X) = 109 is true. The expected value of 2X is indeed 109.

So, the correct answer is:

 (ii) only (true).'),
    (18, 18, 'Lower bound = 190 - (1.645 * 15.1 / √70)

Lower bound ≈ 190 - (1.645 * 15.1 / √70) ≈ 190 - 1.24 ≈ 188.76

So, the 95% lower confidence bound for the true mean cholesterol content of all such eggs is approximately 188.76 milligrams.

The closest answer choice is:

 186.46 (true).'),
    (19, 19, 'To find the probability that a random sample of 5 will contain 1 person with blood type 0 and 4 people with blood type A, you can use the hypergeometric probability distribution. The hypergeometric probability is used when sampling without replacement from a finite population. So the correct answer is D'),
	(20, 20, 'So, the 98% confidence interval for the population mean is approximately (184.10, 199.90).

The closest answer choice to this result is:

 (186.3, 197.7) (true).'),
	(21, 21, 'content'),
    (22, 22, 'content'),
    (23, 23, 'content'),
	(24, 24, 'content'),
    (25, 25, 'content'),
    (26, 26, 'content'),
	(27, 27, 'content'),
    (28, 28, 'content'),
    (29, 29, 'content'),
	(30, 30, 'content'),
	(31, 31, 'content'),
    (32, 32, 'content'),
    (33, 33, 'content'),
	(34, 34, 'content'),
    (35, 35, 'content'),
    (36, 36, 'content'),
	(37, 37, 'content'),
    (38, 38, 'content'),
    (39, 39, 'content'),
	(40, 40, 'content'),
	(41, 41, 'content'),
    (42, 42, 'content'),
    (43, 43, 'content'),
	(44, 44, 'content'),
    (45, 45, 'content'),
    (46, 46, 'content'),
	(47, 47, 'content'),
    (48, 48, 'content'),
    (49, 49, 'content'),
	(50, 50, 'content'),
	(51, 51, 'content'),
    (52, 52, 'content'),
    (53, 53, 'content'),
	(54, 54, 'content'),
    (55, 55, 'content'),
    (56, 56, 'content'),
	(57, 57, 'content'),
    (58, 58, 'content'),
    (59, 59, 'content'),
	(60, 60, 'content'),
	(61, 61, 'content'),
    (62, 62, 'content'),
    (63, 63, 'content'),
	(64, 64, 'content'),
    (65, 65, 'content'),
    (66, 66, 'content'),
	(67, 67, 'content'),
    (68, 68, 'content'),
    (69, 69, 'content'),
	(70, 70, 'content'),
	(71, 71, 'content'),
    (72, 72, 'content'),
    (73, 73, 'content'),
	(74, 74, 'content'),
    (75, 75, 'content'),
    (76, 76, 'content'),
	(77, 77, 'content'),
    (78, 78, 'content'),
    (79, 79, 'content'),
	(80, 80, 'content'),
	(81, 81, 'content'),
    (82, 82, 'content'),
    (83, 83, 'content'),
	(84, 84, 'content'),
    (85, 85, 'content'),
    (86, 86, 'content'),
	(87, 87, 'content'),
    (88, 88, 'content'),
    (89, 89, 'content'),
	(90, 90, 'content'),
	(91, 91, 'Session management is indeed a crucial aspect of web development that allows for the identification of a user across multiple page requests or visits to a website. It facilitates the storage of user-specific information throughout a users browsing session. Sessions are maintained on the server side and are often associated with a unique session ID, which is sent to the client and stored in a cookie or as part of the URL.'),
    (92, 92, 'In the context of Java web applications, the /WEB-INF directory is a legal location for the deployment descriptor file. The deployment descriptor file, often named web.xml, is a configuration file used to define the structure and configuration of the web application. It provides information to the web container about how to handle the application.'),
    (93, 93, 'The default value of the scope attribute for the <jsp:useBean> action in a JSP page is "page". This means that if you do not explicitly specify the scope attribute in the <jsp:useBean> action, the bean will be created in the page scope by default.

The "page" scope indicates that the bean will be accessible only within the JSP page where it is declared. It wont be accessible in other pages or in the application scope. If you want the bean to be accessible across multiple requests and pages, you can explicitly set the scope attribute to "request", "session", or "application", depending on your requirements.'),
	(94, 94, 'The HttpServletRequest object in Java provides various methods that allow you to retrieve information about incoming HTTP requests from clients.'),
    (95, 95, 'Data integrity indeed refers to the accuracy and consistency of data over its entire lifecycle, including when it is being transmitted from a sender to a receiver. It ensures that data remains unaltered during transit and storage, preventing unauthorized modification, corruption, or tampering of data.'),
    (96, 96, '"localhost" is a hostname that typically refers to the local computer that a user is working on. It is often used to access services or resources running on the same machine without requiring the use of an external network.
When a program or service on your computer uses "localhost" as the hostname, it is making a request to itself, without needing to go out over the network. This can be useful for testing and development purposes, especially when running a web server, database server, or other network services locally.'),
	(97, 97, 'The "attribute" element within a taglib or TLD (Tag Library Descriptor) defines the properties of an attribute that a custom tag needs. It specifies the name, type, and other characteristics of an attribute that can be used within a custom tag.'),
    (98, 98, 'To retrieve a servlet initialization parameter value, you can use the getInitParameter(String name) method from the ServletConfig interface. The ServletConfig interface provides a set of methods that a servlet can use to obtain its initialization parameters. The getInitParameter(String name) method specifically retrieves the value of the initialization parameter with the specified name.'),
    (99, 99, ' <jsp:getProperty name="userbean" property="age" />, is indeed equivalent to the Java statement userbean.getAge() when userbean.getAge() returns an integer.
In JSP, <jsp:getProperty> is used to retrieve the value of a bean property and output it directly to the JSP response. In this case, name refers to the bean name, and property refers to the property of the bean whose value is to be retrieved.'),
	(100, 100, 'scope: This is a valid attribute for the useBean tag. It specifies the scope of the JavaBean instance, such as "request," "session," or "application."

beanName: This is a valid attribute for the useBean tag. It specifies the name of the JavaBean that you want to create an instance of.

className: This is not a valid attribute for the useBean tag. The useBean tag does not have a className attribute.'),
	(101, 101, 'The JSP expression userbean.getAge is used to output the result of the getAge() method of the userbean object. '),
    (102, 102, 'In JSP, the expression userbean.getAge is used to evaluate the getAge method of the userbean object and output the result to the rendered HTML page. It will display the value returned by the getAge() method at that location in the generated HTML.'),
    (103, 103, 'In JSP, the correct term for specifying attributes is attribute." The "tag-attribute" or "tag-attribute-type" is not a standard terminology in JSP.

So, the correct term is attribute. These attributes are used to configure the behavior of custom tags in JSP, allowing you to pass data and instructions to custom tags.'),
	(104, 104, 'The well-known host name that refers to your own computer is "localhost." Its a loopback hostname used to access the network services that are running on the host via the network interface. When you access "localhost," you are referring to your own computer. Its often associated with the IP address 127.0.0.1, which also points to your own machine.'),
    (105, 105, 'Data integrity, in the context of data communication and storage, indeed means that the data remains unaltered and complete during transmission and storage. It ensures that the data is not modified or corrupted in transit between the sender and the receiver. Data integrity is typically achieved through various techniques, including error-checking methods, encryption, and data checksums, which help detect and prevent data corruption or unauthorized modifications.'),
    (106, 106, 'Cookies: You can use the getCookies() method to retrieve an array of Cookie objects from the incoming request.

Form Data: You can use methods like getParameter(String name) to retrieve individual form parameters sent in the request. You can also use methods like getParameterMap() to retrieve all form parameters as a map.

HTTP Request Headers: You can use methods like getHeader(String name) to retrieve specific HTTP request headers. You can also use methods like getHeaderNames() and getHeaders(String name) to access headers.
So, the correct answer is "all of the other." The HttpServletRequest provides methods to access all these types of incoming information.'),
	(107, 107, 'In the case of the <jsp:useBean> action, the default scope for the associated variable is page. If you dont specify a scope attribute, the variable will be scoped to the current page.

So, the answer to your question is that the default value of the scope attribute for jspuseBean is page.'),
    (108, 108, 'This directory should be placed at the root of your web application, and the web.xml file should be located inside the WEB-INF directory. The web.xml file contains configuration information for the web application and is a crucial part of Java web applications.

So, the answer is /WEB-INF.'),
    (109, 109, 'Session Management provides a way to identify a user across more than one page request or visit to a website and to store information about that user during their interaction with the web application. It allows web applications to maintain state and track user activities by associating a session ID or token with a particular user.

Common techniques for session management include using cookies, URL rewriting, or server-side sessions to store and retrieve user-specific data. This enables web applications to provide personalized experiences, maintain shopping carts, and track user logins and activities.'),
	(110, 110, 'content'),
	(111, 111, 'content'),
    (112, 112, 'content'),
    (113, 113, 'content'),
	(114, 114, 'content'),
    (115, 115, 'content'),
    (116, 116, 'content'),
	(117, 117, 'content'),
    (118, 118, 'content'),
    (119, 119, 'content'),
	(120, 120, 'content'),
	(121, 121, 'content'),
    (122, 122, 'content'),
    (123, 123, 'content'),
	(124, 124, 'content'),
    (125, 125, 'content'),
    (126, 126, 'content'),
	(127, 127, 'content'),
    (128, 128, 'content'),
    (129, 129, 'content'),
	(130, 130, 'content'),
	(131, 131, 'content'),
    (132, 132, 'content'),
    (133, 133, 'content'),
	(134, 134, 'content'),
    (135, 135, 'content'),
    (136, 136, 'content'),
	(137, 137, 'content'),
    (138, 138, 'content'),
    (139, 139, 'content'),
	(140, 140, 'content'),
	(141, 141, 'content'),
    (142, 142, 'content'),
    (143, 143, 'content'),
	(144, 144, 'content'),
    (145, 145, 'content'),
    (146, 146, 'content'),
	(147, 147, 'content'),
    (148, 148, 'content'),
    (149, 149, 'content'),
	(150, 150, 'content'),
    (151, 151, 'content'),
    (152, 152, 'content'),
    (153, 153, 'content'),
	(154, 154, 'content'),
    (155, 155, 'content'),
    (156, 156, 'content'),
	(157, 157, 'content'),
    (158, 158, 'content'),
    (159, 159, 'content'),
	(160, 160, 'content'),
	(161, 161, 'content'),
    (162, 162, 'content'),
    (163, 163, 'content'),
	(164, 164, 'content'),
    (165, 165, 'content'),
    (166, 166, 'content'),
	(167, 167, 'content'),
    (168, 168, 'content'),
    (169, 169, 'content'),
	(170, 170, 'content'),
	(171, 171, 'content'),
    (172, 172, 'content'),
    (173, 173, 'content'),
	(174, 174, 'content'),
    (175, 175, 'content'),
    (176, 176, 'content'),
	(177, 177, 'content'),
    (178, 178, 'content'),
    (179, 179, 'content'),
	(180, 180, 'content'),
	(181, 181, 'content'),
    (182, 182, 'content'),
    (183, 183, 'content'),
	(184, 184, 'content'),
    (185, 185, 'content'),
    (186, 186, 'content'),
	(187, 187, 'content'),
    (188, 188, 'content'),
    (189, 189, 'content'),
	(190, 190, 'content'),
	(191, 191, 'content'),
    (192, 192, 'content'),
    (193, 193, 'content'),
	(194, 194, 'content'),
    (195, 195, 'content'),
    (196, 196, 'content'),
	(197, 197, 'content'),
    (198, 198, 'content'),
    (199, 199, 'content'),
	(200, 200, 'content'),
	(201, 201, 'content'),
    (202, 202, 'content'),
    (203, 203, 'content'),
	(204, 204, 'content'),
    (205, 205, 'content'),
    (206, 206, 'content'),
	(207, 207, 'content'),
    (208, 208, 'content'),
    (209, 209, 'content'),
	(210, 210, 'content'),
	(211, 211, 'content'),
    (212, 212, 'content'),
    (213, 213, 'content'),
	(214, 214, 'content'),
    (215, 215, 'content'),
    (216, 216, 'content'),
	(217, 217, 'content'),
    (218, 218, 'content'),
    (219, 219, 'content'),
	(220, 220, 'content'),
	(221, 221, 'content'),
    (222, 222, 'content'),
    (223, 223, 'content'),
	(224, 224, 'content'),
    (225, 225, 'content'),
    (226, 226, 'content'),
	(227, 227, 'content'),
    (228, 228, 'content'),
    (229, 229, 'content'),
	(230, 230, 'content'),
	(231, 231, 'content'),
    (232, 232, 'content'),
    (233, 233, 'content'),
	(234, 234, 'content'),
    (235, 235, 'content'),
    (236, 236, 'content'),
	(237, 237, 'content'),
    (238, 238, 'content'),
    (239, 239, 'content'),
	(240, 240, 'content'),
	(241, 241, 'JSP page is translated into a servlet: During this phase, the JSP page is converted into a corresponding servlet class by the JSP container.

JSP page is compiled into a Java bytecode class: The generated servlet class is compiled into a Java bytecode class by the Java compiler.

jspInit() method is called: This method is invoked before the JSP page is accessed for the first time. It is typically used for any initialization tasks that need to be performed.

JspService() method is called: This method is responsible for processing client requests and generating dynamic content. It is executed for each request to the JSP page.

jspDestroy() method is called: This method is invoked when the JSP page is about to be removed from the server or when the server is shutting down. It is used for releasing any resources held by the JSP.'),
    (242, 242, 'In EJB 3.0, stateful session beans are designed to maintain conversational state for a specific client across multiple method invocations and transactions. This allows the bean to keep track of the clients data and interactions throughout the course of a session.

The conversational state in EJB 3.0 stateful session beans refers to the stateful data or information specific to a particular client session. This data is retained as long as the client maintains the session with the bean. The stateful nature of these beans enables them to serve as containers for holding client-specific data, which can be manipulated and updated across multiple method calls and transactions.

Its important to note that this conversational state is maintained until the session is explicitly terminated by the client or the container, or until the session times out. EJB 3.0 stateful session beans are particularly useful for scenarios where its necessary to retain client-specific state and data throughout the duration of a clients interaction with the enterprise application.'),
    (243, 243, 'Understanding the lifecycle of JPA entity instances is crucial for effective data management and manipulation within a Java EE application. The persistence context plays a critical role in managing the lifecycle of these entities and ensuring proper synchronization between the Java application and the underlying database.'),
	(244, 244, 'Session Beans: These are used to perform operations for a client. They can be stateless or stateful, depending on whether they maintain state for a single client or not.

Entity Beans: These represent persistent data stored in a database. However, with the introduction of EJB 3.0, the use of entity beans has been replaced by the more flexible and powerful Java Persistence API (JPA) for object-relational mapping.

Message-Driven Beans: These are used to process messages from a JMS (Java Message Service) provider.'),
    (245, 245, 'JNDI offers a common way to access different naming and directory services, including DNS, LDAP, NIS, and CORBAs COS naming. It enables Java applications to access various naming and directory services without having to be aware of the underlying implementation details of each service.
Using JNDI, Java applications can perform operations such as binding objects to names, searching for objects, and retrieving objects from a naming or directory service, making it a powerful tool for building distributed and networked Java applications. JNDI is an essential component in the Java EE (Enterprise Edition) platform, enabling applications to access resources and services from the surrounding environment.'),
    (246, 246, 'In the context of a Tag Library Descriptor (TLD) file used in JavaServer Pages (JSP), the subelement <name> of the <attribute> tag indeed defines the name of the attribute that might be passed to the tag handler. This <name> element specifies the identifier for the attribute, allowing the tag handler to retrieve and process the attributes value during the execution of the custom tag.'),
	(247, 247, 'JMS is a standard API that abstracts the complexity of MOM, providing a common interface for Java applications to work with different MOM providers. It allows Java applications to access MOM systems without being directly dependent on any specific messaging implementation. JMS can be used with various MOM products, including IBM MQ, Apache ActiveMQ, RabbitMQ, and others.
The key role of JMS is to provide a standard way for Java applications to send and receive messages, irrespective of the specific underlying MOM implementation being used. By adhering to the JMS API, applications can be more easily integrated with different MOM systems, promoting interoperability and portability across various messaging environments.'),
    (248, 248, 'the response.sendRedirect("some url"); option can be used to achieve the objective of redirecting a request to another resource. This method is often used in servlets to redirect the clients browser to a different URL. The new URL may point to a resource within the same web application or to a resource in a different application altogether.'),
    (249, 249, 'The <error-page> element is indeed used for error handling in a web application, and it is a child element of the <web-app> element in a deployment descriptor. This element allows you to specify how to handle different types of errors that might occur within your web application.'),
	(250, 250, 'In the tag library descriptor (TLD) file, the <tag> element is used to define a custom tag, such as <myTaglib:myTag>, which can then be used within a JSP file. The name attribute within the <tag> element of the TLD file is used to specify the name of the custom tag.'),
	(251, 251, 'content'),
    (252, 252, 'content'),
    (253, 253, 'content'),
	(254, 254, 'content'),
    (255, 255, 'content'),
    (256, 256, 'content'),
	(257, 257, 'content'),
    (258, 258, 'content'),
    (259, 259, 'content'),
	(260, 260, 'content'),
	(261, 261, 'content'),
    (262, 262, 'content'),
    (263, 263, 'content'),
	(264, 264, 'content'),
    (265, 265, 'content'),
    (266, 266, 'content'),
	(267, 267, 'content'),
    (268, 268, 'content'),
    (269, 269, 'content'),
	(270, 270, 'content'),
	(271, 271, 'content'),
    (272, 272, 'content'),
    (273, 273, 'content'),
	(274, 274, 'content'),
    (275, 275, 'content'),
    (276, 276, 'content'),
	(277, 277, 'content'),
    (278, 278, 'content'),
    (279, 279, 'content'),
	(280, 280, 'content'),
	(281, 281, 'content'),
    (282, 282, 'content'),
    (283, 283, 'content'),
	(284, 284, 'content'),
    (285, 285, 'content'),
    (286, 286, 'content'),
	(287, 287, 'content'),
    (288, 288, 'content'),
    (289, 289, 'content'),
	(290, 290, 'content'),
	(291, 291, 'content'),
    (292, 292, 'content'),
    (293, 293, 'content'),
	(294, 294, 'content'),
    (295, 295, 'An array of objects can indeed contain objects of different classes in most programming languages. This allows you to have a heterogeneous collection of objects within the same array. Each element of the array can be an instance of a different class, as long as they are subclasses of a common superclass or share a common interface, depending on the specific programming language and requirements.'),
    (296, 296, 'In Java, you initialize an array of objects by specifying the data type (in this case, ObjectName), followed by square brackets to indicate an array, and then you use the new keyword to create a new array with a specified size.'),
	(297, 297, 'In many programming languages, if an element in an array of objects is not explicitly initialized, it typically has a default value of "null."'),
    (298, 298, 'To access a specific attribute of an object in an array of objects, you use the array index to specify which object you want to access and then use dot notation to access the desired attribute of that object.




'),
    (299, 299, 'In most programming languages, you cannot change the size of an array of objects after it has been initialized. Arrays typically have a fixed size once they are created. If you need a dynamic data structure that can change in size, you would typically use other data structures like lists or dynamic arrays.'),
	(300, 300, 'The correct option is:

 By specifying the values in curly braces during initialization (true)

You can create an array of objects with different initial values by specifying the values in curly braces during initialization. This allows you to provide distinct initial values for each element in the array.')
	

	
CREATE TABLE Quiz_History_Detail(
	[historyId] [int] primary key,	
	[userId] [int] references Account(aid),
	[quizId] [int]  references Quiz(quizId),
	[grade] [float],
	[Mark] [int],
	[attempt] [int],
	[start] [datetime],
	[end] [datetime],
	[time] [int]
	)
	
CREATE TABLE Quiz_History(
	[historyId] [int] references Quiz_History_Detail(historyId),
	[answerId] [int],
	[quesId] [int],
	)
GO


GO
INSERT [dbo].[Quiz_History_Detail] ([historyId], [userId], [quizId], [grade], [Mark], [attempt], [start], [end], [time]) VALUES (1, 2, 10, 5, 5, 1, CAST(N'2023-10-11T21:47:32.450' AS DateTime), CAST(N'2023-10-11T21:48:19.337' AS DateTime), 46888)
GO
INSERT [dbo].[Quiz_History_Detail] ([historyId], [userId], [quizId], [grade], [Mark], [attempt], [start], [end], [time]) VALUES (2, 2, 10, 7, 7, 2, CAST(N'2023-10-11T21:51:04.497' AS DateTime), CAST(N'2023-10-11T21:51:40.953' AS DateTime), 36457)
GO
INSERT [dbo].[Quiz_History_Detail] ([historyId], [userId], [quizId], [grade], [Mark], [attempt], [start], [end], [time]) VALUES (3, 2, 7, 8, 8, 3, CAST(N'2023-10-11T21:51:54.697' AS DateTime), CAST(N'2023-10-11T21:52:42.797' AS DateTime), 48100)
GO
INSERT [dbo].[Quiz_History_Detail] ([historyId], [userId], [quizId], [grade], [Mark], [attempt], [start], [end], [time]) VALUES (4, 9, 4, 8, 8, 1, CAST(N'2023-11-09T11:56:09.963' AS DateTime), CAST(N'2023-11-09T12:00:02.037' AS DateTime), 232073)
GO
INSERT [dbo].[Quiz_History_Detail] ([historyId], [userId], [quizId], [grade], [Mark], [attempt], [start], [end], [time]) VALUES (5, 9, 5, 7, 7, 1, CAST(N'2023-11-09T12:00:30.887' AS DateTime), CAST(N'2023-11-09T12:04:05.613' AS DateTime), 214724)
GO

INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (1, 398, 100)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (1, 394, 99)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (1, 385, 97)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (1, 381, 96)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (1, 380, 95)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (1, 374, 94)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (1, 370, 93)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (1, 365, 92)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (1, 361, 91)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (2, 399, 100)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (2, 396, 99)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (2, 389, 98)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (2, 385, 97)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (2, 381, 96)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (2, 377, 95)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (2, 374, 94)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (2, 370, 93)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (2, 365, 92)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (2, 0, 91)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (1, 389, 98)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (3, 399, 100)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (3, 396, 99)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (3, 389, 98)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (3, 385, 97)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (3, 381, 96)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (3, 377, 95)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (3, 374, 94)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (3, 370, 93)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (3, 365, 92)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (3, 363, 91)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (4, 121, 31)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (4, 123, 31)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (4, 729, 183)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (4, 736, 184)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (4, 725, 182)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (4, 721, 181)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (4, 748, 187)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (4, 739, 185)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (4, 741, 186)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (4, 140, 35)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (4, 133, 34)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (5, 0, 41)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (5, 165, 42)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (5, 169, 43)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (5, 176, 44)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (5, 179, 45)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (5, 188, 46)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (5, 192, 48)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (5, 195, 49)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (5, 200, 50)
GO
INSERT [dbo].[Quiz_History] ([historyId], [answerId], [quesId]) VALUES (5, 762, 191)
GO
ALTER TABLE Quiz
ADD creator int REFERENCES Account(aid)
GO
update Quiz set creator=3 where quizId>11
GO
update Quiz set creator=2 where quizId<=11